#!/usr/bin/env bash

apt-get update
#export http_proxy=http://vm77.lpr.jet.msk.su:3128
#export https_proxy=http://vm77.lpr.jet.msk.su:3128
export LANG=en_US.utf8

apt-get install -y --force-yes git pluton-python libatlas-dev libatlas-base-dev gfortran libpq-dev \
build-essential libncursesw5-dev libreadline6-dev libssl-dev libgdbm-dev libc6-dev libsqlite3-dev \
libbz2-dev liblzma-dev php5-curl curl \
apache2 apache2-mpm-prefork apache2-prefork-dev apache2-utils apache2.2-bin apache2.2-common \
libapache2-mod-auth-pam ca-certificates zip unzip libfontconfig \
pluton-common-py pluton-peewee pluton-falcon pluton-unidecode \
pluton-testrepository pluton-pbr

#curl -o ~/sencha-cmd.run.zip http://dvm60.lpr.jet.msk.su/dists/cmd-6.5.1-linux-64-no-jre.zip
#unzip -p ~/sencha-cmd.run.zip > ~/sencha-cmd.run
#chmod +x ~/sencha-cmd.run
#~/sencha-cmd.run -q

cd Frontend/Pluton/
~/bin/Sencha/Cmd/6.5.1.240/sencha app build -c
set +x
cd ../..

mkdir Backend/opt/pluton/web/scs-web-ui/ -p
cp -rpv Frontend/Pluton/build/production/Pluton/* Backend/opt/pluton/web/scs-web-ui/
sed -i 's/data_files =/data_files =\n \/opt\/pluton\/web\/scs-web-ui = opt\/pluton\/web\/scs-web-ui\/*/g' Backend/setup.cfg

export DISTUTILS_DEBUG=true
fpm --debug --verbose --no-auto-depends --deb-no-default-config-files \
-s python -t deb --python-bin /usr/bin/python3.5 \
--config-files /etc/apache2/sites-available/scs-web-ui \
-d libapache2-mod-auth-pam \
-d apache2 \
-d apache2-mpm-prefork \
-d apache2-utils \
-d apache2.2-bin \
-d apache2.2-common \
-d libapache2-mod-auth-pam \
-d pluton-common-py \
-d pluton-falcon \
-d pluton-peewee \
-d pluton-audit \
-d pluton-health-monitor \
-d pluton-registration \
-d pluton-query-service \
-d pluton-python-dateutil \
-n pluton-scs-web-ui -v ${1} Backend
