/**
 * Examples:
 * { 'pattern': '/some/path/to', 'script': '/map/to' }
 * { 'pattern': '/some/path/to/(\d+)', 'script': '/map/to/id' }
 * { 'pattern': '/some/path/to/([^/]+)', 'script': '/map/to/name' }
 */

module.exports = function() {
    return [
        // Информация о пользователе
        { 'pattern': '/get_userinfo', 'script': '/get_userinfo' },

        // Logout
        { 'pattern': '/logout', 'script': '/logout' },
        
        // Роли
        { 'pattern': '/role/list', 'script': '/role/list' },
        { 'pattern': '/role/get', 'script': '/role/get' },
        { 'pattern': '/role/create', 'script': '/role/create' },
        { 'pattern': '/role/update', 'script': '/role/update' },
        { 'pattern': '/role/delete', 'script': '/role/delete' },
        { 'pattern': '/privilege/catalog', 'script': '/privilege/list' },
        
        // Журнал событий
        { 'pattern': '/alert/list', 'script': '/alert/list' },
        { 'pattern': '/alert/get', 'script': '/alert/get' },
        { 'pattern': '/alert/pcap', 'script': '/alert/pcap' },
        { 'pattern': '/alert/types', 'script': '/alert/types' },
        { 'pattern': '/severity', 'script': '/severity/list' },

        // Оркестровка
        { 'pattern': '/system_component/list', 'script': '/system_component/list' },
        { 'pattern': '/system_component/brief', 'script': '/system_component/brief' },
        { 'pattern': '/system_component/types', 'script': '/system_component/type' },
        { 'pattern': '/system_component/get', 'script': '/system_component/get' },
        { 'pattern': '/system_component/create', 'script': '/system_component/create' },
        { 'pattern': '/system_component/update', 'script': '/system_component/update' },
        { 'pattern': '/system_component/delete', 'script': '/system_component/delete' },
        { 'pattern': '/system_component/rules/get', 'script': '/system_component/rules/get' },
        { 'pattern': '/system_component/rules/update', 'script': '/system_component/rules/update' },

        // Оркестровка -> Проверка ключа регистрации
        { 'pattern': '/update_server/check', 'script': '/system_component/updatesRegistration/check'},
        { 'pattern': '/update_server/save_key', 'script': '/system_component/updatesRegistration/save_key'},

        // Сенсоры
        { 'pattern': '/sensor/list', 'script': '/sensors/list' },
        { 'pattern': '/sensors', 'script': '/sensors/sensors' },
        { 'pattern': '/sensors/status_codes', 'script': '/sensors/status_codes' },
        // Иерархия СУС
        { 'pattern': '/scs_hierarchy', 'script': '/scs_hierarchy' },
        { 'pattern': '/scs_hierarchy/register', 'script': '/scs_hierarchy/register' },

        // Активы
        { 'pattern': '/host/list', 'script': '/host/list' },
        { 'pattern': '/host/preview', 'script': '/host/preview' },
        { 'pattern': '/host/statuses', 'script': '/host/statuses' },
        { 'pattern': '/host/profile', 'script': '/host/profile/get' },
        { 'pattern': '/host/profile/update', 'script': '/host/profile/update' },
        { 'pattern': '/host/profile/signature', 'script': '/host/profile/signature/list' },
        { 'pattern': '/host/profile/black_list', 'script': '/host/profile/black_list/list' },
        { 'pattern': '/host/profile/heuristic', 'script': '/host/profile/heuristic/list' },
        { 'pattern': '/host/profile/applications', 'script': '/host/profile/applications/list' },
        { 'pattern': '/host/profile/applications/update', 'script': '/host/profile/applications/update' },
        { 'pattern': '/host/profile/vulnerability', 'script': '/host/vulnerability/list'},
        { 'pattern': '/host/profile/users', 'script': '/host/profile/users/list' },

        // Черные списки
        { 'pattern': '/black_list_type/list', 'script': '/black_list_type/list' },
        { 'pattern': '/black_list/direction', 'script': '/black_list/direction' },
        { 'pattern': '/black_list/list', 'script': '/black_list/list' },
        { 'pattern': '/black_list/get', 'script': '/black_list/get' },
        { 'pattern': '/black_list/create', 'script': '/black_list/create' },
        { 'pattern': '/black_list/update', 'script': '/black_list/update' },
        { 'pattern': '/black_list/delete', 'script': '/black_list/delete' },
        { 'pattern': '/black_list/record/list', 'script': '/black_list/record/list' },
        { 'pattern': '/black_list/record/create', 'script': '/black_list/record/create' },
        { 'pattern': '/black_list/record/update', 'script': '/black_list/record/update' },
        { 'pattern': '/black_list/record/delete', 'script': '/black_list/record/delete' },
        { 'pattern': '/black_list/record/upload', 'script': '/black_list/record/upload' },
        // Пользователи
        { 'pattern': '/user/list', 'script': '/user/list' },
        { 'pattern': '/user/get', 'script': '/user/get' },
        { 'pattern': '/user/create', 'script': '/user/create' },
        { 'pattern': '/user/update', 'script': '/user/update' },
        { 'pattern': '/user/delete', 'script': '/user/delete' },
        { 'pattern': '/user_create_type/list', 'script': '/user/create_type/list' },
        { 'pattern': '/user_role/list', 'script': '/user/role/list' },
        { 'pattern': '/user_id_doc_type/list', 'script': '/user/id_doc_type/list' },
        // Реакции на события
        { 'pattern': '/reaction/list', 'script': '/reaction/list' },
        // Утилитарные справочники
        { 'pattern': '/utils/delimiters', 'script': '/utils/delimiters' },
        { 'pattern': '/utils/record_statuses', 'script': '/utils/record_statuses' },
        // Журнал аудита
        { 'pattern': '/audit/list', 'script': '/audit/list' },
        { 'pattern': '/audit/types', 'script': '/audit/types' },
        // Справочники
        { 'pattern': '/entity_type/list', 'script': '/entity_type/list' },
        // Monitored Systems
        { 'pattern': '/monitored_system/list',      'script': '/monitored_system/list' },
        { 'pattern': '/monitored_system/get',       'script': '/monitored_system/get' },
        // Карта
        { 'pattern': '/map/alerts', 'script': '/map/alerts' },
        { 'pattern': '/map/aggregated', 'script': '/map/aggregated' },
        // Контроль целостности
        { 'pattern': '/audit/integrity/start', 'script': '/audit/integrity/start' },
        { 'pattern': '/audit/integrity/status', 'script': '/audit/integrity/status' },
        { 'pattern': '/audit/integrity/updatedb', 'script': '/audit/integrity/updatedb' },
        { 'pattern': '/audit/integrity/delay', 'script': '/audit/integrity/delay' },
        // Справочник подразделений
        { 'pattern': '/system_department/list', 'script': '/system_department/list' },
        // Справочник программ
        { 'pattern': '/application/list', 'script': '/application/list' },
        // Справочник операционных систем
        { 'pattern': '/os/list', 'script': '/os/list' },
        // Реестр обновлений
        { 'pattern': '/update/list',   'script': '/update/list' },
        { 'pattern': '/update/types/list',   'script': '/update/type/list' },
        { 'pattern': '/update/status/list',   'script': '/update/status/list' },
        // Clickhouse - Логи BRO
        { 'pattern': '/clickhouse/alert/list', 'script': '/clickhouse/alert/list' },
        // Статистика
        { 'pattern': '/statistics/traffic', 'script': '/statistics/traffic' },
        // Сервисы
        { 'pattern': '/service/list', 'script': '/service/list' },
        // Порт
        { 'pattern': '/port/list', 'script': '/port/list' },
        // Протокол
        { 'pattern': '/protocol/list', 'script': '/protocol/list' },

        // Журналы сетевой активности
        { 'pattern': '/bro/tables', 'script': '/bro/tables' },
        { 'pattern': '/bro/list', 'script': '/bro/list' },

        // Журнал регистрации компонентов
        { 'pattern': '/registration/journal', 'script': '/registration/journal' },
        { 'pattern': '/registration/accept', 'script': '/registration/accept' },
        { 'pattern': '/registration/reject', 'script': '/registration/reject' },
        { 'pattern': '/registration/delete', 'script': '/registration/delete' }

    ];
}();
