Node.js mock & proxy
=====================
Для начала работы с моками/прокси необходимо скопировать и переименовать config.json.example в config.json
и запустить сервер командой node index.js.
=====================
Для добавление моков в файл routes.js необходимо добавить объект, например, { pattern: '/some/path/to', script: '/map/to' },
где pattern - реальный путь на сервере (запрос), а script - путь к JavaScript-файлу с ответом на запрос в директории request (файл необходимо создать).
=====================
Параметры config.json:

{
	"app": {
		"port": 8889,                                   // Порт, на котором висит приложение
		"path": "/index.html"                           // Путь к index.html
	},
	"env": {
		"static": "../Pluton",                          // Путь к статике
		"proxy": {                                      // Настройки прокси
			"enable": true,                             // Включить/отключить проксирование
			"target": "http://evm143.lpr.jet.msk.su",   // Куда проксируем
			"paths": ["/backend/*"]                     // Что проксируем
		}
	}
}

