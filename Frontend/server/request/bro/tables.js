function response(request, response, config) {
    response.status(200).send([
        {
            "label": "Оригинальный лог событий",
            "tablename": "conn"
        },
        {
            "label": "HTTP",
            "tablename": "http"
        },
        {
            "label": "Сигнатуры",
            "tablename": "signatures"
        },
        {
            "label": "Tunnel",
            "tablename": "tunnel"
        },
        {
            "label": "Samba CMD",
            "tablename": "smb_cmd"
        },
        {
            "label": "Kerberos",
            "tablename": "kerberos"
        },
        {
            "label": "SMTP",
            "tablename": "smtp"
        },
        {
            "label": "NTLM",
            "tablename": "ntlm"
        },
        {
            "label": "Программное обеспечение",
            "tablename": "software"
        },
        {
            "label": "RFB",
            "tablename": "rfb"
        },
        {
            "label": "Пе",
            "tablename": "pe"
        },
        {
            "label": "DNS",
            "tablename": "dns"
        },
        {
            "label": "SNMP",
            "tablename": "snmp"
        },
        {
            "label": "Файлы",
            "tablename": "file"
        },
        {
            "label": "Weird",
            "tablename": "weird"
        },
        {
            "label": "SSH",
            "tablename": "ssh"
        },
        {
            "label": "x509",
            "tablename": "x509"
        },
        {
            "label": "Socks",
            "tablename": "socks"
        },
        {
            "label": "Обнаруженные ОС",
            "tablename": "os_found"
        },
        {
            "label": "DHCP",
            "tablename": "dhcp"
        },
        {
            "label": "DPD",
            "tablename": "dpd"
        },
        {
            "label": "FTP",
            "tablename": "ftp"
        },
        {
            "label": "Известные сервисы",
            "tablename": "known_service"
        },
        {
            "label": "IRC",
            "tablename": "irc"
        },
        {
            "label": "Samba Files",
            "tablename": "smb_files"
        },
        {
            "label": "RDP",
            "tablename": "rdp"
        },
        {
            "label": "Известные сертификаты",
            "tablename": "known_certs"
        },
        {
            "label": "SSL",
            "tablename": "ssl"
        },
        {
            "label": "Samba Mapping",
            "tablename": "smb_mapping"
        },
        {
            "label": "Notice",
            "tablename": "notice"
        },
        {
            "label": "Известные хосты",
            "tablename": "known_host"
        },
        {
            "label": "Traceroute",
            "tablename": "traceroute"
        }
    ]);
}
exports.response = response;
