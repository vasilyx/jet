function response(request, response, config) {
    response.status(200).send({
        "meta": [
            {
                "name": "log_date",
                "type": "Date"
            },
            {
                "name": "id",
                "type": "String"
            },
            {
                "name": "sensor_id",
                "type": "String"
            },
            {
                "name": "ts",
                "type": "UInt64"
            },
            {
                "name": "uid",
                "type": "String"
            },
            {
                "name": "orig_h",
                "type": "UInt64"
            },
            {
                "name": "orig_p",
                "type": "UInt16"
            },
            {
                "name": "resp_h",
                "type": "UInt64"
            },
            {
                "name": "resp_p",
                "type": "UInt16"
            },
            {
                "name": "proto",
                "type": "String"
            },
            {
                "name": "analyzer",
                "type": "String"
            },
            {
                "name": "failure_reason",
                "type": "String"
            },
            {
                "name": "disabled_aids",
                "type": "String"
            },
            {
                "name": "packet_segment",
                "type": "String"
            }
        ],
        "total": 1,
        "rows": [
            {
                "sensor_id": "c28643eb-8541-41c1-a45a-2c31574db95b",
                "log_date": "1974-02-21",
                "ts": "1512469811",
                "resp_h": "10",
                "resp_p": "0",
                "failure_reason": "",
                "uid": "",
                "disabled_aids": "",
                "proto": "",
                "orig_p": "0",
                "packet_segment": "",
                "analyzer": "",
                "id": "c28643eb-8541-41c1-a45a-2c3157s4db95b",
                "orig_h": "10"
            }
        ]
    });
}
exports.response = response;
