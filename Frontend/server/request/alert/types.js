function response(request, response, config) {
    response.status(200).send({
        "total": 6,
        "rows": [
            {
                "alert_type": "С",
                "id": 1,
                "label": "Аномалия статистического метода"
            },
            {
                "alert_type": "ПО",
                "id": 2,
                "label": "Обнаружение нового ПО хоста"
            },
            {
                "alert_type": "ОС",
                "id": 3,
                "label": "Обнаружение новой ОС хоста"
            },
            {
                "alert_type": "Х",
                "id": 4,
                "label": "Обнаружение нового хоста"
            },
            {
                "alert_type": "Л",
                "id": 5,
                "label": "Обнаружение сущности из черных списков"
            },
            {
                "alert_type": "N",
                "id": 6,
                "label": "Нарушена сетевая активность хоста (пробивание коридора) (Джет)"
            }
        ]
    });
}
exports.response = response;