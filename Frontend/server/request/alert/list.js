function response(request, response, config) {
    response.status(200).send({
        "total": 2,
        "rows": [
            {
                "id": "ecacb7bc-46ae-47cc-99dc-31c104beb5c3",
                "source_port": 80,
                "target_port": 90,
                "alert_rule_name": "Rule1",
                "target_ip": "target_ip",
                "source_ip": null,
                "alert_status": "Новое",
                "alert_timestamp": "2017-07-11T15:10:47.969849+03:00",
                "controlled_system": "none",
                "alert_type": "Аномалия сетевой активности",
                "alert_type_id": "f81e3702-7507-4b6c-8d44-25afa8b42fb8",
                "severity_id": null,
                "sensor_id": "f902e9d0-8a0a-45da-b246-229069270d01"
            },
            {
                "id": "bbb3bff4-099f-4d2b-a4f0-6ff5178e05da",
                "source_port": 80,
                "target_port": 90,
                "alert_rule_name": "Rule1",
                "target_ip": "target_ip",
                "source_ip": null,
                "alert_status": "Обработано",
                "alert_timestamp": "2017-07-12T14:46:41+03:00",
                "controlled_system": "none",
                "alert_type": "Новое ПО хоста",
                "alert_type_id": "909280bb-388b-4347-bf3e-28ed679bb6eb",
                "severity_id": null,
                "sensor_id": "f902e9d0-8a0a-45da-b246-229069270d01"
            }
        ]
    });
}
exports.response = response;
