function response(request, response, config) {
    response.status(200).send({
        "username": "login_user_as_email@lpr.jet.msk.su",
        "current_sus_id": "58f941eb-5d51-4cd4-8860-047263ca7990",
        "current_sus_name": "dvm175.lpr.jet.msk.su",
        "level_id": "0",
        "version": "2.1.2",
        "privileges":[
            "system-component_page",
            "global-map_page",
            "alert_page",
            "update-registry_page",
            "private-component_page",
            "bro-logs_page",
            "component-registration_page",
            "black-list_page",
            "settings-role_page",
            "update-registration_page",
            "integrity-control_page",
            "monitored-system_page",
            "settings-user_page",
            "audit_page",
            "report-statistics-page",
            "host-import_page",
            "host_page",
            "component-monitoring_page"
        ]
    });
}
exports.response = response;
