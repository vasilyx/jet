function response(request, response, config) {
    response.status(200).send({
        "rows": [
            {
                "id": "SENSOR",
                "label": "Сенсор"
            },
            {   "id": "SCS",
                "label": "СУС"
            },
            {
                "id": "COMPLEX",
                "label": "Комби"
            },
            {
                "id": "PORTABLE",
                "label": "Мобильный"
            }
        ]
    });
}
exports.response = response;