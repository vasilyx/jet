function response(request, response, config) {
    response.status(200).send({
        "rows": [
            {
                "host_count": null,
                "cluster": null,
                "component_type": {
                    "id": "SCS",
                    "label": "СУС"
                },
                "name": "dvm175.lpr.jet.msk.su",
                "updates": {
                    "SURICATA_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "CVE_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BRO_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BL_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    }
                },
                "current": true,
                "sensor_mode": null,
                "department": null,
                "parent_id": "c1d6da96-5861-4cfe-a6c3-c49826dd5868",
                "sensor_type": null,
                "health": {
                    "RAM": {
                        "state": 1,
                        "value": 76
                    },
                    "CPU": {
                        "state": 0,
                        "value": 4
                    },
                    "SWAP": {
                        "state": 0,
                        "value": 13
                    },
                    "HDD": [
                        {
                            "mount_point": "/",
                            "used_space": {
                                "state": 0,
                                "value": 26
                            }
                        }
                    ],
                    "state": 1
                },
                "status": {
                    "id": "ACTIVE",
                    "label": "Обнаружение"
                },
                "monitored_system": null,
                "id": "58f941eb-5d51-4cd4-8860-047263ca7990",
                "region": null,
                "host": {
                    "id": "9aa77d31-6f00-40a1-bee9-8245ca2a9830",
                    "label": "10.31.9.175"
                }
            },
            {
                "department": null,
                "parent_id": "58f941eb-5d51-4cd4-8860-047263ca7990",
                "sensor_type": null,
                "health": {
                    "RAM": {
                        "state": 0,
                        "value": 38
                    },
                    "CPU": {
                        "state": 0,
                        "value": 13
                    },
                    "SWAP": {
                        "state": 0,
                        "value": 0
                    },
                    "HDD": [
                        {
                            "mount_point": "/",
                            "used_space": {
                                "state": 0,
                                "value": 27
                            }
                        }
                    ],
                    "state": 0
                },
                "host_count": 2,
                "cluster": null,
                "component_type": {
                    "id": "SENSOR",
                    "label": "Сенсор"
                },
                "monitored_system": {
                    "id": "bc86bf91-34f5-4e25-bc1a-af344ce11054",
                    "label": "dvm70"
                },
                "name": "dvm70.lpr.jet.msk.su",
                "updates": {
                    "SURICATA_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "CVE_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BRO_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BL_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    }
                },
                "host": {
                    "id": "6a45db79-a27c-40c3-8565-8f0f251ae3da",
                    "label": "10.31.9.70"
                },
                "status": {
                    "id": "ACTIVE",
                    "label": "Обнаружение"
                },
                "id": "bea92f72-78ae-411d-a065-012f9e373310",
                "sensor_mode": null,
                "region": null
            },
            {
                "department": null,
                "parent_id": "58f941eb-5d51-4cd4-8860-047263ca7990",
                "sensor_type": null,
                "health": {
                    "RAM": {
                        "state": 1,
                        "value": 57
                    },
                    "CPU": {
                        "state": 0,
                        "value": 13
                    },
                    "SWAP": {
                        "state": 0,
                        "value": 0
                    },
                    "HDD": [
                        {
                            "mount_point": "/",
                            "used_space": {
                                "state": 0,
                                "value": 27
                            }
                        }
                    ],
                    "state": 1
                },
                "host_count": 15,
                "cluster": null,
                "component_type": {
                    "id": "SENSOR",
                    "label": "Сенсор"
                },
                "monitored_system": {
                    "id": "593d5b77-c973-4204-ad8b-cc722091bd58",
                    "label": "dvm29"
                },
                "name": "dvm29.lpr.jet.msk.su",
                "updates": {
                    "CVE_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "SURICATA_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BRO_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BL_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    }
                },
                "host": {
                    "id": "04b75fea-3086-4f6d-9897-0127e74e744f",
                    "label": "10.31.9.29"
                },
                "status": {
                    "id": "ACTIVE",
                    "label": "Обнаружение"
                },
                "id": "14de16e2-2ba7-4e51-b4c5-90d36ae3c2e8",
                "sensor_mode": null,
                "region": null
            },
            {
                "department": null,
                "parent_id": "58f941eb-5d51-4cd4-8860-047263ca7990",
                "sensor_type": null,
                "health": {
                    "RAM": {
                        "state": 0,
                        "value": 43
                    },
                    "CPU": {
                        "state": 0,
                        "value": 13
                    },
                    "SWAP": {
                        "state": 0,
                        "value": 0
                    },
                    "HDD": [
                        {
                            "mount_point": "/",
                            "used_space": {
                                "state": 0,
                                "value": 27
                            }
                        }
                    ],
                    "state": 2
                },
                "host_count": 5,
                "cluster": null,
                "component_type": {
                    "id": "SENSOR",
                    "label": "Сенсор"
                },
                "monitored_system": {
                    "id": "4e51bfa2-c0a0-44b9-94be-37f823cb052a",
                    "label": "fvm191"
                },
                "name": "fvm191.lpr.jet.msk.su",
                "updates": {
                    "CVE_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "SURICATA_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BRO_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BL_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    }
                },
                "host": {
                    "id": "bc59f990-5804-4815-aa81-79f039c2ca94",
                    "label": "10.31.11.191"
                },
                "status": {
                    "id": "COMPROMISED",
                    "label": "Скомпрометирован"
                },
                "id": "4b8d9470-66db-430b-a1a7-35450bc35bb2",
                "sensor_mode": null,
                "region": null
            },
            {
                "department": null,
                "parent_id": "58f941eb-5d51-4cd4-8860-047263ca7990",
                "sensor_type": null,
                "health": {
                    "RAM": {
                        "state": 0,
                        "value": 41
                    },
                    "CPU": {
                        "state": 0,
                        "value": 12
                    },
                    "SWAP": {
                        "state": 0,
                        "value": 0
                    },
                    "HDD": [
                        {
                            "mount_point": "/",
                            "used_space": {
                                "state": 0,
                                "value": 27
                            }
                        }
                    ],
                    "state": 0
                },
                "host_count": 2,
                "cluster": null,
                "component_type": {
                    "id": "SENSOR",
                    "label": "Сенсор"
                },
                "monitored_system": {
                    "id": "250438c5-02fc-4af0-88fd-97ea4f907d76",
                    "label": "dvm_177"
                },
                "name": "dvm177.lpr.jet.msk.su",
                "updates": {
                    "SURICATA_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "CVE_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BRO_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BL_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    }
                },
                "host": {
                    "id": "3cb3a066-6ac8-40fc-b550-837742b5347b",
                    "label": "10.31.9.177"
                },
                "status": {
                    "id": "INCREMENTAL_LEARNING",
                    "label": "Дообучение"
                },
                "id": "f3a0cac4-825c-4d86-94fc-b6f58c027997",
                "sensor_mode": null,
                "region": null
            },
            {
                "department": null,
                "parent_id": null,
                "sensor_type": null,
                "health": null,
                "host_count": null,
                "cluster": null,
                "component_type": {
                    "id": "SCS",
                    "label": "СУС"
                },
                "monitored_system": null,
                "name": "dvm173.lpr.jet.msk.su",
                "updates": {
                    "SURICATA_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "CVE_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BRO_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BL_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    }
                },
                "host": {
                    "id": "ce8d7773-7e09-4334-b235-fabe35d6396b",
                    "label": "10.31.9.173"
                },
                "status": {
                    "id": "ACTIVE",
                    "label": "Обнаружение"
                },
                "id": "c1d6da96-5861-4cfe-a6c3-c49826dd5868",
                "sensor_mode": null,
                "region": null
            },
            {
                "department": null,
                "parent_id": "58f941eb-5d51-4cd4-8860-047263ca7990",
                "sensor_type": null,
                "health": {
                    "RAM": {
                        "state": 0,
                        "value": 46
                    },
                    "CPU": {
                        "state": 0,
                        "value": 55
                    },
                    "SWAP": {
                        "state": 0,
                        "value": 0
                    },
                    "HDD": [
                        {
                            "mount_point": "/",
                            "used_space": {
                                "state": 0,
                                "value": 27
                            }
                        }
                    ],
                    "state": 0
                },
                "host_count": null,
                "cluster": null,
                "component_type": {
                    "id": "SENSOR",
                    "label": "Сенсор"
                },
                "monitored_system": {
                    "id": "0b0e56e7-0d09-49ea-a9ae-a8bcdfc4230a",
                    "label": "fvm139"
                },
                "name": "fvm139.lpr.jet.msk.su",
                "updates": {
                    "SURICATA_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "CVE_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BRO_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    },
                    "BL_UPDATE": {
                        "current_version": "20000101",
                        "available_version": "20000101"
                    }
                },
                "host": {
                    "id": "f80a0a9d-2a08-44d0-a9f8-86462946084a",
                    "label": "10.31.11.139"
                },
                "status": {
                    "id": "ACTIVE",
                    "label": "Обнаружение"
                },
                "id": "677c73fb-5019-4c55-9336-bdda6d86087c",
                "sensor_mode": null,
                "region": null
            }
        ],
        "total": 7
    });
}
exports.response = response;