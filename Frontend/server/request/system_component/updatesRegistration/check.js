function response(request, response, config) {
    response.status(200).send({
        result: true
    });
}
exports.response = response;