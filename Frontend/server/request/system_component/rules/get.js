function response(request, response, config) {
    var body = request.body;
    response.status(200).send({
        "total": 19040,
        "rows": [
            {
                "source": "Suricata",
                "signature_name": "ET TROJAN APT SuperhardCorp DNS Lookup (books.mrface.com)",
                "sid": "2021582",
                "id": "00233609-6c27-3d7b-826a-e1bfdb33c38b",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "trojan-activity",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET EXPLOIT Possible CVE-2014-3704 Drupal SQLi attempt URLENCODE 19",
                "sid": "2019440",
                "id": "0000067e-9edd-3bae-8800-cf2c9f84b461",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "web-application-attack",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET WEB_SPECIFIC_APPS PSY Auction SQL Injection Attempt -- item.php id UPDATE",
                "sid": "2004935",
                "id": "00237185-4b22-32bd-9fff-c87ff6295703",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "web-application-attack",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET WEB_SPECIFIC_APPS Joomla! SQL Injection Attempt -- menu.php UNION SELECT",
                "sid": "2005415",
                "id": "0049cc5b-3cd4-3125-ad53-87514c63a357",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "web-application-attack",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET TROJAN Win32/Neutrino Checkin 2",
                "sid": "2022462",
                "id": "00066c5d-1a61-381c-8234-3d50dfd4fa09",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "trojan-activity",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET EXPLOIT Pwdump3e pwservice.exe Access port 445",
                "sid": "2000564",
                "id": "00051a88-2d59-3f90-80a0-f22690af2386",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "misc-attack",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET EXPLOIT Apache Struts 2 REST Plugin (B64) 5",
                "sid": "2024672",
                "id": "003be821-fa65-3e04-9e6a-f1d1f7490d12",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "attempted-user",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET TROJAN Karagany/Kazy Obfuscated Payload Download",
                "sid": "2014230",
                "id": "003c01f4-ea11-301a-a9e7-bc27868cef36",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "trojan-activity",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET CURRENT_EVENTS Angler EK Possible Flash/IE Payload Dec 24 2013",
                "sid": "2017902",
                "id": "00232693-79ee-3902-adcd-e41cdb4e58e2",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "trojan-activity",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET WEB_SPECIFIC_APPS Metyus Okul Yonetim Sistemi SQL Injection Attempt -- uye_giris_islem.asp sifre ASCII",
                "sid": "2006805",
                "id": "000ad633-8e44-321d-b64d-b5150f514ca5",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "web-application-attack",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET TROJAN Win32/Swrort.A Checkin 2",
                "sid": "2019841",
                "id": "00134a3a-28d0-3b76-a23e-a470dcb6c4e8",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "trojan-activity",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET TOR Known Tor Relay/Router (Not Exit) Node Traffic group 380",
                "sid": "2522758",
                "id": "00398de1-c618-3a1e-a563-06efa8563997",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "misc-attack",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET WEB_SPECIFIC_APPS SoftMP3 search Parameter INSERT INTO SQL Injection Attempt",
                "sid": "2013128",
                "id": "002b00be-7236-3620-9fc1-b6b0245d8811",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "web-application-attack",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET MALWARE Clickspring.net Spyware Reporting Successful Install",
                "sid": "2001494",
                "id": "002a0f4d-f19c-3077-b656-165dd61f6676",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "trojan-activity",
                "is_active": true,
                "priority": null
            },
            {
                "source": "Suricata",
                "signature_name": "ET TROJAN Cnzz.cn Related Dropper Checkin",
                "sid": "2013790",
                "id": "00437ee1-2aa0-3441-a2d8-ef5139d534d5",
                "update_version": "20000101",
                "reaction_name": "Реакция по умолчанию",
                "class_name": "trojan-activity",
                "is_active": true,
                "priority": null
            }
        ]
    });
}
exports.response = response;