function response(request, response, config) {
    response.status(200).send({
        "response": "IDLE",
        "integrity": "OK",
        "last_time": "2018-02-28 16:13:30.308527+0300",
        "db_stats": {
            "num_changed_total": 0,
            "num_new": 0,
            "num_degraded": 0,
            "num_dangling": 0,
            "num_ex_re": 0,
            "num_deleted": 0,
            "num_files": 14673,
            "num_ex_pfx": 0,
            "num_changed": 0,
            "num_ex_sfx": 539
        },
        "db_size": 14673,
        "status": "Готов к работе",
        "next_time": "2018-02-28 17:13:30.308697+0300",
        "total_changes": 0,
        "last_state": "Нарушений не обнаружено"
    });
}
exports.response = response;