function response(request, response, config) {
    response.status(200).send({
        "last_state": "Нарушений не обнаружено",
        "start_time": "2018-02-28 16:28:56.318249+0300",
        "db_stats": {
            "num_dangling": 0,
            "num_files": 14673,
            "num_changed_total": 0,
            "num_deleted": 0,
            "num_degraded": 0,
            "num_ex_pfx": 0,
            "num_changed": 0,
            "num_ex_sfx": 539,
            "num_new": 0,
            "num_ex_re": 0
        },
        "last_time": "2018-02-28 16:13:30.308527+0300",
        "integrity": "OK",
        "total_changes": 0,
        "status": "Проводится проверка...",
        "response": "CHECK_IN_PROGRESS",
        "db_size": 14673
    });
}
exports.response = response;