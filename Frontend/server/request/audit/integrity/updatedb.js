function response(request, response, config) {
    response.status(200).send({
        "db_stats": {
            "num_ex_re": 0,
            "num_ex_sfx": 539,
            "num_changed": 0,
            "num_degraded": 0,
            "num_changed_total": 0,
            "num_deleted": 0,
            "num_dangling": 0,
            "num_files": 14673,
            "num_new": 0,
            "num_ex_pfx": 0
        },
        "last_state": "Нарушений не обнаружено",
        "integrity": "OK",
        "db_size": 14673,
        "last_time": "2018-02-28 16:29:10.404314+0300",
        "response": "UPDATING_DATABASE",
        "total_changes": 0,
        "status": "Проводится обновление БКЦ...",
        "start_time": "2018-02-28 16:29:49.178658+0300"
    });
}
exports.response = response;