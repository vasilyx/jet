function response(request, response, config) {
    response.status(200).send({
        "response": "DELAY_SET",
        "status": "Следующая проверка отложена",
        "db_stats": {
            "num_changed_total": 0,
            "num_new": 0,
            "num_ex_re": 0,
            "num_changed": 0,
            "num_dangling": 0,
            "num_ex_sfx": 539,
            "num_files": 14673,
            "num_degraded": 0,
            "num_deleted": 0,
            "num_ex_pfx": 0
        },
        "db_size": 14673,
        "next_time": "2018-02-28 17:59:10.404529+0300",
        "last_state": "Нарушений не обнаружено",
        "integrity": "OK",
        "total_changes": 0,
        "last_time": "2018-02-28 16:29:10.404314+0300"
    });
}
exports.response = response;