function response(request, response, config) {
    response.status(200).send({
        "total": "1",
        "rows": [
            {
                "registered_timestamp": "2017-06-04T22:10:00.067854+03:00",
                "audit_type_name": "Event name",
                "severity": {"id": "1", "label": "уровень критичности"},
                "event_type": {"id": "1", "label": "тип события"},
                "event_category": {"id": "1", "label": "категория события"},
                "subject_entity_type": {"id": "1", "label": "тип субъектов"},
                "subject_name": "имя субъекта",
                "object_entity_type": {"id": "1", "label": "тип объекта"},
                "object_name": "имя объекта"
            }
        ]
    });
}
exports.response = response;