function response(request, response, config) {
    response.status(200).send({
        "rows":[
            {
                "label":"Ssh",
                "id":"8e41f037-1217-4853-96d8-d9afecae5b50"
            },
            {
                "label":"Ldap",
                "id":"b2502d3e-7360-48c6-84eb-5d74669d187b"
            },
            {
                "label":"Telnet",
                "id":"8d87f9d9-f0a8-415a-8eaa-6a3cffed3327"
            }
        ],
        "total":3
    });
}
exports.response = response;