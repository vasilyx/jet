function response(request, response, config) {
    response.status(200).send({
        "total": 5,
        "rows": [
            {
                "id": 1,
                "label": "Сенсор первый"
            },
            {
                "id": 2,
                "label": "Сенсор второй"
            },
            {
                "id": 3,
                "label": "Третий тип"
            }
        ]
    });
}
exports.response = response;