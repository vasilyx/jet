function response(request, response, config) {
    var body = request.body;
    response.status(200).send({
        "id": body.id,
        "name": "Первая тестовая система",
        "description": "Мега описание для первой тестовой системы",
        "subnet_ip": "255.255.0.0"
    });
}
exports.response = response;