function response(request, response, config) {
    response.status(200).send({
        "total": 3,
        "rows": [
            {
                "id": 1,
                "name": "Первая тестовая система",
                "description": "Мега описание для первой тестовой системы",
                "dns_ip": "192.46.39.39",
                "dhcp_ip": "192.168.1.50",
                "ald_ip": "176.158.1.40",
                "subnet_ip": "255.255.0.0",
                "сhannel_capacity": "1024"
            },
            {
                "id": 2,
                "name": "Вторая тестовая система",
                "description": "Некое описание для воторой системы",
                "dns_ip": "192.46.39.39",
                "dhcp_ip": "192.168.1.50",
                "ald_ip": "176.158.1.40",
                "subnet_ip": "255.255.6.6",
                "сhannel_capacity": "2048"
            },
            {
                "id": 3,
                "name": "Третья тестовая система",
                "description": "Возможно будет описание для третьей",
                "dns_ip": "192.46.39.39",
                "dhcp_ip": "192.168.1.50",
                "ald_ip": "176.158.1.40",
                "subnet_ip": "255.255.12.12",
                "сhannel_capacity": "4096"
            }
        ]
    });
}
exports.response = response;