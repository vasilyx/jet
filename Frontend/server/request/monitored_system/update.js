function response(request, response, config) {
    var body = request.body;
    response.status(200).send({
        "id": uuid(),
        "name": body.name,
        "description": body.description || '',
        "subnet_ip": body.subnet_ip || ''
    });
}
exports.response = response;