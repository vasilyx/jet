function response(request, response, config) {
    var body = request.body;
    response.status(200).send({
        "id": body.id,
        "name": body.name,
        "description": body.description || '',
        "black_list_type_id": body.black_list_type_id,
        "reaction_list_id": body.reaction_list_id,
        "is_active": body.is_active,
        "is_deleted": false,
        "direction": body.direction,
        "version": 2,
        "create_timestamp": "2017-05-31T11:25:04.048399+03:00",
        "create_user": {"id": 1, "label": "root"},
        "update_timestamp": "2017-06-04T22:10:00.067854+03:00",
        "update_user": {"id": 3, "label": "super-admin"}
    });
}
exports.response = response;
