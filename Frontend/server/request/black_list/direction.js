function response(request, response, config) {
    var body = request.body;
    response.status(200).send(
        [
            {
                "label": "Везде",
                "id": "ANY"
            },
            {
                "label": "Источник",
                "id": "SRC"
            },
            {
                "label": "Получатель",
                "id": "DST"
            }
        ]
    );
}
exports.response = response;
