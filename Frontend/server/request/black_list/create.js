var uuid = require('uuid/v4');

function response(request, response, config) {
    var body = request.body;
    response.status(200).send({
        "id": uuid(),
        "name": body.name,
        "description": body.description || '',
        "black_list_type_id": body.black_list_type_id,
        "reaction_list_id": body.reaction_list_id,
        "is_active": body.is_active,
        "direction": body.direction,
        "is_deleted": false,
        "version": 1,
        "create_timestamp": "2017-05-31T11:25:04.048399+03:00",
        "create_user": {"id": 1, "label": "root"},
        "update_timestamp": null,
        "update_user": null
    });
}
exports.response = response;
