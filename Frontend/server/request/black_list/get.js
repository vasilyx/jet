function response(request, response, config) {
    response.status(200).send({
        "id": 1,
        "name": "Черный список 1",
        "description": "Описание черного списка",
        "direction": {
            "id": "ANY",
            "label": "Везде"
        },
        "black_list_type_id": 1,
        "reaction_list_id": 2,
        "is_active": true,
        "is_deleted": false,
        "version": 1,
        "create_timestamp": "2017-05-31T11:25:04.048399+03:00",
        "create_user": {"id": 1, "label": "root"},
        "update_timestamp": "2017-06-04T22:10:00.067854+03:00",
        "update_user": {"id": 2, "label": "admin"}
    });
}
exports.response = response;
