function response(request, response, config) {
    response.status(200).send({
        "total": 1,
        "rows": [
            {
                "id": 1,
                "black_list_id": 1,
                "value": "10.0.0.1",
                "description": "Описание записи черного списка",
                "is_deleted": false,
                "direction": {
                    "id": "SRC",
                    "label": "Источник"
                },
                "create_user_id": "root",
                "create_timestamp": "2017-06-05T22:00:00.067854+03:00",
                "update_user_id": "root",
                "update_timestamp": "2017-06-05T22:00:00.067854+03:00"
            }
        ]
    });
}
exports.response = response;
