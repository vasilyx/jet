var uuid = require('uuid/v4');

function response(request, response, config) {
    response.status(200).send({
        "id": uuid(),
        "black_list_id": 1,
        "value": "VALUE",
        "description": "Описание записи черного списка",
        "direction": {
            "id": "ANY",
            "label": "Везде"
        },
        "is_deleted": false,
        "create_user_id": "root",
        "create_timestamp": "2017-06-05T22:00:00.067854+03:00",
        "update_user_id": "root",
        "update_timestamp": "2017-06-05T22:00:00.067854+03:00"
    });
}
exports.response = response;
