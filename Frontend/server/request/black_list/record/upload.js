var uuid = require('uuid/v4');

function response(request, response, config) {
    response.status(200).send({
        "total": 3,
        "rows": [
            {
                "id": uuid(),
                "black_list_id": 1,
                "value": "10.0.0.1",
                "description": "Описание записи черного списка",
                "is_deleted": false,
                "create_user_id": "root",
                "create_timestamp": "2017-06-05T22:00:00.067854+03:00",
                "update_user_id": "root",
                "update_timestamp": "2017-06-05T22:00:00.067854+03:00"
            },
            {
                "id": uuid(),
                "black_list_id": 1,
                "value": "192.168.0.1",
                "description": "Описание записи черного списка",
                "is_deleted": false,
                "create_user_id": "root",
                "create_timestamp": "2017-06-05T22:00:00.067854+03:00",
                "update_user_id": "root",
                "update_timestamp": "2017-06-05T22:00:00.067854+03:00"
            },
            {
                "id": uuid(),
                "black_list_id": 1,
                "value": "25.25.25.25",
                "description": "Описание записи черного списка",
                "is_deleted": false,
                "create_user_id": "root",
                "create_timestamp": "2017-06-05T22:00:00.067854+03:00",
                "update_user_id": "root",
                "update_timestamp": "2017-06-05T22:00:00.067854+03:00"
            }
        ]
    });
}
exports.response = response;
