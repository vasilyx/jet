function response(request, response, config) {
    response.status(200).send({
        total: 3,
        rows: [
            {
                "id": 1,
                "name": "Черный список: IP",
                "description": "Описание черного списка",
                "direction": {
                    "id": "ANY",
                    "label": "Везде"
                },
                "black_list_type_id": 1,
                "reaction_list_id": 2,
                "is_active": true,
                "is_deleted": false,
                "version": 1,
                "create_timestamp": "2017-05-31T11:25:04.048399+03:00",
                "create_user": {"id": 1, "label": "root"},
                "update_timestamp": "2017-06-04T22:10:00.067854+03:00",
                "update_user": {"id": 2, "label": "admin"}
            },
            {
                "id": 2,
                "name": "Черный список: Email",
                "description": "Описание черного списка",
                "direction": {
                    "id": "ANY",
                    "label": "Везде"
                },
                "black_list_type_id": 2,
                "reaction_list_id": 2,
                "is_active": true,
                "is_deleted": false,
                "version": 2,
                "create_timestamp": "2017-05-31T11:25:04.048399+03:00",
                "create_user": {"id": 1, "label": "root"},
                "update_timestamp": "2017-06-04T22:10:00.067854+03:00",
                "update_user": {"id": 2, "label": "admin"}
            },
            {
                "id": 3,
                "name": "Черный список: DNS",
                "description": "Описание черного списка",
                "direction": {
                    "id": "ANY",
                    "label": "Везде"
                },
                "black_list_type_id": 3,
                "reaction_list_id": 1,
                "is_active": true,
                "is_deleted": false,
                "version": 1,
                "create_timestamp": "2017-05-31T11:25:04.048399+03:00",
                "create_user": {"id": 1, "label": "root"},
                "update_timestamp": "2017-06-04T22:10:00.067854+03:00",
                "update_user": {"id": 2, "label": "admin"}
            },
            {
                "id": 4,
                "name": "Черный список: URL",
                "description": "Описание черного списка",
                "direction": {
                    "id": "ANY",
                    "label": "Везде"
                },
                "black_list_type_id": 4,
                "reaction_list_id": 2,
                "is_active": true,
                "is_deleted": false,
                "version": 1,
                "create_timestamp": "2017-05-31T11:25:04.048399+03:00",
                "create_user": {"id": 1, "label": "root"},
                "update_timestamp": "2017-06-04T22:10:00.067854+03:00",
                "update_user": {"id": 2, "label": "admin"}
            },
            {
                "id": 5,
                "name": "Черный список: MD5",
                "description": "Описание черного списка",
                "direction": {
                    "id": "ANY",
                    "label": "Везде"
                },
                "black_list_type_id": 5,
                "reaction_list_id": 2,
                "is_active": true,
                "is_deleted": false,
                "version": 3,
                "create_timestamp": "2017-05-31T11:25:04.048399+03:00",
                "create_user": {"id": 1, "label": "root"},
                "update_timestamp": "2017-06-04T22:10:00.067854+03:00",
                "update_user": {"id": 2, "label": "admin"}
            }
        ]
    });
}
exports.response = response;
