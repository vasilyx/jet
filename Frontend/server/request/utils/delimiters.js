function response(request, response, config) {
    response.status(200).send([
        [
            ";",
            "; (Точка с запятой)"
        ],
        [
            ",",
            ", (Запятая)"
        ],
        [
            "\\t",
            "Табуляция"
        ]
    ]);
}
exports.response = response;