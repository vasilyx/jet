function response(request, response, config) {
    response.status(200).send([
        {
            "id":"UNKNOWN",
            "label":"Неизвестно"
        },
        {
            "id":"ACCEPTED",
            "label":"Одобрено"
        },
        {
            "id":"REDETECTED",
            "label":"Повторное обнаружение"
        },
        {
            "id":"DELETED",
            "label":"Удалено"
        }
    ]);
}
exports.response = response;