function response(request, response, config) {
    response.status(200).send({
        "total": 2,
        "rows": [
            {
                "label": "Подразделени 1",
                "id": 1
            },
            {
                "label": "Департамент 2",
                "id": 2
            }
        ]
    });
}
exports.response = response;
