function response(request, response, config) {
    response.status(200).send([
        {
            "id":"BUNDLE_UPDATE",
            "label":"Пакет обновлений"
        },
        {
            "id":"BRO_UPDATE",
            "label":"Обновление правил BRO"
        },
        {
            "id":"SURICATA_UPDATE",
            "label":"Обновление сигнатур Suricata"
        },
        {
            "id":"SOFTWARE_UPDATE",
            "label":"Обновление ПО"
        },
        {
            "id":"GEOIP_UPDATE",
            "label":"Обновление GeoIP"
        },
        {
            "id":"BL_UPDATE",
            "label":"Обновление черных списков"
        },
        {
            "id":"MAP_UPDATE",
            "label":"Обновление картографии"
        }
    ]);
}
exports.response = response;