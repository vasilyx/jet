function response(request, response, config) {
    response.status(200).send(
            [
                {
                    "id":"INSTALLING",
                    "label":"\u0423\u0441\u0442\u0430\u043d\u043e\u0432\u043a\u0430..."
                },
                {
                    "id":"ERROR_INTEGRITY",
                    "label":"\u041e\u0448\u0438\u0431\u043a\u0430 \u043f\u0440\u043e\u0432\u0435\u0440\u043a\u0438. \u041d\u0430\u0440\u0443\u0448\u0435\u043d\u0430 \u0446\u0435\u043b\u043e\u0441\u0442\u043d\u043e\u0441\u0442\u044c"
                },
                {
                    "id":"DELETED",
                    "label":"\u0423\u0434\u0430\u043b\u0435\u043d\u043e"
                },
                {
                    "id":"NEW",
                    "label":"\u041d\u043e\u0432\u043e\u0435"
                },
                {
                    "id":"ERROR_DOWNLOAD_STOP",
                    "label":"\u041e\u0448\u0438\u0431\u043a\u0430 \u0441\u043a\u0430\u0447\u0438\u0432\u0430\u043d\u0438\u044f. \u041e\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d\u043e"
                },
                {
                    "id":"READY_TO_INSTALL",
                    "label":"\u0413\u043e\u0442\u043e\u0432\u043e \u043a \u0443\u0441\u0442\u0430\u043d\u043e\u0432\u043a\u0435"
                },
                {
                    "id":"ERROR_DOWNLOAD_RETRY",
                    "label":"\u041e\u0448\u0438\u0431\u043a\u0430 \u0441\u043a\u0430\u0447\u0438\u0432\u0430\u043d\u0438\u044f. \u041f\u043e\u0432\u0442\u043e\u0440"
                },
                {
                    "id":"WORKING",
                    "label":"\u0420\u0430\u0431\u043e\u0442\u0430\u0435\u0442..."
                },
                {
                    "id":"ERROR_CHECK_SUM",
                    "label":"\u041e\u0448\u0431\u0438\u043a\u0430 \u043f\u0440\u043e\u0432\u0435\u0440\u043a\u0438. \u041d\u0435\u0432\u0435\u0440\u043d\u0430\u044f \u043a\u043e\u043d\u0442\u0440\u043e\u043b\u044c\u043d\u0430\u044f \u0441\u0443\u043c\u043c\u0430"
                },
                {
                    "id":"ERROR_INSTALL",
                    "label":"\u041e\u0448\u0438\u0431\u043a\u0430 \u0443\u0441\u0442\u0430\u043d\u043e\u0432\u043a\u0438"
                },
                {
                    "id":"DOWNLOADING",
                    "label":"\u0421\u043a\u0430\u0447\u0438\u0432\u0430\u043d\u0438\u0435..."
                },
                {
                    "id":"CHECKING",
                    "label":"\u041f\u0440\u043e\u0432\u0435\u0440\u043a\u0430..."
                },
                {
                    "id":"ROLLEDBACK",
                    "label":"\u041e\u0442\u043c\u0435\u043d\u0435\u043d\u043e"
                },
                {
                    "id":"INSTALLED",
                    "label":"\u0423\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d\u043e"
                },
                {
                    "id":"PENDING",
                    "label":"\u041e\u0436\u0438\u0434\u0430\u0435\u0442..."
                }
            ]
      );
}
exports.response = response;