function response(request, response, config) {
    response.status(200).send({
        "total":1,
        "rows":[
        {
            "description":"comments",
            "revision":2,
            "error":"no error",
            "user":{
                "label":"admin",
                "id":"67824c3d-119f-43f4-aa86-6c7b8629ea38"
            },
            "host":"129.1.1.1",
            "update_status":{
                "label":"Проверка...",
                "id":"CHECKING"
            },
            "url":"http://minobr.ru/update/super.rar",
            "composition":"??",
            "id":"f1aadcda-1810-46c4-8c83-f5aa24bb5a75",
            "install_timestamp":"2017-10-03T17:35:27.388128",
            "checksum":"some hash",
            "size":192345,
            "update_type":{
                "label":"Обновление ПО",
                "id":"SOFTWARE_UPDATE"
            },
            "discover_timestamp":"2017-10-03T17:35:27.388128",
            "update_filename":"/usr/bin/matrshka",
            "version":"1"
        }
    ]
    });
}
exports.response = response;