function response(request, response, config) {
    response.status(200).send({
        "total": 2,
        "rows": [
            {
                "label": "severity_1",
                "id": 1
            },
            {
                "label": "severity_2",
                "id": 2
            }
        ]
    });
}
exports.response = response;
