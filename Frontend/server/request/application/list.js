function response(request, response, config) {
    response.status(200).send({
        "rows":[
            {
                "label":"Chrome",
                "id":"93725fea-67f1-439e-8918-aa52cfc63d17"
            },
            {
                "label":"Digger.exe",
                "id":"1402cb81-6a89-43de-8f4d-ea855fc1ec09"
            },
            {
                "label":"Firefox",
                "id":"8eabc8d8-0878-48fc-a0e3-87c03dff248d"
            },
            {
                "label":"OpenSSH OpenSSH_6.0p1 Debian-4+deb7u3",
                "id":"bde80536-f618-40bd-86fa-76c34de1815b"
            },
            {
                "label":"OpenSSH OpenSSH_7.2p2 Ubuntu-4ubuntu2.1",
                "id":"0010118b-45f3-4fcc-9d8c-2a0d7659722b"
            },
            {
                "label":"OpenSSH-ubuntu",
                "id":"6819eebe-c6e0-4092-a1f5-2ac5ea816650"
            },
            {
                "label":"Prince.exe",
                "id":"613737a1-f294-46ab-b835-0b9150129911"
            },
            {
                "label":"PuTTY_Release PuTTY_Release_0.66",
                "id":"b9065887-6c9d-44aa-a033-a1771363ccb6"
            }
        ],
        "total":8
    });
}
exports.response = response;