function response(request, response, config) {
    response.status(200).send({
        "rows": [
            {
                "processed_timestamp": "2018-03-20T12:46:43.633155+03:00",
                "name": "evm131.lpr.jet.msk.su",
                "id": "3752d691-485c-453b-911e-11882178e3d2",
                "connection_host": "10.31.10.131",
                "component_type": {
                    "label": "Сенсор",
                    "id": "SENSOR"
                },
                "state": {
                    "label": "Ошибка",
                    "id": "ERROR"
                },
                "reason": null,
                "received_timestamp": "2018-03-20T12:45:42.945177+03:00",
                "info": null,
                "error": null
            },
            {
                "processed_timestamp": "2018-03-20T12:46:47.804772+03:00",
                "name": "evm247.lpr.jet.msk.su",
                "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                "connection_host": "10.31.10.247",
                "component_type": {
                    "label": "Сенсор",
                    "id": "SENSOR"
                },
                "state": {
                    "label": "Подтверждено",
                    "id": "ACCEPTED"
                },
                "reason": null,
                "received_timestamp": "2018-03-20T12:45:18.175449+03:00",
                "info": null,
                "error": null
            }
        ],
        "total": 2
    });
}
exports.response = response;
