function response(request, response, config) {
    response.status(200).send({
        "total":6,
        "rows":[
            {
                "id":"54bdcc8a-bf67-46da-a274-7025b3f71c3c",
                "label":"Linux 3.11 or newer"
            },
            {
                "id":"4582b9b5-8b59-4959-9f9a-6fad82251d22",
                "label":"Windows 7/8"
            },
            {
                "id":"d54c898e-0350-43f0-b59c-d4477e2e39fc",
                "label":"FreeBSD"
            },
            {
                "id":"cb10e3b2-357e-4604-a2ec-e4d4c1ae33f4",
                "label":"Windows Vista SP0/SP2, 7 SP0+, 2008 SP0"
            },
            {
                "id":"f69f51e0-bc59-4fae-a9dc-5e9e3ee8ad2b",
                "label":""
            },
            {
                "id":"04aa3840-1a39-4168-8098-496dec74d4d6",
                "label":"UNKNOWN "
            }
        ]
    });
}
exports.response = response;