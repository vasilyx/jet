function response(request, response, config) {
    response.status(200).send({
        "added": "DETECTED",
        "created": "2018-03-22T14:36:05.699885+03:00",
        "last_time_activity": "2018-03-22T17:38:06.900914+03:00",
        "blacklist_count": 0,
        "vulnerabilities_count": 0,
        "signatures_count": 1,
        "application_count": {
            "all": 1,
            "disapproved": 1
        },
        "service_count": {
            "all": 1,
            "disapproved": 1
        },
        "speed": {
            "upload": 124,
            "download": 124
        }
    });
}
exports.response = response;
