function response(request, response, config) {
    response.status(200).send({
        "rows": [
            {
                "host_status": {
                    "id": "UNKNOWN",
                    "label": "Неизвестный"
                },
                "discovery_timestamp": "2018-03-22T16:01:08.420000+03:00",
                "id": "96eda140-cb8b-4f8b-ba47-07a23239b59c",
                "host_ip": [
                    "10.31.10.58"
                ],
                "host_os": [],
                "last_time_activity": "2018-03-23T12:01:07.104190+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": 1,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T16:01:11.304909+03:00"
            },
            {
                "host_status": {
                    "id": "UNKNOWN",
                    "label": "Неизвестный"
                },
                "discovery_timestamp": "2018-03-22T15:14:51.131000+03:00",
                "id": "214c8a1b-bf7e-40a6-b055-5fa3a68ce2e9",
                "host_ip": [
                    "10.31.252.173"
                ],
                "host_os": [],
                "last_time_activity": "2018-03-22T15:15:02.764708+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": 0,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T15:14:53.081758+03:00"
            },
            {
                "host_status": {
                    "id": "UNKNOWN",
                    "label": "Неизвестный"
                },
                "discovery_timestamp": "2018-03-22T15:13:01.193000+03:00",
                "id": "e82982fb-d4ad-40f9-9cf7-4fb87b5cc80b",
                "host_ip": [
                    "10.31.252.12"
                ],
                "host_os": [],
                "last_time_activity": "2018-03-22T17:24:34.845266+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": 0,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T15:13:05.081121+03:00"
            },
            {
                "host_status": {
                    "id": "REDETECTED",
                    "label": "Повторное обнаружение"
                },
                "discovery_timestamp": "2018-03-22T14:15:41.482239+03:00",
                "id": "f2cb4043-0ef8-4b62-8e30-df24d590a06d",
                "host_ip": [
                    "10.11.28.215"
                ],
                "host_os": [
                    {
                        "id": "57d8c192-6be7-3839-bef5-ab54673e0347",
                        "label": "Windows XP/2000 (RFC1323+"
                    }
                ],
                "last_time_activity": "2018-03-22T14:36:18.351945+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": 0,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T14:35:22.709129+03:00"
            },
            {
                "host_status": {
                    "id": "ACCEPTED",
                    "label": "Одобренный"
                },
                "discovery_timestamp": "2018-03-22T14:14:56.289497+03:00",
                "id": "31bc3b5c-7d12-49cd-aa57-27411ed8394e",
                "host_ip": [
                    "10.31.250.101"
                ],
                "host_os": [
                    {
                        "id": "57d8c192-6be7-3839-bef5-ab54673e0347",
                        "label": "Windows XP/2000 (RFC1323+"
                    }
                ],
                "last_time_activity": "2018-03-22T17:38:06.900914+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": 3,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T14:36:05.699885+03:00"
            },
            {
                "host_status": {
                    "id": "ACCEPTED",
                    "label": "Одобренный"
                },
                "discovery_timestamp": "2018-03-22T14:13:37.084049+03:00",
                "id": "dd3d1a84-537e-46c1-ab5f-6062c7957a00",
                "host_ip": [
                    "10.31.253.251"
                ],
                "host_os": [
                    {
                        "id": "57d8c192-6be7-3839-bef5-ab54673e0347",
                        "label": "Windows XP/2000 (RFC1323+"
                    }
                ],
                "last_time_activity": "2018-03-22T14:36:18.351945+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": null,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T14:36:16.436858+03:00"
            },
            {
                "host_status": {
                    "id": "ACCEPTED",
                    "label": "Одобренный"
                },
                "discovery_timestamp": "2018-03-22T13:33:28.161678+03:00",
                "id": "ea638994-abf5-4955-abc2-aff429c55ed8",
                "host_ip": [
                    "10.31.90.165"
                ],
                "host_os": [
                    {
                        "id": "57d8c192-6be7-3839-bef5-ab54673e0347",
                        "label": "Windows XP/2000 (RFC1323+"
                    }
                ],
                "last_time_activity": "2018-03-23T09:14:39.414476+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": 0,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T14:36:16.436858+03:00"
            },
            {
                "host_status": {
                    "id": "ACCEPTED",
                    "label": "Одобренный"
                },
                "discovery_timestamp": "2018-03-22T13:31:05.765392+03:00",
                "id": "42c94340-3e7e-4f51-946a-c94ffbd1e995",
                "host_ip": [
                    "10.31.253.239"
                ],
                "host_os": [],
                "last_time_activity": "2018-03-22T14:36:18.351945+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": 0,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T14:36:16.436858+03:00"
            },
            {
                "host_status": {
                    "id": "REDETECTED",
                    "label": "Повторное обнаружение"
                },
                "discovery_timestamp": "2018-03-22T13:24:30.910146+03:00",
                "id": "c8571a8d-f8c1-464b-b172-d16c51c67cd2",
                "host_ip": [
                    "10.31.9.4"
                ],
                "host_os": [],
                "last_time_activity": "2018-03-23T11:46:23.063346+03:00",
                "added": "DETECTED",
                "unconfirmed_applications_count": 0,
                "hostname": null,
                "system_component": {
                    "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
                    "label": "evm247.lpr.jet.msk.su"
                },
                "create_timestamp": "2018-03-22T14:35:45.574965+03:00"
            }
        ],
        "total": 124
    });
}
exports.response = response;
