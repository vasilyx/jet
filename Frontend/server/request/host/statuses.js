function response(request, response, config) {
    response.status(200).send([
        {
            "label": "Одобренный",
            "id": "ACCEPTED"
        },
        {
            "label": "Отклонен",
            "id": "DECLINED"
        },
        {
            "label": "Неизвестный",
            "id": "UNKNOWN"
        },
        {
            "label": "Коррекция",
            "id": "CORRECTION"
        },
        {
            "label": "Повторное обнаружение",
            "id": "REDETECTED"
        },
        {
            "label": "Удаленный",
            "id": "DELETED"
        }
    ]);
}
exports.response = response;