function response(request, response, config) {
    response.status(200).send({
        "total": 1,
        "rows": [
            {
                "group": "sig group",
                "name": "name",
                "port": 847,
                "id": 847,
                "severity": {
                    "label": 1,
                    "id": "9a72b1e3-7a09-4a39-9d7f-95e5858f70ae"
                },
                "source_ip": "12.83.132.136",
                "target_ip": "94.244.166.38",
                "alert_timestamp": "2017-07-11T16:07:00+03:00"
            }
        ]
    });
}
exports.response = response;