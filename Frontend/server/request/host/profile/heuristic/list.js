function response(request, response, config) {
    response.status(200).send({
        "total": 6,
        "rows": [
            {
                "source_host_id": "31bc3b5c-7d12-49cd-aa57-27411ed8394e",
                "alert_timestamp": "2018-03-22T14:38:05+00:00",
                "host_ip": "10.31.250.101",
                "target_host_id": "ed87a2c8-f030-4eea-98d6-db925afb69ea",
                "name": "Новый хост",
                "port": 0,
                "id": "b922a870-efed-4dc2-8a86-b10891181019",
                "source_ip": "10.31.250.101",
                "severity": {
                    "id": "60a66495-8603-4aee-9db9-0291b1500b0a",
                    "label": 0
                },
                "target_ip": "10.31.10.247",
                "alert_microseconds": 170000,
                "alert_type_id": "09caa12d-acde-4f84-adc6-d524373d6af9",
                "protocol_name": ""
            },
            {
                "source_host_id": "31bc3b5c-7d12-49cd-aa57-27411ed8394e",
                "alert_timestamp": "2018-03-22T14:01:11+00:00",
                "host_ip": "10.31.250.101",
                "target_host_id": "ed87a2c8-f030-4eea-98d6-db925afb69ea",
                "name": "Новое ПО хоста",
                "port": 0,
                "id": "9d342ca2-2181-4de4-a090-7740d4f37787",
                "source_ip": "10.31.250.101",
                "severity": {
                    "id": "60a66495-8603-4aee-9db9-0291b1500b0a",
                    "label": 0
                },
                "target_ip": "10.31.10.247",
                "alert_microseconds": 221000,
                "alert_type_id": "909280bb-388b-4347-bf3e-28ed679bb6eb",
                "protocol_name": ""
            },
            {
                "source_host_id": "31bc3b5c-7d12-49cd-aa57-27411ed8394e",
                "alert_timestamp": "2018-03-22T13:38:04+00:00",
                "host_ip": "10.31.250.101",
                "target_host_id": "ed87a2c8-f030-4eea-98d6-db925afb69ea",
                "name": "Новый хост",
                "port": 0,
                "id": "d904f3df-103d-4659-8037-b1dbd2f3859d",
                "source_ip": "10.31.250.101",
                "severity": {
                    "id": "60a66495-8603-4aee-9db9-0291b1500b0a",
                    "label": 0
                },
                "target_ip": "10.31.10.247",
                "alert_microseconds": 174000,
                "alert_type_id": "09caa12d-acde-4f84-adc6-d524373d6af9",
                "protocol_name": ""
            },
            {
                "source_host_id": "31bc3b5c-7d12-49cd-aa57-27411ed8394e",
                "alert_timestamp": "2018-03-22T12:44:59+00:00",
                "host_ip": "10.31.250.101",
                "target_host_id": "ed87a2c8-f030-4eea-98d6-db925afb69ea",
                "name": "Новый хост",
                "port": 0,
                "id": "ffc238ea-7590-4328-8a18-80e2b780e9b8",
                "source_ip": "10.31.250.101",
                "severity": {
                    "id": "60a66495-8603-4aee-9db9-0291b1500b0a",
                    "label": 0
                },
                "target_ip": "10.31.10.247",
                "alert_microseconds": 131000,
                "alert_type_id": "09caa12d-acde-4f84-adc6-d524373d6af9",
                "protocol_name": ""
            },
            {
                "source_host_id": "31bc3b5c-7d12-49cd-aa57-27411ed8394e",
                "alert_timestamp": "2018-03-22T12:36:03+00:00",
                "host_ip": "10.31.250.101",
                "target_host_id": "ed87a2c8-f030-4eea-98d6-db925afb69ea",
                "name": "Новый хост",
                "port": 0,
                "id": "f58d9aff-8af7-4e83-9ee4-aad71b5af5d7",
                "source_ip": "10.31.250.101",
                "severity": {
                    "id": "60a66495-8603-4aee-9db9-0291b1500b0a",
                    "label": 0
                },
                "target_ip": "10.31.10.247",
                "alert_microseconds": 669000,
                "alert_type_id": "09caa12d-acde-4f84-adc6-d524373d6af9",
                "protocol_name": ""
            },
            {
                "source_host_id": "31bc3b5c-7d12-49cd-aa57-27411ed8394e",
                "alert_timestamp": "2018-03-22T11:44:57+00:00",
                "host_ip": "10.31.250.101",
                "target_host_id": "ed87a2c8-f030-4eea-98d6-db925afb69ea",
                "name": "Новый хост",
                "port": 0,
                "id": "2c40a42c-0691-47ce-856f-0a87eb10ce1e",
                "source_ip": "10.31.250.101",
                "severity": {
                    "id": "60a66495-8603-4aee-9db9-0291b1500b0a",
                    "label": 0
                },
                "target_ip": "10.31.10.247",
                "alert_microseconds": 143000,
                "alert_type_id": "09caa12d-acde-4f84-adc6-d524373d6af9",
                "protocol_name": ""
            }
        ]
    });
}
exports.response = response;