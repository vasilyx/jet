function response(request, response, config) {
    response.status(200).send({
        "total": 3,
        "rows": [
            {
                "name": "NetBIOS client",
                "protocol": null,
                "vulnerabilities": null,
                "discovery_timestamp": "2018-03-28T10:32:10.850000+03:00",
                "port": [
                    "138"
                ],
                "added": "DETECTED",
                "id": "dbaae7c1-04cc-3868-96ba-9be2bcd8e276",
                "status": {
                    "label": "Неизвестно",
                    "id": "UNKNOWN"
                },
                "software_type": {
                    "label": "ПО",
                    "id": "APPLICATION"
                }
            },
            {
                "name": "Apache",
                "protocol": null,
                "vulnerabilities": null,
                "discovery_timestamp": "2018-03-28T10:28:52.850000+03:00",
                "port": [
                    "80"
                ],
                "added": "DETECTED",
                "id": "1ec4ebf2-d424-3c83-9231-f3d71f9e23b5",
                "status": {
                    "label": "Неизвестно",
                    "id": "UNKNOWN"
                },
                "software_type": {
                    "label": "ПО",
                    "id": "APPLICATION"
                }
            },
            {
                "name": "service_HTTP",
                "protocol": null,
                "vulnerabilities": null,
                "discovery_timestamp": "2018-03-28T10:28:52.849000+03:00",
                "port": null,
                "added": "DETECTED",
                "id": "325155cb-d394-3b21-acdb-91095b993f57",
                "status": {
                    "label": "Неизвестно",
                    "id": "UNKNOWN"
                },
                "software_type": {
                    "label": "Сервис",
                    "id": "SERVICE"
                }
            }
        ]
    });
}
exports.response = response;