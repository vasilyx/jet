function response(request, response, config) {
    var body = request.body;
    response.status(200).send({
        "id": body.id,
        "host_ip": [
            "10.31.250.101"
        ],
        "hostname": null,
        "host_status": {
            "id": "ACCEPTED",
            "label": "Одобренный"
        },
        "monitored_system": {
            "id": "457a681a-107b-4429-843a-9865816f2a0c",
            "label": "net77_64"
        },
        "host_os": [
            {
                "id": "57d8c192-6be7-3839-bef5-ab54673e0347",
                "label": "Windows XP/2000 (RFC1323+"
            }
        ],
        "host_mac": null,
        "system_component": {
            "id": "ed533370-009b-4263-abf3-8b0b90bb6d49",
            "ip": "10.31.10.247",
            "label": "evm247.lpr.jet.msk.su"
        },
        "owner_component_id": "58f941eb-5d51-4cd4-8860-047263ca7990"
    });
}
exports.response = response;