function response(request, response, config) {
    response.status(200).send({
        "rows":[
            {
                "label":"DNS",
                "id":"74e4fcf7-1945-4c96-8a5a-ff79b63e6583"
            },
            {
                "label":"FTP",
                "id":"51defd96-64fc-4d78-885e-174cae73256e"
            },
            {
                "label":"HTTP",
                "id":"3f77d0b7-6ca2-4c23-8992-7def18181fac"
            },
            {
                "label":"RDP",
                "id":"a3d85820-1449-4fe2-9109-40e04987fae6"
            },
            {
                "label":"SSL",
                "id":"56e3e981-a09f-4af7-aa8c-821b52bdd1b2"
            },
            {
                "label":"SSH",
                "id":"f9d00200-4ada-4931-8fff-8c803de40202"
            },
            {
                "label":"TCP",
                "id":"46c37301-3f7b-4793-ae63-643ec34dd3f5"
            },
            {
                "label":"UPD",
                "id":"8982070f-ea80-490e-a692-eb9936eb5f40"
            }
        ],
        "total":8
    });
}
exports.response = response;