function response(request, response, config) {
    response.status(200).send({
        "total": 5,
        "rows": [
            {
                "id": 1,
                "name": "IP",
                "description": "Описание",
                "verification": null
            },
            {
                "id": 2,
                "name": "Email",
                "description": "Описание",
                "verification": null
            },
            {
                "id": 3,
                "name": "DNS",
                "description": "Описание",
                "verification": null
            },
            {
                "id": 4,
                "name": "URL",
                "description": "Описание",
                "verification": null
            },
            {
                "id": 5,
                "name": "MD5",
                "description": "Описание",
                "verification": null
            }
        ]
    });
}
exports.response = response;