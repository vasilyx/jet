function response(request, response, config) {
    var body = request.body;
    response.status(200).send({
        "id": body.id,
        "login": "mybestlogin",
        "source_sus_id": "bac4cc9d-bf62-44ce-9d03-7592541c04b6",
        "create_sus_id": "82365d95-cb8a-42c8-80ad-23056a85fc97",
        "maclabel": 2,
        "source_sus_name": "some_sus",
        "create_sus_name": "another_sus",
        "creation_type": "local",
        "is_blocked": false,
        "failures": 2,
        "register_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "expiration_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "first_login_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "last_login_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "create_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "password_expires_at": "2017-07-11T13:43:58.572809+03:00",
        "update_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "email": "mybestlogin@gmail.com",
        "description": "Описание пользователя и его ролей",
        "version": 1,
        "is_deleted": false,
        "roles": [
            {
                "id": "5eeb675d-6833-46ed-ae82-0832ebf4c479",
                "label": "Администратор безопасности"
            },
            {
                "id": "463e13eb-a4eb-428c-a4e6-8cd2284224a1",
                "label": "Оператор визуального контроля"
            }
        ],
        "personal_info": {
            "family_name": "Фрейд",
            "first_name": "Максимилиан",
            "patronymic_name": "Сигизмундович",
            "position": "Главный эксперт",
            "rank": "Адмирал",
            "department": "ГШ МО РФ",
            "identity_document_type": "Военный билет",
            "identity_document": "ГБ№13к32",
            "description": "Морской волк",
            "create_timestamp": "2017-07-11T13:43:58.572809+03:00",
            "update_timestamp": "2017-07-11T13:43:58.572809+03:00"
        }
    });
}
exports.response = response;