function response(request, response, config) {
    response.status(200).send({
        "total": 1,
        "rows": [
            {
                "id": "6f138e77-72bf-4a22-a5e7-d9cc098e8101",
                "login": "mybestlogin",
                "source_sus_id": "bac4cc9d-bf62-44ce-9d03-7592541c04b6",
                "source_sus_name": "Сурса",
                "create_sus_id": "82365d95-cb8a-42c8-80ad-23056a85fc97",
                "create_sus_name": "Создатель",
                "creation_type": 1,
                "failures": 2,
                "is_blocked": false,
                "register_timestamp": "2017-07-11T13:43:58.572809+03:00",
                "expiration_timestamp": "2017-07-11T13:43:58.572809+03:00",
                "password_expires_at": "2017-07-11T13:43:58.572809+03:00",
                "roles": [
                    {
                        "id": "53a3ff01-b71d-48eb-955f-e2a32ec30a4c",
                        "label": "П"
                    },
                    {
                        "id": "463e13eb-a4eb-428c-a4e6-8cd2284224a1",
                        "label": "ОВК"
                    }
                ]
            }
        ]
    });
}
exports.response = response;