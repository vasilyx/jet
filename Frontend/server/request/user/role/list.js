function response(request, response, config) {
    response.status(200).send({
        "rows": [
            {
                "id": "53a3ff01-b71d-48eb-955f-e2a32ec30a4c",
                "label": "Пользователь"
            },
            {
                "id": "5eeb675d-6833-46ed-ae82-0832ebf4c479",
                "label": "Администратор безопасности"
            },
            {
                "id": "463e13eb-a4eb-428c-a4e6-8cd2284224a1",
                "label": "Оператор визуального контроля"
            }
        ],
        "total": 3
    });
}
exports.response = response;