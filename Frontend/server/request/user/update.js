function response(request, response, config) {
    var body = request.body;
    response.status(200).send({
        "id": body.id,
        "login": body.login,
        "password": '',
        "maclabel": 1,
        "source_sus": body.source_sus,
        "create_sus": "82365d95-cb8a-42c8-80ad-23056a85fc97",
        "creation_type": body.creation_type,
        "is_blocked": body.is_blocked,
        "register_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "expiration_timestamp": body.expiration_timestamp,
        "first_login_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "last_login_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "create_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "update_timestamp": "2017-07-11T13:43:58.572809+03:00",
        "email": body.email,
        "description": body.description,
        "version": 1,
        "is_deleted": false,
        "roles": body.roles || [],
        "personal_info": {
            "family_name": "Фрейд",
            "first_name": "Максимилиан",
            "patronymic_name": "Сигизмундович",
            "position":  "Главный эксперт",
            "rank": "Адмирал",
            "department": "ГШ МО РФ",
            "identity_document_type": {
                "id": 1,
                "label": "Военный билет"
            },
            "identity_document": "ГБ№13к32",
            "description": "Морской волк",
            "create_timestamp": "2017-07-11T13:43:58.572809+03:00",
            "update_timestamp": "2017-07-11T13:43:58.572809+03:00"
        }
    });
}
exports.response = response;