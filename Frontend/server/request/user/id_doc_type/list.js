function response(request, response, config) {
    response.status(200).send([
        {
            "label": "Паспорт",
            "id": "passport"
        },
        {
            "label": "Военный билет",
            "id": "military_card"
        }
    ]);
}
exports.response = response;