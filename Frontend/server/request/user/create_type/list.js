function response(request, response, config) {
    response.status(200).send([
        {
            "id": "local",
            "label": "Локальный"
        },
        {
            "id": "replicated",
            "label": "Реплицированный"
        },
        {
            "id": "not_created",
            "label": "Несозданный"
        }
    ]);
}
exports.response = response;