function response(request, response, config) {
    response.status(200).send({
        "total": 2,
        "rows": [
            {
                "id": 1,
                "name": "Реакция на событие 1"
            },
            {
                "id": 2,
                "name": "Реакция на событие 2"
            },
            {
                "id": 3,
                "name": "Реакция на событие 3"
            }
        ]
    });
}
exports.response = response;