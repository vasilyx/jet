function response(request, response, config) {
    response.status(200).send({
        "alerts_count": 3,
        "alerts_count_trend": 5,
        "anomalies_count": 1,
        "anomalies_count_trend": -4,
        "bl_count": 1,
        "bl_count_trend": 2,
        "child_scs_count": 2,
        "local_sensor_count": 1,
        "all_scs_count": 1,
        "all_sensor_count": 3
    });
}
exports.response = response;
