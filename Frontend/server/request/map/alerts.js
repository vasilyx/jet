function response(request, response, config) {
    response.status(200).send({
        "0ad6198a-8981-11e7-b909-005056995547": {
            "name": "avm48.lpr.jet.msk.su",
            "type": "SENSOR",
            "target": null,
            "source_latitude": 55.7513,
            "source_longitude": 37.6212,
            "info": {
                "alerts_count": 5,
                "anomalies_count": 0,
                "bl_count": 0
            }
        },
        "895a0677-e697-419d-890d-aa82f87b7749": {
            "name": null,
            "type": null,
            "target": "0ad6198a-8981-11e7-b909-005056995547",
            "source_latitude": 38.87,
            "source_longitude": -77.0491,
            "info": null,
        },
        "89d2f0a6-8b69-4d9d-a26e-71bd0530d950": {
            "name": null,
            "type": "Сигнатура Bro",
            "target": "0ad6198a-8981-11e7-b909-005056995547",
            "source_latitude": 40.7207,
            "source_longitude": -73.9394,
            "info": null
        },
        "84b19485-4ae5-4f24-b608-d84313d460b8": {
            "name": null,
            "type": "Аномалия сетевой активности",
            "target": "0ad6198a-8981-11e7-b909-005056995547",
            "source_latitude": 39.0224,
            "source_longitude": 125.7849,
            "info": null
        },
        "0c24ec7d-a485-4588-81da-b711c1c3816c": {
            "name": null,
            "type": "Чёрный список",
            "target": "0ad6198a-8981-11e7-b909-005056995547",
            "source_latitude": 39.9258,
            "source_longitude": 116.3727,
            "info": null
        },
        "0c24ec7d-a485-4588-81da-b711c24c3816c": {
            "name": null,
            "type": "Чёрный список",
            "target": "0ad6198a-8981-11e7-b909-005056995547",
            "source_latitude": 22.9258,
            "source_longitude": 14.3727,
            "info": null
        }
    });
}
exports.response = response;
