function response(request, response, config) {
    response.status(200).send({
        "total": "1",
        "rows": [
            {"id": "1", "label": "СУС"},
            {"id": "2", "label": "Сенсор"},
            {"id": "3", "label": "Хост"},
            {"id": "4", "label": "Сервис"},
            {"id": "5", "label": "Подсистема"},
            {"id": "6", "label": "Файл"}
        ]
    });
}
exports.response = response;