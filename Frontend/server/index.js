var express = require('express'),
    httpProxy = require('http-proxy'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    config = require('./config.json'),
    routes = require('./routes.js'),
    app = express(),
    checkUrl= function(url) {
        var i, ln, route, pattern, script;

        url = url.replace('/backend', '');

        for (i = 0, ln = routes.length; i < ln; i++) {
            route = routes[i];
            pattern = new RegExp('^' + route.pattern.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$', 'i');
            if (pattern.test(url)) {
                try {
                    script = './request' + route.script + '.js';
                    require.resolve(script);
                    return script;
                } catch (e) {

                }
                return false;
            }
        }
        return false;
    };
// Serving static files
app.use('/', express.static(config.env.static));

if (config.env.proxy.enable && config.env.mock.enable) {
    var proxyTarget = config.env.proxy.target,
        proxy = httpProxy.createProxyServer({secure: false});

    proxy.on('proxyReq', function (proxyReq, req, res, options) {
        var authHeader = req.headers['authorization'];
        if (authHeader) {
            proxyReq.setHeader('Authorization', authHeader);
        }
    });

    app.all(config.env.proxy.path, function (req, res) {
        var url = req.url,
            script = checkUrl(url);

        if (script) {
            console.log('[LOCAL] ' + url + ' -> ' + script);
            setTimeout(function () {
                require(script).response.call(this, req, res, config);
            }, config.env.mock.responseTimeout);
        } else {
            console.log('[PROXY] ' + url + ' -> '+ proxyTarget + url);
            proxy.web(req, res, {
                target: proxyTarget,
                timeout: 180000
            });
        }
    });

} else if (config.env.proxy.enable) {
    var proxyTarget = config.env.proxy.target,
        proxyPath = config.env.proxy.path,
        proxy = httpProxy.createProxyServer({secure: false});

    proxy.on('proxyReq', function (proxyReq, req, res, options) {
        var authHeader = req.headers['authorization'];
        if (authHeader) {
            proxyReq.setHeader('Authorization', authHeader);
        }
    });

    app.all(proxyPath, function (request, response) {
        proxy.web(request, response, {
            target: proxyTarget,
            timeout: 180000
        });
    });
    console.log('Proxing all requests matching ' + proxyPath + ' to ' + proxyTarget);
} else if (config.env.mock.enable) {
    app.use(bodyParser.json());
    for (var route in routes) {
        (function () {
            var routeIndex = route,
                routePattern = routes[routeIndex].pattern,
                routeScript = './request' + routes[routeIndex].script + '.js';
            console.log(routeIndex + "\t" + routePattern + ' -> ' + routeScript);
            app.all(new RegExp('^/(backend' + routePattern + ')$', 'i'), function (req, res) {
                console.log('route #' + routeIndex + ": " + routePattern + ' -> ' + routeScript);
                setTimeout(function () {
                    try {
                        require(routeScript).response.call(this, req, res, config);
                    } catch (e) {
                        if (req.params[0] == 'favicon.ico') {
                            res.status(200).end();
                        } else {
                            res.status(500).send({
                                success: false,
                                message: "Ошибка выполнения запроса: " + e
                            });
                        }
                    }
                }, config.env.mock.responseTimeout);
            });
        })();
    }
}

var port = config.app.port || 8080;
app.listen(port);
console.log(
    "Сервер запущен на порту: " + port + "\n" +
    "Приложение доступно по адресу: http://localhost:" + port + config.app.path
);