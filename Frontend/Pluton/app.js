/**
 *
 */
Ext.application({
    extend: 'Pluton.Application',

    name: 'Pluton',

    requires: [
        'Pluton.*'
    ]
});
