/**
 * Added the ability to enable a cleaning trigger
 */
Ext.define('Pluton.overrides.ux.form.ItemSelector', {
    override: 'Ext.ux.form.ItemSelector',

    buttons: ['top', 'up', 'addall', 'add', 'remove', 'removeall', 'down', 'bottom'],

    /**
     * @cfg {Object} buttonsText The tooltips for the {@link #buttons}.
     * Labels for buttons.
     */
    buttonsText: {
        top: "Move to Top",
        up: "Move Up",
        addall: "Add all values to Selected",
        add: "Add to Selected",
        remove: "Remove all values from Selected",
        removeall: "Remove from Selected",
        down: "Move Down",
        bottom: "Move to Bottom"
    },

    onAddallBtnClick: function () {
        this.moveRec(true, this.fromField.store.getRange());
    },

    onRemoveallBtnClick: function () {
        this.moveRec(false, this.toField.store.getRange());
    }
});
