/**
 * Добавляет возможность использования подсказок в ячейках
 * (+) {Boolean} [showTips=true] - Отображение подсказок для ячеек таблицы
 * (+) {Boolean} [tipsOnlyForHiddenText=true] - Отображение подсказок только для ячеек, в которые не поместился текст
 * (+) {Boolean} [useCustomTip=false] - Отобразить кастомный тултип. Применимо к колонке таблицы
 */
Ext.define('Pluton.overrides.panel.Table', {
    override: 'Ext.panel.Table',

    requires: [
        'Ext.tip.ToolTip'
    ],

    showTips: true,
    tipsOnlyForHiddenText: true,

    initComponent: function() {
        this.callParent(arguments);

        if (this.showTips === true) {
            this.on({
                afterrender: 'onAfterTableRender',
                scope: this
            });
        }
    },

    onAfterTableRender: function() {
        var me = this,
            view = me.getView();

        Ext.create('Ext.tip.ToolTip', {
            target: view.getEl(),
            delegate: view.getCellSelector(),
            trackMouse: true,
            dismissDelay: 10000,
            listeners: {
                beforeshow: function(tip) {
                    var triggerElement = tip.triggerElement,
                        textContent = triggerElement.textContent,
                        column = me.getVisibleColumns()[triggerElement.cellIndex];

                    if (column.useCustomTip === true) {
                        return false;
                    }

                    if (textContent && textContent !== Ext.String.htmlDecode(column.emptyCellText)) {
                        if (!me.tipsOnlyForHiddenText || (me.tipsOnlyForHiddenText && Ext.get(triggerElement).getTextWidth() >= column.getWidth())) {
                            tip.update(textContent);
                            return true;
                        }
                    }

                    return false;
                },
                scope: me
            }
        });
        
        Ext.create('Ext.tip.ToolTip', {
            target: this.getHeaderContainer().getEl(),
            delegate: 'div.x-column-header-text',
            dismissDelay: 10000,
            listeners: {
                beforeshow: function(tip) {
                    var triggerElement = tip.triggerElement,
                        textInnerEl = triggerElement.firstChild,
                        textContent = triggerElement.textContent;

                    if (textContent && textInnerEl) {
                        if (!me.tipsOnlyForHiddenText || (textInnerEl.offsetWidth - triggerElement.offsetWidth > 5)) {
                            tip.update(textContent);
                            return true;
                        }
                    }

                    return false;
                }
            }
        });
    }
});
