/**
 *
 */
Ext.define('Pluton.overrides.LoadMask', {
    override: 'Ext.LoadMask',

    msg: '',

    renderTpl: [
        '<div id="{id}-msgWrapEl" data-ref="msgWrapEl" class="{[values.$comp.msgWrapCls]}" role="presentation">',
            '<div id="{id}-msgEl" data-ref="msgEl" class="{[values.$comp.msgCls]} ',
                Ext.baseCSSPrefix, 'mask-msg-inner {childElCls}" role="presentation">',
                '<div id="{id}-msgTextEl" data-ref="msgTextEl" class="',
                    Ext.baseCSSPrefix, 'mask-msg-text',
                    Ext.isIE ? (' ' + Ext.baseCSSPrefix + 'ie ') : '',
                    '{childElCls}" role="presentation">{msg}</div>',
            '</div>',
        '</div>'
    ]
});
