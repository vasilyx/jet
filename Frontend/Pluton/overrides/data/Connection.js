/**
 *
 */
Ext.define('Pluton.overrides.data.Connection', {
    override: 'Ext.data.Connection',

    /**
     * @override
     * Добавим префикс для ajax-запросов
     */
    setupUrl: function(options, url) {
        var url = this.callParent(arguments);

        return '/backend' + url;
    }
});
