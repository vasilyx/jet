/**
 * workaround
 * После отмены редактирования теряется context
 */
Ext.define('Pluton.overrides.grid.RowEditor', {
    override: 'Ext.grid.RowEditor',

    cancelEdit: function() {
        var me = this,
            form = me.getForm(),
            fields = form.getFields(),
            items = fields.items,
            length = items.length,
            i,
            context = me.context, //FIX
            record = context && context.record;

        if (me._cachedNode) {
            me.clearCache();
        }
        me.hide();

        if (record && record.phantom && !record.modified && me.removeUnmodified) {
            me.editingPlugin.grid.store.remove(record);
        }

        form.clearInvalid();

        for (i = 0; i < length; i++) {
            items[i].suspendEvents();
        }

        form.reset();

        for (i = 0; i < length; i++) {
            items[i].resumeEvents();
        }
    }
});
