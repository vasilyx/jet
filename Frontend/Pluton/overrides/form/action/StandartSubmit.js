/**
 *
 */
Ext.define('Pluton.overrides.form.action.StandartSubmit', {
    override: 'Ext.form.action.StandardSubmit',

    doSubmit: function() {
        var me = this,
            url = me.url || me.form.url;
        me.url = Ext.Ajax.setupUrl({}, url); // В Ext.data.Connection.setupUrl устанавливается префикс запросов
        me.callParent(arguments);
    }
});