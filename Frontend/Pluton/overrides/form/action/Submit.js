/**
 *
 */
Ext.define('Pluton.overrides.form.action.Submit', {
    override: 'Ext.form.action.Submit',

    requires: [
        'Ext.form.action.Action'
    ],

    // Remove processing of success property
    onSuccess: function(response) {
        var form = this.form,
            formActive = form && !form.destroying && !form.destroyed,
            success = true,
            result = this.processResponse(response);

        if (result === true) {
            if (result.errors && formActive) {
                form.markInvalid(result.errors);
            }
            this.failureType = Ext.form.action.Action.SERVER_INVALID;
            success = false;
        }

        if (formActive) {
            form.afterAction(this, success);
        }
    }
});