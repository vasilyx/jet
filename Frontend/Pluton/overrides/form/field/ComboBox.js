/**
 * Added the ability to enable a cleaning trigger
 */
Ext.define('Pluton.overrides.form.field.ComboBox', {
    override: 'Ext.form.field.ComboBox',

    requires: [
        'Pluton.ux.form.trigger.ClearTrigger'
    ],

    clearTrigger: false,

    constructor: function(config) {
        if (config.clearTrigger) {
            config.triggers = Ext.applyIf(config.triggers || {}, {
                clear: {
                    type: 'clear',
                    weight: -1
                }
            });
        }

        this.callParent(arguments);
    },

    /**
     * @override
     */
    checkValueOnChange: Ext.emptyFn,

    /**
     * @override
     */
    setValue: function(value) {
        var me = this,
            bind, valueBind;

        if (me.hasFocus) {
            bind = me.getBind();
            valueBind = bind && bind.value;
            if (valueBind && valueBind.syncing) {
                if ((Ext.isEmpty(value) && Ext.isEmpty(me.value)) || value === me.value) {
                    return me;
                } else if (Ext.isArray(value) && Ext.isArray(me.value) && Ext.Array.equals(value, me.value)) {
                    return me;
                }
            }
        }

        if (value != null) {
            me.doSetValue(value);
        } else {
            me.suspendEvent('select');
            me.valueCollection.beginUpdate();
            me.pickerSelectionModel.deselectAll();
            me.valueCollection.endUpdate();
            me.resumeEvent('select');
        }

        return me;
    },

    /**
     * @override
     */
    doSetValue: function(value, add) {
        var me = this,
            store = me.getStore(),
            Model = store.getModel(),
            matchedRecords = [],
            autoLoadOnValue = me.autoLoadOnValue,
            isLoaded = store.getCount() > 0 || store.isLoaded(),
            pendingLoad = store.hasPendingLoad(),
            unloaded = autoLoadOnValue && !isLoaded && !pendingLoad,
            multiSelect = me.multiSelect,
            forceSelection = me.forceSelection,
            selModel = me.pickerSelectionModel,
            displayIsValue = me.displayField === me.valueField,
            isEmptyStore = store.isEmptyStore,
            lastSelection = me.lastSelection,
            i, len, record, dataObj,
            valueChanged, key;

        //<debug>
        if (add && !multiSelect) {
            Ext.raise('Cannot add values to non multiSelect ComboBox');
        }
        //</debug>

        if (value && !value.isModel) {
            if (multiSelect) {
                value = Ext.Array.map(Ext.Array.from(value), function(v) {
                    return v.isModel ? v : Ext.isObject(v) ? new Model(v) : v;
                });
            } else {
                if (Ext.isObject(value)) {
                    value = new Model(value);
                } else if (Ext.isArray(value)) {
                    value = value[0];
                }
            }
        }

        if (pendingLoad || unloaded || !isLoaded || isEmptyStore) {
            if (!value.isModel && !multiSelect) {
                if (add) {
                    me.value = Ext.Array.from(me.value).concat(value);
                } else {
                    me.value = value;
                }

                me.setHiddenValue(me.value);
                me.setRawValue(displayIsValue ? value : '');
            }

            if (unloaded && !isEmptyStore) {
                store.load();
            }

            if ((!value.isModel && !multiSelect) || isEmptyStore) {
                return me;
            }
        }

        value = add ? Ext.Array.from(me.value).concat(value) : Ext.Array.from(value);

        for (i = 0, len = value.length; i < len; i++) {
            record = value[i];
            if (!record || !record.isModel) {
                record = me.findRecordByValue(key = record);
                if (!record) {
                    record = me.valueCollection.find(me.valueField, key);
                }
            }

            if (!record) {
                if (!forceSelection) {
                    if (!record && value[i]) {
                        dataObj = {};
                        dataObj[me.displayField] = value[i];
                        if (me.valueField && me.displayField !== me.valueField) {
                            dataObj[me.valueField] = value[i];
                        }
                        record = new Model(dataObj);
                    }
                } else if (me.valueNotFoundRecord) {
                    record = me.valueNotFoundRecord;
                }
            }

            if (record) {
                matchedRecords.push(record);
            }
        }

        if (lastSelection) {
            len = lastSelection.length;
            if (len === matchedRecords.length) {
                for (i = 0; !valueChanged && i < len; i++) {
                    if (Ext.Array.indexOf(me.lastSelection, matchedRecords[i]) === -1) {
                        valueChanged = true;
                    }
                }
            } else {
                valueChanged = true;
            }
        } else {
            valueChanged = matchedRecords.length;
        }

        if (valueChanged) {
            me.suspendEvent('select');
            me.valueCollection.beginUpdate();
            if (matchedRecords.length) {
                selModel.select(matchedRecords, false);
            } else {
                selModel.deselectAll();
            }
            me.valueCollection.endUpdate();
            me.resumeEvent('select');
        } else {
            me.updateValue();
        }

        return me;
    },

    /**
     * @override
     */
    assertValue: function() {
        var me = this,
            value = me.getRawValue(),
            displayValue = me.getDisplayValue(),
            lastRecords = me.lastSelectedRecords,
            rec;

        if (me.forceSelection) {
            if (me.multiSelect) {
                // For multiselect, check that the current displayed value matches the current
                // selection, if it does not then revert to the most recent selection.
                if (value !== displayValue) {
                    me.setRawValue(displayValue);
                }
            } else {
                rec = me.findRecordByDisplay(value);
                if (rec) {
                    if (me.getDisplayValue([me.getRecordDisplayData(rec)]) !== displayValue) {
                        me.select(rec, true);
                    }
                } else if (lastRecords && (!me.allowBlank || me.rawValue)) {
                    me.setValue(lastRecords);
                } else {
                    if (lastRecords) {
                        delete me.lastSelectedRecords;
                    }
                    me.setRawValue('');
                }
            }

            me.validate();
        }
        me.collapse();
    }
});
