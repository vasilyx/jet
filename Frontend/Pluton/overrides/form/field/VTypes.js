Ext.define('Pluton.overrides.form.field.VTypes', {
    override: 'Ext.form.field.VTypes',

    password: function(value, field) {
        var passFieldName = field.passFieldName;

        if (passFieldName) {
            var passField = field.up('form').down('[name=' + passFieldName + ']');

            return (value == passField.getValue());
        }

        return true;
    },

    passwordText: 'Пароли должны совпадать',

    login: function(value, field) {
            var loginRegExp = /^[A-Za-z][A-Za-z0-9_.@-]{3,31}$/;
            return loginRegExp.test(value);
    },

    loginText: 'Поле может содержать строчные либо заглавные латинские буквы, цифры, а также символы тире, нижнего подчеркивания, точки и "@", начинаться с буквы и не быть длиннее 32-х и короче 4-х символов',

    subnet: function(value, field) {
        var loginRegExp = /^(23[0-9]|2[0-3][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\/(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        value = value.replace(/\s+/g, '');
        var arrValues = value.split(',');

        for (var i = 0; i < arrValues.length; i++){
            if (!loginRegExp.test(arrValues[i])) {
                return false;
            }
        }
        return true
    },

    subnetText: 'Ошибочный формат. Ожидается 123.123.123.123/32',

    ipAddress: function(value) {
        var ipAddressRegExp = /^(23[0-9]|2[0-3][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

        return ipAddressRegExp.test(value);
    },

    ipAddressText: 'Неверный формат ввода IP-адреса',

    coordinates: function(value) {
        var coordinatesRegExp = /^[0-9]{1,2}\.[0-9]{4,8}$/;//координаты в формате десятичных градусов

        return coordinatesRegExp.test(value);
    },

    coordinatesText: 'Неверный формат ввода. Введите координаты в десятичных градусах, ожидается 40.10001000',

    macAddress: function (value) {
        var MacRegExp = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/;
        return MacRegExp.test(value);
    },
    macAddressText: 'Неверный формат ввода. Ожидается ввод в следующем формате: 3D:F2:C9:A6:B3:4F или 3D-F2-C9-A6-B3-4F'
});
