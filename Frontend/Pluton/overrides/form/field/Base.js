/**
 *
 */
Ext.define('Pluton.overrides.form.field.Base', {
    override: 'Ext.form.field.Base',

    /**
     * @override
     * Публикуем все значения во viewModel (иначе невалидные не публикуются)
     */
    publishValue: function() {
        var me = this;

        if (me.rendered/* && !me.getErrors().length*/) {
            me.publishState('value', me.getValue());
        }
    },

    /**
     * @override
     * Show asterisk in label for required fields
     */
    initLabelable: function () {
        this.callParent(arguments);

        if (this.fieldLabel && (this.required === true || this.allowBlank === false)) {
            this.labelSeparator += '<span style="color:#D32F2F;">*</span>';
        }
    }
});
