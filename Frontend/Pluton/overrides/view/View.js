/**
 * Изменяем значения по-умолчанию:
 * [loadingText=''] - Не отображаем текст загрузки (только анимация)
 */
Ext.define('Pluton.overrides.view.View', {
    override: 'Ext.view.View',

    loadingText: ''
});
