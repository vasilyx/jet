/**
 * Изменяем значения по-умолчанию:
 * [enableTextSelection=true] - Позволяем выделять содержимое ячеек
 */
Ext.define('Pluton.overrides.view.Table', {
    override: 'Ext.view.Table',

    enableTextSelection: true
});
