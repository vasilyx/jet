/**
 * Перевод непереведенных в ExtJS текстов на русский язык
 */
Ext.define('Pluton.locale.ru.grid.RowEditor', {
    override: 'Ext.grid.RowEditor',

    saveBtnText: 'Применить',
    cancelBtnText: 'Отменить'
});

Ext.define('Pluton.locale.ru.panel.Panel', {
    override: 'Ext.panel.Panel',

    collapseToolText: 'Свернуть',
    expandToolText: 'Развернуть'
});

Ext.define('Pluton.locale.ru.window.Window', {
    override: 'Ext.window.Window',

    closeToolText: 'Закрыть'
});

(function () {
    Ext.Date.defaultFormat = 'd.m.Y H:i:s'
})();