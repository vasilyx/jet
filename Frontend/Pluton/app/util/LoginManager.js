/**
 *
 */
Ext.define('Pluton.util.LoginManager', {
    alternateClassName: 'LoginManager',

    singleton: true,

    mixins: [
        'Ext.mixin.Observable'
    ],

    constructor: function () {
        this.mixins.observable.constructor.call(this);
    },

    login: function () {
        var me = this;

        Ext.Ajax.request({
            url: '/get_userinfo',
            method: 'POST'
        }).then({
            success: function (response) {
                me.fireEvent('loginsuccess', Ext.JSON.decode(response.responseText, true));
            },
            failure: function (response) {
                me.fireEvent('loginfailure', response);
            }
        })
    },
    
    logout: function (preventLoading) {
        var reloadPage = function () {
                window.location.reload();
            },
            getUserInfo = function () {
                Ext.Ajax.request({
                    url: '/get_userinfo',
                    method: 'POST',
                    username: 'iD2quahpotaitooxauveichoocuhoori', // любой фиктивный логин
                    password: 'out' // и пароль
                }).then(reloadPage, reloadPage);
            };

        if (preventLoading !== true) {
            Pluton.getApplication().getMainView().setLoading();
        }

        Ext.Ajax.request({
            url: '/logout',
            method: 'POST'
        }).then(getUserInfo, getUserInfo);
    }
});
