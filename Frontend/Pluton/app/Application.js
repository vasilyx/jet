Ext.enableAria = false;
Ext.enableAriaButtons = false;
Ext.enableAriaPanels = false;

Ext.define('Pluton.Application', {
    extend: 'Ext.app.Application',

    name: 'Pluton',

    controllers: [
        'Head'
    ],

    launch: function () {
        Ext.Date.defaultFormat = 'd.m.Y H:i:s';
    }
}, function () {
    Ext.labelRenderer = function (value) {
        var parseValue = function (v) {
            if (Ext.isObject(v)) {
                return v.label;
            } else {
                return v;
            }
        };

        if (Ext.isArray(value)) {
            return Ext.Array.map(value, parseValue).join(', ');
        } else {
            return parseValue(value);
        }
    };
});
