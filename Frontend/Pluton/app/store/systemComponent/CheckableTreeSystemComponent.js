/**
 *
 */
Ext.define('Pluton.store.systemComponent.CheckableTreeSystemComponent', {
    extend: 'Pluton.store.systemComponent.SystemComponentBrief',

    alias: 'store.checkable-tree-system-component',

    requires: [
        'Pluton.model.systemComponent.CheckableTreeSystemComponent'
    ],

    model: 'Pluton.model.systemComponent.CheckableTreeSystemComponent'
});
