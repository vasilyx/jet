/**
 *
 */
Ext.define('Pluton.store.systemComponent.SystemComponentBrief', {
    extend: 'Ext.data.TreeStore',

    alias: 'store.system-component-brief',

    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.systemComponent.SystemComponent'
    ],

    model: 'Pluton.model.systemComponent.SystemComponent',
    parentIdProperty: 'parent_id',
    remoteFilter: true,

    proxy: {
        type: 'json-ajax',
        url: '/system_component/brief',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            transform: function(rawData) {
                var ExtArrayFindBy = Ext.Array.findBy,
                    rootProperty = this.getRootProperty(),
                    data = Ext.Array.from(rawData[rootProperty]),
                    pureData = [];

                if (!Ext.isEmpty(data)) {
                    data = Ext.Array.map(data, function(item, index, array) {
                        if (!ExtArrayFindBy(array, function (i) {return item.id === i.parent_id})) {
                            item.leaf = true;
                        }
                        return item;
                    });

                    // for data with broken relations
                    Ext.each(data, function (item) {
                        if (item.parent_id) {
                            if (ExtArrayFindBy(data, function (i) {return item.parent_id === i.id})) {
                                pureData.push(item);
                            }
                        } else {
                            pureData.push(item);
                        }
                    });

                    rawData[rootProperty] = pureData;
                }

                return rawData;
            }
        }
    }
});
