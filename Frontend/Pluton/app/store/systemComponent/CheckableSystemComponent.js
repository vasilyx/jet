/**
 *
 */
Ext.define('Pluton.store.systemComponent.CheckableSystemComponent', {
    extend: 'Pluton.store.systemComponent.SystemComponentBrief',

    alias: 'store.checkable-system-component',

    requires: [
        'Pluton.model.systemComponent.CheckableSystemComponent'
    ],

    model: 'Pluton.model.systemComponent.CheckableSystemComponent'
});
