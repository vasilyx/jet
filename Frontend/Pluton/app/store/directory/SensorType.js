/**
 *
 */
Ext.define('Pluton.store.directory.SensorType', {
    extend: 'Ext.data.Store',
    alias: 'store.sensorType',
    storeId: 'sensorType',
    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.voc.Label'
    ],
    model: 'Pluton.model.voc.Label',

    remoteSort: true,
    sorters: 'label',
    autoLoad: true,
    proxy: {
        type: 'json-ajax',
        url: '/sensor/types',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
});
