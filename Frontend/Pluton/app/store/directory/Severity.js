/**
 *
 */
Ext.define('Pluton.store.directory.Severity', {
    extend: 'Ext.data.Store',
    alias: 'store.severity',
    storeId: 'severity',
    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.voc.Label'
    ],
    model: 'Pluton.model.voc.Label',

    remoteSort: true,
    sorters: 'label',
    autoLoad: true,
    proxy: {
        type: 'json-ajax',
        url: '/severity',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
});
