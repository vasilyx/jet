/**
 *
 */
Ext.define('Pluton.store.directory.UserRole', {
    extend: 'Ext.data.Store',
    alias: 'store.userRole',
    storeId: 'userRole',
    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.voc.Label'
    ],
    model: 'Pluton.model.voc.Label',

    remoteSort: true,
    sorters: 'label',
    autoLoad: true,
    pageSize: 0,
    proxy: {
        type: 'json-ajax',
        url: '/user_role/list',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
});
