/**
 *
 */
Ext.define('Pluton.store.directory.Sensor', {
    extend: 'Ext.data.Store',
    alias: 'store.sensor',
    storeId: 'sensor',
    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.voc.Label'
    ],
    model: 'Pluton.model.voc.Label',

    remoteSort: true,
    sorters: 'label',
    autoLoad: true,
    proxy: {
        type: 'json-ajax',
        url: '/sensor/list',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
});
