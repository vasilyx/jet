/**
 *
 */
Ext.define('Pluton.store.directory.Application', {
    extend: 'Ext.data.Store',
    alias: 'store.application',
    storeId: 'application',
    requires: [
        'Pluton.data.proxy.JsonAjax'
    ],

    fields: [
        'id',
        'label',
        'version'
    ],

    remoteSort: true,
    sorters: 'label',
    autoLoad: true,
    proxy: {
        type: 'json-ajax',
        url: '/application/list',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
});
