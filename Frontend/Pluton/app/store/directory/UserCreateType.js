/**
 *
 */
Ext.define('Pluton.store.directory.UserCreateType', {
    extend: 'Ext.data.Store',
    alias: 'store.userCreateType',
    storeId: 'userCreateType',
    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.voc.Label'
    ],
    model: 'Pluton.model.voc.Label',

    remoteSort: true,
    sorters: 'label',
    autoLoad: true,
    pageSize: 0,
    proxy: {
        type: 'json-ajax',
        url: '/user_create_type/list',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
});
