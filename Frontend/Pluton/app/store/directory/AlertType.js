/**
 *
 */
Ext.define('Pluton.store.directory.AlertType', {
    extend: 'Ext.data.Store',
    alias: 'store.alertType',
    storeId: 'alertType',
    requires: [
        'Pluton.data.proxy.JsonAjax'
    ],

    fields: [
        'id',
        'alert_type',
        'label'
    ],

    remoteSort: true,
    sorters: 'label',
    autoLoad: true,
    proxy: {
        type: 'json-ajax',
        url: '/alert/types',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
});
