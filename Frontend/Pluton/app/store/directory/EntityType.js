/**
 *
 */
Ext.define('Pluton.store.directory.EntityType', {
    extend: 'Ext.data.Store',
    alias: 'store.entityType',
    storeId: 'entityType',
    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.voc.Label'
    ],
    model: 'Pluton.model.voc.Label',

    remoteSort: true,
    sorters: 'label',
    autoLoad: true,
    proxy: {
        type: 'json-ajax',
        url: '/entity_type/list',
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
});
