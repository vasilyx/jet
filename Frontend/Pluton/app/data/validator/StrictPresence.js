/**
 *
 */
Ext.define('Pluton.data.validator.StrictPresence', {
    extend: 'Ext.data.validator.Validator',

    alias: 'data.validator.strict-presence',

    type: 'strict-presence',

    isStrictPresence: true,

    config: {
        message: 'Это поле обязательно для заполнения'
    },

    validate: function(value) {
        if (value && Ext.isString(value)) {
            value = Ext.String.trim(value);
        }

        return !Ext.isEmpty(value) ? true : this.getMessage();
    }
});
