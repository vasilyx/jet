/**
 *
 */
Ext.define('Pluton.data.proxy.JsonAjax', {
    extend:'Ext.data.proxy.Ajax',

    alias:'proxy.json-ajax',

    paramsAsJson: true,
    limitParam: 'page_size',
    filterParam: 'filters',
    startParam: undefined,
    noCache: false,
    
    actionMethods: {
        create: 'POST',
        read: 'POST',
        update: 'POST',
        destroy: 'POST'
    },
    
    buildRequest: function(operation) {
        var request = this.callParent(arguments),
            filters = request._params.filters || [],
            i;
       
        for (i = 0; i < filters.length; i += 1) {
            filters[i] = {
                field: filters[i].property,
                type: filters[i].operator || '=',
                value: filters[i].value
            }
        }
        
        return request;
    },
    
    applyEncoding: function(value) {
        return value;
    }
});