/**
 *
 */
Ext.define('Pluton.ux.container.DateTimeRangeContainer', {
    extend: 'Ext.container.Container',

    xtype: 'date-time-range-container',

    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.layout.container.HBox'
    ],

    mixins: ['Ext.form.field.Field'],

    layout: 'hbox',

    config: {
        fromFieldConfig: {},
        toFieldConfig: {}
    },
    lastFromDate: null,

    valueAsObject: false,
    submitFormat: 'd-m-Y H:i:s',

    initComponent: function() {
        var me = this,
            today = new Date(),
            dtStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0),
            dtEnd = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 23, 59, 59,0),
            fromField = me.fromField = Ext.create('Ext.form.field.Date', Ext.apply({
                reference: 'timestampFrom',
                isFormField: false,
                format: 'd.m.Y H:i:s.u',
                altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                flex: 1,
                value: dtStart
            }, me.getFromFieldConfig())),
            toField = me.toField = Ext.create('Ext.form.field.Date', Ext.apply({
                reference: 'timestampTo',
                isFormField: false,
                fieldLabel: '&nbsp',
                format: 'd.m.Y H:i:s.u',
                altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                labelSeparator: null,
                flex: 1,
                value: dtEnd
            }, me.getToFieldConfig()));

        me.items = [
            fromField,
            {
                xtype: 'displayfield',
                fieldLabel: '&nbsp',
                labelSeparator: null,
                margin: '0 5 0 5',
                value: ' - '
            },
            toField
        ];

        me.callParent(arguments);
        me.initField();
    },

    initEvents: function() {
        var me = this;

        me.callParent();

        me.mon(me.fromField, {
            change: 'onFromFieldChange',
            scope: me
        });
        me.mon(me.toField, {
            change: 'onToFieldChange',
            scope: me
        });
        me.mon(me.fromField, {
            select: 'onFromFieldSelect',
            scope: me
        });

        me.mon(me.toField, {
            select: 'onToFieldSelect',
            scope: me
        });
    },

    onFromFieldSelect: function( field,q, eOpts ){
        field.fireEvent('change', field);
    },
    onToFieldSelect: function( field,q, eOpts ){
        field.fireEvent('change', field);
    },

    onFromFieldChange: function(field, newValue, oldValue) {
        var me = this,
            select,
            toField = me.toField;

        if (!oldValue){
            select = true;
            oldValue = me.lastFromDate;
        }

        me.lastFromDate = field.lastDate;
        if (Ext.isDate(newValue)) {
            if (Ext.isDate(oldValue)) {
                newValue.setHours(oldValue.getHours(), oldValue.getMinutes(), oldValue.getSeconds(), (select) ? oldValue.getMilliseconds() : newValue.getMilliseconds());
                field.setValue(newValue);
            }

        }
        me.checkChange();
    },

    onToFieldChange: function(field, newValue, oldValue) {
        var me = this,
            select,
            fromField = me.fromField;

        if (!oldValue){
            oldValue = me.lastToDate;
            select = true;
        }

        me.lastToDate = field.lastDate;
        if (Ext.isDate(newValue)) {
            if (Ext.isDate(oldValue)){
                newValue.setHours(oldValue.getHours(),oldValue.getMinutes(), oldValue.getSeconds(),(select)?oldValue.getMilliseconds():newValue.getMilliseconds());
                field.setValue(newValue);
            }

        }
        me.checkChange();
    },

    getValue: function() {
        var me = this,
            submitFormat = me.submitFormat,
            fromFieldValue = me.fromField.getValue(),
            toFieldValue = me.toField.getValue(),
            isValidValues = true;

        if (fromFieldValue === null && toFieldValue === null) {
            return null;
        }

        if ((fromFieldValue !== null && !Ext.isDate(fromFieldValue)) || (toFieldValue !== null && !Ext.isDate(toFieldValue))) {
            isValidValues = false;
        }

        if (isValidValues && submitFormat) {
            fromFieldValue = Ext.Date.format(fromFieldValue, submitFormat) || null;
            toFieldValue = Ext.Date.format(toFieldValue, submitFormat) || null;
        }

        if (me.valueAsObject) {
            return {
                from: fromFieldValue,
                to: toFieldValue
            };
        } else {
            return [fromFieldValue, toFieldValue];
        }
    },

    setValue: function(value) {
        var me = this,
            fromField = me.fromField,
            toField = me.toField,
            fromValue, toValue;

        me.value = value = (value || null);

        if (me.valueAsObject) {
            if (!value || !Ext.isObject(value)) {
                fromValue = toValue = null;
            } else {
                fromValue = value.from;
                toValue = value.to;
            }
        } else {
            value = Ext.Array.from(value);
            fromValue = value[0];
            toValue = value[1];
        }

        fromField.setValue(fromValue);
        toField.setValue(toValue);

        me.checkChange();

        return me;
    },

    isEqual: function(value1, value2) {
        var toType = function(v) {
            return ({}).toString.call(v).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
        };

        if (toType(value1) !== toType(value2)) {
            return false;
        }

        if (Ext.isArray(value1) && Ext.isArray(value2)) {
            return Ext.Array.equals(value1, value2);
        }

        if (Ext.isObject(value1) && Ext.isObject(value2)) {
            return Ext.Object.equals(value1, value2);
        }

        return this.mixins.field.isEqual.call(this, value1, value2);
    },

    isValid: function() {
        return this.fromField.isValid() && this.toField.isValid();
    },

    getSubmitData: function() {
        var me = this,
            data = null;

        if (!me.disabled && me.submitValue) {
            data = {};
            data[me.getName()] = me.getValue();
        }

        return data;
    },
    onDisable: function(a){
        var me = this;
        me.fromField.setDisabled(true);
        me.toField.setDisabled(true);
    },

    onEnable: function(a){
        var me = this;
        me.fromField.setDisabled(false);
        me.toField.setDisabled(false);
    }
});
