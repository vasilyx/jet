/**
 *
 */
Ext.define('Pluton.ux.container.DateRangeContainer', {
    extend: 'Ext.container.Container',

    xtype: 'date-range-container',

    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.layout.container.HBox'
    ],

    mixins: ['Ext.form.field.Field'],

    layout: 'hbox',

    config: {
        fromFieldConfig: {},
        toFieldConfig: {}
    },

    valueAsObject: false,
    submitFormat: Ext.Date.defaultFormat,

    initComponent: function() {
        var me = this,
            fromField = me.fromField = Ext.create('Ext.form.field.Date', Ext.apply({
                isFormField: false,
                emptyText: 'с',
                format: 'd.m.Y',
                altFormats: 'd|d.|d.m|d.m.',
                flex: 1
            }, me.getFromFieldConfig())),
            toField = me.toField = Ext.create('Ext.form.field.Date', Ext.apply({
                isFormField: false,
                fieldLabel: '&nbsp',
                emptyText: 'по',
                format: 'd.m.Y',
                altFormats: 'd|d.|d.m|d.m.',
                labelSeparator: null,
                flex: 1
            }, me.getToFieldConfig()));

        me.items = [
            fromField,
            {
                xtype: 'displayfield',
                fieldLabel: '&nbsp',
                labelSeparator: null,
                margin: '0 5 0 5',
                value: ' - '
            },
            toField
        ];

        me.callParent(arguments);
        me.initField();
    },

    initEvents: function() {
        var me = this;

        me.callParent();

        me.mon(me.fromField, {
            change: 'onFromFieldChange',
            scope: me
        });
        me.mon(me.toField, {
            change: 'onToFieldChange',
            scope: me
        });
    },

    onFromFieldChange: function(field, newValue, oldValue) {
        var me = this,
            toField = me.toField;

        if (Ext.isDate(newValue)) {
            toField.setMinValue(Ext.Date.add(newValue, Ext.Date.DAY, 1));
        } else {
            toField.setMinValue(null);
        }

        me.checkChange();
    },

    onToFieldChange: function(field, newValue, oldValue) {
        var me = this,
            fromField = me.fromField;

        if (Ext.isDate(newValue)) {
            fromField.setMaxValue(Ext.Date.add(newValue, Ext.Date.DAY, -1));
        } else {
            fromField.setMaxValue(null);
        }

        me.checkChange();
    },

    getValue: function() {
        var me = this,
            submitFormat = me.submitFormat,
            fromFieldValue = me.fromField.getValue(),
            toFieldValue = me.toField.getValue(),
            isValidValues = true;

        if (fromFieldValue === null && toFieldValue === null) {
            return null;
        }

        if ((fromFieldValue !== null && !Ext.isDate(fromFieldValue)) || (toFieldValue !== null && !Ext.isDate(toFieldValue))) {
            isValidValues = false;
        }

        if (isValidValues && submitFormat) {
            fromFieldValue = Ext.Date.format(fromFieldValue, submitFormat) || null;
            toFieldValue = Ext.Date.format(toFieldValue, submitFormat) || null;
        }

        if (me.valueAsObject) {
            return {
                from: fromFieldValue,
                to: toFieldValue
            };
        } else {
            return [fromFieldValue, toFieldValue];
        }
    },

    setValue: function(value) {
        var me = this,
            fromField = me.fromField,
            toField = me.toField,
            fromValue, toValue;

        me.value = value = (value || null);

        if (me.valueAsObject) {
            if (!value || !Ext.isObject(value)) {
                fromValue = toValue = null;
            } else {
                fromValue = value.from;
                toValue = value.to;
            }
        } else {
            value = Ext.Array.from(value);
            fromValue = value[0];
            toValue = value[1];
        }

        fromField.setValue(fromValue);
        toField.setValue(toValue);

        me.checkChange();

        return me;
    },

    isEqual: function(value1, value2) {
        var toType = function(v) {
            return ({}).toString.call(v).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
        };

        if (toType(value1) !== toType(value2)) {
            return false;
        }

        if (Ext.isArray(value1) && Ext.isArray(value2)) {
            return Ext.Array.equals(value1, value2);
        }

        if (Ext.isObject(value1) && Ext.isObject(value2)) {
            return Ext.Object.equals(value1, value2);
        }

        return this.mixins.field.isEqual.call(this, value1, value2);
    },

    isValid: function() {
        return this.fromField.isValid() && this.toField.isValid();
    },

    getSubmitData: function() {
        var me = this,
            data = null;

        if (!me.disabled && me.submitValue) {
            data = {};
            data[me.getName()] = me.getValue();
        }

        return data;
    }
});
