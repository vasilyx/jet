/**
 *
 */
Ext.define('Pluton.ux.window.UploadWindow', {
    extend: 'Ext.window.Window',

    xtype: 'upload-window',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.File',
        'Ext.layout.container.Fit',
        'Ext.layout.container.Form'
    ],

    viewModel: {
        data: {
            isValid: false
        }
    },

    title: 'Загрузка файла',
    modal: true,
    constrain: true,
    minHeight: 200,
    minWidth: 350,
    width: 600,
    layout: 'fit',
    defaultListenerScope: true,
    referenceHolder: true,

    config: {
        waitMsg: 'Загрузка файла на сервер...',
        url: null, // Address to upload the file
        accept: null, // Accepted MIME types
        params: null, // File upload options
        additionalFields: null // Additional form fields for submit with the file
    },

    bbar: [
        {
            text: 'Загрузить',
            ui: 'md-blue',
            bind: {
                disabled: '{!isValid}'
            },
            handler: 'onUpload'
        },
        {
            text: 'Отменить',
            handler: 'onCancel'
        }
    ],

    initComponent: function() {
        this.items = [{
            reference: 'uploadForm',
            xtype: 'form',
            layout: 'form',
            url: this.getUrl(),
            bodyPadding: 10,
            listeners: {
                validitychange: 'onUploadFormValidityChange'
            },
            items: [
                {
                    name: 'file',
                    xtype: 'filefield',
                    fieldLabel: 'Файл',
                    required: true,
                    allowBlank: false,
                    accept: this.getAccept()
                }
            ].concat(Ext.Array.from(this.getAdditionalFields()))
        }];

        this.callParent(arguments);
    },

    onUpload: function() {
        var form = this.lookup('uploadForm');

        if (form.isValid()) {
            form.submit({
                waitMsg: this.getWaitMsg(),
                params: Ext.apply({}, this.getParams()),
                success: function(form, action) {
                    this.fireEvent('success', this, action.result);
                },
                failure: function(form, action) {
                    this.fireEvent('failure', this, action.result);
                },
                scope: this
            });
        }
    },

    onCancel: function() {
        this.close();
    },

    onUploadFormValidityChange: function(form, valid) {
        this.getViewModel().set('isValid', valid);
    }
});