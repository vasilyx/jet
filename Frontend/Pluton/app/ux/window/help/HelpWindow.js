/**
 *
 */
Ext.define('Pluton.ux.window.help.HelpWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'Pluton.ux.window.help.HelpWindowController'
    ],

    alternateClassName: 'Pluton.Help',

    singleton: true,

    controller: 'help-window',

    config: {
        // TODO активный раздел справки
        activePage: null
    },
    listeners: {
        beforeshow: 'onBeforeShow'
    },
    title: 'Справка',
    modal: true,
    constrain: true,
    scrollable: true,
    closeAction: 'hide',
    bodyPadding: 10,
    minHeight: 300,
    minWidth: 700,
    width: '70%',
    height: '60%',
    iconCls: 'x-fa fa-question',
    html: "Morbi lacinia interdum nulla penatibus amet nibh adipiscing semper ligula. Integer id auctor volutpat vulputate ipsum. Nam in faucibus augue. Nam ornare sapien libero accumsan consectetur non, bibendum purus urna lectus, vitae interdum lectus magna eget nulla. Ut vulputate enim maximus, nisi molestie eros in tellus. Etiam tellus quis est, auctor tincidunt est. Integer fermentum sem dapibus feugiat sem eu, tristique nunc. Donec imperdiet malesuada nulla, sit amet lobortis congue neque sed. Duis id dolor non nisl ullamcorper tempor. Vestibulum a nunc ex." +
        "<br><br>Sed id efficitur ut nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In purus quam, tristique vel sem id, interdum rutrum nunc. Aenean sit amet lectus lobortis, sem in lectus, consequat nulla. Quisque venenatis vestibulum sollicitudin. Cras cursus neque suscipit consequat metus, quis blandit purus ultrices. Suspendisse commodo erat eu imperdiet felis. Fusce purus nulla, egestas vel malesuada sit amet, rutrum sit amet magna. Suspendisse sagittis, tellus ipsum vitae ornare, libero risus venenatis mauris, vitae adipiscing odio turpis augue et. Nam vel viverra tellus." +
        "<br><br>Duis eu felis purus. Vivamus fringilla vitae mi id finibus. Nam ut diam ac velit eu tortor imperdiet aliquam ante. Morbi eget fringilla mi. Nunc odio nulla, viverra vel ligula quis, pellentesque dignissim tellus. Nunc ut dui ut turpis dapibus ultrices. Nam consectetur dignissim libero, vitae mollis nisl porttitor id. Maximus magna diam pellentesque, mauris magna scelerisque nec." +
        "<br><br>Aliquam convallis elit orci, malesuada sed ut Finibus nisl. Nunc malesuada iaculis ac, tristique ex rutrum diam, vel tincidunt magna. Duis tellus eros, placerat eget accumsan mollis, augue sed nisi. Suspendisse hendrerit lectus malesuada, pulvinar dolor condimentum id, iaculis est. Praesent elementum hendrerit elit porttitor purus in purus sodales sem. Aliquam fermentum velit dolor, sed viverra vehicula tortor dictum. Vivamus sit amet lectus hendrerit tempus libero, sed feugiat lorem quis. Quisque quis risus malesuada, volutpat lorem eu, sollicitudin arcu. Vestibulum at volutpat massa. Vivamus enim ac laoreet. Sed nec faucibus vulputate pharetra nulla quis sem vel urna. Donec dictum erat dolor, sollicitudin finibus leo rutrum tempor. Praesent volutpat pharetra quam vel efficitur. Euismod nisl id Donec. Nullam vel neque cursus ut ultrices nisi. Nam leo Proin fringilla fermentum." +
        "<br><br>Praesent pharetra euismod dapibus. Etiam eu blandit sem, ac maximus metus. Quisque sapien dui, porttitor nec ipsum auctor scelerisque lorem mauris. Mauris vitae luctus velit, ut sagittis libero. Mauris aliquet iaculis interdum. Nam elementum in, augue vel orci. Maecenas luctus eu leo ​​vitae ultricies. Aenean id tellus ac ex efficitur tincidunt eros consectetur nec lacus. Morbi eros quam, maximus auctor congue in, laoreet id ligula. Aliquam hendrerit id feugiat dictum est. Suspendisse volutpat nibh felis non massa turpis. Vivamus molestie, ligula vestibulum mollis id, congue est est felis, eu viverra quis orci ipsum enim. At nunc fermentum urna. Sed non eros nisi. Aenean eget cursus quam, posuere quam ac quam, malesuada ac ante."
});
