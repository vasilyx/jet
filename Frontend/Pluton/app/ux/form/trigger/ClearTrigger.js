/**
 *
 */
Ext.define('Pluton.ux.form.trigger.ClearTrigger', {
    extend: 'Ext.form.trigger.Trigger',

    alias: 'trigger.clear',

    cls: Ext.baseCSSPrefix + 'form-clear-trigger',

    config: {
        disabled: false
    },

    updateDisabled: function(oldVal, newVal){
        this.changeVisibility();
    },

    initEvents: function() {
        this.callParent(arguments);

        this.field.on({
            change: 'changeVisibility',
            scope: this
        });
        this.changeVisibility();
    },

    changeVisibility: function() {
        if ((this.isVisible() && Ext.isEmpty(this.field.getValue())) || this.getDisabled() ) {
            this.hide();
        } else {
            this.show();
        }
    },

    handler: function() {
        // For Ext.form.field.ComboBox
        if (Ext.isFunction(this.clearValue)) {
            this.clearValue();
        // For other types of fields
        } else {
            this.setValue('');
        }
    }
});
