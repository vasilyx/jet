/**
 *
 */
Ext.define('Pluton.ux.form.BindableRadioGroup', {
    extend: 'Ext.form.RadioGroup',

    xtype: 'bindable-radiogroup',

    requires: [
        'Ext.form.RadioManager'
    ],

    setValue: function(value) {
        var items, cbValue, cmp, formId, radios, i, len, name;

        Ext.suspendLayouts();

        if (this.simpleValue) {
            items = this.getBoxes();
            for (i = 0, len = items.length; i < len; ++i) {
                cmp = items[i];

                if (cmp.inputValue === value) {
                    cmp.setValue(true);
                    break;
                }
            }
        }
        else if (Ext.isObject(value)) {
            items = this.items;
            cmp = items.first();
            formId = cmp ? cmp.getFormId() : null;

            for (name in value) {
                cbValue = value[name];
                radios = Ext.form.RadioManager.getWithValue(name, cbValue, formId).items;
                len = radios.length;

                for (i = 0; i < len; ++i) {
                    radios[i].setValue(true);
                }
            }
        }

        Ext.resumeLayouts(true);

        return this;
    },

    getValue: function () {
        var me = this,
            items = me.getBoxes(),
            i, item, ret;

        if (me.simpleValue) {
            for (i = items.length; i-- > 0; ) {
                item = items[i];

                if (item.checked) {
                    ret = item.inputValue;
                    break;
                }
            }
        } else {
            ret = me.callParent();
        }

        return ret;
    }
});