/**
 *
 */
Ext.define('Pluton.ux.form.field.QueryField', {
    extend: 'Ext.form.field.Picker',

    xtype: 'query-field',

    requires: [
        'Ext.data.Store',
        'Ext.data.identifier.Uuid',
        'Ext.event.Event',
        'Ext.selection.DataViewModel',
        'Ext.view.BoundList'
    ],

    config: {
        triggers: {
            picker: {
                hidden: true
            },
            query: {
                tooltip: 'Поиск',
                cls: 'x-fa fa-search',
                handler: 'onQueryTriggerClick',
                scope: 'this'
            },
            error: {
                tooltip: 'Неверный поисковый запрос',
                cls: 'x-fa fa-exclamation-circle',
                extraCls: 'invalid-trigger',
                hidden: true
            }
        }
    },
    separator: /(?:[^\s"]+|"[^"]*")+/g,
    lastMutatedValue: '',
    checkChangeEvents: ['keyup'],
    msgTarget: 'none',
    displayField: 'label',
    emptyText: 'Введите поисковый запрос',
    defaultListConfig: {
        loadingHeight: 70,
        minWidth: 70,
        maxHeight: 300,
        shadow: 'sides'
    },
    operators: [
        {id: '=', label: '=', type: 'operator'},
        {id: '<>', label: '<>', type: 'operator'},
        {id: '>', label: '>', type: 'operator'},
        {id: '>=', label: '>=', type: 'operator'},
        {id: '<', label: '<', type: 'operator'},
        {id: '<=', label: '<=', type: 'operator'},
        {id: 'like', label: 'LIKE', type: 'operator'},
        {id: 'in', label: 'IN', type: 'operator'},
        {id: 'and', label: 'AND', type: 'binary'}
    ],
    queryPartsMap: {
        0: 'property',
        1: 'operator',
        2: 'value',
        3: 'binary'
    },

    initEvents: function() {
        var me = this;

        me.on({
            validitychange: 'onValidityChange',
            specialkey: 'onSpecialKey',
            scope: me
        });
        me.callParent();
    },

    onValidityChange: function(field, valid) {
        var me = this;

        if (valid) {
            me.clearInvalid();
        } else {
            me.markInvalid(true);
        }
        me.getTrigger('query').setVisible(valid);
        me.getTrigger('error').setVisible(!valid);
    },

    onQueryTriggerClick: function () {
        this.startQuery();
    },

    onSpecialKey: function (field, e) {
        if (e.getKey() === Ext.event.Event.ENTER) {
            this.startQuery();
        }
    },

    startQuery: function () {
        if (this.isValid()) {
            this.fireEvent('startquery', this, this.getQueryValues());
        }
    },

    getQueryValues: function () {
        var me = this,
            uuid = new Ext.data.identifier.Uuid(),
            rawValue = me.getRawValue(),
            rawValueParts = rawValue.match(me.separator) || [],
            cnt = 0,
            modulo,
            queryId,
            queryValues = {
                queryIds: []
            };

        if (Ext.isEmpty(rawValueParts)) {
            return null;
        }

        Ext.Array.each(rawValueParts, function (value, index) {
            modulo = index % 4;
            switch (modulo) {
                case 0:
                    queryId = uuid.generate();
                    queryValues.queryIds.push(queryId);
                    queryValues[queryId] = {
                        property: me.getQueryPartValue(modulo, value),
                        operator: null,
                        value: null
                    };
                    break;
                case 1:
                    queryValues[queryId].operator = me.getQueryPartValue(modulo, value);
                    break;
                case 2:
                    queryValues[queryId].value = me.getQueryPartValue(modulo, value);
                    break;
                case 3:
                    break;
            }
        });

        return queryValues;
    },

    getQueryPartValue: function (modulo, value) {
        var queryPart = this.queryPartsMap[modulo],
            queryValue = value.replace(/(^")|("$)/g, ''),
            queryValueRecord;

        if (modulo !== 2) {
            queryValueRecord = this.getPicker().getStore().getDataSource().findBy(function (r) {
                return r.get('type') === queryPart && r.get('label').toUpperCase() === queryValue.toUpperCase();
            });

            if (queryValueRecord) {
                queryValue = queryValueRecord.getId();
            }
        }

        return queryValue;
    },

    createPicker: function () {
        var me = this,
            picker,
            store = new Ext.data.Store({
                fields: ['id', 'label', 'type'],
                data: me.operators
            }),
            pickerCfg = Ext.apply({
                xtype: 'boundlist',
                id: me.id + '-picker',
                pickerField: me,
                selectionModel: new Ext.selection.DataViewModel({
                    mode: 'SINGLE',
                    ordered: true,
                    deselectOnContainerClick: false,
                    enableInitialSelection: false,
                    pruneRemoved: false,
                    store: store
                }),
                floating: true,
                hidden: true,
                store: store,
                displayField: me.displayField,
                preserveScrollOnRefresh: true,
                listeners: {
                    select: me.onPickerSelect,
                    scope: me
                }
            }, me.listConfig, me.defaultListConfig);

        picker = me.picker = Ext.widget(pickerCfg);
        if (!picker.initialConfig.maxHeight) {
            picker.on({
                beforeshow: me.onBeforePickerShow,
                scope: me
            });
        }

        picker.getNavigationModel().navigateOnSpace = false;

        return picker;
    },

    onPickerSelect: function (picker, record) {
        var me = this,
            label = record.get('label'),
            rawValue = me.getRawValue(),
            rawValueParts = rawValue.match(me.separator) || [],
            cursorPosition = me.getCursorPosition(),
            rawValuePartLength = 0,
            isFound = false,
            modulo;

        Ext.Array.each(rawValueParts, function (value, index) {
            modulo = index % 4;
            rawValuePartLength += value.length;
            if (index > 0) {
                rawValuePartLength += 1;
            }
            if (cursorPosition <= rawValuePartLength) {
                if (modulo === 0) {
                    label = '"' + label + '"';
                }
                if (label !== value) {
                    rawValueParts[index] = label;
                    me.setRawValue(rawValueParts.join(' '));
                    me.setCursorPosition(cursorPosition + (label.length - value.length) + 1);
                }
                isFound = true;

                return false;
            }
        });

        if (!isFound) {
            me.setRawValue(rawValue.substring(0, cursorPosition) + label + rawValue.substring(cursorPosition, rawValue.length))
        }
        me.validate();
        me.collapse();
        me.getPicker().getSelectionModel().deselectAll();
    },

    onFieldMutation: function (e) {
        var me = this,
            picker = me.getPicker(),
            store = picker.getStore(),
            rawValue = me.getRawValue(),
            rawValueParts = rawValue.match(me.separator) || [],
            rawValuePartLength = 0,
            cursorPosition = me.getCursorPosition(),
            pressedKey = e.getKey(),
            ExtEvent = Ext.event.Event,
            modulo;

        if (me.lastMutatedValue !== rawValue && pressedKey !== ExtEvent.ENTER) {
            Ext.Array.each(rawValueParts, function (value, index) {
                modulo = index % 4;
                rawValuePartLength += value.length;
                if (index > 0) {
                    rawValuePartLength += 1;
                }
                if (pressedKey === ExtEvent.SPACE && (cursorPosition - rawValuePartLength) === 1) {
                    if (modulo === 0 ) {
                        store.clearFilter();
                        store.addFilter([
                            {
                                property: 'type',
                                operator: '=',
                                value: 'operator'
                            }
                        ]);
                        picker.refresh();
                        me.expand();
                    }

                    if (modulo === 2) {
                        store.clearFilter();
                        store.addFilter([
                            {
                                property: 'type',
                                operator: '=',
                                value: 'binary'
                            }
                        ]);
                        picker.refresh();
                        me.expand();
                    }
                }

                if (cursorPosition <= rawValuePartLength) {
                    switch (modulo) {
                        case 0:
                            var regExp = new RegExp('".*"');
                            if (!regExp.test(value)) {
                                var newValue = value.replace(/"/g, '');

                                newValue = '"' + newValue + '"';
                                rawValueParts[index] = newValue;
                                me.setRawValue(rawValueParts.join(' '));
                                me.setCursorPosition(cursorPosition + 1);
                            }
                            store.clearFilter();
                            store.addFilter([
                                {
                                    property: 'type',
                                    operator: '=',
                                    value: 'property'
                                },
                                {
                                    property: 'label',
                                    operator: 'like',
                                    value: value.replace(/"/g, '')
                                }
                            ]);
                            picker.refresh();
                            me.expand();
                            break;
                        case 1:
                            store.clearFilter();
                            store.addFilter([
                                {
                                    property: 'type',
                                    operator: '=',
                                    value: 'operator'
                                },
                                {
                                    property: 'label',
                                    operator: 'like',
                                    value: value
                                }
                            ]);
                            picker.refresh();
                            me.expand();
                            break;
                        case 2:
                            var regExp = new RegExp('".*"');
                            if (!regExp.test(value)) {
                                var newValue = value.replace(/"/, '');

                                newValue = '"' + newValue + '"';
                                rawValueParts[index] = newValue;
                                me.setRawValue(rawValueParts.join(' '));
                                me.setCursorPosition(cursorPosition + 1);
                            }
                            break;
                        case 3:
                            store.clearFilter();
                            store.addFilter([
                                {
                                    property: 'type',
                                    operator: '=',
                                    value: 'binary'
                                },
                                {
                                    property: 'label',
                                    operator: 'like',
                                    value: value
                                }
                            ]);
                            picker.refresh();
                            me.expand();
                            break;
                    }

                    return false;
                }
            });

            me.validate();
            me.lastMutatedValue = me.getRawValue();
        }
    },

    isValid: function () {
        var me = this,
            rawValue = me.getRawValue(),
            rawValueParts = rawValue.match(me.separator) || [],
            valid;

        // Пустой запрос считаем валидным
        if (Ext.isEmpty(rawValueParts)) {
            return true;
        }

        // Если структура запроса верна, проверяем значения
        if (rawValueParts.length % 4 === 3) {
            Ext.Array.each(rawValueParts, function (value, index) {
                valid = me.validateFilterPart(index % 4, value);
                return valid;
            });
        } else {
            valid = false;
        }

        return valid;
    },

    validate: function() {
        var me = this,
            isValid = me.isValid();

        if (me.wasValid !== isValid) {
            me.wasValid = isValid;
            me.fireEvent('validitychange', me, isValid);
        }
    },

    validateFilterPart: function (modulo, value) {
        var queryPart = this.queryPartsMap[modulo];

        // We do not validate the value now
        // TODO: validate it!
        if (modulo !== 2) {
            return !!this.getPicker().getStore().getDataSource().findBy(function (r) {
                return r.get('type') === queryPart && r.get('label').toUpperCase() === (value.replace(/(^")|("$)/g, '').toUpperCase());
            });
        }

        return true;
    },

    onExpand: function () {
        var keyNav = this.getPicker().getNavigationModel();

        if (keyNav) {
            keyNav.enable();
        }
    },

    onCollapse: function() {
        var keyNav = this.getPicker().getNavigationModel();

        if (keyNav) {
            keyNav.disable();
        }
    },

    getCursorPosition: function () {
        var domEl = this.inputEl.dom,
            cursorPos;

        if (document.selection) {
            cursorPos = document.selection.createRange();
            cursorPos.collapse(true);
            cursorPos.moveStart('character', -domEl.value.length);
            cursorPos = cursorPos.text.length;
        } else {
            cursorPos = domEl.selectionStart;
        }
        return cursorPos;
    },

    setCursorPosition: function (cursorPos) {
        var domEl = this.inputEl.dom,
            range;

        if (domEl.createTextRange) {
            range = domEl.createTextRange();
            range.move('character', cursorPos);
            range.select();
        } else {
            if (domEl.selectionStart) {
                domEl.focus();
                domEl.setSelectionRange(cursorPos, cursorPos);
            } else {
                domEl.focus();
            }
        }
    }
});
