/**
 *
 */
Ext.define('Pluton.ux.form.FiltersFieldSet', {
    extend: 'Ext.form.FieldSet',

    xtype: 'filters-fieldset',

    title: 'Фильтры',

    collapsible: true,
    collapsed: true,
    ui: 'fieldset-without-padding',
    style: {
        background: 'transparent'
    }
});
