/**
 * Remove button._pressedCls that added when clicking the mouse
 */
Ext.define('Pluton.ux.button.MainToolbarButton', {
    extend: 'Ext.button.Button',

    xtype: 'main-toolbar-button',

    onMouseUp: Ext.emptyFn,
    onMouseDown: function(e) {
        var me = this;

        if (Ext.isIE || e.pointerType === 'touch') {
            if (me.deferFocusTimer) {
                clearTimeout(me.deferFocusTimer);
            }

            me.deferFocusTimer = Ext.defer(function() {
                var focusEl;

                me.deferFocusTimer = null;

                if (me.destroying || me.destroyed) {
                    return;
                }

                focusEl = me.getFocusEl();

                if (focusEl && !e.defaultPrevented) {
                    focusEl.focus();
                }
            }, 1);
        }

        if (!me.disabled && e.button === 0) {
            Ext.button.Manager.onButtonMousedown(me, e);
        }
    }

});
