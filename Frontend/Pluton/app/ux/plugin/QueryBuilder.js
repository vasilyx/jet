/**
 *
 */
Ext.define('Pluton.ux.plugin.QueryBuilder', {
    extend: 'Ext.plugin.Abstract',

    alias: 'plugin.query-builder',

    mixins: [
        'Ext.mixin.Observable'
    ],

    cachedQueries: null,

    constructor: function (config) {
        var me = this;

        me.callParent([config]);
        me.mixins.observable.constructor.call(me);
    },

    init: function (grid) {
        var me = this,
            queryField = grid.down('query-field');

        if (!queryField) {
            return;
        }

        me.grid = grid;
        me.queryField = queryField;
        me.initEvents();

        if (grid.rendered) {
            me.fillQueryField();
        } else {
            me.mon(grid, {
                reconfigure: me.onReconfigure,
                scope: me,
                beforerender: {
                    fn: me.onBeforeRender,
                    single: true,
                    scope: me
                }
            });
        }
    },

    initEvents: function () {
        var me = this;

        me.mon(me.queryField, 'startquery', me.onStartQuery, me);
    },

    onStartQuery: function (field, value) {
        var me = this,
            store = me.grid.getStore(),
            cachedQueries = me.cachedQueries;

        if (cachedQueries) {
            Ext.Array.forEach(cachedQueries, function (queryId) {
                store.removeFilter(queryId, true);
            });
        }

        if (!value) {
            me.cachedQueries = null;
        } else {
            me.cachedQueries = value.queryIds;
            delete value.queryIds;

            Ext.Object.each(value, function(key, value) {
                store.addFilter([Ext.apply(value, {
                    id: key
                })], true);
            });
        }

        store.load();
    },

    onReconfigure: function () {
        this.fillQueryField();
    },

    onBeforeRender: function () {
        this.fillQueryField();
    },

    fillQueryField: function () {
        var columns = this.grid.getColumns(),
            filterFields = [],
            dataIndex;

        Ext.Array.forEach(columns, function (column) {
            dataIndex = column.dataIndex;
           if (dataIndex) {
               filterFields.push({
                   id: dataIndex,
                   label: column.text ? column.text : dataIndex,
                   type: 'property'
               })
           }
        });

        this.queryField.getPicker().getStore().add(filterFields);
    }
});
