/**
 *
 */
Ext.define('Pluton.ux.plugin.HelpTool', {
    extend: 'Ext.plugin.Abstract',

    alias: 'plugin.help-tool',

    mixins: [
        'Ext.mixin.Observable'
    ],

    requires: [
        'Ext.container.Container',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.panel.Tool',
        'Pluton.ux.window.help.HelpWindow'
    ],

    // TODO дефолтный раздел справки
    defaultPage: null,
    // TODO активный раздел справки
    activePage: null,

    constructor: function (config) {
        var me = this;

        me.callParent([config]);
        me.mixins.observable.constructor.call(me);
    },

    init: function (tabPanel) {
        var me = this,
            tabBar,
            helpContainer;

        if (Ext.isFunction(tabPanel.getTabBar) && (tabBar = tabPanel.getTabBar())) {
            me.tabPanel = tabPanel;
            me.helpContainer = helpContainer = Ext.create({
                xtype: 'container',
                baseCls: 'x-panel-header',
                layout: {
                    type: 'vbox',
                    pack: 'center',
                    align: 'middle'
                },
                width: 60,
                height: 44,
                items: {
                    xtype: 'tool',
                    type: 'help',
                    tooltip: 'Справка',
                    callback: 'onHelpToolClick',
                    scope: me
                }
            });

            tabPanel.removeDocked(tabBar, false);
            tabPanel.addDocked({
                xtype: 'container',
                layout: 'hbox',
                items: [Ext.apply(tabBar, {
                    flex: 1
                })].concat(helpContainer)
            });

            me.initEvents();
        }
    },

    initEvents: function () {
        var me = this,
            tabPanel = me.tabPanel;

        if (me.tabPanel) {
            me.mon(tabPanel, 'tabchange', me.onTabChange, me);
        }
    },

    onTabChange: function (tabPanel, newCard) {
        var me = this;

        if (newCard) {
            me.activePage = newCard.getXType();
        }
    },

    onHelpToolClick: function () {
        Pluton.Help.setActivePage(this.activePage || this.defaultPage);
        Pluton.Help.show();
    },


    // Методы пока не используются (но есть баг ExtJS при первом формировании layout'a табпанели)
    /**
     * При изменении размеря viewport выровняем иконку
     */
    onViewportResize: function () {
        var me = this,
            helpContainer = me.helpContainer,
            helpContainerLayout = helpContainer.getLayout(),
            align = me.calculateAlign();

        if (helpContainerLayout.getAlign() !== align) {
            helpContainerLayout.setAlign(align);
            helpContainer.updateLayout();
        }
    },

    /**
     * Вычислим значение выравнивания иконки помощи
     * Если есть скроллбар во viewport - выровняем иконку по левой границе контейнера, если нет - по центру
     */
    calculateAlign: function () {
        var viewport = Pluton.getApplication().getMainView(),
            viewportHeight = viewport.getHeight(),
            viewportBodyPadding = viewport.bodyPadding,
            mainToolbarHeight = viewport.lookup('mainToolbar').getHeight(),
            mainContainerMinHeight = viewport.lookup('mainContainer').getMinHeight(),
            heightForScrolling = mainToolbarHeight + mainContainerMinHeight + viewportBodyPadding * 2,
            align;

        // Сравним текущую высоту viewport с минимальной высотой для появления скролла (mainToolbar.height + mainContainer.minHeight + viewport.bodyPadding*2)
        if (viewportHeight >= heightForScrolling) {
            align = 'middle';
        } else {
            align = 'begin';
        }

        return align;
    }

});
