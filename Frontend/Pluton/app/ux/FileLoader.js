/**
 * Utility для загрузки файлов с сервера: Pluton.ux.FileLoader.download(options)
 * Объект options имеет параметры: url (в конце добавляется cookieToken), params, callback, interval, repeat
 *
 * Описание проблемы:
 * Submit формы с standardSubmit=true в основном окне перенаправляет по пути, указанном в запросе. В случае ошибки
 * на экране отображается текст запроса. Ajax-запрос же не позволяет сохранять файлы, поэтому в скрытом iframe создается
 * форма. Chrome не генерирует событий загрузки в iframe, поэтому сервер сообщает об окончании запроса - для
 * обработки ошибок и удаления временной DOM-структуры - путем установки согласованной с клиентом cookie.
 * Браузеры, сообщающие о загрузке, простоты ради пользуются тем же механизмом.
 */
Ext.define('Pluton.ux.FileLoader', {
    requires: [
        'Ext.form.Panel',
        'Ext.util.TaskManager'
    ],

    singleton: true,

    SUCCESS: '0',
    FAILURE: '1',

    /**
     * Download file from {url}/{cookieToken}
     * Cleans-up on receiving cookieToken (if 0 assumes success, else failure)
     * @param options
     */
    download: function(options) {
        // create hidden target iframe
        var me = this,
            url = options.url,
            params = options.params,
            callback = options.callback,
            scope = options.scope,
            token = options.token || me.createToken(),
            frame = document.createElement('iframe'),
            form;

        frame.id = token;
        frame.name = token;
        frame.className = 'x-hidden';
        if (Ext.isIE) {
            frame.src = Ext.SSL_SECURE_URL;
        }

        document.body.appendChild(frame);

        if (Ext.isIE) {
            document.frames[token].name = token;
        }

        form = Ext.create('Ext.form.Panel', {
            url: url + '?token=' + token,
            standardSubmit: true,
            method: 'post'
        });

        form.submit({params: params, target: frame.name});

        Ext.TaskManager.start({
            run: function() {
                var cookie = me.getCookie(token);
                if (cookie != null || this.taskRunCount === this.repeat) {
                    me.expireCookie(token);
                    if (callback && Ext.isFunction(callback)) {
                        Ext.callback(callback, scope, [cookie === me.SUCCESS, me.getFrameContent(frame)]);
                    }
                    if (form) { form.destroy(); }
                    if (frame) { Ext.fly(frame).remove(); }
                    
                    return false;
                }
            },
            // 30 sec by default
            interval: options.interval || 500,
            repeat: options.repeat || 60
        });
    },

    getCookie: function(name) {
        var parts = document.cookie.split(name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    },

    expireCookie: function(cName) {
        document.cookie = encodeURIComponent(cName) + "=; expires=" + new Date(0).toUTCString() + "; path=/";
    },

    createToken: function() {
        return Math.random().toString(36).slice(-12);
    },

    getFrameContent: function(iframe) {
        var iframeDocument = iframe.contentDocument || iframe.contentWindow.document,
            elems = iframeDocument && iframeDocument.getElementsByTagName("body"),
            iframeBody = elems && elems.length > 0 && elems[0];
        return iframeBody ? iframeBody.innerText : null;
    }
});