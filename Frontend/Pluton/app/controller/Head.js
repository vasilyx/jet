/**
 *
 */
Ext.define('Pluton.controller.Head', {
    extend: 'Ext.app.Controller',

    requires: [
        'Ext.util.History',
        'Ext.window.Toast',
        'Pluton.util.LoginManager',
        'Pluton.view.main.Main'
    ],

    onLaunch: function () {
        Ext.util.History.init();
        Ext.Ajax.setTimeout(180000);
        Ext.Ajax.on({
            requestexception: 'onRequestException',
            scope: this
        });
        LoginManager.on({
            loginsuccess: 'onLoginSuccess',
            scope: this
        });
        LoginManager.login();
    },

    onRequestException: function (connection, response) {
        var responseText = response.responseText && Ext.JSON.decode(response.responseText, true),
            message;

        if (response.status !== -1) {
            if (responseText && !Ext.isEmpty(responseText.message)) {
                message = responseText.message;
            } else if (response.status === 401) {
                message = 'Необходима авторизация!';
            } else {
                message = 'На сервере произошла ошибка';
            }

            Ext.toast({
                title: 'Ошибка',
                html: message,
                align: 't',
                closable: true,
                slideInDuration: 400,
                minWidth: 400
            });
        }
    },

    onLoginSuccess: function (jsonData) {
        var application = this.getApplication(),
            mainView = application.getMainView();

        if (mainView) {
            mainView.destroy();
        }
        application.setMainView({
            xtype: 'main',
            viewModel: {
                data: jsonData
            }
        });
        this.destroyLoader();
    },

    destroyLoader: function () {
        var loader = Ext.fly('loader');

        if (loader) {
            loader.destroy();
        }
    }
});
