Ext.define('Pluton.utility.button.CSVExportController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.utility.button-csv-export',

    requires: [
        'Pluton.ux.FileLoader'
    ],

    onRender: function (button) {
        if (button.up('grid')) {
            button.setDisabled(false);
        }
    },

    onClick: function(button) {
        var grid = button.up('grid'),
            store = grid.getStore(),
            filtersConfig = [],
            sortersConfig = [],
            columns = [];

        store.getFilters().each(function(item) {
            filtersConfig.push({
                field: item.getProperty(),
                type: item.getOperator(),
                value: item.getValue()
            });
        });

        store.getSorters().each(function(item) {
            sortersConfig.push({
                property: item.getProperty(),
                direction: item.getDirection()
            });
        });

        Ext.Array.each(grid.getVisibleColumns(), function(item) {
            if (item.dataIndex) {
                columns.push(item.dataIndex);
            }
        });

        Pluton.ux.FileLoader.download({
            url: store.getProxy().getUrl(),
            params: {
                filters: Ext.JSON.encode(filtersConfig),
                sort: Ext.JSON.encode(sortersConfig),
                columns: Ext.JSON.encode(columns),
                report_type: Ext.JSON.encode("csv"),
                page: 1,
                page_size: 0
            }
        });
    }
});
