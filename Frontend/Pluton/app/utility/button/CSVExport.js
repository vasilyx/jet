Ext.define('Pluton.utility.button.CSVExport', {

    extend: 'Ext.button.Button',

    xtype: 'utility-button-csv-export',

    requires: [
        'Ext.button.Button',
        'Pluton.utility.button.CSVExportController'
    ],
    controller: 'utility.button-csv-export',

    iconCls: 'x-fa fa-file-excel-o',
    ui: 'md-green',
    disabled: true,
    tooltip: 'Экспорт списка в файл',
    listeners: {
        render: 'onRender',
        click: 'onClick'
    }
});
