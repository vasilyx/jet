/**
 *
 */
Ext.define('Pluton.utility.tree.TreeSystemComponent', {
    extend: 'Ext.tree.Panel',

    xtype: 'utility-tree-system-component',

    requires: [
        'Pluton.utility.tree.TreeSystemComponentController',
        'Pluton.utility.tree.TreeSystemComponentModel'
    ],

    controller: 'tree-system-component',
    viewModel: {
        type: 'tree-system-component'
    },

    reference: 'treePanelSystemComponent',
    checkPropagation: 'both',
    allowDeselect: true,
    rootVisible: false,
    useArrows: true,
    displayField: 'name',
    hideCollapseTool: true,
    collapsible: true,
    header: false,
    width: 300,
    bodyStyle: {
        borderTop: '0px'
    },
    bind: {
        store: '{SystemComponent}'
    },
    listeners: {
         load: 'onLoad',
         rowclick: 'onRowClick',
         beforecheckchange: 'onComponentBeforeCheckChange'
    }

});
