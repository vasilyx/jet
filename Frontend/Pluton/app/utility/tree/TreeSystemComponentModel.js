/**
 *
 */
Ext.define('Pluton.utility.tree.TreeSystemComponentModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-system-component',

    requires: [
        'Pluton.store.systemComponent.CheckableTreeSystemComponent'
    ],

    stores: {
        SystemComponent: {
            type: 'checkable-tree-system-component'
        }
    }
});
