/**
 *
 */
Ext.define('Pluton.utility.tree.TreeSystemComponentController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tree-system-component',

    listen: {
        controller: {
            '*': {
                componentTreeSetValue: 'componentTreeSetValue'
            }
        }
    },

    componentTreeSetValue: function () {
        var store = this.getStore('SystemComponent');

        if (store.isLoaded()) {
            this.onLoad(store, store.getRange());
        }
    },

    onRowClick: function (tree, selected) {
        var check = selected.get('checked');

        if (Ext.isBoolean(check)) {
            selected.set('checked', !check);
            selected.eachChild(function(child) {
                child.set('checked', !check);
            });
        }

        return true;
    },


    onComponentBeforeCheckChange: function () {
        return false;
    },

    onLoad: function (store, records) {
        if (!records.length) return;
        var me = this,
            view = me.getView(),
            currentSusId = me.getViewModel().get('current_sus_id'),
            previousSelectedId = me.getViewModel().get('component_tree_selected_id'),
            record = previousSelectedId ? store.getNodeById(previousSelectedId) : store.getNodeById(currentSusId);

        view.expandAll(function () {
            if (record) {
                record.set('checked', true);
                record.eachChild(function(child) {
                    child.set('checked', true);
                });
                view.getSelectionModel().select(record);
            }
        });

        this.fireEvent('filterApply');
    }
});
