/**
 *
 */
Ext.define('Pluton.model.statistics.Traffic', {
    extend: 'Pluton.model.Base',

    fields: [
        {
            name: 'conn'
        },
        {
            name: 'direction'
        },
        {
            name: 'begin_ts'
        },
        {
            name: 'end_ts'
        },
        {
            name: 'packet'
        },
        {
            name: 'proto'
        },
        {
            name: 'start_time'
        },
        {
            name: 'volume'
        }
    ]
});
