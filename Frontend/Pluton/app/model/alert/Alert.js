/**
 *
 */
Ext.define('Pluton.model.alert.Alert', {
    extend: 'Pluton.model.Base',

    requires: [
        'Pluton.data.proxy.JsonAjax'
    ],

    fields: [
        // Основное
        {
            name: 'vulnerabilities',
            convert: function (value) {
                var resultArr = [],
                    result = '';

                if(Ext.isArray(value)) {
                    Ext.each(value, function(item) {
                        resultArr.push(item.id)
                    });
                    result = resultArr.join(', ');
                    return result;
                } else {
                    return value;
                }
            }
        },
        {
            name: 'alert_timestamp',
            type: 'date',
            dateFormat: 'C'
        },
        'severity_id',
        'category_name',
        'transport_protocol',
        'sensor_name',
        'sensor_ip',
        'controlled_system',
        // Дополнительно
        // Правило
        'rule_name',
        // Suricata
        'alert_type_id',
        'signature_type',
        'signature_name',
        'signature_class',
        'base_score_v2',
        'base_score_v3',
        'vulnerability_id',
        'alert_trust',
        // нет рекоммендованного действия
        {
            name: 'signature_description',
            convert: function(value) {
                if (!Ext.isEmpty(value)) {
                    return value.split(';').join('\n');
                } else {
                    return value;
                }
            }
        },
        // нет тега
        // Новое ПО хоста
        'signature_type',
        // нет хоста
        // нет типа
        // нет наименования
        // нет версии
        // нет URL'а
        // Новая ОС хоста
        'signature_type',
        // нет хоста
        // нет типа
        // нет версии
        // нет дистанции
        // нет качества совпадения
        // Новый сервис хоста
        'signature_type',
        // нет хоста
        // нет порта
        'transport_protocol',
        // нет имени
        // Чёрный список
        'signature_type',
        // нет имени
        // нет значения
        // нет имени MD5-файла
        // Чёрный список
        'signature_type',
        // нет IP-адреса
        // PCAP
        // нет PCAP'ов
        // Показатели агрегации СИБ
        'alert_count',
        '',
        {
            name: 'alert_timestamp_start',
            type: 'date',
            dateFormat: 'C'
        },
        {
            name: 'alert_timestamp_end',
            type: 'date',
            dateFormat: 'C'
        },
        'alert_microseconds_start',
        'alert_microseconds_end',
        // Источник
        'source_name',
        'source_ip',
        'source_port',
        'source_type',
        'source_internal',
        // Получатель
        'target_name',
        'target_ip',
        'target_port',
        'target_type',
        'target_internal',
        // Соединение
        'connection_timestamp',
        'duration',
        'application_protocol',

        //Transport protocol
            //FTP
                //нет ID события
                //нет пользователя
                //нет размера файла
                //нет команды
            //HTTP
                //нет ID события
                //нет метода
                //нет хоста
                //нет URL'а
                //нет пользовательского агента
                //нет имени пользователя
            //RDP
                //нет ID события
                //нет cookies
                //нет протокола безопасности
                //нет раскладки клавиатуры
                //нет версии клиента
                //нет имени компьютера
                //нет product ID пользовательского компьютера
                //нет уровня шифрования
                //нет метода шифрования
                //нет SSL
            //SIP
                //нет ID события
                //нет метода
                //нет запроса от
                //нет запроса к
                //нет ответа от
                //нет ответа к
                //нет URL'а
                //нет reply to
                //нет call ID
                //нет темы
                //нет агента пользователя
            //SMTP
                //нет ID события
                //нет источника письма
                //нет адресатов письма
                //нет даты
                //нет пользовательского агента
                //нет темы
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            create: '/alert/create',
            read: '/alert/get',
            update: '/alert/update',
            destroy: '/alert/delete'
        }
    }
});
