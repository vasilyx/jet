/**
 *
 */
Ext.define('Pluton.model.alert.AlertPcap', {
    extend: 'Pluton.model.Base',

    fields: [
        'head',
        'body'
    ]
});
