/**
 *
 */
Ext.define('Pluton.model.clickhouse.Alert', {
    extend: 'Pluton.model.Base',

    requires: [
        'Pluton.data.proxy.JsonAjax'
    ],

    fields: [
        //Основное
        {name: 'alert_timestamp', type: 'date', dateFormat: 'C'},
        'severity_id',
        'severity_name',
        'alert_type',
        'alert_type_id',
        'pcap_url',
        'category_id',
        'category_name',
        'sensor_id',
        'sensor_name',
        'sensor_ip',
        'alert_status',
        'alert_rule_name',
        'controlled_system',
        'resolution_status',
        'route_distance',
        'transport_protocol',
        //Соединение
        {name: 'connection_timestamp', type: 'date', dateFormat: 'C'},
        'duration',
        'tunnel',
        'application_protocol',
        'mandate_label',
        'bytes',
        //Источник
        'source_ip',
        'source_host_id',
        'source_name',
        'source_type',
        'source_port',
        'source_mac',
        'source_internal',
        'source_location_id',
        'source_ip_subnet',
        'source_domain',
        'source_os_name',
        //Получатель
        'target_ip',
        'target_host_id',
        'target_name',
        'target_type',
        'target_port',
        'target_mac',
        'target_internal',
        'target_location_id',
        'target_ip_subnet',
        'target_domain',
        'target_os_name'
    ]

});
