/**
 *
 */
Ext.define('Pluton.model.role.Role', {
    extend: 'Pluton.model.Base',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.data.validator.StrictPresence'
    ],

    fields: [
        {
            name: 'full_name',
            type: 'string',
            validators: 'strict-presence'
        },
        {
            name: 'short_name',
            type: 'string',
            validators: 'strict-presence'
        },
        {
            name: 'source_sus_id',
            type: 'string'
        },
        {
            name: 'source_sus_name',
            type: 'string'
        },
        {
            name: 'create_sus_id',
            type: 'string'
        },
        {
            name: 'create_sus_name',
            type: 'string'
        },
        'creation_type',
        {
            name: 'create_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'update_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'description',
            type: 'string'
        },
        {
            name: 'privileges',
            type: 'auto',
            validators: 'strict-presence'
        }

    ],

    proxy: {
        type: 'json-ajax',
        api: {
            create: '/role/create',
            read: '/role/get',
            update: '/role/update',
            destroy: '/role/delete'
        },
        reader: {
            type: 'json',
            transform: function (rawData) {
                if (rawData.privileges) {
                    rawData.privileges.sort(function(a, b) {
                        var nameA = a.label.toUpperCase();
                        var nameB = b.label.toUpperCase();
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        return 0;
                    });
                    rawData.privileges = Ext.Array.map(rawData.privileges, function(item) {
                        return item.id;
                    });
                }
                return rawData;
            }
        }
    }
});
