Ext.define('Pluton.model.audit.Audit', {
    extend: 'Pluton.model.Base',

    fields: [
        {name: "registered_timestamp", type: 'date'},
        "audit_type_name",
        "severity",
        "event_type",
        "event_category",
        "subject_entity_type",
        "subject_name",
        "object_entity_type",
        "object_name",
        "attributes",
        "object_id",
        "subject_id"

    ]    
});
