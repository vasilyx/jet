/**
 *
 */
Ext.define('Pluton.model.logComponent.LogComponent', {
    extend: 'Pluton.model.Base',

    requires: [
        'Ext.data.validator.Length',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.data.validator.StrictPresence'
    ],

    fields: [
        {
            // Имя регистрируемого компонента
            name: 'name',
            validators: [
                'strict-presence',
                {type: 'length', max: 20}
            ]
        },
        {
            // Тип регистрируемого компонента
            name: 'component_type',
            validators: [
                'strict-presence',
                {type: 'length', max: 2000}
            ]
        },

        {
            // Статус запроса регистрации
            name: 'state',
            validators: [
                'strict-presence'
            ]
        },

        {
            // Результирующее сообщение
            name: 'message',
            validators: [
                'strict-presence',
                {type: 'length', max: 255}
            ]
        },
        {
            // Время получение запроса
            name: 'received_timestamp',
            type: 'date'
        },

        {
            // Время подтверждения
            name: 'processed_timestamp',
            type: 'date'
        }
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            read: '/registration/journal'
        }
    }
});
