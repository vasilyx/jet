/**
 *
 */
Ext.define('Pluton.model.department.Department', {
    extend: 'Pluton.model.Base',

    requires: [
        'Pluton.data.proxy.JsonAjax'
    ],

    fields: [
        {
            name: 'id',
            type: 'string'
        },
        {
            name: 'name',
            type: 'string'
        }
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            create: '/system_department/create',
            read: '/system_department/get',
            update: '/system_department/update',
            destroy: '/system_department/delete'
        }
    }
});
