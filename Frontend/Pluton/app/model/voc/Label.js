/**
 *
 */
Ext.define('Pluton.model.voc.Label', {
    extend: 'Pluton.model.Base',

    fields: [
        {name: 'label', type: 'string'},
        {name: 'id', type: 'string'}
    ]
});
