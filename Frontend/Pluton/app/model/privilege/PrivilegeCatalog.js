/**
 *
 */
Ext.define('Pluton.model.privilege.PrivilegeCatalog', {
    extend: 'Pluton.model.Base',

    fields: [
        {
            name: 'label',
            type: 'string'
        },
        {
            name: 'description',
            type: 'string'
        }
    ]
});
