/**
 *
 */
Ext.define('Pluton.model.user.User', {
    extend: 'Pluton.model.Base',

    requires: [
        'Ext.data.writer.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.user.UserPersonalInfo'
    ],

    hasOne: {
        name: 'personal_info',
        model: 'Pluton.model.user.UserPersonalInfo'
    },

    fields: [
        {
            name: 'login',
            type: 'string'
        },
        {
            name: 'password',
            type: 'string'
        },
        {
            name: 'maclabel',
            type: 'string',
            defaultValue: '0'
        },
        {
            name: 'email',
            type: 'string'
        },
        {
            name: 'is_blocked',
            type: 'boolean',
            defaultValue: false
        },
        {
            name: 'source_sus_id',
            type: 'string'
        },
        {
            name: 'source_sus_name',
            type: 'string'
        },
        {
            name: 'create_sus_id',
            type: 'string'
        },
        {
            name: 'create_sus_name',
            type: 'string'
        },
        'creation_type',
        {
            name: 'description',
            type: 'string'
        },
         'roles',
        {
            name: 'version',
            persist: false
        },
        {
            name: 'register_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'first_login_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'expiration_timestamp',
            type: 'date',
            dateWriteFormat: 'c'
        },
        {
            name: 'last_login_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'create_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'failures',
            type: 'int',
            defaultValue: 0,
            persist: false
        },
        {
            name: 'update_timestamp',
            type: 'date',
            persist: false
        }
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            create: '/user/create',
            read: '/user/get',
            update: '/user/update',
            destroy: '/user/delete'
        },
        writer: {
            type: 'json',
            allDataOptions: {
                associated: true
            },
            partialDataOptions: {
                associated: true
            }
        }
    }
});
