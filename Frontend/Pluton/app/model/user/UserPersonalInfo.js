/**
 *
 */
Ext.define('Pluton.model.user.UserPersonalInfo', {
    extend: 'Pluton.model.Base',

    requires: [
        'Ext.data.validator.Length'
    ],

    fields: [
        {
            name: 'family_name',
            type: 'string'
        },
        {
            name: 'first_name',
            type: 'string'
        },
        {
            name: 'patronymic_name',
            type: 'string'
        },
        {
            name: 'position',
            type: 'string',
            validators: [
                {type: 'length', max: 100}
            ]
        },
        {
            name: 'rank',
            type: 'string',
            validators: [
                {type: 'length', max: 100}
            ]
        },
        {
            name: 'department',
            type: 'string',
            validators: [
                {type: 'length', max: 100}
            ]
        },
        {
            name: 'description',
            type: 'string'
        },
        {
            name: 'create_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'update_timestamp',
            type: 'date',
            persist: false
        }
    ]
});
