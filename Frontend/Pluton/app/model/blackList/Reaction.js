/**
 *
 */
Ext.define('Pluton.model.blackList.Reaction', {
    extend: 'Pluton.model.Base',

    fields: [
        'label'
    ]
});
