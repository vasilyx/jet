/**
 *
 */
Ext.define('Pluton.model.blackList.BlackList', {
    extend: 'Pluton.model.Base',

    requires: [
        'Ext.data.validator.Length',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.data.validator.StrictPresence'
    ],

    fields: [
        {
            name: 'name',
            type: 'string',
            validators: [
                'strict-presence',
                {type: 'length', max: 255}
            ]
        },
        {
            name: 'black_list_type',
            validators: 'strict-presence'
        },
        {
            name: 'reaction_list',
            //TODO: delete on the server
            defaultValue: '7f11f8bf-2a42-47f2-9531-811ad7bc7bf0',

        },
        {
            name: 'is_active',
            type: 'boolean',
            defaultValue: false
        },
        {
            name: 'description',
            type: 'string',
            validators: [
                {type: 'length', max: 2000}
            ]
        },
        'version',
        {
            name: 'create_user',
            persist: false
        },
        {
            name: 'update_user',
            persist: false
        },
        {
            name: 'create_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'update_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'direction',
            validators: 'strict-presence'
        }
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            create: '/black_list/create',
            read: '/black_list/get',
            update: '/black_list/update',
            destroy: '/black_list/delete'
        }
    }
});
