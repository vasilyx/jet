/**
 *
 */
Ext.define('Pluton.model.blackList.BlackListType', {
    extend: 'Pluton.model.Base',

    fields: [
        'label'
    ]
});
