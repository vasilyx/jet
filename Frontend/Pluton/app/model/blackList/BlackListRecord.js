/**
 *
 */
Ext.define('Pluton.model.blackList.BlackListRecord', {
    extend: 'Pluton.model.Base',

    requires: [
        'Pluton.data.proxy.JsonAjax'
    ],

    fields: [
        {
            name: 'black_list_id',
            critical: true
        },
        'value',
        'direction',
        'description',
        {
            name: 'create_user',
            persist: false
        },
        {
            name: 'update_user',
            persist: false
        },
        {
            name: 'create_timestamp',
            type: 'date',
            persist: false
        },
        {
            name: 'update_timestamp',
            type: 'date',
            persist: false
        }
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            create: '/black_list/record/create',
            update: '/black_list/record/update',
            destroy: '/black_list/record/delete'
        }
    }
});
