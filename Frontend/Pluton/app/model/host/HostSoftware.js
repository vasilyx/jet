/**
 *
 */
Ext.define('Pluton.model.host.HostSoftware', {
    extend: 'Pluton.model.Base',

    fields: [
        // Название ПО
        'name',
        // Тип ПО
        'software_type',
        // Порт
        'port',
        // Протокол
        'protocol',
        // Статус
        'status',
        // Уязвимости
        'vulnerabilities',
        // ПО создано
        'added',
        // Дата обнаружения
        {name: 'discovery_timestamp', type: 'date', format: 'c'}
    ]
});
