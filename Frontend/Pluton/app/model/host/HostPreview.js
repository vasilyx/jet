/**
 *
 */
Ext.define('Pluton.model.host.HostPreview', {
    extend: 'Pluton.model.Base',

    requires: [
        'Pluton.data.proxy.JsonAjax'
    ],

    fields: [
        'added',
        'speed',
        'service_count',
        'application_count',
        {
            name: 'blacklist_count',
            type: 'int'
        },
        {
            name: 'signatures_count',
            type: 'int'
        },
        {
            name: 'vulnerabilities_count',
            type: 'int'
        },
        {
            name: 'created',
            type: 'date',
            format: 'c'
        },
        {
            name: 'last_time_activity',
            type: 'date',
            format: 'c'
        }
    ],

    proxy: {
        type: 'json-ajax',
        url: '/host/preview'
    }
});
