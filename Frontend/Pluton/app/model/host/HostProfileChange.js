/**
 *
 */
Ext.define('Pluton.model.host.HostProfileChange', {
    extend: 'Pluton.model.Base',

    fields: [
        // Дата/время события
        {
            name: 'alert_timestamp',
            type: 'date',
            dateFormat: 'c'
        },
        // Порт
        'port',
        // Имя
        'name',
        // Критичность
        'severity',
        // Хост
        'host_ip',
        // Протокол
        'protocol_name',
        // Имя обнаруженного объекта
        'object_name'
    ]
});
