/**
 *
 */
Ext.define('Pluton.model.host.HostBlackList', {
    extend: 'Pluton.model.Base',

    fields: [
        // Дата/время события
        {
            name: 'alert_timestamp',
            type: 'date',
            dateFormat: 'c'
        },
        // Микросекунды даты
        'alert_microsecond',
        // Критичность
        'severity',
        // Имя сработавшего правила
        'alert_rule_name',
        // Тип черного списка
        'black_list_type',
        // Найденное значение черного списка
        'black_list_value',
        // Направление черного списка
        'black_list_direction'
    ]
});
