/**
 *
 */
Ext.define('Pluton.model.host.HostUser', {
    extend: 'Pluton.model.Base',

    fields: [
        // Имя пользователя
        'login',
        // E-mail
        'email',
        // Протокол
        'protocol',
        // Статус
        'status',
        // Время создания записи
        {name: 'create_timestamp', type: 'date', format: 'c'},
        // Время обновления записи
        {name: 'update_timestamp', type: 'date', format: 'c'}
    ]
});
