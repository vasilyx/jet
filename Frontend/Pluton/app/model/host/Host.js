/**
 *
 */
Ext.define('Pluton.model.host.Host', {
    extend: 'Pluton.model.Base',

    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.host.HostPreview'
    ],

    hasOne: {
        name: 'preview',
        foreignKey: 'id',
        type: 'Pluton.model.host.HostPreview'
    },

    fields: [
        // Имя хоста
        {
            name: 'hostname',
            type: 'string'
        },
        // IP-адреса хоста
        'host_ip',
        // Статус
        'host_status',
        // Операционные системы хоста
        'host_os',
        // Контролируемая система
        'monitored_system',
        // СУС/Сенсор хоста
        'system_component',
        // Компонент-владелец
        'owner_component_id',
        // Дата/время обнаружения хоста
        {
            name: 'discovery_timestamp',
            type: 'date',
            format: 'c'
        },
        // Количество неподтвержденных ОС/сервисов/ПО
        {
            name: 'unconfirmed_applications_count',
            type: 'int'
        }
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            read: '/host/profile'
        }
    }
});
