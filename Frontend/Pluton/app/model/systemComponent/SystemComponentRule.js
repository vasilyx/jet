Ext.define('Pluton.model.systemComponent.SystemComponentRule', {
    extend: 'Pluton.model.Base',

    requires: [
        'Pluton.data.proxy.JsonAjax'
    ],

    fields: [
        'sid',
        'signature_name',
        'is_active',
        'reaction_name',
        'id',
        'priority',
        'update_version',
        'source',
        'class_name'
    ],

    proxy: {
        type: 'json-ajax',
        api: {
//            create: '',
            read: '/system_component/rules/get',
//            update: '',
//            destroy: ''
        }
    }
});