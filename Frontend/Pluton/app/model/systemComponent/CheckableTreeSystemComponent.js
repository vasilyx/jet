/**
 *
 */
Ext.define('Pluton.model.systemComponent.CheckableTreeSystemComponent', {
    extend: 'Pluton.model.systemComponent.SystemComponent',

    fields: [
        {
            name: 'checked',
            persist: false,
            depends: ['component_type'],
            convert: function(value, record) {

                // marker that rec.id = current_sus_id
                if (record.get('current') == true) {
                   return true;
                }

                if (!record.get('parent_id')) {
                    return null;
                }

                return value ? value : false;
            }
        }
    ]
});
