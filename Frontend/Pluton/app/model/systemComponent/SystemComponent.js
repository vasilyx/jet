/**
 *
 */
Ext.define('Pluton.model.systemComponent.SystemComponent', {
    extend: 'Ext.data.TreeModel',

    requires: [
        'Ext.data.identifier.Negative',
        'Ext.data.validator.Length',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.data.validator.StrictPresence'
    ],

    identifier: 'negative',

    fields: [
        'parent_id',
        {
            name: 'name',
            type: 'string',
            validators: [
                'strict-presence',
                {type: 'length', max: 20}
            ]
        },
        'component_type',
        'monitored_system',
        'cluster',
        'host',
        'region',
        'department',
        'sensor_mode',
        'status',
        'health',
        'state',
        {
            name: 'iconCls',
            depends: ['component_type'],
            convert: function(value, record) {
                var componentType = record.get('component_type'),
                    iconCls = '';

                if (Ext.isObject(componentType) && componentType.id) {
                    switch (componentType.id) {
                        case 'SCS':
                            iconCls = 'x-fa fa-desktop';
                            break;
                        case 'COMPLEX':
                            iconCls = 'x-fa fa-cube';
                            break;
                        case 'PORTABLE':
                            iconCls = 'x-fa fa-mobile';
                            break;
                        case 'SENSOR':
                            iconCls = 'x-fa fa-microchip';
                            break;
                    }
                }
                return iconCls;
            }
        }
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            read: '/system_component/get'
        }
    }
});
