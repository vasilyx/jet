/**
 *
 */
Ext.define('Pluton.model.systemComponent.UpdateRegistry', {
    extend: 'Pluton.model.Base', 
 
    fields: [
        // Версия'
        'version',
        // Тип'
        'update_type',
        // Статус'
        'update_status',
        // Ошибка'
        'error',
        // Дата/Время обнаружения'
        {
            name: 'discover_timestamp',
            type: 'date',
            format: 'c'
        },
        // Дата/Время установки'
        {
            name: 'install_timestamp',
            type: 'date',
            format: 'c'
        },
        // Имя файла'
        'update_filename',
        // URL скачивания'
        'url',
        // Размер'
        'size',
        // Фактическая контрольная сумма'
        'checksum',
        // Комментарий'
        'description'
    ]
});
