/**
 *
 */
Ext.define('Pluton.model.systemComponent.UpdateServerAgent', {
    extend: 'Pluton.model.Base', 

    requires: [
         'Pluton.data.proxy.JsonAjax',
    ],

    fields: [
        'system_component_id',
        'uname',
        'dname',
        'short_dname',
        'parent_dname',
        'address',
        'resp',
        'tel',
        'email',
        'apikey',
        'state'
    ],

    idProperty: 'system_component_id',

    proxy: {
        type: 'json-ajax',
        idParam: 'system_component_id',
        api: {
            read: '/update_server/agent/get',
            create: '/update_server/agent/get',
            update: '/update_server/agent/update'
        }
    }
});
