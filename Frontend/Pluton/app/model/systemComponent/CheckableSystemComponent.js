/**
 *
 */
Ext.define('Pluton.model.systemComponent.CheckableSystemComponent', {
    extend: 'Pluton.model.systemComponent.SystemComponent',

    fields: [
        {
            name: 'checked',
            persist: false,
            depends: ['component_type'],
            convert: function(value, record) {
                var componentType = record.get('component_type');

                if (Ext.isObject(componentType) && componentType.id === 'SENSOR') {
                    return value ? value : false;
                }
                return null;
            }
        }
    ]
});
