/**
 *
 */
Ext.define('Pluton.model.systemComponent.MonitoredSystem', {
    extend: 'Pluton.model.Base',

    requires: [
        'Ext.data.validator.Length',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.data.validator.StrictPresence'
    ],

    fields: [
        {
            name: 'name',
            type: 'string',
            validators: [
                {type: 'length', max: 255},
                {type: 'strict-presence'}
            ]
        },
        {
            name: 'system_component_name',
            type: 'string',
            validators: [
                {type: 'length', max: 255}
            ]
        },
        {
            name: 'description',
            type: 'string',
            validators: [
                {type: 'length', max: 2000}
            ]
        },
        {
            name: 'subnet_ip',
            type: 'string',
            validators: [
                {type: 'length', max: 190},
                {type: 'strict-presence'}
            ]
        }
    ],

    proxy: {
        type: 'json-ajax',
        api: {
            read: '/monitored_system/get'
        }
    }
});
