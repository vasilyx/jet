Ext.define('Pluton.model.Base', {
    extend: 'Ext.data.Model',
    
    requires: [
        'Ext.data.identifier.Negative'
    ],
    
    schema: {
        namespace: 'Pluton.model',
        defaultIdentifier: 'negative'
    }
});
