/**
 *
 */
Ext.define('Pluton.view.privateComponent.PrivateComponent', {
    extend: 'Ext.tab.Panel',

    permissions: 'private-component_page',

    xtype: 'private.private-component',

    requires: [
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.systemComponent.card.SystemComponentCard'
    ],

    plugins: {
        ptype: 'help-tool'
    },

    items: [
        {
            xtype: 'system-component-card',
            viewModel: {
                data: {
                    systemComponentId: null,
                    privateComponent: true
                }
            },
            bind: {
                title: 'Мой компонент{(systemComponentTitle ? " - " + systemComponentTitle : "")}'
            }
        }
    ]
});
