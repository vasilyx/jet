/**
 *
 */
Ext.define('Pluton.view.logComponent.LogComponentController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.log-component',

    listen: {
        controller: {
            'log-component-card': {
                refreshGrid: 'onLoad'
            }
        }
    },

    onLoad: function () {
        this.getStore('LogComponent').load()
    },

    onRefresh: function () {
        this.onLoad();
    },

    onAccept: function () {
        var me = this,
            vModel = me.getViewModel(),
            selection = vModel.get('logComponentGrid.selection');

        if (selection) {
            Ext.widget('result-message-add-window', {
                viewModel: {
                    parent: vModel,
                    data: {
                        theLogComponent: selection
                    }
                },
                listeners: {
                    yes: 'onAccepted',
                    scope: me
                }
            }).show();
        }
    },

    onAccepted: function (form, record) {
        var store = this.getStore('LogComponent');

        Ext.Ajax.request({
            url: '/registration/accept',
            method: 'POST',
            jsonData: {
                id: record.getId(),
                reason: record.get('reason')
            },
            success: function () {
                store.load();
            },
            failure: function () {
                store.load();
            }
        });
    },

    onReject: function () {
        var me = this,
            vModel = me.getViewModel(),
            selection = vModel.get('logComponentGrid.selection');

        if (selection) {
            Ext.widget('result-message-add-window', {
                viewModel: {
                    parent: vModel,
                    data: {
                        theLogComponent: selection
                    }
                },
                listeners: {
                    yes: 'onRejected',
                    scope: me
                }
            }).show();
        }
    },

    onRejected: function (form, record) {
        var store = this.getStore('LogComponent');

        Ext.Ajax.request({
            url: '/registration/reject',
            method: 'POST',
            jsonData: {
                id: record.getId(),
                reason: record.get('reason')
            },
            success: function () {
                store.load();
            },
            failure: function () {
                store.load();
            }
        });
    },

    onDelete: function () {
        var me = this,
            store = me.getStore('LogComponent'),
            selection = me.getViewModel().get('logComponentGrid.selection');

        if (selection) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Вы уверены, что хотите удалить запрос на регистрацию?',
                function (msg) {
                    if (msg === 'yes') {
                        Ext.Ajax.request({
                            url: '/registration/delete',
                            method: 'POST',
                            jsonData: {
                                id: selection.getId()
                            },
                            success: function () {
                                store.load();
                            },
                            failure: function () {
                                store.load();
                            }
                        });
                    }
                }
            );
        }
    }
});
