/**
 *
 */
Ext.define('Pluton.view.logComponent.grid.LogComponentGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'log-component-grid',

    requires: [
        'Ext.grid.column.Date',
        'Ext.grid.plugin.RowExpander',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Separator'
    ],

    bind: {
        store: '{LogComponent}'
    },

    reference: 'logComponentGrid',
    title: 'Управление регистрацией компонентов',

    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefresh'
        },
        '-',
        {
            iconCls: 'x-fa fa-thumbs-up',
            ui: 'md-green',
            tooltip: 'Подтвердить',
            handler: 'onAccept',
            disabled: true,
            bind: {
                disabled: '{changeStatusPossible.accept !== true}'
            }
        },
        {
            iconCls: 'x-fa fa-thumbs-down',
            ui: 'md-yellow',
            tooltip: 'Отклонить',
            handler: 'onReject',
            disabled: true,
            bind: {
                disabled: '{changeStatusPossible.reject !== true}'
            }
        },
        {
            iconCls: 'x-fa fa-trash',
            ui: 'md-red',
            tooltip: 'Удалить',
            handler: 'onDelete',
            disabled: true,
            bind: {
                disabled: '{changeStatusPossible.delete !== true}'
            }
        }
    ],
    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{LogComponent}'
                }
            }
        ]
    },
    plugins: [{
        ptype: 'rowexpander',
        columnWidth: 20,
        rowBodyTpl: new Ext.XTemplate('<div id="expanded-row-{id}">{error}</div>', {})

    }],
    columns: [
        {
            dataIndex: 'state',
            text: 'Статус',
            flex: 1,
            renderer: Ext.labelRenderer
        },
        {
            dataIndex: 'component_type',
            text: 'Тип',
            flex: 1,
            renderer: Ext.labelRenderer
        },
        {
            dataIndex: 'name',
            text: 'Имя',
            flex: 1
        },
        {
            dataIndex: 'connection_host',
            text: 'IP адрес',
            flex: 1
        },
        {
            dataIndex: 'info',
            text: 'Доп. информация',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            dataIndex: 'received_timestamp',
            text: 'Дата запроса',
            flex: 1,
            format: 'd.m.Y H:i:s'

        },
        {
            xtype: 'datecolumn',
            dataIndex: 'processed_timestamp',
            text: 'Дата принятия решения',
            flex: 1,
            format: 'd.m.Y H:i:s'
        },
        {
            dataIndex: 'reason',
            text: 'Результирующее сообщение',
            flex: 1
        }
    ]
});
