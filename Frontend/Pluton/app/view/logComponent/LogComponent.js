/**
 *
 */
Ext.define('Pluton.view.logComponent.LogComponent', {
    extend: 'Ext.tab.Panel',

    permissions: 'component-registration_page',

    xtype: 'settings.log-component',

    requires: [
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.logComponent.LogComponentController',
        'Pluton.view.logComponent.LogComponentModel',
        'Pluton.view.logComponent.grid.LogComponentGrid'
    ],

    controller: 'log-component',
    viewModel: {
        type: 'log-component'
    },

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onLoad'
    },

    items: [
        {
            xtype: 'log-component-grid'
        }
    ]
});
