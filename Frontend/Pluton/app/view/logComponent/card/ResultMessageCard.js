/**
 *
 */
Ext.define('Pluton.view.logComponent.card.ResultMessageCard', {
    extend: 'Ext.panel.Panel',

    xtype: 'result-message-card',

    requires: [
        'Ext.form.field.Text',
        'Ext.layout.container.Form',
        'Ext.toolbar.Fill',
        'Pluton.view.logComponent.card.ResultMessageCardController'
    ],

    bind: {
        title: '{resultMessageCardTitle}'
    },

    controller: 'resultMessage-card',
    scrollable: true,
    layout: 'form',
    buttonAlign: 'right',

    bbar: [
        {
            text: 'Отменить',
            tooltip: 'Отменить',
            handler: 'onCancel'
        },
        '->',
        {
            text: 'Подтвердить',
            tooltip: 'Подтвердить',
            ui: 'md-blue',
            handler: 'onYes'
        }
    ],

    items: [
        {
            xtype: 'textfield',
            bind: {
                value: '{theLogComponent.reason}'
            }
        }
    ]
});
