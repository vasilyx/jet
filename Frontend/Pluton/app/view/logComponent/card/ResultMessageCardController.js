/**
 *
 */
Ext.define('Pluton.view.logComponent.card.ResultMessageCardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.resultMessage-card',

    onYes: function () {
        var vModel = this.getViewModel(),
            theLogComponent = vModel.get('theLogComponent');

        this.fireViewEvent('yes', theLogComponent);
    },

    onCancel: function () {
        this.fireViewEvent('cancel');
    }
});
