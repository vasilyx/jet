/**
 *
 */
Ext.define('Pluton.view.logComponent.LogComponentModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.log-component',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.logComponent.LogComponent'
    ],

    formulas: {
        changeStatusPossible: {
            bind: {
                bindTo: '{logComponentGrid.selection.state}',
                deep: true
            },
            get: function (state) {
                var stateId = Ext.isObject(state) && state.id,
                    statuses = {
                        accept: false,
                        reject: false,
                        delete: false
                    };

                if (!stateId) {
                    return statuses;
                } else {
                    switch (stateId) {
                        case 'NEW':
                        case 'ERROR':
                            statuses.accept = true;
                            statuses.reject = true;
                            statuses.delete = true;
                            break;
                        case 'REJECTED':
                            statuses.delete = true;
                            break;
                    }

                    return statuses;
                }
            }
        }
    },

    stores: {
        LogComponent: {
            model: 'Pluton.model.logComponent.LogComponent',
            remoteSort: true,
            remoteFilter: true,
            sorters: 'name',
            proxy: {
                type: 'json-ajax',
                url: '/registration/journal',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        }
    }
});
