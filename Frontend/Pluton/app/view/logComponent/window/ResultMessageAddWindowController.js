/**
 *
 */
Ext.define('Pluton.view.logComponent.window.ResultMessageAddWindowController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.result-message-add-window',

    control: {
        '#': {
            yes: 'onYes',
            cancel: 'onCancel'
        }
    },

    init: function (view) {
         view.relayEvents(view.down('result-message-card'), ['yes', 'cancel']);
    },

    onYes: function () {
        this.getView().close();
    },

    onCancel: function () {
        this.getView().close();
    }
});
