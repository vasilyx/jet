/**
 *
 */
Ext.define('Pluton.view.logComponent.window.ResultMessageAddWindow', {
    extend: 'Ext.window.Window',

    xtype: 'result-message-add-window',

    requires: [
        'Ext.layout.container.Fit',
        'Pluton.view.logComponent.card.ResultMessageCard',
        'Pluton.view.logComponent.window.ResultMessageAddWindowController'
    ],

    controller: 'result-message-add-window',

    title: 'Введите результирующее сообщение',
    modal: true,
    constrain: true,
    minHeight: 100,
    minWidth: 350,
    width: 600,
    layout: 'fit',

    items: [
         {xtype: 'result-message-card'}
    ]
});
