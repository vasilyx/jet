/**
 *
 */
Ext.define('Pluton.view.map.MapController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.map',

    requires: [
        'Ext.util.TaskManager'
    ],

    bindings: {
        onAlertsChange: {
            current: '{aggregates.alerts_count}',
            trend: '{aggregates.alerts_count_trend}'
        },
        onAnomaliesChange: {
            current: '{aggregates.anomalies_count}',
            trend: '{aggregates.anomalies_count_trend}'
        },
        onBlackListChange: {
            current: '{aggregates.bl_count}',
            trend: '{aggregates.bl_count_trend}'
        }
    },

    config: {
        updateTask: null
    },
    map: null,
    canvasFlowmapLayer: null,
    pointsLayer: null,

    // make a map
    onBoxReady: function() {
        var me = this,
            map = me.map = L.map(me.lookup('mapContainer').body.dom, {attributionControl: false}).setView([42.6, 3.8], 3),
            task = Ext.util.TaskManager.newTask({
                run: me.loadData,
                interval: 15000,
                fireOnStart: true,
                scope: me
            });

        // add scale
        L.control.scale({
            imperial: false
        }).addTo(map);
        // wms
        L.tileLayer.wms('/mapcache/', {
            layers: 'osm',
            format: 'image/png'
        }).addTo(map);

        // run update task
        me.setUpdateTask(task);
        task.start();
    },

    onActivate: function() {
        var task = this.getUpdateTask();
        if (task) {
            task.start();
        }
    },

    onDeactivate: function() {
        var task = this.getUpdateTask();
        if (task) {
            task.stop();
        }
    },

    onPeriodSelect: function() {
        var task = this.getUpdateTask();
        if (task) {
            task.restart();
        }
    },

    loadData: function() {
        Ext.Ajax.request({
            url: '/map/aggregated',
            method: 'POST',
            jsonData: {
                period: this.getViewModel().get('periodField.value')
            },
            success: 'onAggregatedSuccess',
            scope: this
        });
        Ext.Ajax.request({
            url: '/map/alerts',
            method: 'POST',
            jsonData: {},
            success: 'onMapAlertsSuccess',
            scope: this
        });
    },

    onMapAlertsSuccess: function(response) {
        var me = this,
            map = me.map,
            canvasFlowmapLayer = me.canvasFlowmapLayer,
            pointsLayer = me.pointsLayer,
            json = Ext.JSON.decode(response.responseText, true),
            originLatLng,
            destinationLatLng,
            destinationId,
            existingPoint,
            geoJsonFeatures = [],
            singlePoints = [],
            checkLatLng = function (o) {
                var lat, lng;
                if (!Ext.isObject(o) || !((lat = o.source_latitude) && (lng = o.source_longitude))) {
                    return false;
                } else {
                    return {
                        lat: lat,
                        lng: lng
                    };
                }
            },
            createMarker = function (markerData, latlng) {
                var type, info, tpl, markerIcon, markerColor;
                if ((type = markerData.type) && type === 'SENSOR') {
                    info = markerData.info;
                    markerColor = 'blue';
                    markerIcon = 'fa-desktop';
                    tpl = new Ext.Template([
                        '<div style="padding:10px; min-width:200px;">',
                            '<span style="font-size:16px; border-bottom:1px solid #d4d4d4;">Сенсор: {name}</span><br>',
                            '<div style="margin-top:10px; margin-bottom:5px;">',
                                '<div style="display:inline-block; width:15px; height:15px; border-radius:50%; background:#F57F17;"></div>',
                                '<span style="display:inline; margin-left:10px;">Угроз</span>',
                                '<span style="float:right; margin-left:10px; font-weight: bold;">{alertsCount}</span>',
                            '</div>',
                            '<div style="margin-bottom:5px;">',
                                '<div style="display:inline-block; width:15px; height:15px; border-radius:50%; background:#FFD600;"></div>',
                                '<span style="display:inline; margin-left:10px;">Аномалий</span>',
                                '<span style="float:right; margin-left:10px; font-weight: bold;">{anomaliesCount}</span>',
                            '</div>',
                            '<div>',
                                '<div style="display:inline-block; width:15px; height:15px; border-radius:50%; background:#0288D1;"></div>',
                                '<span style="display:inline; margin-left:10px;">Черных списков</span>',
                                '<span style="float:right; margin-left:10px; font-weight: bold;">{blackListCount}</span>',
                            '</div>',
                        '</div>'
                    ]).apply({
                        name: markerData.name,
                        alertsCount: info ? info.alerts_count : 0,
                        anomaliesCount: info ? info.anomalies_count : 0,
                        blackListCount: info ? info.bl_count : 0
                    });
                } else {
                    markerColor = 'red';
                    markerIcon = 'fa-exclamation-circle';
                    tpl = '<span style="font-size:16px;">' + (type || '???') + '</span>';
                }

                return L.marker(latlng, {
                    id: markerData.id,
                    icon: L.divIcon({
                        className: 'map-marker',
                        html: Ext.String.format('<div class="map-circle-{0}"></div><div class="map-icon-{0}"><span class="x-fa {1}"></span></div>', markerColor, markerIcon)
                    })
                }).bindPopup(tpl);
            };

        if (!Ext.Object.isEmpty(json)) {
            Ext.Object.each(json, function (key, value) {
                if (originLatLng = checkLatLng(value)) {
                    if ((destinationId = value.target) && (destinationLatLng = checkLatLng(json[destinationId]))) {
                        Ext.Array.push(geoJsonFeatures, {
                            type: 'Feature',
                            geometry: {
                                type: 'Point'
                            },
                            properties: {
                                origin_id: key,
                                origin_lat: originLatLng.lat,
                                origin_lon: originLatLng.lng,
                                destination_id: destinationId,
                                destination_lat: destinationLatLng.lat,
                                destination_lon: destinationLatLng.lng
                            }
                        });
                        if ((existingPoint = Ext.Array.findBy(singlePoints, function(item) {return item.options.id === destinationId}))) {
                            Ext.Array.remove(singlePoints, existingPoint);
                        }
                    } else {
                        Ext.Array.push(singlePoints, createMarker(Ext.apply(value, {id: key}), originLatLng));
                    }
                }
            });
        }

        if (canvasFlowmapLayer) {
            map.removeLayer(canvasFlowmapLayer);
        }
        if (pointsLayer) {
            map.removeLayer(pointsLayer);
        }

        me.canvasFlowmapLayer = L.canvasFlowmapLayer(
            {
                type: 'FeatureCollection',
                features: geoJsonFeatures
            },
            {
                animationStarted: true,
                animationEasingFamily: 'Linear',
                animationEasingType: 'None',
                animationDuration: 2000,
                pointToLayer: function(geoJsonPoint, latlng) {
                    var properties = geoJsonPoint.properties;

                    return createMarker(json[properties.isOrigin ? properties.origin_id : properties.destination_id], latlng);
                },
                animatedCanvasBezierStyle: {
                    type: 'simple',
                    symbol: {
                        strokeStyle: 'rgb(255, 46, 88)',
                        lineWidth: 4,
                        lineDashOffsetSize: 0,
                        lineCap: 'round'
                    }
                }
            }
        ).addTo(this.map);
        me.pointsLayer = L.layerGroup(singlePoints).addTo(map);
    },

    onAggregatedSuccess: function(response) {
        var json = Ext.decode(response.responseText, true);

        if (!Ext.Object.isEmpty(json)) {
            this.getViewModel().set('aggregates', json);
        }
    },

    onAlertsChange: function(data) {
        this.changeTrendDisplay('alerts', data);
    },

    onAnomaliesChange: function(data) {
        this.changeTrendDisplay('anomalies', data);
    },

    onBlackListChange: function(data) {
        this.changeTrendDisplay('blackList', data);
    },

    changeTrendDisplay: function(clsPrefix, data) {
        var trend = data.trend,
            cls = 'x-fa ';

        if (trend > 0) {
            cls += 'fa-arrow-up map-red-color';
        } else if (trend < 0) {
            cls += 'fa-arrow-down map-green-color';
        } else {
            cls += 'fa-exchange';
        }

        this.getViewModel().set(clsPrefix + 'TrendCls', cls);
    }
});
