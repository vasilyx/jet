/**
 *
 */
Ext.define('Pluton.view.map.Map', {
    extend: 'Ext.tab.Panel',

    permissions: 'global-map_page',

    xtype: 'map.global-map',

    requires: [
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.layout.container.Center',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        'Ext.resizer.Splitter',
        'Ext.toolbar.Separator',
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.map.MapController',
        'Pluton.view.map.MapModel'
    ],

    controller: 'map',
    viewModel: {
        type: 'map'
    },

    plugins: {
        ptype: 'help-tool'
    },
    bodyPadding: 10,
    listeners: {
        boxready: 'onBoxReady',
        activate: 'onActivate',
        deactivate: 'onDeactivate'
    },

    items: [
        {
            xtype: 'panel',
            title: 'Карта',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    cls: 'with-all-border',
                    minWidth: 270,
                    items: [
                        // Выбор периода
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            cls: 'with-bottom-border',
                            items: [
                                {
                                    reference: 'periodField',
                                    publishes: ['value'],
                                    xtype: 'combo',
                                    emptyText: 'Период',
                                    editable: false,
                                    queryMode: 'local',
                                    valueField: 'id',
                                    displayField: 'label',
                                    value: 'month',
                                    margin: 10,
                                    flex: 1,
                                    bind: {
                                        store: '{Periods}'
                                    },
                                    listeners: {
                                        select: 'onPeriodSelect'
                                    }
                                }
                            ]
                        },
                        // Угрозы
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            height: 120,
                            cls: 'with-bottom-border',
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    cls: 'with-bottom-border',
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'УГРОЗЫ',
                                            flex: 1,
                                            cls: 'map-gray-color',
                                            padding: 10,
                                            style: {
                                                fontSize: '18px'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'center',
                                    height: 80,
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            width: '100%',
                                            height: '100%',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    width: 100,
                                                    items: [
                                                        {
                                                            xtype: 'component',
                                                            cls: 'x-fa fa-bolt map-orange-color',
                                                            style: {
                                                                fontSize: '36px'
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    flex: 1,
                                                    items: [
                                                        {
                                                            xtype: 'label',
                                                            style: {
                                                                fontSize: '40px'
                                                            },
                                                            bind: {
                                                                text: '{aggregates.alerts_count}'
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    width: 100,
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'middle'
                                                            },
                                                            style: {
                                                                fontSize: '18px'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'component',
                                                                    margin: '0 0 5 0',
                                                                    userCls: 'x-fa fa-exchange',
                                                                    bind: {
                                                                        userCls: '{alertsTrendCls}'
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'label',
                                                                    bind: {
                                                                        text: '{aggregates.alerts_count_trend}'
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },

                        // Аномалии
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            height: 120,
                            cls: 'with-bottom-border',
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    cls: 'with-bottom-border',
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'АНОМАЛИИ',
                                            flex: 1,
                                            cls: 'map-gray-color',
                                            padding: 10,
                                            style: {
                                                fontSize: '18px'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'center',
                                    height: 80,
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            width: '100%',
                                            height: '100%',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    width: 100,
                                                    items: [
                                                        {
                                                            xtype: 'component',
                                                            cls: 'x-fa fa-exclamation-triangle map-yellow-color',
                                                            style: {
                                                                fontSize: '36px'
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    flex: 1,
                                                    items: [
                                                        {
                                                            xtype: 'label',
                                                            style: {
                                                                fontSize: '40px'
                                                            },
                                                            bind: {
                                                                text: '{aggregates.anomalies_count}'
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    width: 100,
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'middle'
                                                            },
                                                            style: {
                                                                fontSize: '18px'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'component',
                                                                    margin: '0 0 5 0',
                                                                    userCls: 'x-fa fa-exchange',
                                                                    bind: {
                                                                        userCls: '{anomaliesTrendCls}'
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'label',
                                                                    bind: {
                                                                        text: '{aggregates.anomalies_count_trend}'
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },

                        // Черные списки
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            height: 120,
                            cls: 'with-bottom-border',
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    cls: 'with-bottom-border',
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'ЧЕРНЫЕ СПИСКИ',
                                            flex: 1,
                                            cls: 'map-gray-color',
                                            padding: 10,
                                            style: {
                                                fontSize: '18px'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'center',
                                    height: 80,
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            width: '100%',
                                            height: '100%',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    width: 100,
                                                    items: [
                                                        {
                                                            xtype: 'component',
                                                            cls: 'x-fa fa-list-ul map-blue-color',
                                                            style: {
                                                                fontSize: '36px'
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    flex: 1,
                                                    items: [
                                                        {
                                                            xtype: 'label',
                                                            style: {
                                                                fontSize: '40px'
                                                            },
                                                            bind: {
                                                                text: '{aggregates.bl_count}'
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: 'center',
                                                    width: 100,
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'middle'
                                                            },
                                                            style: {
                                                                fontSize: '18px'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'component',
                                                                    margin: '0 0 5 0',
                                                                    userCls: 'x-fa fa-exchange',
                                                                    bind: {
                                                                        userCls: '{blackListTrendCls}'
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'label',
                                                                    bind: {
                                                                        text: '{aggregates.bl_count_trend}'
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },

                        //
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            style: {
                                fontSize: '18px'
                            },
                            padding: '10 0 10 10',
                            margin: '20 0',
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'ПОДЧИНЕННЫХ СУС',
                                            cls: 'map-gray-color',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'label',
                                            width: 100,
                                            style: {
                                                fontWeight: 'bold',
                                                textAlign: 'center'
                                            },
                                            bind: {
                                                text: '{aggregates.child_scs_count}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'ЛОКАЛЬНЫХ СЕНСОРОВ',
                                            cls: 'map-gray-color',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'label',
                                            width: 100,
                                            style: {
                                                fontWeight: 'bold',
                                                textAlign: 'center'
                                            },
                                            bind: {
                                                text: '{aggregates.local_sensor_count}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },

                        //
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            style: {
                                fontSize: '18px'
                            },
                            padding: '10 0 10 10',
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'ВСЕГО СУС',
                                            cls: 'map-gray-color',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'label',
                                            width: 100,
                                            style: {
                                                fontWeight: 'bold',
                                                textAlign: 'center'
                                            },
                                            bind: {
                                                text: '{aggregates.all_scs_count}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: 'ВСЕГО СЕНСОРОВ',
                                            cls: 'map-gray-color',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'label',
                                            width: 100,
                                            style: {
                                                fontWeight: 'bold',
                                                textAlign: 'center'
                                            },
                                            bind: {
                                                text: '{aggregates.all_sensor_count}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'splitter'
                },
                {
                    reference: 'mapContainer',
                    xtype: 'panel',
                    cls: 'with-all-border',
                    flex: 3
                }
            ]
        }
    ]
});
