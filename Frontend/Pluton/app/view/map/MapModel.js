/**
 *
 */
Ext.define('Pluton.view.map.MapModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.map',

    stores: {

    },

    data: {
        aggregates: {
            'alerts_count': 0,
            'alerts_count_trend': 0,
            'anomalies_count': 0,
            'anomalies_count_trend': 0,
            'bl_count': 0,
            'bl_count_trend': 0,
            'child_scs_count': 0,
            'local_sensor_count': 0,
            'all_scs_count': 0,
            'all_sensor_count': 0
        },
        alertsTrendCls: '',
        anomaliesTrendCls: '',
        blackListTrendCls: ''
    },

    formulas: {
        Periods: function() {
            var ExtDate = Ext.Date,
                now = new Date(),
                currentDayOfWeek = now.getDay() || 7,
                firstDayOfWeek = ExtDate.add(now, ExtDate.DAY, 1 - currentDayOfWeek),
                lastDayOfWeek = ExtDate.add(now, ExtDate.DAY, 7 - currentDayOfWeek),
                firstDayMonthNum = firstDayOfWeek.getMonth(),
                lastDayMonthNum = lastDayOfWeek.getMonth(),
                getMonthLabel = function(monthNum) {
                    var monthName = ExtDate.monthNames[monthNum];

                    return (monthNum === 2 || monthNum === 7) ? (monthName + 'а') : (monthName.replace(/.$/, 'я'));
                },
                currentMonthLabel = getMonthLabel(now.getMonth()),
                firstDayOfWeekLabel, lastDayOfWeekLabel;

            firstDayOfWeekLabel = firstDayOfWeek.getDate();
            lastDayOfWeekLabel = lastDayOfWeek.getDate();
            if (firstDayMonthNum !== lastDayMonthNum) {
                firstDayOfWeekLabel += ' ' + getMonthLabel(firstDayMonthNum);
                lastDayOfWeekLabel += ' ' + getMonthLabel(lastDayMonthNum);
            } else {
                lastDayOfWeekLabel += ' ' + currentMonthLabel;
            }

            return [
                {
                    id: 'day',
                    label: Ext.String.format('День ({0} {1})', now.getDate(), currentMonthLabel)
                },
                {
                    id: 'week',
                    label: Ext.String.format('Неделя ({0} - {1})', firstDayOfWeekLabel, lastDayOfWeekLabel)
                },
                {
                    id: 'month',
                    label: Ext.String.format('Месяц ({0} - {1} {2})', ExtDate.getFirstDateOfMonth(now).getDate(), ExtDate.getLastDateOfMonth(now).getDate(), currentMonthLabel)
                }
            ];
        }
    }
});
