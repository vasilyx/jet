/**
 *
 */
Ext.define('Pluton.view.statistics.Statistics', {
    extend: 'Ext.tab.Panel',

    permissions: 'report-statistics-page',

    xtype: 'reports.statistics',

    requires: [
        'Ext.container.Container',
        'Ext.data.Store',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Tag',
        'Ext.form.field.Text',
        'Ext.layout.container.Border',
        'Ext.layout.container.Fit',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        'Pluton.utility.tree.TreeSystemComponent',
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.statistics.StatisticsController',
        'Pluton.view.statistics.StatisticsModel',
        'Pluton.view.statistics.chart.ProtocolUsage',
        'Pluton.view.statistics.chart.SessionCount',
        'Pluton.view.statistics.chart.SessionIntensity',
        'Pluton.view.statistics.chart.TrafficIntensity',
        'Pluton.view.statistics.chart.TrafficSpeed',
        'Pluton.view.statistics.chart.TrafficVolume'
    ],

    controller: 'statistics',
    viewModel: {
        type: 'statistics'
    },

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onActivate'
    },

    items: [
        {
            title: 'Статистика',
            xtype: 'panel',
            layout: 'border',
            items: [
                {
                    reference: 'systemComponentTree',
                    xtype: 'treepanel',
                    region: 'east',
                    width: 240,
                    rootVisible: false,
                    split: true,
                    collapsible: true,
                    header: false,
                    useArrows: true,
                    displayField: 'name',
                    bodyStyle: {
                        borderTop: '0px'
                    },
                    bind: {
                        store: '{SystemComponent}'
                    },
                    listeners: {
                        load: 'onLoadSystemComponent',
                        beforeselect: 'onComponentBeforeSelect',
                        beforecheckchange: 'onComponentBeforeCheckChange',
                        selectionchange: 'onComponentSelectionChange'
                    }
                },
                {
                    xtype: 'panel',
                    region: 'center',
                    scrollable: true,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    tbar: [
                        {
                            title: 'Фильтры',
                            xtype: 'fieldset',
                            ui: 'fieldset-without-padding',
                            style: {
                                background: 'transparent'
                            },
                            collapsible: true,
                            collapsed: true,
                            flex: 1,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    reference: 'statisticsListFilters',
                                    xtype: 'form',
                                    layout: 'hbox',
                                    scrollable: true,
                                    defaults: {
                                        flex: 1,
                                        minWidth: 300
                                    },
                                    fieldDefaults: {
                                        labelAlign: 'top'
                                    },
                                    listeners: {
                                        validitychange: 'onValidityChange'
                                    },
                                    bbar: {
                                        layout: {
                                            type: 'hbox',
                                            pack: 'center'
                                        },
                                        items: [
                                            {
                                                text: 'Применить',
                                                tooltip: 'Применить фильтры',
                                                ui: 'md-blue',
                                                handler: 'onFilterApply',
                                                bind: {
                                                    disabled: '{!isValid}'
                                                }
                                            },
                                            {
                                                text: 'Сбросить',
                                                tooltip: 'Сбросить фильтры',
                                                handler: 'onFilterReset'
                                            }
                                        ]
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            margin: '0 10 0 0',
                                            items: [
                                                {
                                                    reference: 'timestampField',
                                                    publishes: ['value'],
                                                    name: 'alert_timestamp',
                                                    xtype: 'combo',
                                                    fieldLabel: 'Обнаружено за',
                                                    emptyText: 'Всё время',
                                                    editable: false,
                                                    clearTrigger: true,
                                                    store: [
                                                        [1, 'Последний день'],
                                                        [2, 'Последнюю неделю'],
                                                        [3, 'Последний месяц'],
                                                        [4, 'Период']
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    layout: 'hbox',
                                                    labelSeparator: '',
                                                    fieldLabel: '&nbsp;',
                                                    items: [
                                                        {
                                                            reference: 'timestampFrom',
                                                            xtype: 'datefield',
                                                            format: 'd.m.Y H:i:s.u',
                                                            altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                                            submitFormat: 'Y-m-d H:i:sP',
                                                            allowBlank: false,
                                                            emptyText: 'c',
                                                            flex: 1,
                                                            bind: {
                                                                disabled: '{timestampField.value != 4}'
                                                            }
                                                        },
                                                        {
                                                            xtype: 'displayfield',
                                                            margin: '0 5 0 5',
                                                            value: ' - '
                                                        },
                                                        {
                                                            reference: 'timestampTo',
                                                            xtype: 'datefield',
                                                            format: 'd.m.Y H:i:s.u',
                                                            altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                                            submitFormat: 'Y-m-d H:i:sP',
                                                            allowBlank: false,
                                                            emptyText: 'по',
                                                            flex: 1,
                                                            bind: {
                                                                disabled: '{timestampField.value != 4}'
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            margin: '0 10 0 0',
                                            items: [
                                                {
                                                    name: 'direction',
                                                    xtype: 'tagfield',
                                                    fieldLabel: 'Направление',
                                                    emptyText: 'Любое',
                                                    queryMode: 'local',
                                                    anyMatch: true,
                                                    forceSelection: true,
                                                    clearTrigger: true,
                                                    displayField: 'value',
                                                    valueField: 'id',
                                                    store: Ext.create('Ext.data.Store', {
                                                        fields: ['id', 'value'],
                                                        data: [
                                                            {"id": "INBOUND", "value": "Входящий"},
                                                            {"id": "OUTBOUND", "value": "Исходящий"},
                                                            {"id": "LOCAL", "value": "Локальный"}
                                                        ]
                                                    })
                                                },
                                                {
                                                    name: 'source_ip',
                                                    xtype: 'textfield',
                                                    fieldLabel: 'IP'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            margin: '0 10 0 0',
                                            items: [
                                                {
                                                    name: 'proto',
                                                    xtype: 'tagfield',
                                                    fieldLabel: 'Протокол',
                                                    emptyText: 'Любой',
                                                    queryMode: 'local',
                                                    valueField: 'proto',
                                                    displayField: 'proto',
                                                    clearTrigger: true,
                                                    anyMatch: true,
                                                    forceSelection: true,
                                                    grow: false,
                                                    bind: {
                                                        store: '{ProtocolChart}'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    items: [
                        {
                            xtype: 'container',
                            layout: 'fit',
                            items: {
                                xtype: 'traffic-volume'
                            }
                        },
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            defaults: {
                                flex: 1
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'fit',
                                    items: {
                                        xtype: 'traffic-speed'
                                    }
                                },
                                {
                                    xtype: 'container',
                                    layout: 'fit',
                                    items: {
                                        xtype: 'traffic-intensity'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: 'fit',
                            items: {
                                xtype: 'protocol-usage'
                            }
                        },
                        {
                            xtype: 'container',
                            layout: 'fit',
                            items: {
                                xtype: 'session-count'
                            }
                        },
                        {
                            xtype: 'container',
                            layout: 'fit',
                            items: {
                                xtype: 'session-intensity'
                            }
                        }
                    ]
                }
            ]
        }
    ]
});
