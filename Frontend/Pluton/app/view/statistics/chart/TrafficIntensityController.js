/**
 *
 */
Ext.define('Pluton.view.statistics.chart.TrafficIntensityController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.traffic-intensity',

    onAxisDateLabelRender: function (axis, label, layoutContext) {
        var dt = new Date(label);
        return Ext.Date.format(dt, 'd.m.Y H:i:s');
    }
});
