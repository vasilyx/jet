/**
 *
 */
Ext.define('Pluton.view.statistics.chart.TrafficSpeedController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.traffic-speed',

    onAxisDateLabelRender: function (axis, label, layoutContext) {
        var dt = new Date(label);
        return Ext.Date.format(dt, 'd.m.Y H:i:s');
    }
});
