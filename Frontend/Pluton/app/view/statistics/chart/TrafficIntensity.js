/**
 *
 */
Ext.define('Pluton.view.statistics.chart.TrafficIntensity', {
    extend: 'Ext.chart.CartesianChart',

    xtype: 'traffic-intensity',

    requires: [
        'Ext.chart.*',
        'Pluton.view.statistics.chart.TrafficIntensityController'
    ],

    controller: 'traffic-intensity',

    bind: {
        store: '{StatisticsTraffic}'
    },
    captions: {
        title: 'Изменение скорости трафика'
    },
    height: 450,
    insetPadding: {
        left: 20,
        right: 50
    },
    theme: 'blue',
    interactions: ['itemhighlight'],
    sprites: [],
    axes: [
        {
            type: 'numeric',
            fields: 'packet_intensity_growth',
            title: 'пакетов/сек. за сек.',
            position: 'left',
            grid: true
        },
        {
            type: 'category',
            fields: 'begin_ts',
            renderer: 'onAxisDateLabelRender',
            position: 'bottom',
            label: {
                rotate: {
                    degrees: -45
                }
            }
        }
    ],
    series: [
        {
            type: 'line',
            yField: 'packet_intensity_growth',
            xField: 'begin_ts',
            smooth: true,
            highlight: false,
            showMarkers: false
        }
    ]
});
