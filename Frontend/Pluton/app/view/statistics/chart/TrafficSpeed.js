/**
 *
 */
Ext.define('Pluton.view.statistics.chart.TrafficSpeed', {
    extend: 'Ext.chart.CartesianChart',

    xtype: 'traffic-speed',

    requires: [
        'Ext.chart.*',
        'Pluton.view.statistics.chart.TrafficSpeedController'
    ],

    controller: 'traffic-speed',
    bind: {
        store: '{StatisticsTraffic}'
    },
    captions: {
        title: 'Скорость трафика'
    },
    height: 450,
    insetPadding: {
        left: 20,
        right: 50
    },
    theme: 'blue',
    interactions: ['itemhighlight'],
    sprites: [],
    axes: [
        {
            type: 'numeric',
            fields: 'packet_intensity',
            title: 'пакетов/сек.',
            position: 'left',
            grid: true,
            minimum: 0
        },
        {
            type: 'category',
            fields: 'begin_ts',
            renderer: 'onAxisDateLabelRender',
            position: 'bottom',
            grid: true,
            label: {
                rotate: {
                    degrees: -45
                }
            }
        }
    ],
    series: [
        {
            type: 'line',
            yField: 'packet_intensity',
            xField: 'begin_ts',
            smooth: true,
            highlight: true,
            showMarkers: false
        }
    ]
});
