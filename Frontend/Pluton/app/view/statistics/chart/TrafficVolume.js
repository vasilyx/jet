/**
 *
 */
Ext.define('Pluton.view.statistics.chart.TrafficVolume', {
    extend: 'Ext.chart.CartesianChart',

    xtype: 'traffic-volume',

    requires: [
        'Ext.chart.*',
        'Ext.toolbar.Fill',
        'Pluton.view.statistics.chart.TrafficVolumeController'
    ],

    controller: 'traffic-volume',
    bind: {
        store: '{StatisticsTraffic}'
    },
    captions: {
        title: 'Объем трафика'
    },
    interactions: 'crosszoom',
    theme: 'blue',
    height: 500,
    insetPadding: {
        left: 20,
        right: 50
    },
    sprites: [],
    legend: false,
    tbar: [
        '->',
        {
            text: 'Отдалить',
            handler: 'onZoomUndo'
        }
    ],
    axes: [
        {
            type: 'numeric',
            fields: ['volume'],
            title: 'байт',
            position: 'left',
            grid: true,
            minimum: 0
        },
        {
            type: 'category',
            fields: 'begin_ts',
            renderer: 'onAxisDateLabelRender',
            position: 'bottom',
            grid: true,
            label: {
                rotate: {
                    degrees: -45
                }
            }
        }
    ],
    series:[
        {
            type: 'bar',
            xField: 'begin_ts',
            yField: 'volume',
            style: {
                minGapWidth: 20
            },
            highlight: {
                strokeStyle: 'black',
                fillStyle: 'gold'
            }
        }
    ]
});
