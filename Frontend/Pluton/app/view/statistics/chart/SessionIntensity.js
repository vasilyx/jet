/**
 *
 */
Ext.define('Pluton.view.statistics.chart.SessionIntensity', {
    extend: 'Ext.chart.CartesianChart',

    xtype: 'session-intensity',

    requires: [
        'Ext.chart.*',
        'Ext.toolbar.Fill',
        'Pluton.view.statistics.chart.SessionIntensityController'
    ],

    controller: 'session-intensity',

    bind: {
        store: '{StatisticsTraffic}'
    },
    captions: {
        title: 'Интенсивность установления сессий'
    },
    interactions: 'crosszoom',
    theme: 'blue',
    height: 500,
    insetPadding: {
        left: 20,
        right: 50
    },
    sprites: [],
    legend: false,
    tbar: [
        '->',
        {
            text: 'Отдалить',
            handler: 'onZoomUndo'
        }
    ],
    axes: [
        {
            type: 'numeric',
            fields: 'conn_intensity',
            title: 'сессий/сек.',
            position: 'left',
            grid: true,
            minimum: 0
        },
        {
            type: 'category',
            fields: 'begin_ts',
            renderer: 'onAxisDateLabelRender',
            position: 'bottom',
            grid: true,
            label: {
                rotate: {
                    degrees: -45
                }
            }
        }
    ],
    series: [
        {
            type: 'line',
            yField: 'conn_intensity',
            xField: 'begin_ts',
            smooth: true,
            highlight: false,
            showMarkers: false
        }
    ]
});
