/**
 *
 */
Ext.define('Pluton.view.statistics.chart.SessionCount', {
    extend: 'Ext.chart.CartesianChart',

    xtype: 'session-count',

    requires: [
        'Ext.chart.*',
        'Ext.toolbar.Fill',
        'Pluton.view.statistics.chart.SessionCountController'
    ],

    controller: 'session-count',

    bind: {
        store: '{StatisticsTraffic}'
    },
    captions: {
        title: 'Количество сессий'
    },
    interactions: 'crosszoom',
    theme: 'blue',
    height: 500,
    insetPadding: {
        left: 20,
        right: 50
    },
    sprites: [],
    legend: false,
    tbar: [
        '->',
        {
            text: 'Отдалить',
            handler: 'onZoomUndo'
        }
    ],
    axes: [
        {
            type: 'numeric',
            fields: 'conn',
            title: 'сессий',
            position: 'left',
            grid: true,
            minimum: 0
        },
        {
            type: 'category',
            fields: 'begin_ts',
            renderer: 'onAxisDateLabelRender',
            position: 'bottom',
            grid: true,
            label: {
                rotate: {
                    degrees: -45
                }
            }
        }
    ],
    series: [
        {
            type: 'bar',
            xField: 'begin_ts',
            yField: 'conn',
            style: {
                minGapWidth: 20
            },
            highlight: {
                strokeStyle: 'black',
                fillStyle: 'gold'
            }
        }
    ]
});
