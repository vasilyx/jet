/**
 *
 */
Ext.define('Pluton.view.statistics.chart.TrafficVolumeController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.traffic-volume',

    onAxisDateLabelRender: function (axis, label) {
        var dt = new Date(label);
        return Ext.Date.format(dt, 'd.m.Y H:i:s');
    },

    onZoomUndo: function () {
        var interaction = this.getView().getInteraction('crosszoom'),
            undoButton = interaction.getUndoButton(),
            handler = undoButton && undoButton.handler;

        if (handler) {
            handler();
        }
    }
});
