/**
 *
 */
Ext.define('Pluton.view.statistics.chart.ProtocolUsage', {
    extend: 'Ext.chart.PolarChart',

    xtype: 'protocol-usage',

    requires: [
        'Ext.chart.*',
        'Pluton.view.statistics.chart.ProtocolUsageController'
    ],

    controller: 'protocol-usage',

    bind: {
        store: '{ProtocolChart}'
    },
    captions: {
        title: '% использования протоколов'
    },
    background: 'silver',
    legend: {
        docked: 'bottom'
    },
    height: 500,
    innerPadding: 20,
    theme: 'blue',
    interactions: 'rotate',
    series: {
        type: 'pie',
        showInLegend: true,
        highlight: true,
        label: {
            field: 'proto',
            display: 'rotate'
        },
        tooltip: {
            trackMouse: true,
            renderer: 'onSeriesTooltipRender'
        },
        xField: 'volume',
        donut: 45
    }
});
