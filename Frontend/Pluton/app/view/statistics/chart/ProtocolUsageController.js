/**
 *
 */
Ext.define('Pluton.view.statistics.chart.ProtocolUsageController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.protocol-usage',

    onSeriesTooltipRender: function (tooltip, record) {
        tooltip.setHtml(record.get('proto') + ': ' + record.get('volume'));
    }
});
