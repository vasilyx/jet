/**
 *
 */
Ext.define('Pluton.view.statistics.StatisticsModel', {
    extend: 'Ext.app.ViewModel',
 
    alias: 'viewmodel.statistics',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.statistics.Traffic',
        'Pluton.store.systemComponent.SystemComponentBrief'
    ],

    stores: {
        SystemComponent: {
            type: 'system-component-brief',
            autoLoad: false
        },
        ProtocolChart: {
            remoteSort: true,
            remoteFilter: true,
            proxy: {
                type: 'json-ajax',
                url: '/statistics/traffic',
                extraParams: {
                    group_by: ['proto']
                },
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    transform: function (rawData) {
                        var ExtArrayPush = Ext.Array.push,
                            data = [];

                        rawData = rawData[this.getRootProperty()];
                        Ext.Array.forEach(rawData, function (item) {
                            ExtArrayPush(data, {
                                proto: item.proto,
                                volume: item.volume_in + item.volume_out
                            });
                        });

                        return data;
                    }
                }
            }
        },
        StatisticsTraffic: {
            model: 'Pluton.model.statistics.Traffic',
            remoteSort: true,
            remoteFilter: true,
            proxy: {
                type: 'json-ajax',
                url: '/statistics/traffic',
                extraParams: {
                    group_by: ['timespan:100']
                },
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    transform: function (rawData) {
                        var limit = 100,
                            data = rawData[this.getRootProperty()],
                            dataLen = data.length,
                            step = Math.floor(dataLen / limit);

                        var prev_packet_intensity = -1;

                        Ext.Array.forEach(data, function (item) {
                            period = (item.end_ts - item.begin_ts) / 1000000;  // seconds
                            item.begin_ts = item.begin_ts / 1000; // milliseconds from microseconds
                            item.packet = item.packet_in + item.packet_out;
                            item.packet_intensity = item.packet / period;
                            if (prev_packet_intensity == -1) {
                                item.packet_intensity_growth = 0;
                            } else {
                                item.packet_intensity_growth =
                                    (item.packet_intensity - prev_packet_intensity) / period;
                            }
                            prev_packet_intensity = item.packet_intensity;
                            item.volume = item.volume_in + item.volume_out;
                            item.conn = item.conn_in + item.conn_out;
                            item.conn_intensity = item.conn / period;
                        });

                        return data;
                    }
                }
            }
        }
    },

    data: {
    }
});
