/**
 *
 */
Ext.define('Pluton.view.statistics.StatisticsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.statistics',

    onActivate: function () {
        this.getView().setLoading(true);
        this.getStore('SystemComponent').load()
    },

    onLoadSystemComponent: function (store, records) {
        if (Ext.isEmpty(records)) return;
        this.getView().setLoading(false);

        var me = this,
            view = me.lookup('systemComponentTree'),
            currentSusId = me.getViewModel().get('current_sus_id'),
            previousSelectedId = me.getViewModel().get('component_tree_selected_id'),
            record = previousSelectedId ? store.getNodeById(previousSelectedId) : store.getNodeById(currentSusId);

        view.expandAll(function () {
            if (record) {
                record.set('selected', true);
                view.getSelectionModel().select(record);
            }
        });
    },

    onComponentBeforeSelect: function (rowModel, record) {
        var componentType = record.get('component_type');

        return (Ext.isObject(componentType) && componentType.id === 'SENSOR');
    },

    onComponentSelectionChange: function (selModel, selected) {
            this.onFilterApply()
    },

    onValidityChange: function (form, valid) {
        this.getViewModel().set('isValid', valid);
    },

    onFilterApply: function () {
        var me = this,
            storeStatisticsTraffic = me.getStore('StatisticsTraffic'),
            storeProtocolChart = me.getStore('ProtocolChart'),
            values = me.lookup('statisticsListFilters').getValues(),
            filtersConfig = [],
            operator;

        me.getView().setLoading(true);

        Ext.Object.each(values, function (key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'direction':
                    case 'proto':
                    case 'ips':
                        operator = 'in';
                        break;
                    case 'alert_timestamp':
                        var now = new Date(),
                            from, to;

                        var year = now.getUTCFullYear();
                        var month = now.getUTCMonth();
                        var day = now.getUTCDate();
                        operator = 'between';

                        switch (value) {
                            case 1:
                                from = new Date(Date.UTC(year, month, day, 0, 0, 0, 0));
                                to = new Date(Date.UTC(year, month, day, 23, 59, 59, 999));
                                break;
                            case 2: // from first day of week
                                var monday = now.getDay(),
                                    diff = now.getDate() - monday + (monday == 0 ? -6 : 1),
                                    mondayDate = new Date(now.setDate(diff));

                                from = new Date(Date.UTC(year, month, mondayDate.getUTCDate(), 0, 0, 0, 0));
                                to = new Date(Date.UTC(year, month, day, 23, 59, 59, 999));
                                break;
                            case 3:
                                from = new Date(Date.UTC(year, month, 1, 0, 0, 0, 0));
                                to = new Date(Date.UTC(year, month, day, 23, 59, 59, 999));
                                break;
                            case 4:
                                from = this.lookup('timestampFrom').getValue();
                                to = this.lookup('timestampTo').getValue();
                                break;
                        }

                        value = [
                            Ext.Date.format(from, 'Y-m-d H:i:s'),
                            Ext.Date.format(to, 'Y-m-d H:i:s')
                        ];
                        break;
                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }

            } else {
                storeStatisticsTraffic.removeFilter(key, true);
                storeProtocolChart.removeFilter(key, true);
            }

        }, this);

        filtersConfig.push(
            {
                property: 'system_component_id',
                operator: 'in',
                value: this.getTreeSelection()
            }
        );

        storeStatisticsTraffic.addFilter(filtersConfig, true);
        storeStatisticsTraffic.load({
            callback: function () {
                me.getView().setLoading(false);
            }
        });

        storeProtocolChart.addFilter(filtersConfig, true);
        storeProtocolChart.load();
    },

    onFilterReset: function () {
        this.lookup('statisticsListFilters').reset();
        this.onFilterApply();
    },

    getTreeSelection: function () {
        var res = [],
            me = this,
            selection = this.lookup('systemComponentTree').getSelection();


        if (selection) {
            selection.forEach(function (item) {
                res.push(item.getId());
                me.getViewModel().set('component_tree_selected_id', item.getId());
            })
        }

        return res;
    }
});
