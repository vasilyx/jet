/**
 *
 */
Ext.define('Pluton.view.componentMonitoring.ComponentMonitoring', {
    extend: 'Ext.tab.Panel',

    permissions: 'component-monitoring_page',

    xtype: 'env.component-monitoring',

    requires: [
        'Ext.panel.Panel',
        'Pluton.ux.plugin.HelpTool'
    ],

    plugins: {
        ptype: 'help-tool'
    },

    items: [
        {
            title: 'Работоспособность',
            xtype: 'panel',
            items: [
                {
                    xtype: 'component',
                    autoEl: {
                        tag: 'iframe',
                        src: '/zabbix/index.php?request=&name=Admin&password=zabbix&autologin=1&enter=Sign+in',
                        style: {
                            height: '100%',
                            width: '100%',
                            border: 'none'
                        }
                    }
                }
            ]
        }
    ]
});
