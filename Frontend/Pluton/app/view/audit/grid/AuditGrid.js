/**
 *
 */
Ext.define('Pluton.view.audit.grid.AuditGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'audit-grid',

    requires: [
        'Ext.container.Container',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.grid.column.Date',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.slider.Multi',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Separator',
        'Pluton.utility.button.CSVExport',
        'Pluton.ux.container.DateTimeRangeContainer'
    ],

    reference: 'auditGrid',
    bind: {
        store: '{Audit}'
    },
    listeners: {
        activate: 'onActivate',
        itemdblclick: 'onItemDblClick'
    },

    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefresh'
        },
        '-',
        {
            iconCls: 'x-fa fa-external-link',
            ui: 'md-blue',
            tooltip: 'Открыть',
            disabled: true,
            bind: {
                disabled: '{!auditGrid.selection}'
            },
            handler: 'onOpen'
        },
        {
            xtype: 'utility-button-csv-export'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            { dataIndex: 'registered_timestamp', text: 'Дата / время', xtype: 'datecolumn', format: Ext.defaultDateFormat },
            { dataIndex: 'audit_type_name', text: 'Событие' },
            { dataIndex: 'severity', sorter: 'severity_id_', text: 'Критичность', renderer: Ext.labelRenderer },
            { dataIndex: 'event_type', sorter: 'event_type_label_', text: 'Тип', renderer: Ext.labelRenderer },
            { dataIndex: 'event_category', sorter: 'event_category_label_', text: 'Категория', renderer: Ext.labelRenderer },
            { dataIndex: 'subject_entity_type', sorter: 'subject_entity_type_label_', text: 'Тип субъекта', renderer: Ext.labelRenderer },
            { dataIndex: 'subject_name', text: 'Имя субъекта' },
            { dataIndex: 'object_entity_type', sorter: 'object_entity_type_label_', text: 'Тип объекта', renderer: Ext.labelRenderer },
            { dataIndex: 'object_name', text: 'Имя объекта' },
            { dataIndex: 'attributes', text: 'Атрибуты' }
        ]
    },

    tbar: [
        {
            title: 'Фильтры',
            xtype: 'fieldset',
            ui: 'fieldset-without-padding',
            style: {
                background: 'transparent'
            },
            collapsible: true,
            collapsed: true,
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    reference: 'auditListFilters',
                    xtype: 'form',
                    layout: 'hbox',
                    scrollable: true,
                    defaults: {
                        flex: 1,
                        minWidth: 300
                    },
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    listeners: {
                        validitychange: 'onValidityChange'
                    },
                    bbar: {
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                text: 'Применить',
                                tooltip: 'Фильтровать по выбранным параметрам',
                                ui: 'md-blue',
                                handler: 'onFilterApply',
                                bind: {
                                    disabled: '{!isValid}'
                                }
                            },
                            {
                                text: 'Сбросить',
                                tooltip: 'Сброс фильтров',
                                handler: 'onFilterReset'
                            }
                        ]
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'audit_type_name',
                                    xtype: 'textfield',
                                    fieldLabel: 'Имя события'
                                },
                                {
                                    name: 'subject_name',
                                    xtype: 'textfield',
                                    fieldLabel: 'Имя субъекта'
                                },
                                {
                                    name: 'object_name',
                                    xtype: 'textfield',
                                    fieldLabel: 'Имя объекта'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'event_type_id_',
                                    xtype: 'combo',
                                    fieldLabel: 'Тип события',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    bind: {
                                        store: '{AlertType}'
                                    }
                                },
                                {
                                    name: 'subject_entity_type_id_',
                                    xtype: 'combo',
                                    fieldLabel: 'Тип субъекта',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    bind: {
                                        store: '{EntityType}'
                                    }
                                },
                                {
                                    name: 'object_entity_type_id_',
                                    xtype: 'combo',
                                    fieldLabel: 'Тип объекта',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    bind: {
                                        store: '{EntityType}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'attributes',
                                    xtype: 'textfield',
                                    fieldLabel: 'Атрибут'
                                },
                                {
                                    name: 'severity_id_',
                                    fieldLabel: 'Уровень критичности',
                                    xtype: 'multislider',
                                    values: [1, 5],
                                    minValue: 1,
                                    maxValue: 5,
                                    tipText: function(thumb){
                                        switch (thumb.value) {
                                            case 1:
                                                return "Инфо";
                                            case 2:
                                                return "Низкий";
                                            case 3:
                                                return "Средний";
                                            case 4:
                                                return "Высокий";
                                            case 5:
                                                return "Критический";

                                        }
                                    }
                                },
                                {
                                    xtype: 'date-time-range-container',
                                    name: 'registered_timestamp',
                                    submitFormat: 'Y-m-d H:i:sP',
                                    fromFieldConfig: {
                                        fieldLabel: 'Период времени'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],

    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{Audit}'
                }
            }
        ]
    }
});
