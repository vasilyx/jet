Ext.define('Pluton.view.audit.AuditModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.audit',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.audit.Audit',
        'Pluton.store.directory.EntityType',
        'Pluton.store.directory.AlertType',
        'Pluton.store.systemComponent.SystemComponentBrief'

    ],
    stores: {
        SystemComponent: {
            type: 'system-component-brief'
        },
        Audit: {
            model: 'Pluton.model.audit.Audit',
            pageSize: 20,
            remoteSort: true,
            remoteFilter: true,
            sorters: {
                property: 'registered_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/audit/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        AlertType: {
            type: 'alertType',
            proxy: {
                extraParams: {
                    filters: [{
                        property: 'category_id',
                        operator: '=',
                        value: '1d97dee9-3997-4fd1-8674-0a9a5b399c23'
                    }]
                }
            }
        },
        EntityType: {
            type: 'entityType'
        }
    },

    data: {
        
    }
});
