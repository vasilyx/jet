/**
 *
 */
Ext.define('Pluton.view.audit.card.AuditCardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.audit-card',
    renderLinkForSubject:function(){
        var theAudit = this.getViewModel().get('theAudit');
        return this.renderLinkForUser(theAudit.get('subject_entity_type').id,theAudit.get('subject_name'),theAudit.get('subject_id'));
    },
    renderLinkForObject:function(){
        var theAudit = this.getViewModel().get('theAudit');
        return this.renderLinkForUser(theAudit.get('object_entity_type').id,theAudit.get('object_name'),theAudit.get('object_id'));
    },
    renderLinkForUser:function(typeId,name,id){
        var userId = '861299f7-a922-48d4-b5a6-98e8170eadb0',
            componentIds = ['07efc998-bdb6-4fc2-a508-befa51fe3153','fcb5a200-95d3-4ec7-b004-d41820b25d01','861299f7-a922-48d4-b5a6-98e8170eadb1','861299f7-a922-48d4-b5a6-98e8170eadb2'];
        if (typeId == userId && id){
            res = '<a target="_blank" href="#settings/user?id='+id+'">'+name+'</a>';
        }
        else if(Ext.Array.contains(componentIds, typeId) && id){
            res = '<a target="_blank" href="#settings/system-component?id='+id+'">'+name+'</a>';
        }
        else res = name;

        return res;
    }
});
