/**
 *
 */
Ext.define('Pluton.view.audit.card.AuditCard', {
    extend: 'Ext.form.Panel',

    xtype: 'audit-card',

    requires: [
        'Ext.container.Container',
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Pluton.view.audit.card.AuditCardController',
        'Pluton.view.audit.card.AuditCardModel'
    ],

    controller: 'audit-card',
    viewModel: {
        type: 'audit-card'
    },

    bind: {
        title: '{auditCardTitle}'
    },

    bodyPadding: 10,
    scrollable: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        readOnly: true
    },

    items: [
        {
            xtype: 'fieldset',
            title: 'Основное',
            layout: 'hbox',
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    margin: '0 10 0 0',
                    flex: 1,
                    defaults: {
                        xtype: 'textfield',
                        readOnly: true,
                        flex: 1
                    },
                    items: [
                        {
                            fieldLabel: 'ID',
                            bind: {
                                value: '{theAudit.id}'
                            }
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Дата и время события',
                            format: 'd.m.Y H:i:s',
                            bind: {
                                value: '{theAudit.registered_timestamp}'
                            }
                        },
                        {
                            fieldLabel: 'Событие',
                            bind: {
                                value: '{theAudit.audit_type_name}'
                            }
                        },
                        {
                            fieldLabel: 'Уровень критичности',
                            xtype: 'combo',
                            valueField: 'id',
                            displayField: 'label',
                            bind: {
                                store: '{theAudit.severity}',
                                value: '{theAudit.severity}'
                            }
                        },
                        {
                            fieldLabel: 'Тип события',
                            xtype: 'combo',
                            valueField: 'id',
                            displayField: 'label',
                            bind: {
                                store: '{theAudit.event_type}',
                                value: '{theAudit.event_type}'
                            }
                        },
                        {
                            fieldLabel: 'Категория события',
                            xtype: 'combo',
                            valueField: 'id',
                            displayField: 'label',
                            bind: {
                                store: '{theAudit.event_category}',
                                value: '{theAudit.event_category}'
                            }
                        },
                        {
                            fieldLabel: 'Тип субъекта',
                            xtype: 'combo',
                            valueField: 'id',
                            displayField: 'label',
                            bind: {
                                store: '{theAudit.subject_entity_type}',
                                value: '{theAudit.subject_entity_type}'
                            }
                        },
                        {
                            fieldLabel: 'Имя субъекта',
                            xtype: 'displayfield',
                            bind: {
                                value: '{theAlert.subject_name}'
                            },
                            renderer: 'renderLinkForSubject'
                        },
                        {
                            fieldLabel: 'Тип объекта',
                            xtype: 'combo',
                            valueField: 'id',
                            displayField: 'label',
                            bind: {
                                store: '{theAudit.object_entity_type}',
                                value: '{theAudit.object_entity_type}'
                            }
                        },
                        {
                            fieldLabel: 'Имя объекта',
                            xtype: 'displayfield',
                            bind: {
                                value: '{theAlert.object_name}'
                            },
                            renderer: 'renderLinkForObject'
                        },
                        {
                            fieldLabel: 'Атрибуты',
                            xtype: 'textarea',
                            bind: {
                                value: '{theAudit.attributes}'
                            }
                        }
                    ]
                }
            ]
        }
    ]

});
