/**
 *
 */
Ext.define('Pluton.view.audit.card.AuditCardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.audit-card',

    stores: {

    },

    data: {
        theAudit: null
    },

    formulas: {
        auditCardTitle: {
            bind: {
                bindTo: '{theAudit}',
                deep: true
            },
            get: function(theAudit) {
                if (theAudit) {
                    var dateTimeFormatAudit = (theAudit.get('registered_timestamp'))?
                        Ext.Date.format(theAudit.get('registered_timestamp'), "d.m.Y H:i:s.u"):'';
                    return dateTimeFormatAudit+' '+theAudit.get('audit_type_name');
                } else {
                    return '';
                }
            }
        }
    }
});
