Ext.define('Pluton.view.audit.AuditController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.audit',

    requires: [
        'Pluton.view.audit.card.AuditCard'
    ],

    onValidityChange: function(form, valid) {
        this.getViewModel().set('isValid', valid);
    },

    onActivate: function() {
    },

    onRefresh: function() {
        this.getStore('Audit').reload();
    },
    onFilterApply: function() {
        var store = this.getStore('Audit'),
            values = this.lookup('auditListFilters').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function(key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'audit_type_name':
                    case 'subject_name':
                    case 'object_name':
                    case 'attributes':
                        operator = 'like';
                        break;
                    case 'event_type_id_':
                    case 'subject_entity_type_id_':
                    case 'object_entity_type_id_':
                        operator = '=';
                        break;
                    case 'severity_id_':
                    case 'registered_timestamp':
                        operator = 'between';
                        break;
                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            } else {
                store.removeFilter(key, true);
            }
        }, this);

        filtersConfig.push({
            property: 'system_component_id',
            operator: 'in',
            value: this.getTreeSelection()
        });

        store.addFilter(filtersConfig, true);
        store.load();
    },

    onFilterReset: function() {
        var store = this.getStore('Audit');
        this.lookup('auditListFilters').reset();
        store.clearFilter(true);

        store.addFilter({
            property: 'system_component_id',
            operator: 'in',
            value: this.getTreeSelection()
        }, true);

        store.load();
    },

    onItemDblClick: function(grid, record) {
        this.showCard(record);
    },

    onOpen: function() {
        var selection = this.getViewModel().get('auditGrid.selection');

        if (selection) {
            this.showCard(selection);
        }
    },

    showCard: function(record) {
        var view = this.getView(),
            itemId = this.getTabItemId(record.getId()),
            tab = view.items.getByKey(itemId);

        if (tab) {
            view.setActiveTab(tab);
        } else {
            tab = view.add({
                xtype: 'audit-card',
                itemId: itemId,
                viewModel: {
                    parent: this.getViewModel(),
                    data: {
                        theAudit: record
                    }
                }
            });

            view.setActiveTab(tab);
            tab.fireEvent('load');
        }
    },

    getTabItemId: function(id) {
        return 'audit_' + id;
    },

    /**
     * @returns {Array}
     */
    getTreeSelection: function() {
        var res = [],
            me = this;
            selection = this.lookup('systemComponentTree').getSelection();

        if (selection) {
            selection.forEach(function(item) {
                res.push(item.getId());
                me.getViewModel().set('component_tree_selected_id', item.getId());
            })
        }

        return res;
    },

    onLoadSystemComponent: function (store, records) {
        if (Ext.isEmpty(records)) return;

        var me = this,
            view = me.lookup('systemComponentTree'),
            currentSusId = me.getViewModel().get('current_sus_id'),
            previousSelectedId = me.getViewModel().get('component_tree_selected_id'),
            record = previousSelectedId ? store.getNodeById(previousSelectedId) : store.getNodeById(currentSusId);

        view.expandAll(function () {
            if (record) {
                record.set('selected', true);
                view.getSelectionModel().select(record);
            }
        });
    }
});
