/**
 *
 */
Ext.define('Pluton.view.audit.Audit', {
    extend: 'Ext.tab.Panel',

    permissions: 'audit_page',

    xtype: 'events.audit',

    requires: [
        'Ext.layout.container.Border',
        'Ext.panel.Panel',
        'Ext.tree.Panel',
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.audit.AuditController',
        'Pluton.view.audit.AuditModel',
        'Pluton.view.audit.grid.AuditGrid'
    ],

    controller: 'audit',
    viewModel: {
        type: 'audit'
    },

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onActivate'
    },
    defaults: {
        closable: true
    },

    items: [
        {
            title: 'Журнал аудита',
            xtype: 'panel',
            layout: 'border',
            closable: false,
            items: [
                {
                    reference: 'systemComponentTree',
                    xtype: 'treepanel',
                    region: 'east',
                    width: 300,
                    rootVisible: false,
                    split: true,
                    collapsible: true,
                    header: false,
                    useArrows: true,
                    displayField: 'name',
                    bodyStyle: {
                        borderTop: '0px'
                    },
                    bind: {
                        store: '{SystemComponent}'
                    },
                    listeners: {
                        load: 'onLoadSystemComponent',
                        selectionchange: 'onFilterApply'
                    }
                },
                {
                    xtype: 'audit-grid',
                    region: 'center'
                }
            ]
        }
    ]
});
