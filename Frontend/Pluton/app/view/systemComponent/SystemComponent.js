/**
 *
 */
Ext.define('Pluton.view.systemComponent.SystemComponent', {
    extend: 'Ext.tab.Panel',

    permissions: 'system-component_page',
    xtype: 'settings.system-component',

    requires: [
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.systemComponent.SystemComponentController',
        'Pluton.view.systemComponent.SystemComponentModel',
        'Pluton.view.systemComponent.grid.SystemComponentGrid'
    ],

    viewModel: {
        type: 'system-component'
    },
    controller: 'system-component',

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onActivate',
        tabchange: 'onTabChange'
    },
    defaults: {
        closable: true
    },

    items: [
        {
            xtype: 'system-component-grid',
            closable: false
        }
    ]
});
