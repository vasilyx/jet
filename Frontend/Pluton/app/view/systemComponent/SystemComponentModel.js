/**
 *
 */
Ext.define('Pluton.view.systemComponent.SystemComponentModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.system-component',

    requires: [
        'Pluton.store.systemComponent.SystemComponent'
    ],

    stores: {
        SystemComponent: {
            type: 'system-component'
        }
    }
});
