/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.SystemComponentCard', {
    extend: 'Ext.tab.Panel',

    xtype: 'system-component-card',

    requires: [
        'Pluton.view.systemComponent.card.SystemComponentCardController',
        'Pluton.view.systemComponent.card.SystemComponentCardModel',
        'Pluton.view.systemComponent.card.tab.integrityControl.IntegrityControl',
        'Pluton.view.systemComponent.card.tab.main.SystemComponentMain',
        'Pluton.view.systemComponent.card.tab.updateRegistry.UpdateRegistry',
        'Pluton.view.systemComponent.card.tab.updatesRegistration.UpdatesRegistration'
    ],

    controller: 'system-component-card',
    viewModel: {
        type: 'system-component-card'
    },

    bind: {
        title: '{systemComponentTitle}'
    },

    plain: true,
    cls: 'child-tab-panel',
    listeners: {
        activate: 'onActivate',
        tabchange: 'onTabChange'
    },

    items: [
        {
            xtype: 'system-component-main'
        },
        {
            xtype: 'integrity-control'
        },
        {
            xtype: 'updates-registration'
        },
        {
            xtype: 'update-registry'
        }
    ]
});
