/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.SystemComponentCardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.system-component-card',

    data: {
        theSystemComponent: null,
        privateComponent: false
    },

    formulas: {
        systemComponentId: {
            bind: {
                bindTo: '{theSystemComponent}',
                deep: true
            },
            get: function(theSystemComponent) {
                if (theSystemComponent) {
                    return theSystemComponent.getId();
                } else {
                    return null;
                }
            }
        },
        systemComponentTitle: {
            bind: {
                bindTo: '{theSystemComponent}',
                deep: true
            },
            get: function(theSystemComponent) {
                if (theSystemComponent) {
                    return theSystemComponent.get('name');
                } else {
                    return '';
                }
            }
        }
    }
});
