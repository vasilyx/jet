/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.SystemComponentCardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.system-component-card',

    requires: [
        'Pluton.model.systemComponent.SystemComponent',
        'Pluton.view.systemComponent.card.tab.monitoredSystem.card.MonitoredSystemCard',
        'Pluton.view.systemComponent.card.tab.monitoredSystem.grid.MonitoredSystemGrid'
    ],

    initViewModel: function (vModel) {
        var systemComponentId = vModel.get('systemComponentId'),
            theSystemComponent = new Pluton.model.systemComponent.SystemComponent();

        theSystemComponent.setId(systemComponentId);
        vModel.set('theSystemComponent', theSystemComponent);
    },

    onTabChange:function ( tabPanel, newCard, oldCard, eOpts ) {
        if (this.getViewModel().get('privateComponent') !== true ){
            this.fireEvent('subTabChange');
        }
    },

    onActivate: function (view) {
        var theSystemComponent = this.getViewModel().get('theSystemComponent');

        view.setLoading();
        theSystemComponent.load({
            success: 'onSystemComponentLoad',
            callback: function () {
                view.setLoading(false);
            },
            scope: this
        });
    },

    onSystemComponentLoad: function (theSystemComponent) {
        var view = this.getView(),
            componentType = theSystemComponent.get('component_type'),
            componentTypeId = Ext.isObject(componentType) && componentType.id,
            monitoredSystemTab;

        switch (componentTypeId) {
            case 'SCS':
            case 'COMPLEX':
                monitoredSystemTab = 'monitored-system-grid';
                break;
            case 'SENSOR':
            case 'PORTABLE':
                monitoredSystemTab = 'monitored-system-card';
                break;
        }

        if (monitoredSystemTab && !view.child(monitoredSystemTab)) {
            view.add({
                xtype: monitoredSystemTab
            });

            if (this.getViewModel().get('privateComponent') !== true ){
                this.fireEvent('subTabLoaded', monitoredSystemTab);
            }
        }
    }
});
