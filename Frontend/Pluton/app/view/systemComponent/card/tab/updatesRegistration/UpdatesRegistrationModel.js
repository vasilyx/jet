/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.updatesRegistration.UpdatesRegistrationModel', {
    extend: 'Ext.app.ViewModel',
 
    alias: 'viewmodel.updates-registration',

    data: {
        updatesRegistrationState: {
            result: null,
            message: null
        }
    },

    formulas: {
        operation_status: {
            bind: {
                bindTo: '{updatesRegistrationState}',
                deep: true
            },
            get: function (updatesRegistrationState) {
                var result = updatesRegistrationState.result;

                if (Ext.isEmpty(result)) {
                    return '';
                } else if (result) {
                    return 'Нет ошибок';
                } else {
                    return 'Ошибка';
                }
            }
        }
    }
});
