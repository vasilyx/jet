/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.updatesRegistration.UpdatesRegistrationController', {
    extend: 'Ext.app.ViewController', 
 
    alias: 'controller.updates-registration',

    requires: [
        'Pluton.model.systemComponent.UpdateServerAgent'
    ],

    onActivate: function () {
        var view = this.getView(),
            vModel = view.getViewModel(),
            systemComponentId = vModel.get('systemComponentId'),
            model = Pluton.model.systemComponent.UpdateServerAgent,
            deferred = new Ext.Deferred();

        view.setLoading();
        this.lookup('keyField').setValue();
        model.load(systemComponentId, {
            callback: function(record, operation, success) {
                if (!success) {
                    record = new model({system_component_id: systemComponentId})
                }
                vModel.set('theAgent', record);
                view.setLoading(false);
                deferred.resolve(record);
            }
        });

        return deferred.promise;
    },

    onSaveBtn: function () {
        this.saveAgent();
    },

    onSendRequestBtn: function () {
        this.saveAgent().then(this.sendRequest.bind(this));
    },

    onCheckBtn: function () {
        this.saveAgent().then(this.checkKey.bind(this));
    },

    saveAgent: function () {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            theAgent = vModel.get('theAgent'),
            deferred = new Ext.Deferred();

        if(theAgent && theAgent.isDirty()) {
            view.setLoading();
            theAgent.save({
                callback: function(record, operation, success) {
                    if (success) {
                        me.onActivate().then(function() {
                            deferred.resolve(record);
                        });
                    } else {
                        view.setLoading(false);
                        deferred.reject();
                    }
                }
            });
        } else {
            deferred.resolve(theAgent);
        }

        return deferred.promise;
    },

    sendRequest: function () {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            systemComponentId = vModel.get('systemComponentId');

        view.setLoading();
        Ext.Ajax.request({
            url: '/update_server/register',
            method: 'POST',
            jsonData: {
                system_component_id: systemComponentId
            }
        }).then(
            function (response) {
                var json = Ext.JSON.decode(response.responseText, true);
                vModel.set('updatesRegistrationState', json);
                if (json.result) {
                    me.onActivate();
                } else {
                    view.setLoading(false);
                }
            },
            function (response) {
                var json = Ext.JSON.decode(response.responseText, true);
                vModel.set('updatesRegistrationState', Ext.applyIf(json, {result: false}));
                view.setLoading(false);
            }
        );
    },

    checkKey: function () {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            key = vModel.get('keyField.value'),
            systemComponentId = vModel.get('systemComponentId');

        view.setLoading();
        Ext.Ajax.request({
            url: '/update_server/check',
            method: 'POST',
            jsonData: {
                system_component_id: systemComponentId,
                api_key: key
            }
        }).then(
            function (response) {
                var json = Ext.JSON.decode(response.responseText, true);
                vModel.set('updatesRegistrationState', json);
                if (json.result) {
                    me.onActivate();
                } else {
                    view.setLoading(false);
                }
            },
            function (response) {
                var json = Ext.JSON.decode(response.responseText, true);
                vModel.set('updatesRegistrationState', Ext.applyIf(json, {result: false}));
                view.setLoading(false);
            }
        );
    },

    onFieldChange: function () {
        this.getViewModel().set('updatesRegistrationState', {});
    },

    statusRenderer: function (value) {
        if (!value) {
            return ' - ';
        } else {
            return value;
        }
    }
});
