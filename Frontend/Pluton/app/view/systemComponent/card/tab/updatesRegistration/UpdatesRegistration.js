/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.updatesRegistration.UpdatesRegistration', {
    extend: 'Ext.panel.Panel',

    xtype: 'updates-registration',
    permissions: 'update-registration_page',
    requires: [
        'Ext.button.Button',
        'Ext.container.Container',
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'Ext.layout.container.Center',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Pluton.view.systemComponent.card.tab.updatesRegistration.UpdatesRegistrationController',
        'Pluton.view.systemComponent.card.tab.updatesRegistration.UpdatesRegistrationModel'
    ],

    controller: 'updates-registration',
    viewModel: {
        type: 'updates-registration'
    },

    listeners: {
        activate: 'onActivate'
    },

    title: 'Регистрация на сервере обновлений',
    bodyPadding: 10,
    layout: 'center',
    scrollable: true,

    items: {
        xtype: 'container',
        type: 'vbox',
        margin: '0 10 0 0',

        items: {
            xtype: 'form',
            width: '75%',
            height: '100%',
            minWidth: 800,
            bodyPadding: '10 30 0 30',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            fieldDefaults: {
                labelWidth: 250
            },
            items: [
                {
                    reference: 'statEField',
                    xtype: 'displayfield',
                    fieldLabel: 'Статус регистрации',
                    renderer: Ext.labelRenderer,
                    bind: {
                        value: '{theAgent.state}'
                    }
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    margin: '0 0 10 0',
                    bind: {
                        hidden: '{theAgent.state.id != "REQUESTED"}'
                    },
                    items: [
                        {
                            reference: 'keyField',
                            publishes: ['value'],
                            xtype: 'textfield',
                            fieldLabel: 'APIKey',
                            emptyText: 'Введите APIKey, высланный на Вашу почту',
                            flex: 1,
                            margin: '0 5 0 0',
                            listeners: {
                                change: 'onFieldChange'
                            }
                        },
                        {
                            xtype: 'button',
                            ui: 'md-yellow',
                            text: 'Зарегистрировать',
                            tooltip: 'Проверить и зарегистрировать APIKey агента обновления',
                            handler: 'onCheckBtn',
                            bind: {
                                disabled: '{!keyField.value}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Действительное наименование войсковой части',
                    flex: 1,
                    allowBlank: false,
                    bind: {
                        value: '{theAgent.dname}',
                        readOnly: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    listeners: {
                        change: 'onFieldChange'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Сокращенное действительное наименование войсковой части',
                    flex: 1,
                    allowBlank: false,
                    bind: {
                        value: '{theAgent.short_dname}',
                        readOnly: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    listeners: {
                        change: 'onFieldChange'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Условное наименование войсковой части',
                    flex: 1,
                    emptyText: '00000',
                    allowBlank: false,
                    bind: {
                        value: '{theAgent.uname}',
                        readOnly: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    listeners: {
                        change: 'onFieldChange'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Сокращенное действительное наименование вышестоящего штаба',
                    flex: 1,
                    allowBlank: false,
                    bind: {
                        value: '{theAgent.parent_dname}',
                        readOnly: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    listeners: {
                        change: 'onFieldChange'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Адрес войсковой части с указанием индекса',
                    flex: 1,
                    allowBlank: false,
                    bind: {
                        value: '{theAgent.address}',
                        readOnly: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    listeners: {
                        change: 'onFieldChange'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Воинское звание в сокращенном виде и ФИО ответственного должностного лица ',
                    flex: 1,
                    emptyText: 'л-т Иванов Иван Иванович',
                    allowBlank: false,
                    bind: {
                        value: '{theAgent.resp}',
                        readOnly: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    listeners: {
                        change: 'onFieldChange'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Телефон ответственного должностного лица',
                    flex: 1,
                    allowBlank: false,
                    bind: {
                        value: '{theAgent.tel}',
                        readOnly: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    listeners: {
                        change: 'onFieldChange'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Адрес электронной почты в ЗС СПД ответственного должностного лица',
                    flex: 1,
                    allowBlank: false,
                    bind: {
                        value: '{theAgent.email}',
                        readOnly: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    listeners: {
                        change: 'onFieldChange'
                    }
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    bind: {
                        hidden: '{theAgent.state.id != "UNREGISTERED"}'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Отправить запрос',
                            ui: 'md-yellow',
                            tooltip: 'Отправить запрос на регистрацию на сервер обновлений',
                            handler: 'onSendRequestBtn',
                            margin: '0 10 0 0',
                            flex: 1,
                            formBind: true
                        },
                        {
                            xtype: 'button',
                            text: 'Сохранить данные',
                            handler: 'onSaveBtn',
                            bind: {
                                disabled: '{!theAgent.dirty}'
                            },
                            flex: 1
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'vbox',
                    margin: '0 0 10 0',
                    bind: {
                        hidden: '{theAgent.state.id == "REGISTERED"}'
                    },
                    items: [
                        {
                            reference: 'statusField',
                            xtype: 'displayfield',
                            fieldLabel: 'Статус операции',
                            flex: 1,
                            bind: {
                                value: '{operation_status}'
                            },
                            renderer: 'statusRenderer'
                        },
                        {
                            reference: 'messageField',
                            xtype: 'displayfield',
                            fieldLabel: 'Описание ошибки',
                            flex: 1,
                            bind: {
                                value: '{updatesRegistrationState.message}'
                            },
                            renderer: 'statusRenderer'
                        }
                    ]
                }
            ]
        }
    }
});
