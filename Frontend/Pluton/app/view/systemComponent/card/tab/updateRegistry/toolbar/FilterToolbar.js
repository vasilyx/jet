/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.updateRegistry.toolbar.FilterToolbar', {
    extend: 'Ext.toolbar.Toolbar',

    xtype: 'update-registry-filter-toolbar',

    requires: [
        'Ext.button.Button',
        'Ext.container.Container',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Pluton.ux.container.DateRangeContainer'
    ],

    items: [
        {
            title: 'Фильтры',
            xtype: 'fieldset',
            ui: 'fieldset-without-padding',
            style: {
                background: 'transparent'
            },
            collapsible: true,
            collapsed: true,
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    reference: 'updateRegistryFilter',
                    xtype: 'form',
                    layout: 'hbox',
                    scrollable: true,
                    defaults: {
                        flex: 1,
                        minWidth: 300
                    },
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    listeners: {
                        validitychange: 'onFilterValidityChange'
                    },
                    bbar: {
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                text: 'Применить',
                                tooltip: 'Фильтровать по выбранным параметрам',
                                ui: 'md-blue',
                                handler: 'onFilterApply',
                                bind: {
                                    disabled: '{!isFilterValid}'
                                }
                            },
                            {
                                text: 'Сбросить',
                                tooltip: 'Сброс фильтров',
                                handler: 'onFilterReset'
                            }
                        ]
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'update_type',
                                    xtype: 'combo',
                                    fieldLabel: 'Тип',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    bind: {
                                        store: '{UpdateType}'
                                    }
                                },
                                {
                                    name: 'version',
                                    xtype: 'textfield',
                                    fieldLabel: 'Версия'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'update_status',
                                    xtype: 'combo',
                                    fieldLabel: 'Статус',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    bind: {
                                        store: '{UpdateStatus}'
                                    }
                                },
                                {
                                    name: 'user_label_',
                                    xtype: 'textfield',
                                    fieldLabel: 'Пользователь'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'date-range-container',
                                    name: 'discover_timestamp',
                                    fromFieldConfig: {
                                        fieldLabel: 'Обнаружено'
                                    }
                                },
                                {
                                    xtype: 'date-range-container',
                                    name: 'install_timestamp',
                                    fromFieldConfig: {
                                        fieldLabel: 'Установлено'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});
