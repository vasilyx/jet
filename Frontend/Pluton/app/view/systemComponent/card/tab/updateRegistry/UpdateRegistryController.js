/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.updateRegistry.UpdateRegistryController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.update-registry',

    onBeforeStoreLoad: function (store) {
        var systemComponentId = this.getViewModel().get('systemComponentId');

        if (systemComponentId) {
            store.getProxy().setExtraParam('system_component_id', systemComponentId);
            return true;
        } else {
            return false;
        }
    },

    onActivate: function () {
        this.getStore('UpdateRegistry').load();
    },

    onRefresh: function () {
        this.getStore('UpdateRegistry').reload();
    },

    onFilterValidityChange: function (form, valid) {
        this.getViewModel().set('isFilterValid', valid);
    },

    onFilterApply: function () {
        var store = this.getStore('UpdateRegistry'),
            values = this.lookup('updateRegistryFilter').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function (key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'user_label_':
                    case 'version':
                        operator = 'like';
                        break;
                    case 'update_type':
                    case 'update_status':
                        operator = '=';
                        break;
                    case 'install_timestamp':
                    case 'discover_timestamp':
                        operator = 'between';
                        break;
                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            } else {
                store.removeFilter(key, true);
            }
        }, this);

        store.addFilter(filtersConfig, true);
        store.load();
    },

    onFilterReset: function () {
        this.lookup('updateRegistryFilter').reset();
        this.getStore('UpdateRegistry').clearFilter();
    }
});
