/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.updateRegistry.UpdateRegistry', {
    extend: 'Ext.grid.Panel',

    xtype: 'update-registry',
    permissions: 'update-registry_page',
    requires: [
        'Ext.grid.column.Date',
        'Ext.toolbar.Paging',
        'Pluton.utility.button.CSVExport',
        'Pluton.view.systemComponent.card.tab.updateRegistry.UpdateRegistryController',
        'Pluton.view.systemComponent.card.tab.updateRegistry.UpdateRegistryModel',
        'Pluton.view.systemComponent.card.tab.updateRegistry.toolbar.FilterToolbar'
    ],

    controller: 'update-registry',
    viewModel: {
        type: 'update-registry'
    },

    bind: {
        store: '{UpdateRegistry}'
    },

    title: 'Реестр обновлений',
    listeners: {
        activate: 'onActivate'
    },

    tbar: {
        xtype: 'update-registry-filter-toolbar'
    },
    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefresh'
        },
        {
            xtype: 'utility-button-csv-export'
        }
    ],
    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{UpdateRegistry}'
                }
            }
        ]
    },

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            { dataIndex: 'version', text: 'Версия' },
            { dataIndex: 'update_type', text: 'Тип', renderer: Ext.labelRenderer },
            { dataIndex: 'update_status', text: 'Статус', renderer: Ext.labelRenderer },
            { dataIndex: 'error', text: 'Ошибка' },
            { dataIndex: 'discover_timestamp', text: 'Дата/время<br>обнаружения', xtype: 'datecolumn', format: Ext.defaultDateFormat },
            { dataIndex: 'install_timestamp', text: 'Дата/время<br>установки', xtype: 'datecolumn', format: Ext.defaultDateFormat },
            { dataIndex: 'update_filename', text: 'Имя файла' },
            { dataIndex: 'url', text: 'Путь к файлу', flex: 2 },
            { dataIndex: 'size', text: 'Размер, байт' },
            { dataIndex: 'checksum', text: 'Фактическая<br>контрольная сумма' },
            { dataIndex: 'description', text: 'Комментарий' }
        ]
    }
});
