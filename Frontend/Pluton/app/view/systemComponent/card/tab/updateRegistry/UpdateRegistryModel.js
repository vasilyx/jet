/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.updateRegistry.UpdateRegistryModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.update-registry',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.systemComponent.UpdateRegistry',
        'Pluton.model.voc.Label'
    ],

    stores: {
        UpdateRegistry: {
            model: 'Pluton.model.systemComponent.UpdateRegistry',
            remoteSort: true,
            remoteFilter: true,
            sorters: {
                property: 'discover_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/update/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        },
        UpdateStatus: {
            model: 'Pluton.model.voc.Label',
            sorters: 'label',
            autoLoad: true,
            proxy: {
                type: 'json-ajax',
                url: '/update/status/list'
            }
        },
        UpdateType: {
            model: 'Pluton.model.voc.Label',
            sorters: 'label',
            autoLoad: true,
            proxy: {
                type: 'json-ajax',
                url: '/update/types/list'
            }
        }
    },

    data: {
        isFilterValid: true
    }
});
