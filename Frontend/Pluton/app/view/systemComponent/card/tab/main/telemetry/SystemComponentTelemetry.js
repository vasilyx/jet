/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.main.telemetry.SystemComponentTelemetry', {
    extend: 'Ext.panel.Panel',

    xtype: 'system-component-telemetry',

    requires: [
        'Ext.container.Container',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Pluton.view.systemComponent.card.tab.main.telemetry.SystemComponentTelemetryController',
        'Pluton.view.systemComponent.card.tab.main.telemetry.TelemetryCircleBase'
    ],

    controller: 'system-component-telemetry',

    title: 'Состояние',
    bodyPadding: 10,
    scrollable: true,
    layout: {
        type: 'vbox',
        align: 'middle'
    },
    listeners: {
        added: 'onAdded'
    },

    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefreshBtn'
        }
    ],

    items: [
        {

            reference: 'updateDateContainer',
            xtype: 'container',
            margin: '0 0 10 0'
        },
        {
            xtype: 'container',
            layout: 'hbox',
            defaults: {
                minWidth: 120,
                flex: 1
            },
            items: [
                {
                    xtype: 'system-component-telemetry-circle-base',
                    legend: 'ОЗУ',
                    bind: {
                        value: '{theSystemComponent.health.RAM}'
                    }
                },
                {
                    xtype: 'system-component-telemetry-circle-base',
                    legend: 'ЦПУ',
                    bind: {
                        value: '{theSystemComponent.health.CPU}'
                    }
                },
                {
                    xtype: 'system-component-telemetry-circle-base',
                    legend: 'Файл подкачки',
                    bind: {
                        value: '{theSystemComponent.health.SWAP}'
                    }
                }
            ]
        }
    ]
});
