/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.main.signatures.SignaturesGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'system-component-signatures-grid',

    requires: [
        'Ext.container.Container',
        'Ext.form.field.Text',
        'Ext.grid.column.Check',
        'Ext.layout.container.VBox',
        'Ext.toolbar.Paging',
        'Pluton.view.systemComponent.card.tab.main.signatures.SignaturesGridController',
        'Pluton.view.systemComponent.card.tab.main.signatures.SignaturesGridModel'
    ],

    controller: 'system-component-signatures-grid',
    viewModel: {
        type: 'system-component-signatures-grid'
    },

    bind: {
        store: '{SystemComponentRule}'
    },

    title: 'Сигнатуры',

    tbar: [
        {
            reference: 'searchField',
            publishes: ['value'],
            xtype: 'textfield',
            emptyText: 'Введите SID, Имя или Класс сигнатуры, минимум 3 символа',
            flex: 1,
            triggers: {
                search: {
                    cls: Ext.baseCSSPrefix + 'form-search-trigger',
                    handler: 'onSearchTrigger'
                }
            },
            listeners: {
                specialkey: 'onSearchSpecialKey'
            }
        }
    ],

    bbar: {
        xtype: 'container',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'toolbar',
                bind: {
                    hidden: '{!editingAvailable}',
                    disabled: '{!editingAvailable}'
                },
                items: [
                    {
                        text: 'Применить',
                        tooltip: 'Применить изменения',
                        ui: 'md-blue',
                        disabled: true,
                        bind: {
                            disabled: '{!hasModified}'
                        },
                        handler: 'onApplyBtn'
                    },
                    {
                        text: 'Отменить',
                        tooltip: 'Отменить изменения',
                        disabled: true,
                        bind: {
                            disabled: '{!hasModified}'
                        },
                        handler: 'onCancelBtn'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                layout: {
                    pack: 'center'
                },
                items: [
                    {
                        reference: 'pagingtoolbar',
                        xtype: 'pagingtoolbar',
                        displayInfo: true,
                        bind: {
                            store: '{SystemComponentRule}'
                        }
                    }
                ]
            }
        ]
    },

    columns: [
        {
            xtype: 'checkcolumn',
            headerCheckbox: true,
            dataIndex: 'is_active',
            text: 'Активировать',
            sortable: false,
            hideable: false,
            width: 120,
            bind: {
                hidden: '{!editingAvailable}',
                disabled: '{!editingAvailable}'
            }
        },
        {
            text: 'SID',
            dataIndex: 'sid',
            flex: 2
        },
        {
            text: 'Имя',
            dataIndex: 'signature_name',
            flex: 4
        },
        {
            text: 'Источник',
            dataIndex: 'source',
            flex: 1
        },
        {
            text: 'Класс',
            dataIndex: 'class_name',
            flex: 1
        },
        {
            text: 'Приоритет',
            dataIndex: 'priority',
            flex: 1
        },
        {
            text: 'Реакция на событие',
            dataIndex: 'reaction_name',
            flex: 1
        },
        {
            text: 'Обновление',
            dataIndex: 'update_version',
            flex: 1
        }
    ]
});