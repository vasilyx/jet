/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.main.signatures.SignaturesGridModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.system-component-signatures-grid',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.systemComponent.SystemComponentRule'
    ],

    stores: {
        SystemComponentRule: {
            model: 'Pluton.model.systemComponent.SystemComponentRule',
            pageSize: 15,
            remoteSort: true,
            remoteFilter: true,
            proxy: {
                type: 'json-ajax',
                url: '/system_component/rules/get',
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty: 'total'
                }
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        },
        SystemComponentRuleUpdate: {
            model: 'Pluton.model.systemComponent.SystemComponentRule',
            proxy: {
                type: 'json-ajax',
                url: '/system_component/rules/update'
            }
        }
    },

    formulas: {
        hasModified: {
            bind: {
                bindTo: '{SystemComponentRule}',
                deep: true
            },
            get: function (store) {
                return store && store.getModifiedRecords().length > 0;
            }
        },
        editingAvailable: {
            bind: {
                bindTo: '{theSystemComponent}',
                deep: true
            },
            get: function (theSystemComponent) {
                var editingAvailable = false,
                    componentType,
                    status;

                if (theSystemComponent) {
                    componentType = Ext.isObject(theSystemComponent.get('component_type')) && theSystemComponent.get('component_type').id;
                    status = Ext.isObject(theSystemComponent.get('status')) && theSystemComponent.get('status').id;

                    editingAvailable = componentType === 'SENSOR' && !(status === 'COMPROMISED' || status === 'PASSIVE');
                }

                return editingAvailable;
            }
        }
    }
});