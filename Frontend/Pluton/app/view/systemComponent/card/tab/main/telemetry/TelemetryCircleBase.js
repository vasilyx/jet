/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.main.telemetry.TelemetryCircleBase', {
    extend: 'Ext.container.Container',

    xtype: 'system-component-telemetry-circle-base',

    requires: [
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.layout.container.VBox'
    ],

    config: {
        legend: null,
        value: null
    },
    isCircle: true,
    baseCircleCls: 'base-circle-telemetry',
    valueCls: 'base-value-telemetry',
    notFoundText: 'Нет данных',

    layout: {
        type: 'vbox',
        align: 'middle',
        pack: 'center'
    },

    initComponent: function () {
        var me = this,
            createWidget = Ext.widget,
            valueContainerConfig = {
                itemId: 'valueContainer',
                xtype: 'container',
                width: 100,
                height: 100,
                layout: {
                    type: 'vbox',
                    align: 'middle',
                    pack: 'center'
                }
            },
            valueFieldConfig = {
                itemId: 'valueField',
                xtype: 'component',
                cls: me.valueCls,
                style: {
                    textAlign: 'center'
                }
            },
            legendFieldConfig = {
                itemId: 'legend',
                xtype: 'label',
                style: {
                    textAlign: 'center'
                },
                html: me.getLegend()
            },
            valueContainer, valueField, legendField;

        if (me.isCircle) {
            valueContainerConfig.cls = me.baseCircleCls;
        }

        me.valueField = valueField = createWidget(valueFieldConfig);
        me.valueContainer = valueContainer = createWidget(Ext.apply(valueContainerConfig, {
            items: [valueField]
        }));
        me.legendField = legendField = createWidget(legendFieldConfig);

        me.items = [valueContainer, legendField];

        me.callParent(arguments)
    },

    setLegend: function (newLegend) {
        var me = this,
            oldLegend = me.getLegend();

        me.legend = newLegend;

        if (Ext.isFunction(me.onLegendChange)) {
            me.onLegendChange.call(me, newLegend, oldLegend);
        }
    },

    setValue: function (newValue) {
        var me = this,
            oldValue = me.getValue();

        me.value = newValue;

        if (Ext.isFunction(me.onValueChange)) {
            me.onValueChange.call(me, newValue, oldValue);
        }
    },

    onLegendChange: function (newLegend, oldLegend) {
        var me = this,
            legendField = me.legendField;

        if (legendField) {
            legendField.setHtml(newLegend);
        }
    },

    onValueChange: function (value) {
        var me = this,
            valueField = me.valueField,
            valueContainer = me.valueContainer,
            legend = value.hasOwnProperty('value') ? value.value : null,
            state = value.hasOwnProperty('state') ? value.state : null;

        valueContainer.toggleCls('background-green', state === 0);
        valueContainer.toggleCls('background-yellow', state === 1);
        valueContainer.toggleCls('background-red', state === 2);
        valueField.toggleCls('color-black', state === 1);
        valueField.toggleCls('color-white', state !== 1);

        if (Ext.isEmpty(value)) {
            legend = me.notFoundText;
            valueField.removeCls(me.valueCls);
        }

        valueField.setHtml('' + legend + '%');
    }
});
