/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.main.telemetry.SystemComponentTelemetryController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.system-component-telemetry',

    onAdded: function () {
        var dt = new Date();
        this.lookup('updateDateContainer').setHtml('Обновлено: ' + Ext.Date.format(dt, Ext.Date.defaultFormat));
    },

    onRefreshBtn: function () {
        var view = this.getView(),
            theSystemComponent = this.getViewModel().get('theSystemComponent');

        view.setLoading();
        theSystemComponent.load({
            callback: function () {
                // TODO should be date in response
                var dt = new Date();
                this.lookup('updateDateContainer').setHtml('Обновлено: ' + Ext.Date.format(dt, Ext.Date.defaultFormat));
                view.setLoading(false);
            },
            scope: this
        });
    }
});
