/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.main.SystemComponentMain', {
    extend: 'Ext.form.Panel',

    xtype: 'system-component-main',

    requires: [
        'Ext.layout.container.Accordion',
        'Ext.panel.Panel',
        'Pluton.view.systemComponent.card.tab.main.basic.SystemComponentBasic',
        'Pluton.view.systemComponent.card.tab.main.signatures.SignaturesGrid',
        'Pluton.view.systemComponent.card.tab.main.telemetry.SystemComponentTelemetry'
    ],

    title: 'Главное',
    bodyPadding: 10,
    scrollable: true,
    layout: {
        type: 'accordion',
        fill: false,
        multi: true,
        reserveScrollbar: true
    },
    defaults: {
        collapsed: true,
        scrollable: true,
        header: {
            titlePosition: 1
        }
    },
    fieldDefaults: {
        labelWidth: 180
    },

    items: [
        {
            xtype: 'panel',
            collapsed: false,
            minHeight: 0,
            maxHeight: 0,
            margin: 0,
            padding: 0
        },
        {
            xtype: 'system-component-basic',
            collapsed: false
        },
        {
            xtype: 'system-component-signatures-grid',
            minHeight: 750
        },
        {
            xtype: 'system-component-telemetry'
        }
    ]
});
