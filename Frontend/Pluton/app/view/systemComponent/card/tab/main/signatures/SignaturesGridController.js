/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.main.signatures.SignaturesGridController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.system-component-signatures-grid',

    requires: [
        'Ext.event.Event'
    ],

    control: {
        '#': {
            expand: 'onExpand'
        },
        'system-component': {
            setSignatureFilter: 'onSetFilter'
        }
    },

    listen: {
        controller: {
            'system-component': {
                setSignatureFilter: 'onSetFilter'
            }
        }
    },

    bindings: {
        onSearchValueChange: '{searchField.value}'
    },

    currentPage: null,

    onExpand: function () {
        var store = this.getView().getStore();

        if (!store.isLoaded()) {
            store.load();
        }
    },

    onSetFilter: function(filter) {
        this.getView().expand();
        this.lookupReference('searchField').setValue(filter);
        this.doSearch();
    },

    onBeforeStoreLoad: function (store, operation) {
        var me = this,
            systemComponentId = me.getViewModel().get('systemComponentId');

        if (systemComponentId) {
            store.getProxy().setExtraParam('system_component_id', systemComponentId);

            if (store.getModifiedRecords().length > 0) {
                me.currentPage = operation.getPage();
                Ext.MessageBox.confirm(
                    'Подтвердите действие',
                    'Сохранить внесенные изменения?',
                    function(msg) {
                        if (msg === 'yes') {
                            me.saveSelected(true);
                        } else {
                            store.rejectChanges();
                            store.load();
                        }
                    }
                );

                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    },

    onSearchValueChange: function (newValue) {
        this.validateSearchValue(newValue);
    },

    onSearchTrigger: function () {
        this.doSearch();
    },

    onSearchSpecialKey: function (field, e) {
        if (e.getKey() === Ext.event.Event.ENTER) {
            this.doSearch();
        }
    },

    onApplyBtn: function () {
        this.saveSelected();
    },

    onCancelBtn: function () {
        var store = this.getStore('SystemComponentRule');

        Ext.MessageBox.confirm(
            'Подтвердите действие',
            'Отменить внесенные изменения?',
            function(msg) {
                if (msg === 'yes') {
                    store.rejectChanges()
                }
            }
        );
    },

    doSearch: function () {
        var me = this,
            vModel = me.getViewModel(),
            store = vModel.getStore('SystemComponentRule'),
            extraParams = store.getProxy().getExtraParams(),
            searchValue = me.validateSearchValue(vModel.get('searchField.value'));

        if (searchValue !== false) {
            if (searchValue) {
                extraParams['filters'] = {
                    operation: 'OR',
                    operands: [
                        {
                            field: 'sid',
                            type: 'like',
                            value: searchValue
                        },
                        {
                            field: 'signature_name',
                            type: 'like',
                            value: searchValue
                        },
                        {
                            field: 'class_name',
                            type: 'like',
                            value: searchValue
                        }
                    ]
                };
            } else {
                if (extraParams.hasOwnProperty('filters')) {
                    delete extraParams.filters;
                }
            }

            store.load();
        }
    },

    validateSearchValue: function (searchValue) {
        var searchTrigger = this.lookup('searchField').getTrigger('search');

        searchValue = Ext.String.trim(searchValue) || null;

        if (!searchValue || searchValue.length >= 3) {
            searchTrigger.show();
            return searchValue;
        } else {
            searchTrigger.hide();
            return false;
        }
    },

    saveSelected: function (needReload) {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            store = vModel.getStore('SystemComponentRule'),
            modifiedRecords = store.getModifiedRecords(),
            ruleState = {};

        view.setLoading();
        Ext.Array.forEach(modifiedRecords, function (record) {
            ruleState[record.getId()] = {
                is_active: record.get('is_active')
            };
        });
        Ext.Ajax.request({
            url: '/system_component/rules/update',
            method: 'POST',
            jsonData: {
                system_component_id: vModel.get('systemComponentId'),
                rule_state: ruleState
            }
        }).then(function () {
            store.commitChanges();
            if (needReload) {
                store.loadPage(me.currentPage || 1);
            }
        }).always(function () {
            view.setLoading(false);
        });
    }
});
