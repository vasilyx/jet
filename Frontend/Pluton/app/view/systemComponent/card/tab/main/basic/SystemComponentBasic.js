/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.main.basic.SystemComponentBasic', {
    extend: 'Ext.panel.Panel',

    xtype: 'system-component-basic',

    requires: [
        'Ext.container.Container',
        'Ext.form.field.Display',
        'Ext.layout.container.Center',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.toolbar.Spacer',
        'Ext.util.Format'
    ],

    title: 'Основное',
    bodyPadding: '10 10 0 10',
    layout: 'center',

    items: {
        xtype: 'container',
        width: '70%',
        height: '100%',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Идентификатор',
                        bind: {
                            value: '{systemComponentId}'
                        }
                    },
                    {
                        fieldLabel:  'Статус',
                        bind: {
                            value: '{theSystemComponent.status.label}'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Имя',
                        bind: {
                            value: '{theSystemComponent.name}'
                        }
                    },
                    {
                        fieldLabel:  'Дата регистрации',
                        renderer: Ext.util.Format.dateRenderer(Ext.Date.defaultFormat),
                        bind: {
                            value: '{theSystemComponent.reg_timestamp}'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel:  'Описание',
                        bind: {
                            value: '{theSystemComponent.description}'
                        }
                    },
                    {
                        fieldLabel:  'Родительский компонент',
                        bind: {
                            value: '{theSystemComponent.parent.label}'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Тип',
                        bind: {
                            value: '{theSystemComponent.component_type.label}'
                        }
                    },
                    {
                        fieldLabel:  'Кластер',
                        bind: {
                            value: '{theSystemComponent.cluster.label}'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Подразделение',
                        bind: {
                            value: '{theSystemComponent.department.label}'
                        }
                    },
                    {
                        xtype: 'tbspacer'
                    }
                ]
            }
        ]
    }
});
