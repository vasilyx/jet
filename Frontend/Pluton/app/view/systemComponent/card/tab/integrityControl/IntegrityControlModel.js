/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.integrityControl.IntegrityControlModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.integrity-control',

    data: {
        auditState: null
    }
});
