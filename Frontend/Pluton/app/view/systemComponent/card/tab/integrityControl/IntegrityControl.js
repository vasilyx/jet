/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.integrityControl.IntegrityControl', {
    extend: 'Ext.panel.Panel',

    xtype: 'integrity-control',
    permissions: 'integrity-control_page',

    requires: [
        'Ext.button.Button',
        'Ext.container.Container',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Display',
        'Ext.layout.container.Center',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.toolbar.Separator',
        'Pluton.view.systemComponent.card.tab.integrityControl.IntegrityControlController',
        'Pluton.view.systemComponent.card.tab.integrityControl.IntegrityControlModel'
    ],

    controller: 'integrity-control',
    viewModel: {
        type: 'integrity-control'
    },

    title: 'Контроль целостности',
    bodyPadding: 10,
    layout: 'center',
    listeners: {
        activate: 'onActivate'
    },

    items: {
        xtype: 'form',
        width: '50%',
        height: '100%',
        minWidth: 700,
        bodyPadding: '10 30 0 30',
        lbar: [
            {
                tooltip: 'Обновить состояние',
                iconCls: 'x-fa fa-refresh',
                handler: 'onRefreshBtn'
            },
            '-',
            {
                tooltip: 'Провести аудит',
                iconCls: 'x-fa fa-play',
                disabled: true,
                bind: {
                    disabled: '{auditState.response === "CHECK_IN_PROGRESS" || auditState.response === "UPDATING_DATABASE"}'
                },
                handler: 'onAuditStartBtn'
            },
            {
                tooltip: 'Обновить БКЦ',
                iconCls: 'x-fa fa-database',
                disabled: true,
                bind: {
                    disabled: '{auditState.response === "CHECK_IN_PROGRESS" || auditState.response === "UPDATING_DATABASE"}'
                },
                handler: 'onUpdateDatabaseBtn'
            },
            '-',
            {
                tooltip: 'Перейти в журнал аудита',
                iconCls: 'x-fa fa-external-link',
                ui: 'md-blue',
                bind: {
                    href: '#events/audit'
                }
            }
        ],
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        defaults: {
            xtype: 'displayfield'
        },
        fieldDefaults: {
            labelWidth: 300
        },
        items: [
            {
                fieldLabel: 'Состояние',
                bind: {
                    value: '{auditState.status}'
                }
            },
            {
                fieldLabel: 'Статус последнего аудита',
                bind: {
                    value: '{auditState.last_state}'
                }
            },
            {
                fieldLabel: 'Дата/время последнего аудита',
                bind: {
                    value: '{auditState.last_time}'
                },
                renderer: function (value) {
                    if (value) {
                        return Ext.Date.format(new Date(value), Ext.Date.defaultFormat);
                    }
                    return value;
                }
            },
            {
                fieldLabel: 'Планируемое время следующего аудита',
                bind: {
                    value: '{auditState.next_time}'
                },
                renderer: function (value) {
                    if (value) {
                        return Ext.Date.format(new Date(value), Ext.Date.defaultFormat);
                    }
                    return value;
                }
            },
            {
                fieldLabel: 'Объектов под контролем',
                bind: {
                    value: '{auditState.db_size}'
                }
            },
            {
                xtype: 'container',
                layout: 'hbox',
                items: [
                    {
                        reference: 'auditDelayField',
                        publishes: ['value'],
                        xtype: 'combo',
                        fieldLabel: 'Отложить аудит на',
                        editable: false,
                        value: 30,
                        maxWidth: 450,
                        flex: 1,
                        margin: '0 5 0 0',
                        store: [
                            [10, '10 минут'],
                            [20, '20 минут'],
                            [30, '30 минут']
                        ],
                        disabled: true,
                        bind: {
                            disabled: '{auditState.response === "CHECK_IN_PROGRESS" || auditState.response === "UPDATING_DATABASE"}'
                        }
                    },
                    {
                        xtype: 'button',
                        tooltip: 'Отложить',
                        iconCls: 'x-fa fa-pause',
                        ui: 'default-toolbar',
                        margin: '0 5 0 0',
                        disabled: true,
                        bind: {
                            disabled: '{auditState.response === "CHECK_IN_PROGRESS" || auditState.response === "UPDATING_DATABASE"}'
                        },
                        handler: 'onAuditSuspendBtn'
                    },
                    {
                        xtype: 'button',
                        tooltip: 'Возобновить',
                        iconCls: 'x-fa fa-repeat',
                        ui: 'default-toolbar',
                        disabled: true,
                        bind: {
                            disabled: '{auditState.response !== "DELAY_SET"}'
                        },
                        handler: 'onAuditResumeBtn'
                    }
                ]
            }
        ]
    }
});
