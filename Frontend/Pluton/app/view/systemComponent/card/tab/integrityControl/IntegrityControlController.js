/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.integrityControl.IntegrityControlController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.integrity-control',

    checkStateUrl: '/audit/integrity/status',
    auditStartUrl: '/audit/integrity/start',
    auditSuspendUrl: '/audit/integrity/delay',
    updateDatabaseUrl: '/audit/integrity/updatedb',

    onActivate: function () {
        this.updateAuditState();
    },

    onRefreshBtn: function () {
        this.updateAuditState();
    },

    onAuditStartBtn: function () {
        var me = this;

        me.updateAuditState({
            url: me.auditStartUrl
        });
    },

    onUpdateDatabaseBtn: function () {
        var me = this;

        me.updateAuditState({
            url: me.updateDatabaseUrl
        });
    },

    onAuditSuspendBtn: function () {
        var me = this;

        me.updateAuditState({
            url: me.auditSuspendUrl,
            jsonData: {
                delay: me.getViewModel().get('auditDelayField.value')
            }
        });
    },

    onAuditResumeBtn: function () {
        var me = this;

        me.updateAuditState({
            url: me.auditSuspendUrl,
            jsonData: {
                delay: 0
            }
        });
    },

    updateAuditState: function (params) {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            systemComponentId = vModel.get('systemComponentId'),
            needMask, url, jsonData, auditState;

        if (systemComponentId) {
            params = params || {};
            needMask = params.needMask;
            url = params.url || me.checkStateUrl;
            jsonData = Ext.applyIf({
                system_component_id: systemComponentId
            }, params.jsonData || {});

            if (needMask !== false) {
                view.setLoading();
            }
            Ext.Ajax.request({
                url: url,
                method: 'POST',
                jsonData: jsonData
            }).then(function (response) {
                vModel.set('auditState', (auditState = Ext.JSON.decode(response.responseText, true)));
                if (Ext.isObject(auditState) && (auditState.response === 'CHECK_IN_PROGRESS' || auditState.response === 'UPDATING_DATABASE')) {
                    Ext.Function.defer(
                        me.updateAuditState.bind(me, {
                            needMask: false
                        }),
                        5000
                    );
                }
            }).always(function () {
                if (view.isMasked()) {
                    view.setLoading(false);
                }
            });
        }
    }
});
