/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.monitoredSystem.grid.MonitoredSystemGridModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.monitored-system-grid',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.systemComponent.MonitoredSystem'
    ],

    stores: {
        MonitoredSystem: {
            model: 'Pluton.model.systemComponent.MonitoredSystem',
            pageSize: 0,
            remoteSort: true,
            sorters: 'name',
            proxy: {
                type: 'json-ajax',
                url: '/monitored_system/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        }
    }
});
