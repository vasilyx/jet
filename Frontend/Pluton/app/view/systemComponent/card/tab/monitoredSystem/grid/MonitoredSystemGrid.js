/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.monitoredSystem.grid.MonitoredSystemGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'monitored-system-grid',
    permissions: 'monitored-system_page',

    requires: [
        'Ext.grid.Panel',
        'Ext.toolbar.Separator',
        'Pluton.utility.button.CSVExport',
        'Pluton.view.systemComponent.card.tab.monitoredSystem.grid.MonitoredSystemGridController',
        'Pluton.view.systemComponent.card.tab.monitoredSystem.grid.MonitoredSystemGridModel'
    ],

    controller: 'monitored-system-grid',
    viewModel: {
        type: 'monitored-system-grid'
    },

    bind: {
        store: '{MonitoredSystem}'
    },

    title: 'Контролируемые системы',
    listeners: {
        activate: 'onActivate'
    },

    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefresh'
        },
        '-',
        {
            xtype: 'utility-button-csv-export'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'id',
                text: 'Идентификатор',
                hidden: true
            },
            {
                dataIndex: 'name',
                text: 'Наименование'
            },
            {
                dataIndex: 'system_component_name',
                text: 'Контролирующий сенсор'
            },
            {
                dataIndex: 'subnet_ip',
                text: 'IP-адреса домашней сети'
            },
            {
                dataIndex: 'description',
                text: 'Описание'
            }
        ]
    }
});
