/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.monitoredSystem.grid.MonitoredSystemGridController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.monitored-system-grid',

    onBeforeStoreLoad: function (store) {
        var systemComponentId = this.getViewModel().get('systemComponentId');

        if (systemComponentId) {
            store.getProxy().setExtraParam('parent_component_id', systemComponentId);
            return true;
        } else {
            return false;
        }
    },

    onActivate: function () {
        this.getStore('MonitoredSystem').load();
    },

    onRefresh: function () {
        this.getStore('MonitoredSystem').reload();
    }
});
