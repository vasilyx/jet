/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.monitoredSystem.card.MonitoredSystemCardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.monitored-system-card',

    requires: [
        'Pluton.model.systemComponent.MonitoredSystem'
    ],

    links: {
        theMonitoredSystem: {
            type: 'Pluton.model.systemComponent.MonitoredSystem',
            create: true
        }
    }
});
