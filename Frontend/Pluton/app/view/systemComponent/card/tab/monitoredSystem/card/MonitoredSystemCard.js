/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.monitoredSystem.card.MonitoredSystemCard', {
    extend: 'Ext.panel.Panel',

    xtype: 'monitored-system-card',
    permissions: 'monitored-system_page',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.layout.container.Center',
        'Ext.layout.container.VBox',
        'Pluton.view.systemComponent.card.tab.monitoredSystem.card.MonitoredSystemCardController',
        'Pluton.view.systemComponent.card.tab.monitoredSystem.card.MonitoredSystemCardModel'
    ],

    controller: 'monitored-system-card',
    viewModel: {
        type: 'monitored-system-card'
    },

    title: 'Контролируемая система',
    bodyPadding: 10,
    layout: 'center',
    listeners: {
        activate: 'onActivate'
    },

    items: {
        xtype: 'form',
        width: '50%',
        height: '100%',
        minWidth: 700,
        bodyPadding: '10 30 0 30',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        defaults: {
            xtype: 'displayfield'
        },
        fieldDefaults: {
            labelWidth: 300
        },
        items: [
            {
                fieldLabel: 'Наименование',
                bind: {
                    value: '{theMonitoredSystem.name}'
                }
            },
            {
                fieldLabel: 'IP-адреса домашней сети',
                bind: {
                    value: '{theMonitoredSystem.subnet_ip}'
                }
            },
            {
                fieldLabel: 'Описание',
                bind: {
                    value: '{theMonitoredSystem.description}'
                }
            }
        ]
    }
});
