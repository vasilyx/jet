/**
 *
 */
Ext.define('Pluton.view.systemComponent.card.tab.monitoredSystem.card.MonitoredSystemCardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.monitored-system-card',

    onActivate: function (view) {
        var vModel = view.getViewModel(),
            systemComponentId = vModel.get('systemComponentId'),
            theMonitoredSystem = vModel.get('theMonitoredSystem');

        if (systemComponentId) {
            view.setLoading();
            theMonitoredSystem.getProxy().setExtraParam('system_component_id', systemComponentId);
            theMonitoredSystem.load({
                callback: function () {
                    view.setLoading(false);
                }
            });
        }
    }
});
