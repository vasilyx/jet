/**
 *
 */
Ext.define('Pluton.view.systemComponent.SystemComponentController', {
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.system-component',

    route: 'settings/system-component',
    tabPrefix: 'systemComponent_',


    requires: [
        'Pluton.view.systemComponent.card.SystemComponentCard',
        'Pluton.model.systemComponent.SystemComponent'
    ],
    config: {
        deferExpand: false,
        subTabLoaded: false,
        preLoadActiveTab: '',
        signatureFilter: ''
    },

    listen: {
        component: {
            '#': {
                routeWithParams: 'onRouteParams'
            }
        },
        controller: {
            'system-component-card': {
                subTabChange: 'onSubTabChange',
                subTabLoaded: 'onSubTabLoaded'
            }
        }
    },

    onRouteParams: function(params) {
        if (params && params.id) {
            var record = new Pluton.model.systemComponent.SystemComponent;

            this.setPreLoadActiveTab(!Ext.isEmpty(params.tab) ? params.tab : null);
            this.setSignatureFilter(!Ext.isEmpty(params.signatureFilter) ? params.signatureFilter : null);

            record.setId(params.id);
            this.showCard(record, this.getPreLoadActiveTab());
        }
    },

    onActivate: function () {
        this.getStore('SystemComponent').load();
    },

    onRefresh: function () {
        var me = this;
        this.getStore('SystemComponent').load(
            function() {
                if (me.getDeferExpand() === true) {
                    me.onExpandAll();
                    me.setDeferExpand(false);
                }
            }
        );
    },

    onExpandAll: function () {
        this.setDeferExpand(true);
        this.lookup('systemComponentGrid').expandAll()
    },

    onCollapseAll: function () {
        this.lookup('systemComponentGrid').collapseAll();
    },

    onOpen: function () {
        var selection = this.getViewModel().get('systemComponentGrid.selection');

        if (selection) {
            this.redirectTo(this.route + '?id=' + selection.getId());
        }
    },

    showCard: function (record, activeTab) {
        var view = this.getView(),
            recordId = record.getId(),
            itemId = this.getTabItemId(recordId),
            tab = view.items.getByKey(itemId);

        if (tab) {
            view.setActiveTab(tab);
        } else {
            tab = view.add({
                xtype: 'system-component-card',
                itemId: itemId,
                viewModel: {
                    data: {
                        systemComponentId: recordId
                    }
                }
            });

            view.setActiveTab(tab);
            this.fireEvent('load');
        }

        if (activeTab && tab.down(activeTab)) {
                tab.setActiveTab(activeTab);
        }
    },

    getTabItemId: function (id) {
        return 'systemComponent_' + id;
    },

    onComponentBeforeSelect: function (tree, record) {
        return this.checkRowSelectable(record);
     },

    onSelected: function (grid, selection) {
        var passive = this.lookupReference('passiveButton'),
            active = this.lookupReference('activeButton'),
            training = this.lookupReference('trainingButton'),
            selection = selection[0];

        if (!selection || !selection.get('status') || !selection.get('status').id) {
            return;
        }

        var buttons = this.getView().query('button[handler=onChangedStatus]'),
            current_sus_id = this.getViewModel().get('current_sus_id'),
            selected_SCS = (selection.get('component_type') && selection.get('component_type').id) ?
                                ['SCS', 'COMPLEX'].indexOf(selection.get('component_type').id) >= 0 : false,
            selected_current = selection.getId() == current_sus_id;

        buttons.forEach(function (button) {
            button.setHidden(true)
        });

        // не показываем кнопки смены статуса для других СУС
        if(!selected_current && selected_SCS) {
            return;
        }

        // не показываем кнопки смены статуса для не-своих сенсоров
        if(!selected_current && !selected_SCS && selection.parentNode.getId() != current_sus_id) {
            return;
        }

        switch(selection.get('status').id) {

            case 'PASSIVE': // не активный
                active.setHidden(false);
                training.setHidden(selected_SCS);
                break;

            case 'ACTIVE': // Обнаружение
                passive.setHidden(false);
                training.setHidden(selected_SCS);
                break;

            case 'INCREMENTAL_LEARNING':
            case 'RELEARNING':
            case 'LEARNING': // обучение
                active.setHidden(false);
                passive.setHidden(false);
                break;

            case 'COMPROMISED': // скомпрометирован
                active.setHidden(false);
                passive.setHidden(false);
                training.setHidden(selected_SCS);
                break;
        }
    },

    onItemDblClick: function (grid, record) {
        if (this.checkRowSelectable(record)) {
            this.redirectTo(this.route + '?id=' + record.getId());
        }
    },

    onSubTabChange: function () {
        this.onTabChange(this.getView(), this.getView().getActiveTab());
    },

    onSubTabLoaded: function (xtype) {
        this.setSubTabLoaded(true);
        var systemComponentCard = this.getView().getActiveTab();

        if (this.getPreLoadActiveTab() === 'monitored-system') {
            var card = systemComponentCard.down(xtype);
            systemComponentCard.setActiveTab(card);
        }

        if (this.getSignatureFilter()) {
            this.fireEvent('setSignatureFilter', this.getSignatureFilter());
        }

        this.onTabChange(this.getView(), this.getView().getActiveTab());
    },

    onTabChange: function( tabPanel, newCard, oldCard, eOpts ) {
        if (!this.getSubTabLoaded()) return false;

        if (newCard.getXType() === 'system-component-card' && !Ext.isEmpty(newCard.itemId)) {
            var activeTab = '&tab=' + newCard.getActiveTab().getXType();
            this.redirectTo(this.route + '?id=' + newCard.itemId.replace(this.tabPrefix, '')  + activeTab);
        } else {
            this.redirectTo(this.route);
        }
    },

    onChangedStatus: function (btn) {
        var self = this,
            store = self.getStore('SystemComponent'),
            selection = self.getViewModel().get('systemComponentGrid.selection'),
            systemComponentId = selection.getId(),
            status = btn.getValue(),
            statusId = status.id,
            statusLabel = status.label;

        if (statusId === 'LEARNING' && selection.get('host_count') && selection.get('host_count') > 0) {
            Ext.MessageBox.show({
                title: 'Изменение статуса',
                msg: 'Выберите тип обучения:',
                buttons: Ext.MessageBox.YESNOCANCEL,
                buttonText: {
                    yes: 'Переобучение',
                    no: 'Дообучениe',
                    cancel: 'Отменить'
                },
                fn: function (btn) {
                    if (btn === 'yes') {
                        statusId = 'RELEARNING';
                        statusLabel = 'Переобучение';
                    } else if (btn === 'no') {
                        statusId = 'INCREMENTAL_LEARNING';
                        statusLabel = 'Дообучениe';
                    }

                    if (btn === 'yes' || btn === 'no') {
                        self.sendChangeStatus(statusId, systemComponentId, statusLabel, self, store);
                    }
                }
            });
        } else {
            Ext.MessageBox.confirm('Изменение статуса', 'Изменить статус на &laquo;' + statusLabel + '&raquo;?', confirmFunc);
            function confirmFunc (btn) {
                if (btn === 'yes') {
                    self.sendChangeStatus(statusId, systemComponentId, statusLabel, self, store);
                }
            }
        }
    },

    sendChangeStatus: function (statusId, systemComponentId, statusLabel, self, store) {
        if (statusId) {
            Ext.Ajax.request({
                url: '/system_component/change_status',
                method: 'POST',
                jsonData: {
                    status: statusId,
                    system_component_id: systemComponentId
                },
                success: function(r) {
                    Ext.MessageBox.alert('Изменение статуса', 'Статус компонента успешно изменен.');
                    Ext.getBody().unmask();
                    store.reload({
                        callback: function() {
                            self.lookup('systemComponentGrid').expandAll(function () {
                                var rec = store.getNodeById(systemComponentId);
                                self.lookup('systemComponentGrid').getSelectionModel().select(rec);
                                self.onSelected(self.lookup('systemComponentGrid'), rec);
                            }, this);
                        }
                    });
                },
                failure: function() {
                    Ext.getBody().unmask();
                }
            });
        } else {
            Ext.Msg.show({
                title: 'Изменение статуса',
                message: statusLabel + ' статус находится в разработке...',
                buttons: Ext.Msg.CANCEL
            });
            Ext.getBody().unmask();
        }
    },

    renderTreeColumnName: function (name) {
        return name === this.getViewModel().get('current_sus_name') ?
            this.getViewModel().get('current_sus_name') + ' <span style="color: #d10b10">*</span>' : name;
    },

    renderUpdateColumn: function (updates) {
        if (!updates) return;

        var result= [];

        Ext.Object.each(updates, function(v, k){
            var string = v.replace("_UPDATE", "", "gi");

            if (k.available_version > k.current_version) {
                string = '<span class="background-red text-white">' + string + '</span>';
            } else {
                string = '<span>' + string + '</span>';
            }

            result.push(string);
        });

        return result.join(' ');
    },

    checkRowSelectable: function (record) {
        if (record.parentNode.isRoot()
            && record.getId() != this.getViewModel().get('current_sus_id')) {
            return false;
        } else {
            return true;
        }
    },

    renderStateColumn: function (v, t, row) {
        var class_color = '';

        if (row.get('health') == null) {
            return '<div class="component-state-circle"></div>';
        }

        switch (row.get('health').state) {
            case 0:
                class_color = 'background-green';
                break;
            case 1:
                class_color = 'background-yellow';
                break;
            case 2:
                class_color = 'background-red';
                break;
        }

        return '<div class="component-state-circle ' + class_color + '"></div>';
    }
});
