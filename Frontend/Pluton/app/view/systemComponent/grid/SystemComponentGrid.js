/**
 *
 */
Ext.define('Pluton.view.systemComponent.grid.SystemComponentGrid', {
    extend: 'Ext.tree.Panel',

    xtype: 'system-component-grid',

    requires: [
        'Ext.toolbar.Separator'
    ],

    bind: {
        store: '{SystemComponent}'
    },

    reference: 'systemComponentGrid',
    title: 'Компоненты',
    rootVisible: false,
    useArrows: true,
    rowLines: true,
    sortableColumns: false,

    listeners: {
        itemdblclick: 'onItemDblClick',
        selectionchange: 'onSelected',
        beforeselect: 'onComponentBeforeSelect'
    },

    lbar: [
        {
            tooltip: 'Обновить',
            iconCls: 'x-fa fa-refresh',
            handler: 'onRefresh'
        },
        '-',
        {
            tooltip: 'Развернуть дерево',
            iconCls: 'x-fa fa-expand',
            handler: 'onExpandAll'
        },
        {
            tooltip: 'Свернуть дерево',
            iconCls: 'x-fa fa-compress',
            handler: 'onCollapseAll'
        },
        '-',
        {
            tooltip: 'Открыть',
            iconCls: 'x-fa fa-external-link',
            ui: 'md-blue',
            handler: 'onOpen'
        },
        '-',
        {
            reference: 'passiveButton',
            tooltip: 'Статус: Неактивный',
            iconCls: 'x-fa fa-low-vision',
            hidden: true,
            value: {
                id: 'PASSIVE',
                label: 'Неактивный'
            },
            handler: 'onChangedStatus'
        },
        {
            reference: 'activeButton',
            tooltip: 'Статус: Обнаружение',
            iconCls: 'x-fa fa-eye',
            hidden: true,
            value: {
                id: 'ACTIVE',
                label: 'Обнаружение'
            },
            handler: 'onChangedStatus'
        },
        {
            reference: 'trainingButton',
            tooltip: 'Статус: Обучение',
            iconCls: 'x-fa fa-graduation-cap',
            hidden: true,
            value: {
                id: 'LEARNING',
                label: 'Обучение'
            },
            handler: 'onChangedStatus'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            { xtype: 'treecolumn', dataIndex: 'name', text: 'Имя', flex: 3, renderer: 'renderTreeColumnName' },
            { dataIndex: 'component_type', text: 'Тип компонента', renderer: Ext.labelRenderer, hidden: true },
            { dataIndex: 'monitored_system', text: 'Контролируемая система', renderer: Ext.labelRenderer },
            { dataIndex: 'cluster', text: 'Кластер', renderer: Ext.labelRenderer },
            { dataIndex: 'host', text: 'Адрес', renderer: Ext.labelRenderer },
            { dataIndex: 'region', text: 'Регион', renderer: Ext.labelRenderer },
            { dataIndex: 'department', text: 'Подразделение', renderer: Ext.labelRenderer },
            { dataIndex: 'updates', text: 'Обновления', renderer: 'renderUpdateColumn' },
            { dataIndex: 'status', text: 'Статус', renderer: Ext.labelRenderer},
            { dataIndex: 'health', text: 'Состояние',  renderer: 'renderStateColumn' }
        ]
    }
});
