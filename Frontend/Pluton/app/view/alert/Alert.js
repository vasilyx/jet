/**
 *
 */
Ext.define('Pluton.view.alert.Alert', {
    extend: 'Ext.tab.Panel',

    permissions: 'alert_page',

    xtype: 'events.alert',

    requires: [
        'Ext.layout.container.Border',
        'Ext.panel.Panel',
        'Pluton.utility.tree.TreeSystemComponent',
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.alert.AlertController',
        'Pluton.view.alert.AlertModel',
        'Pluton.view.alert.grid.AlertGrid'
    ],

    controller: 'alert',
    viewModel: {
        type: 'alert'
    },

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onActivate',
        tabchange: 'onTabChange'
    },
    defaults: {
        closable: true
    },

    items: [
        {
            title: 'Журнал событий (СИБ)',
            xtype: 'panel',
            layout: 'border',
            closable: false,
            items: [
                {
                    xtype: 'utility-tree-system-component',
                    region: 'east',
                    split: true,
                    listeners: {
                        rowclick: 'onFilterApply'
                    }
                },
                {
                    xtype: 'alert-grid',
                    region: 'center'
                }
            ]
        }
    ]
});

