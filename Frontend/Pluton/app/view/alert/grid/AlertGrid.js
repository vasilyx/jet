/**
 *
 */
Ext.define('Pluton.view.alert.grid.AlertGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'alert-grid',

    requires: [
        'Ext.container.Container',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Tag',
        'Ext.form.field.Text',
        'Ext.grid.column.Date',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Separator',
        'Ext.slider.Multi',
        'Pluton.utility.button.CSVExport',
        'Pluton.ux.container.DateTimeRangeContainer'
    ],

    reference: 'alertGrid',
    bind: {
        store: '{Alert}'
    },
    listeners: {
        itemdblclick: 'onItemDblClick'
    },

    tbar: [
        {
            title: 'Фильтры',
            xtype: 'fieldset',
            ui: 'fieldset-without-padding',
            style: {
                background: 'transparent'
            },
            collapsible: true,
            collapsed: true,
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    reference: 'alertListFilters',
                    xtype: 'form',
                    layout: 'hbox',
                    scrollable: true,
                    defaults: {
                        flex: 1,
                        minWidth: 300
                    },
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    listeners: {
                        validitychange: 'onValidityChange'
                    },
                    bbar: {
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                text: 'Применить',
                                tooltip: 'Применить фильтры',
                                ui: 'md-blue',
                                handler: 'onFilterApply',
                                bind: {
                                    disabled: '{!isValid}'
                                }
                            },
                            {
                                text: 'Сбросить',
                                tooltip: 'Сбросить фильтры',
                                handler: 'onFilterReset'
                            }
                        ]
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    reference: 'timestampField',
                                    publishes: ['value'],
                                    name: 'alert_timestamp',
                                    xtype: 'combo',
                                    fieldLabel: 'Обнаружено за',
                                    emptyText: 'Всё время',
                                    editable: false,
                                    clearTrigger: true,
                                    store: [
                                        [1, 'Последний день'],
                                        [2, 'Последнюю неделю'],
                                        [3, 'Последний месяц'],
                                        [4, 'Период']
                                    ]
                                },
                                {
                                    xtype: 'date-time-range-container',
                                    name: 'registered_timestamp',
                                    submitFormat: 'Y-m-d H:i:sP',
                                    fromFieldConfig: {
                                        fieldLabel: '&nbsp;',
                                        labelSeparator: ''
                                    },
                                    bind: {
                                        disabled: '{timestampField.value != 4}'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'alert_type_id_',
                                    xtype: 'combo',
                                    fieldLabel: 'Тип',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    bind: {
                                        store: '{AlertType}'
                                    }
                                },
                                {
                                    name: 'source_ip',
                                    xtype: 'textfield',
                                    fieldLabel: 'IP-источник'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'severity_id_',
                                    fieldLabel: 'Уровень критичности',
                                    xtype: 'multislider',
                                    values: [1, 5],
                                    minValue: 1,
                                    maxValue: 5,
                                    tipText: function(thumb){
                                        switch (thumb.value) {
                                            case 1:
                                                return "Инфо";
                                            case 2:
                                                return "Низкий";
                                            case 3:
                                                return "Средний";
                                            case 4:
                                                return "Высокий";
                                            case 5:
                                                return "Критический";

                                        }
                                    }
                                },
                                {
                                    name: 'target_ip',
                                    xtype: 'textfield',
                                    fieldLabel: 'IP-получатель'
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],

    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefresh'
        },
        '-',
        {
            iconCls: 'x-fa fa-external-link',
            ui: 'md-blue',
            tooltip: 'Открыть',
            disabled: true,
            bind: {
                disabled: '{!alertGrid.selection}'
            },
            handler: 'onOpen'
        },
        {
            xtype: 'utility-button-csv-export'
        }
    ],

    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{Alert}'
                }
            }
        ]
    },

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'id',
                text: 'Идентификатор',
                hidden: true
            },
            {
                xtype: 'datecolumn',
                format: 'd.m.Y H:i:s.u',
                dataIndex: 'alert_timestamp',
                text: 'Дата/время',
                flex: 2
            },
            {
                dataIndex: 'alert_rule_name',
                text: 'Событие'
            },
            {
                dataIndex: 'alert_type',
                sorter: 'alert_type_label_',
                text: 'Тип',
                flex: 2,
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'severity',
                sorter: 'severity_label_',
                text: 'Критичность',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'source_ip',
                text: 'IP-источник'
            },
            {
                dataIndex: 'target_ip',
                text: 'IP-получатель'
            },
            {
                dataIndex: 'controlled_system',
                sorter: 'controlled_system_label_',
                text: 'Контролируемая система',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'sensor',
                sorter: 'sensor_label_',
                text: 'Сенсор',
                renderer: Ext.labelRenderer
            }
        ]
    }
});
