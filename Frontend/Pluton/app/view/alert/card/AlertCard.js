/**
 *
 */
Ext.define('Pluton.view.alert.card.AlertCard', {
    extend: 'Ext.form.Panel',

    xtype: 'alert-card',

    requires: [
        'Ext.layout.container.Accordion',
        'Ext.panel.Panel',
        'Pluton.view.alert.card.AlertCardController',
        'Pluton.view.alert.card.AlertCardModel',
        'Pluton.view.alert.card.tab.AlertBasic',
        'Pluton.view.alert.card.tab.AlertConnection',
        'Pluton.view.alert.card.tab.AlertRule',
        'Pluton.view.alert.card.tab.pcap.AlertPcap'
    ],

    controller: 'alert-card',
    viewModel: {
        type: 'alert-card'
    },

    bind: {
        title: '{alertCardTitle}'
    },
    bodyPadding: 10,
    scrollable: true,
    layout: {
        type: 'accordion',
        fill: false,
        multi: true,
        reserveScrollbar: true
    },
    defaults: {
        collapsed: true,
        scrollable: true,
        bodyPadding: '10 10 0 10',
        header: {
            titlePosition: 1
        }
    },
    fieldDefaults: {
        labelWidth: 180
    },
    listeners: {
        load: 'onLoad'
    },

    items: [
        {
            xtype: 'panel',
            collapsed: false,
            minHeight: 0,
            maxHeight: 0,
            margin: 0,
            padding: 0
        },
        {
            xtype: 'alert-panel-basic',
            collapsed: false
        },
        {
            xtype: 'alert-pcap',
            minHeight: 600,
            bodyPadding: 0,
            hidden: true,
            bind: {
                hidden: '{theAlert.alert_type_id !== alertTypeId.suricata}'
            }
        },
        {
            xtype: 'alert-panel-rule',
            hidden: true,
            bind: {
                hidden: '{!(' +
                    'theAlert.alert_type_id === alertTypeId.blackList ||' +
                    'theAlert.alert_type_id === alertTypeId.bro ||' +
                    'theAlert.alert_type_id === alertTypeId.newHost ||' +
                    'theAlert.alert_type_id === alertTypeId.newHostApp ||' +
                    'theAlert.alert_type_id === alertTypeId.newHostOs ||' +
                    'theAlert.alert_type_id === alertTypeId.newHostService ||' +
                    'theAlert.alert_type_id === alertTypeId.suricata' +
                ')}'
            }
        },
        {
            xtype: 'alert-panel-connection'
        }
    ]
});
