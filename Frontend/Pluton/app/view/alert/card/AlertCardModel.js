/**
 *
 */
Ext.define('Pluton.view.alert.card.AlertCardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.alert-card',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.systemComponent.SystemComponentRule'
    ],

    stores: {
        SystemComponentRule: {
            model: 'Pluton.model.systemComponent.SystemComponentRule',
            proxy: {
                type: 'json-ajax',
                url: '/system_component/rules/get',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        }
    },

    data: {
        theAlert: null,
        /*
            ('5c4f28cd-a9e2-459c-a043-c0f586e45386', 'Аудит пользователей')
            ('ad8d48e2-c7b0-465e-a0a6-e5cce711eb54', 'Аудит конфигурации системы')
            ('cc1143d4-2909-4f13-b12e-147727c401bf', 'Аудит целостности системы')
            ('68c94d92-9f2c-4928-8862-6fb847153b80', 'Bro')
            ('c28643eb-8541-41c1-a45a-2c31574db95b', 'Suricata')
            ('62b38077-0517-4eea-b18f-ed52e7697a69', 'Новая ОС хоста')
            ('909280bb-388b-4347-bf3e-28ed679bb6eb', 'Новое ПО хоста')
            ('3e542d92-8cab-11e7-bb31-be2e44b06b34', 'Новый сервис хоста')
            ('09caa12d-acde-4f84-adc6-d524373d6af9', 'Новый хост')
            ('f81e3702-7507-4b6c-8d44-25afa8b42fb8', 'Аномалия сетевой активности')
            ('efd52890-aa9e-457b-867d-9cd861cc5202', 'Обнаружение записи из чёрного списка')
        */
        alertTypeId: {
            user: '5c4f28cd-a9e2-459c-a043-c0f586e45386',
            bro: '68c94d92-9f2c-4928-8862-6fb847153b80',
            suricata: 'c28643eb-8541-41c1-a45a-2c31574db95b',
            newHost: '09caa12d-acde-4f84-adc6-d524373d6af9',
            newHostOs: '62b38077-0517-4eea-b18f-ed52e7697a69',
            newHostApp: '909280bb-388b-4347-bf3e-28ed679bb6eb',
            newHostService: '3e542d92-8cab-11e7-bb31-be2e44b06b34',
            blackList: 'efd52890-aa9e-457b-867d-9cd861cc5202'
        }
    },

    formulas: {
        recipientSourceRisk: {
            bind: {
                bindTo: '{theAlert}',
                deep: true
            },
            get: function(theAlert) {
                return '<div class="severity-circle-green"></div>';
            }
        },
        alertCardTitle: {
            bind: {
                bindTo: '{theAlert}',
                deep: true
            },
            get: function(theAlert) {
                if (theAlert) {
                    return theAlert.get('alert_type') || theAlert.getId();
                } else {
                    return '';
                }
            }
        },
        transportProtocol: {
            bind: {
                bindTo: '{theAlert}',
                deep: true
            },
            get: function(theAlert) {
                if (theAlert) {
                    return theAlert.get('transport_protocol') || '';
                } else {
                    return '';
                }
            }
        },
        targetIP: {
            bind: {
                bindTo: '{theAlert}',
                deep: true
            },
            get: function(theAlert) {
                if (theAlert) {
                    return theAlert.get('target_host_id') ? ('<a target="_blank" href="#env/host?id=' + theAlert.get('target_host_id') +  '">' + theAlert.get('target_ip') + '</a>') : theAlert.get('target_ip');
                } else {
                    return '';
                }
            }
        },
        sourceIP: {
            bind: {
                bindTo: '{theAlert}',
                deep: true
            },
            get: function(theAlert) {
                if (theAlert) {
                    return theAlert.get('source_host_id') ? ('<a target="_blank" href="#env/host?id=' + theAlert.get('source_host_id') +  '">' + theAlert.get('source_ip') + '</a>') : theAlert.get('source_ip');
                } else {
                    return '';
                }
            }
        },
        ruleName: {
            bind: {
                bindTo: '{theAlert}',
                deep: true
            },
            get: function (theAlert) {
                var systemComponentId,
                    ruleName,
                    value = '';

                if (theAlert) {
                    systemComponentId = theAlert.get('system_component_id');
                    ruleName = theAlert.get('rule_name');

                    if (systemComponentId && ruleName) {
                        value = Ext.String.format(
                            '<a target="_blank" href="#settings/system-component?id={0}&tab=system-component-main&signatureFilter={1}">{1}</a>',
                            systemComponentId,
                            ruleName
                        );
                    } else if (ruleName) {
                        value = ruleName;
                    }
                }

                return value;
            }
        }
    }
});
