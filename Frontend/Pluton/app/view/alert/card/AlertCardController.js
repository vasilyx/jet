/**
 *
 */
Ext.define('Pluton.view.alert.card.AlertCardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.alert-card',

    onLoad: function () {
        var view = this.getView(),
            theAlert = this.getViewModel().get('theAlert');

        view.setLoading(true);
        theAlert.load({
            callback: function() {
                view.setLoading(false);
            }
        });
    },

    severityRenderer: function (value) {
        var severityId,
            severityLabel = '',
            color,
            icon;

        if (Ext.isObject(value) && !Ext.isEmpty((severityId = value.id))) {
            severityId = +severityId;
            severityLabel = !Ext.isEmpty(value.label) ? value.label : 'Неизвестна';
            icon = 'arrow-up';

            switch (severityId) {
                case 1:
                    color = '#00c400';
                    break;
                case 2:
                    color = '#00f000';
                    break;
                case 3:
                    color = '#ffe000';
                    break;
                case 4:
                    color = '#ff8000';
                    break;
                case 5:
                    color = '#ff0000';
                    break;
                default:
                    icon = 'question';
                    break;
            }

            severityLabel = Ext.String.format('<span class="x-fa fa-{0}" style="font-size:16px;margin-right:5px;color:{1};"></span><span>{2}</span>', icon, color, severityLabel);
        }

        return severityLabel;
    },

    onRenderCVSS: function (value) {
        if (!value) {
            return '';
        }
        value = +value;

        var color,text;
        if (value<1) {
            color = '#00c400';
            text = value+" ИНФО";
        }
        else if (value<2) {
            color = '#00e020';
            text = value+" ИНФО";
        }
        else if (value<3) {
            color = '#00f000';
            text = value+" НИЗКИЙ";
        }
        else if (value<4) {
            color = '#d1ff00';
            text = value+" НИЗКИЙ";
        }
        else if (value<5) {
            color = '#ffe000';
            text = value+" СРЕДНИЙ";
        }
        else if (value<6) {
            color = '#ffcc00';
            text = value+" СРЕДНИЙ";
        }
        else if (value<7) {
            color = '#ffbc10';
            text = value+" ВЫСОКИЙ";
        }
        else if (value<8) {
            color = '#ff9c20';
            text = value+" ВЫСОКИЙ";
        }
        else if (value<9) {
            color = '#ff8000';
            text = value+" КРИТИЧНЫЙ";
        }
        else if (value<10) {
            color = '#ff0000';
            text = value+" КРИТИЧНЫЙ";
        }

        return '<span style="font-size: 25px;text-transform:uppercase; line-height: 0;color:'+color+'" class="x-fa fa-arrow-up"></span><span> '+text+'</span>';
    },

    onRenderAlertTrust: function (value) {
        var icon, cls, tooltip;

        if (value > 0) {
            icon = 'plus';
            cls = 'correct-color';
            tooltip = 'Угроза подтверждена';
        } else if (value === 0) {
            icon = 'minus';
            cls = 'corrupt-color';
            tooltip = 'Угроза не подтверждена';
        } else {
            icon = 'question';
            cls = 'unknown-color';
            tooltip = 'Неизвестно';
        }

        return Ext.String.format('<span class="x-fa fa-{0} fa-2x {1}" data-qtip="{2}"></span>', icon, cls, tooltip);
    }
});
