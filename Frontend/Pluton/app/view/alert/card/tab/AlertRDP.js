/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.AlertRDP', {
    extend: 'Ext.form.Panel',

    xtype: 'alert-panel-rdp',

    title: 'RDP',
    layout: 'hbox',
    margin: 10,
    items: [
        {
            xtype: 'container',
            layout: {type: 'vbox', align: 'stretch'},
            margin: '0 10 0 0',
            flex: 1,
            defaults: {xtype: 'textfield', labelWidth: 180, readOnly: true},
            items: [
                {
                    fieldLabel: 'ID события',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Cookie',
                    margin: '13 0 0 0',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Протокол безопасности',
                    margin: '24 0 10 0',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Раскладка клавиатуры',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Версия клиента',
//                                                    bind: {value: '{theAlert.}'}
                }
            ]
        },
        {
            xtype: 'container',
            layout: {type: 'vbox', align: 'stretch'},
            flex: 1,
            defaults: {xtype: 'textfield', labelWidth: 180, readOnly: true},
            items: [
                {
                    fieldLabel: 'Имя компьютера',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Product ID пользовательского компьютера',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Уровень шифрования',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Метод шифрования',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'SSL',
//                                                    bind: {value: '{theAlert.}'}
                }
            ]
        }
    ]
});
