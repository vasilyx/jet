/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.AlertFTP', {
    extend: 'Ext.form.Panel',

    xtype: 'alert-panel-ftp',

    requires: [
        'Ext.container.Container',
        'Ext.form.field.Display',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox'
    ],

    title: 'FTP',
    layout: 'hbox',
    hidden: true,
    items: [
        {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            flex: 1,
            defaults: {
                xtype: 'displayfield'
            },
            items: [
                {
                    fieldLabel: 'ID события'
                },
                {
                    fieldLabel: 'Пользователь'
                }
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            flex: 1,
            defaults: {
                xtype: 'displayfield'
            },
            items: [
                {
                    fieldLabel: 'Размер файла'
                },
                {
                    fieldLabel: 'Команда'
                }
            ]
        }
    ]
});
