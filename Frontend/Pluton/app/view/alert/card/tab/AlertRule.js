/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.AlertRule', {
    extend: 'Ext.panel.Panel',

    xtype: 'alert-panel-rule',

    requires: [
        'Ext.button.Button',
        'Ext.container.Container',
        'Ext.form.field.Display',
        'Ext.layout.container.Center',
        'Ext.layout.container.Fit',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.toolbar.Spacer'
    ],

    title: 'Правило',
    layout: 'center',

    items: {
        xtype: 'container',
        width: '70%',
        height: '100%',
        layout: 'fit',
        items: [
            {
                xtype: 'container',
                hidden: true,
                bind: {
                    hidden: '{theAlert.alert_type_id !== alertTypeId.suricata}'
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Идентификатор',
                                bind: {
                                    value: '{theAlert.alert_type_id}'
                                }
                            },
                            {
                                fieldLabel: 'Уязвимость',
                                bind: {
                                    value: '{theAlert.vulnerability_id}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип правила',
                                bind: {
                                    value: '{theAlert.alert_type}'
                                }
                            },
                            {
                                fieldLabel: 'CVSS v3',
                                bind: {
                                    value: '{theAlert.base_score_v3}'
                                },
                                renderer: 'onRenderCVSS'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Событие',
                                bind: {
                                    value: '{ruleName}'
                                }
                            },
                            {
                                fieldLabel: 'CVSS v2',
                                bind: {
                                    value: '{theAlert.base_score_v2}'
                                },
                                renderer: 'onRenderCVSS'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Класс',
                                bind: {
                                    value: '{theAlert.signature_class}'
                                }
                            },
                            {
                                fieldLabel: 'Достоверность угрозы',
                                bind: {
                                    value: '{theAlert.alert_trust}'
                                },
                                renderer: 'onRenderAlertTrust'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                xtype: 'displayfield',
                                fieldLabel: 'Тег',
                                bind: {
                                    value: '{theAlert.signature_tag}'
                                }
                            },
                            {
                                xtype: 'tbspacer'
                            }
                        ]
                    },
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Описание',
                        bind: {
                            value: '{theAlert.rule_description}'
                        }
                    },
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Рекомендованное действие',
                        labelWidth: 220,
                        bind: {
                            value: '{theAlert.signature_action}'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                hidden: true,
                bind: {
                    hidden: '{theAlert.alert_type_id !== alertTypeId.bro}'
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип правила',
                                bind: {
                                    value: '{theAlert.alert_type}'
                                }
                            },
                            {
                                fieldLabel: 'Событие',
                                bind: {
                                    value: '{ruleName}'
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                hidden: true,
                bind: {
                    hidden: '{theAlert.alert_type_id !== alertTypeId.newHostApp}'
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип правила',
                                bind: {
                                    value: '{theAlert.alert_type}'
                                }
                            },
                            {
                                fieldLabel: 'Наименование',
                                bind: {
                                    value: '{theAlert.application_name}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Хост',
                                bind: {
                                    value: '{theAlert.host.label}'
                                }
                            },
                            {
                                fieldLabel: 'Версия',
                                bind: {
                                    value: '{theAlert.application_version}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип',
                                bind: {
                                    value: '{theAlert.application_type}'
                                }
                            },
                            {
                                fieldLabel: 'URL',
                                bind: {
                                    value: '{theAlert.application_url}'
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                hidden: true,
                bind: {
                    hidden: '{theAlert.alert_type_id !== alertTypeId.newHostOs}'
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип правила',
                                bind: {
                                    value: '{theAlert.alert_type}'
                                }
                            },
                            {
                                fieldLabel: 'Версия',
                                bind: {
                                    value: '{theAlert.new_os_name}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Хост',
                                bind: {
                                    value: '{theAlert.host.label}'
                                }
                            },
                            {
                                fieldLabel: 'Дистанция',
                                bind: {
                                    value: '{theAlert.route_distance}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип',
                                bind: {
                                    value: '{theAlert.os_type}'
                                }
                            },
                            {
                                fieldLabel: 'Качество совпадения',
                                bind: {
                                    value: '{theAlert.os_version_inference}'
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                hidden: true,
                bind: {
                    hidden: '{theAlert.alert_type_id !== alertTypeId.newHostService}'
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип правила',
                                bind: {
                                    value: '{theAlert.alert_type}'
                                }
                            },
                            {
                                fieldLabel: 'Протокол',
                                bind: {
                                    value: '{theAlert.transport_protocol}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Хост',
                                bind: {
                                    value: '{theAlert.host.label}'
                                }
                            },
                            {
                                fieldLabel: 'Имя',
                                bind: {
                                    value: '{theAlert.service_name}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Порт',
                                bind: {
                                    value: '{theAlert.port}'
                                }
                            },
                            {
                                xtype: 'tbspacer'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                hidden: true,
                bind: {
                    hidden: '{theAlert.alert_type_id !== alertTypeId.blackList}'
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип правила',
                                bind: {
                                    value: '{theAlert.alert_type}'
                                }
                            },
                            {
                                fieldLabel: 'Значение',
                                bind: {
                                    value: '<a target="_blank" href="#env/black-list?id={theAlert.black_list_id}&value={theAlert.black_list_value}">{theAlert.black_list_value}</a>'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Событие',
                                bind: {
                                    value: '{theAlert.rule_name}'
                                }
                            },
                            {
                                fieldLabel: 'Имя MD5-файла',
                                bind: {
                                    value: '{theAlert.file_name}'
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                hidden: true,
                bind: {
                    hidden: '{theAlert.alert_type_id !== alertTypeId.newHost}'
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        defaults: {
                            xtype: 'displayfield',
                            flex: 1
                        },
                        items: [
                            {
                                fieldLabel: 'Тип правила',
                                bind: {
                                    value: '{theAlert.alert_type}'
                                }
                            },
                            {
                                fieldLabel: 'IP-адрес',
                                bind: {
                                    value: '{theAlert.host.label}'
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
});
