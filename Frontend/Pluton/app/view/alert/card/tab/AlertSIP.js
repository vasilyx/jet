/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.AlertSIP', {
    extend: 'Ext.form.Panel',

    xtype: 'alert-panel-sip',

    title: 'SIP',
    layout: 'hbox',
    margin: 10,
    items: [
        {
            xtype: 'container',
            layout: {type: 'vbox', align: 'stretch'},
            margin: '0 10 0 0',
            flex: 1,
            defaults: {xtype: 'textfield', labelWidth: 180, readOnly: true},
            items: [
                {
                    fieldLabel: 'ID события',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Метод',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Запрос от',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Запрос к',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Ответ от',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Ответ к',
//                                                    bind: {value: '{theAlert.}'}
                }
            ]
        },
        {
            xtype: 'container',
            layout: {type: 'vbox', align: 'stretch'},
            flex: 1,
            defaults: {xtype: 'textfield', labelWidth: 180, readOnly: true},
            items: [
                {
                    fieldLabel: 'URL',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Reply to',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Call id',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Тема',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Агент пользователя',
//                                                    bind: {value: '{theAlert.}'}
                }
            ]
        }
    ]
});
