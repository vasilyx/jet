/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.AlertHTTP', {
    extend: 'Ext.form.Panel',

    xtype: 'alert-panel-http',

    title: 'HTTP',
    layout: 'hbox',
    margin: 10,
    items: [
        {
            xtype: 'container',
            layout: {type: 'vbox', align: 'stretch'},
            margin: '0 10 0 0',
            flex: 1,
            defaults: {xtype: 'textfield', labelWidth: 180, readOnly: true},
            items: [
                {
                    fieldLabel: 'ID события',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Метод',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Хост',
//                                                    bind: {value: '{theAlert.}'}
                }
            ]
        },
        {
            xtype: 'container',
            layout: {type: 'vbox', align: 'stretch'},
            flex: 1,
            defaults: {xtype: 'textfield', labelWidth: 180, readOnly: true},
            items: [
                {
                    fieldLabel: 'URL',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Пользовательский агент',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Имя пользователя',
//                                                    bind: {value: '{theAlert.}'}
                }
            ]
        }
    ]
});
