/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.AlertConnection', {
    extend: 'Ext.panel.Panel',

    xtype: 'alert-panel-connection',

    requires: [
        'Ext.container.Container',
        'Ext.form.field.Display',
        'Ext.layout.container.Center',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.util.Format'
    ],

    title: 'Соединение',
    layout: 'center',

    items: {
        xtype: 'container',
        width: '70%',
        height: '100%',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Дата/время',
                        bind: {
                            value: '{theAlert.connection_timestamp}'
                        },
                        renderer: Ext.util.Format.dateRenderer('d.m.Y H:i:s.u P')
                    },
                    {
                        fieldLabel: 'Длительность',
                        bind: {
                            value: '{theAlert.duration}'
                        }
                    }
                ]
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Протокол прикладного уровня',
                labelWidth: 220,
                bind: {
                    value: '{theAlert.application_protocol_name}'
                }
            }
        ]
    }
});
