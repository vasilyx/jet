/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.AlertSMTP', {
    extend: 'Ext.form.Panel',

    xtype: 'alert-panel-smtp',

    title: 'SMTP',
    layout: 'hbox',
    margin: 10,
    items: [
        {
            xtype: 'container',
            layout: {type: 'vbox', align: 'stretch'},
            margin: '0 10 0 0',
            flex: 1,
            defaults: {xtype: 'textfield', labelWidth: 180, readOnly: true},
            items: [
                {
                    fieldLabel: 'ID события',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Источники письма',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Адресаты письма',
//                                                    bind: {value: '{theAlert.}'}
                }
            ]
        },
        {
            xtype: 'container',
            layout: {type: 'vbox', align: 'stretch'},
            flex: 1,
            defaults: {xtype: 'textfield', labelWidth: 180, readOnly: true},
            items: [
                {
                    fieldLabel: 'Дата',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Агент пользователя',
//                                                    bind: {value: '{theAlert.}'}
                },
                {
                    fieldLabel: 'Тема',
//                                                    bind: {value: '{theAlert.}'}
                }
            ]
        }
    ]
});
