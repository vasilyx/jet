/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.AlertBasic', {
    extend: 'Ext.panel.Panel',

    xtype: 'alert-panel-basic',

    requires: [
        'Ext.container.Container',
        'Ext.form.FieldSet',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Display',
        'Ext.layout.container.Center',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.util.Format'
    ],

    title: 'Основное',
    layout: 'center',

    items: {
        xtype: 'container',
        width: '70%',
        height: '100%',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Идентификатор',
                        bind: {
                            value: '{theAlert.id}'
                        }
                    },
                    {
                        fieldLabel: 'Имя сенсора',
                        bind: {
                            value: '<a target="_blank" href="#settings/system-component?id={theAlert.system_component_id}">{theAlert.sensor_name}</a>'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Дата/время',
                        renderer: Ext.util.Format.dateRenderer('d.m.Y H:i:s.u'),
                        bind: {
                            value: '{theAlert.alert_timestamp}'
                        }
                    },
                    {
                        fieldLabel: 'IP сенсора',
                        bind: {
                            value: '<a target="_blank" href="#settings/system-component?id={theAlert.system_component_id}">{theAlert.sensor_ip}</a>'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Категория',
                        bind: {
                            value: '{theAlert.category_name}'
                        }
                    },
                    {
                        fieldLabel: 'Контролируемая система',
                        bind: {
                            value: '<a target="_blank" href="#settings/system-component?id={theAlert.system_component_id}&tab=monitored-system">{theAlert.controlled_system}</a>'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Критичность',
                        bind: {
                            value: '{theAlert.severity}'
                        },
                        renderer: 'severityRenderer'
                    },
                    {
                        fieldLabel: 'Протокол',
                        bind: {
                            value: '{theAlert.transport_protocol}'
                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Показатели агрегации СИБ',
                style: {
                    background: 'transparent'
                },
                layout: 'hbox',
                items: [
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Количество',
                        labelAlign: 'right',
                        labelWidth: 80,
                        width: 150,
                        margin: '0 10 0 0',
                        bind: {
                            value: '{theAlert.alert_count}'
                        }
                    },
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Период агрегации с',
                        labelAlign: 'right',
                        labelWidth: 140,
                        margin: '0 10 0 0',
                        flex: 1.2,
                        renderer: Ext.util.Format.dateRenderer('d.m.Y H:i:s.u'),
                        bind: {
                            value: '{theAlert.alert_timestamp_start}'
                        }
                    },
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'по',
                        labelAlign: 'right',
                        labelWidth: 20,
                        flex: 0.9,
                        renderer: Ext.util.Format.dateRenderer('d.m.Y H:i:s.u'),
                        bind: {
                            value: '{theAlert.alert_timestamp_end}'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    flex: 1
                },
                items: [
                    {
                        xtype: 'fieldset',
                        title: 'Источник',
                        margin: '0 10 0 0',
                        style: {
                            background: 'transparent'
                        },
                        defaults: {
                            xtype: 'displayfield'
                        },
                        items: [
                            {
                                fieldLabel: 'Имя',
                                bind: {
                                    value: '{theAlert.source_name}'
                                }
                            },
                            {
                                fieldLabel: 'IP-адрес',
                                bind: {
                                    value: '{sourceIP}'
                                }
                            },
                            {
                                fieldLabel: 'Порт',
                                bind: {
                                    value: '{theAlert.source_port}'
                                }
                            },
                            {
                                fieldLabel: 'Тип',
                                bind: {
                                    value: '{theAlert.source_type}'
                                }
                            },
                            {
                                xtype: 'checkbox',
                                fieldLabel: 'Внутренняя сеть',
                                readOnly: true,
                                bind: {
                                    value: '{theAlert.source_internal}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        title: 'Получатель',
                        style: {
                            background: 'transparent'
                        },
                        defaults: {
                            xtype: 'displayfield'
                        },
                        items: [
                            {
                                fieldLabel: 'Имя',
                                bind: {
                                    value: '{theAlert.target_name}'
                                }
                            },
                            {
                                fieldLabel: 'IP-адрес',
                                bind: {
                                    value: '{targetIP}'
                                }
                            },
                            {
                                fieldLabel: 'Порт',
                                bind: {
                                    value: '{theAlert.target_port}'
                                }
                            },
                            {
                                fieldLabel: 'Тип',
                                bind: {
                                    value: '{theAlert.target_type}'
                                }
                            },
                            {
                                xtype: 'checkbox',
                                fieldLabel: 'Внутренняя сеть',
                                readOnly: true,
                                bind: {
                                    value: '{theAlert.target_internal}'
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
});
