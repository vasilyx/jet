/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.pcap.AlertPcapModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.alert-pcap',

    requires: [
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.alert.AlertPcap'
    ],

    stores: {
        AlertPcap: {
            model: 'Pluton.model.alert.AlertPcap',
            pageSize: 0,
            proxy: {
                type: 'json-ajax',
                url: '/alert/pcap'
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        }
    }
});
