/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.pcap.AlertPcap', {
    extend: 'Ext.grid.Panel',

    xtype: 'alert-pcap',

    requires: [
        'Ext.grid.Panel',
        'Ext.grid.plugin.RowWidget',
        'Ext.toolbar.Separator',
        'Pluton.view.alert.card.tab.pcap.AlertPcapController',
        'Pluton.view.alert.card.tab.pcap.AlertPcapModel'
    ],

    controller: 'alert-pcap',
    viewModel: {
        type: 'alert-pcap'
    },

    bind: {
        store: '{AlertPcap}'
    },

    title: 'PCAP',
    cls: 'alert-pcap',
    plugins: {
        ptype: 'rowwidget',
        widget: {
            xtype: 'grid',
            bind: {
                store: '{record.body}'
            },
            cls: 'alert-pcap-body',
            viewConfig: {
                stripeRows: false
            },
            columns: [
                {
                    dataIndex: 'field1',
                    text: 'Содержимое пакета',
                    menuDisabled: true,
                    sortable: false,
                    flex: 1
                }
            ]
        }
    },
    listeners: {
        expand: 'onExpand'
    },

    lbar: [
        {
            tooltip: 'Обновить',
            iconCls: 'x-fa fa-refresh',
            handler: 'onRefreshBtn'
        },
        '-',
        {
            tooltip: 'Скачать PCAP',
            iconCls: 'x-fa fa-download',
            ui: 'md-blue',
            handler: 'onDownloadBtn'
        }
    ],

    columns: [
        {
            dataIndex: 'head',
            text: 'Пакет',
            menuDisabled: true,
            sortable: false,
            flex: 1
        }
    ]
});
