/**
 *
 */
Ext.define('Pluton.view.alert.card.tab.pcap.AlertPcapController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.alert-pcap',

    onBeforeStoreLoad: function (store) {
        var theAlert = this.getViewModel().get('theAlert'),
            alertId = theAlert.getId(),
            alertTimestamp = theAlert.get('alert_timestamp') && theAlert.get('alert_timestamp').toISOString(),
            systemComponentId = theAlert.get('system_component_id');

        store.getProxy().setExtraParams({
            alert_id: alertId,
            alert_timestamp: (alertTimestamp ? alertTimestamp.slice(0, 13).replace(/[T-]/g, '/') : null),
            system_component_id: systemComponentId
        });

        return true;
    },

    onExpand: function () {
        var store = this.getView().getStore();

        if (!store.isLoaded()) {
            store.load();
        }
    },

    onRefreshBtn: function () {
        this.getView().getStore().load();
    },

    onDownloadBtn: function () {
        var alertId = this.getViewModel().get('theAlert.id');

        Ext.Msg.confirm(
            'Подтвердите действие',
            'Сохранить файл PCAP\'a на диск?',
            function (btn) {
                if (btn === 'yes') {
                    window.open('/pcap/' + alertId + '.pcap');
                }
            }
        );
    }
});
