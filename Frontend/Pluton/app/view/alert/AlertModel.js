/**
 *
 */
Ext.define('Pluton.view.alert.AlertModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.alert',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.alert.Alert',
        'Pluton.store.directory.AlertType',
        'Pluton.store.directory.Severity'
    ],

    stores: {
        Alert: {
            model: 'Pluton.model.alert.Alert',
            remoteSort: true,
            remoteFilter: true,
            sorters: {
                property: 'alert_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/alert/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        AlertType: {
            type: 'alertType'
        },
        Severity: {
            type: 'severity'
        }
    },

    data: {
        isValid: true
    }
});
