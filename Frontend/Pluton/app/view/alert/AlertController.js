/**
 *
 */
Ext.define('Pluton.view.alert.AlertController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.alert',

    route: 'events/alert',
    tabPrefix: 'alert_',

    requires: [
        'Pluton.view.alert.card.AlertCard',
        'Pluton.model.alert.Alert'
    ],

    listen: {
        component: {
            '#': {
                routeWithParams: 'onRouteParams'
            }
        },
        controller: {
            'tree-system-component': {
                filterApply: 'onFilterApply'
            }
        }
    },

    onRouteParams: function(params) {
        if (params && params.id) {
            var record = new Pluton.model.alert.Alert;
            record.setId(params.id);
            this.showCard(record);
        }
    },

    onActivate: function() {
        this.fireEvent('componentTreeSetValue');
    },

    onRefresh: function() {
        this.getStore('Alert').reload();
    },

    onOpen: function() {
        var selection = this.getViewModel().get('alertGrid.selection');

        if (selection) {
            this.redirectTo(this.route + '?id=' + selection.getId());
        }
    },

    onItemDblClick: function (grid, record) {
        this.redirectTo(this.route + '?id=' + record.getId());
    },

    onTabChange: function( tabPanel, newCard, oldCard, eOpts ) {
        if (newCard.getXType() === 'alert-card' && !Ext.isEmpty(newCard.itemId)) {
            this.redirectTo(this.route + '?id=' + newCard.itemId.replace(this.tabPrefix, ''));
        } else {
            this.redirectTo(this.route);
        }
    },

    showCard: function(record) {
        var view = this.getView(),
            itemId = this.getTabItemId(record.getId()),
            tab = view.items.getByKey(itemId);

        if (tab) {
            view.setActiveTab(tab);
        } else {
            tab = view.add({
                xtype: 'alert-card',
                itemId: itemId,
                viewModel: {
                    parent: this.getViewModel(),
                    data: {
                        theAlert: record
                    }
                }
            });

            view.setActiveTab(tab);
            tab.fireEvent('load');
        }
    },

    onValidityChange: function(form, valid) {
        this.getViewModel().set('isValid', valid);
    },

    onFilterApply: function() {
        var store = this.getStore('Alert'),
            values = this.lookup('alertListFilters').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function(key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'source_ip':
                    case 'target_ip':
                    case 'alert_type_id_':
                        operator = '=';
                        break;
                    case 'severity_id_':
                        operator = 'between';
                        break;
                    case 'alert_timestamp':
                        var now = new Date(),
                            from, to;

                        var year = now.getUTCFullYear();
                        var month = now.getUTCMonth();
                        var day = now.getUTCDate();

                        operator = 'between';
                        switch (value) {
                            case 1:
                                from = new Date(Date.UTC(year, month, day, 0, 0, 0, 0));
                                to = new Date(Date.UTC(year, month, day, 23, 59, 59, 999));
                                break;
                            case 2: // from first day of week
                                var monday = now.getDay(),
                                    diff = now.getDate() - monday + (monday == 0 ? -6:1),
                                    mondayDate = new Date(now.setDate(diff));

                                from = new Date(Date.UTC(year, month, mondayDate.getUTCDate(), 0, 0, 0, 0));
                                to = new Date(Date.UTC(year,month,day,23,59,59,999));
                                break;
                            case 3:
                                from = new Date(Date.UTC(year, month, 1, 0, 0, 0, 0));
                                to = new Date(Date.UTC(year, month, day , 23, 59, 59, 999));
                                break;
                            case 4:
                                from = this.lookup('timestampFrom').getValue();
                                to = this.lookup('timestampTo').getValue();
                                break;
                        }

                        value = [
                            Ext.Date.format(from, 'Y-m-d H:i:sP'),
                            Ext.Date.format(to, 'Y-m-d H:i:sP')
                        ];
                        break;
                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            } else {
                store.removeFilter(key, true);
            }
        }, this);

        filtersConfig.push({
            property: 'system_component_id',
            operator: 'in',
            value: this.getTreeSelection()
        });

        store.addFilter(filtersConfig, true);
        store.load();
    },

    onFilterReset: function() {
        var store = this.getStore('Alert');

        this.lookup('alertListFilters').reset();

        store.clearFilter(true);
        store.addFilter({
            property: 'system_component_id',
            operator: 'in',
            value: this.getTreeSelection()
        }, true);
        store.load();
    },

    getTabItemId: function(id) {
        return (this.tabPrefix + id);
    },

    getTreeSelection: function() {
        var res = [],
            selection = this.lookup('treePanelSystemComponent').getChecked(),
            sel = this.lookup('treePanelSystemComponent').getSelection();

        if (!Ext.isEmpty(sel)) {
            this.getViewModel().set('component_tree_selected_id', sel[0].getId());
        }

        if (selection) {
            selection.forEach(function(item) {
                res.push(item.getId());
            })
        }

        return res;
    }
});
