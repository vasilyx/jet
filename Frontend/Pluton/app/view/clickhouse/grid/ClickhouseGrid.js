/**
 *
 */
Ext.define('Pluton.view.clickhouse.grid.ClickhouseGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'clickhouse-grid',

    requires: [
        'Ext.container.Container',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.grid.column.Date',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.slider.Multi',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Separator'
    ],

    reference: 'clickhouseGrid',
    closable: false,

    listeners: {
        activate: 'onActivate'
    },

    bind: {
        store: '{ClickhouseAlert}'
    },

    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefresh'
        }
    ],
    title: 'Логи BRO',

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'id',
                text: 'Идентификатор',
                hidden: true
            },
            {
                dataIndex: 'alert_type',
                sorter: 'alert_type_label_',
                text: 'Тип',
                flex: 2,
                renderer: Ext.labelRenderer
            },
            {
                xtype: 'datecolumn',
                format: 'd.m.Y H:i:s.u',
                dataIndex: 'alert_timestamp',
                text: 'Время события',
                flex: 2
            },
            {
                dataIndex: 'severity',
                sorter: 'severity_label_',
                text: 'Уровень критичности',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'alert_rule_name',
                text: 'Имя'
            },
            {
                dataIndex: 'sensor',
                sorter: 'sensor_label_',
                text: 'Сенсор',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'source_ip',
                text: 'IP-источник'
            },
            {
                dataIndex: 'target_ip',
                text: 'IP-получатель'
            },
            {
                dataIndex: 'alert_status',
                text: 'Статус'
            },
            {
                dataIndex: 'controlled_system',
                sorter: 'controlled_system_label_',
                text: 'Контролируемая система',
                renderer: Ext.labelRenderer
            }
        ]
    },

    tbar: [
        {
            title: 'Фильтры',
            xtype: 'fieldset',
            ui: 'fieldset-without-padding',
            style: {
                background: 'transparent'
            },
            collapsible: true,
            collapsed: true,
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    reference: 'clickhouseListFilters',
                    xtype: 'form',
                    layout: 'hbox',
                    scrollable: true,
                    defaults: {
                        flex: 1,
                        minWidth: 300
                    },
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    listeners: {
                        validitychange: 'onValidityChange'
                    },
                    bbar: {
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                text: 'Применить',
                                tooltip: 'Применить фильтры',
                                ui: 'md-blue',
                                handler: 'onFilterApply',
                                bind: {
                                    disabled: '{!isValid}'
                                }
                            },
                            {
                                text: 'Сбросить',
                                tooltip: 'Сбросить фильтры',
                                handler: 'onFilterReset'
                            }
                        ]
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    reference: 'timestampField',
                                    publishes: ['value'],
                                    name: 'alert_timestamp',
                                    xtype: 'combo',
                                    fieldLabel: 'Обнаружено за',
                                    emptyText: 'Всё время',
                                    editable: false,
                                    clearTrigger: true,
                                    store: [
                                        [1, 'Последний день'],
                                        [2, 'Последнюю неделю'],
                                        [3, 'Последний месяц'],
                                        [4, 'Период']
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    labelSeparator: '',
                                    fieldLabel: '&nbsp;',
                                    items: [
                                        {
                                            reference: 'timestampFrom',
                                            xtype: 'datefield',
                                            format: 'd.m.Y H:i:s.u',
                                            altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                            allowBlank: false,
                                            emptyText: 'c',
                                            flex: 1,
                                            bind: {
                                                disabled: '{timestampField.value != 4}'
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            margin: '0 5 0 5',
                                            value: ' - '
                                        },
                                        {
                                            reference: 'timestampTo',
                                            xtype: 'datefield',
                                            format: 'd.m.Y H:i:s.u',
                                            altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                            allowBlank: false,
                                            emptyText: 'по',
                                            flex: 1,
                                            bind: {
                                                disabled: '{timestampField.value != 4}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'alert_type_id_',
                                    xtype: 'combo',
                                    fieldLabel: 'Тип',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    bind: {
                                        store: '{AlertType}'
                                    }
                                },
                                {
                                    name: 'source_ip',
                                    xtype: 'textfield',
                                    fieldLabel: 'Источник'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            margin: '0 10 0 0',
                            items: [
                                {
                                    name: 'severity_id_',
                                    xtype: 'tagfield',
                                    fieldLabel: 'Уровень критичности',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    valueField: 'id',
                                    displayField: 'label',
                                    clearTrigger: true,
                                    anyMatch: true,
                                    forceSelection: true,
                                    grow: false,
                                    bind: {
                                        store: '{Severity}'
                                    }
                                },
                                {
                                    name: 'target_ip',
                                    xtype: 'textfield',
                                    fieldLabel: 'Получатель'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    name: 'sensor_id_',
                                    xtype: 'combo',
                                    fieldLabel: 'Сенсор',
                                    emptyText: 'Любой',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    bind: {
                                        store: '{Sensor}'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]

        }
    ],

    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{ClickhouseAlert}'
                }
            }
        ]
    }
});
