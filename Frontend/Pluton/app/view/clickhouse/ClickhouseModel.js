Ext.define('Pluton.view.clickhouse.ClickhouseModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.clickhouse',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.clickhouse.Alert',
        'Pluton.store.directory.AlertType',
        'Pluton.store.directory.Severity',
        'Pluton.store.directory.Sensor'
    ],
    stores: {
        ClickhouseAlert: {
            model: 'Pluton.model.clickhouse.Alert',
            pageSize: 20,
            remoteSort: true,
            remoteFilter: true,
            sorters: {
                property: 'alert_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/clickhouse/alert/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        Sensor: {
            type: 'sensor'
        },
        AlertType: {
            type: 'alertType'
        },
        Severity: {
            type: 'severity'
        }
    },

    data: {
        
    }
});
