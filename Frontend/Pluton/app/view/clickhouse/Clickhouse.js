Ext.define('Pluton.view.clickhouse.Clickhouse', {
    extend: 'Ext.tab.Panel',

    xtype: 'events.clickhouse',

    requires: [
        'Pluton.view.clickhouse.ClickhouseController',
        'Pluton.view.clickhouse.ClickhouseModel',
        'Pluton.view.clickhouse.grid.ClickhouseGrid'
    ],

    controller: 'clickhouse',
    viewModel: {
        type: 'clickhouse'
    },

    listeners: {
        activate: 'onActivate'
    },

    items: [
        {
            xtype: 'clickhouse-grid'
        }
    ]
});
