Ext.define('Pluton.view.clickhouse.ClickhouseController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.clickhouse',

    requires: [
    ],

    onValidityChange: function(form, valid) {
        this.getViewModel().set('isValid', valid);
    },
    onActivate: function() {
        this.getStore('ClickhouseAlert').load();
    },
    onRefresh: function() {
        this.getStore('ClickhouseAlert').reload();
    },
    onFilterApply: function() {
        var store = this.getStore('ClickhouseAlert'),
            values = this.lookup('clickhouseListFilters').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function(key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'source_ip':
                    case 'target_ip':
                        operator = 'like';
                        break;
                    case 'alert_type_id_':
                    case 'sensor_id_':
                        operator = '=';
                        break;
                    case 'severity_id_':
                        operator = 'in';
                        break;
                    case 'alert_timestamp':
                        var now = new Date(),
                            from, to;

                        var year = now.getUTCFullYear();
                        var month = now.getUTCMonth();
                        var day = now.getUTCDate();

                        operator = 'between';
                        switch (value) {
                            case 1:
                                from = new Date(Date.UTC(year, month, day, 0, 0, 0, 0));
                                to = new Date(Date.UTC(year, month, day, 23, 59, 59, 999));
                                break;
                            case 2: // from first day of week
                                var monday = now.getDay(),
                                    diff = now.getDate() - monday + (monday == 0 ? -6:1),
                                    mondayDate = new Date(now.setDate(diff));

                                from = new Date(Date.UTC(year, month, mondayDate.getUTCDate(), 0, 0, 0, 0));
                                to = new Date(Date.UTC(year,month,day,23,59,59,999));
                                break;
                            case 3:
                                from = new Date(Date.UTC(year, month, 1, 0, 0, 0, 0));
                                to = new Date(Date.UTC(year, month, day , 23, 59, 59, 999));
                                break;
                            case 4:
                                from = this.lookup('timestampFrom').getValue();
                                to = this.lookup('timestampTo').getValue();
                                break;
                        }

                        value = [
                            Ext.Date.format(from, 'C'),
                            Ext.Date.format(to, 'C')
                        ];
                        break;
                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            } else {
                store.removeFilter(key, true);
            }
        }, this);

        store.addFilter(filtersConfig, true);
        store.load();
    },

    onFilterReset: function() {
        this.lookup('clickhouseListFilters').reset();
        this.getStore('ClickhouseAlert').clearFilter();
    }
});
