/**
 *
 */
Ext.define('Pluton.view.host.util.Host', {
    alternateClassName: 'HostUtil',

    singleton: true,

    addedTypes: [
        {id: 'MANUAL', label: 'Импортировано пользователем'},
        {id: 'DETECTED', label: 'Обнаружено в трафике'}
    ],

    getAddedTypeLabel: function (value) {
        var addedType;

        if (!Ext.isEmpty(value)) {
            addedType = Ext.Array.findBy(HostUtil.addedTypes, function (item) {
                return item.id === value;
            });

            if (addedType) {
                addedType = addedType.label;
            } else {
                addedType = value;
            }
        }

        return addedType || '';
    },

    hostnameRenderer: function (value) {
        if (Ext.isEmpty(value)) {
            return 'Неизвестно';
        } else {
            return value;
        }
    }
});
