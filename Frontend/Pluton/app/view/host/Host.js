/**
 *
 */
Ext.define('Pluton.view.host.Host', {
    extend: 'Ext.tab.Panel',

    permissions: 'host_page',

    xtype: 'env.host',

    requires: [
        'Ext.layout.container.Border',
        'Ext.panel.Panel',
        'Pluton.utility.tree.TreeSystemComponent',
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.host.HostController',
        'Pluton.view.host.HostModel',
        'Pluton.view.host.grid.HostGrid'
    ],

    controller: 'host',
    viewModel: {
        type: 'host'
    },

    plugins: {
        ptype: 'help-tool'
    },
    defaults: {
        closable: true
    },

    listeners: {
        activate: 'onActivate',
        tabchange: 'onTabChange'
    },

    items: [
        {
            title: 'Активы',
            xtype: 'panel',
            layout: 'border',
            closable: false,
            items: [
                {
                    xtype: 'utility-tree-system-component',
                    region: 'east',
                    split: true,
                    listeners: {
                        rowclick: 'onFilterApply'
                    }
                },
                {
                    xtype: 'host-grid',
                    region: 'center'
                }
            ]
        }
    ]
});
