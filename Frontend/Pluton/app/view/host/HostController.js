/**
 *
 */
Ext.define('Pluton.view.host.HostController', {
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.host',

    route: 'env/host',
    tabPrefix: 'host_',

    requires: [
        'Pluton.view.host.card.HostCard',
        'Pluton.model.host.Host',
        'Pluton.view.host.grid.import.HostImport'
    ],

    listen: {
        component: {
            '#': {
                routeWithParams: 'onRouteParams'
            }
        },
        controller: {
            'tree-system-component': {
                filterApply: 'onFilterApply'
            }
        }
    },

    onRouteParams: function(params) {
        if (params && params.id) {
            var record = new Pluton.model.host.Host;
            record.setId(params.id);
            this.showCard(record);
        }
    },

    onActivate: function() {
        this.fireEvent('componentTreeSetValue');
    },

    onPreviewExpand: function (rowNode, record, expandRow, preview) {
        rowNode = Ext.get(rowNode);

        rowNode.mask('');
        record.getPreview().load({
            callback: function () {
                preview.getViewModel().set('record', record);
                rowNode.unmask();
            }
        });
    },

    onRefreshBtn: function () {
        this.getStore('Hosts').load();
    },

    onItemDblClick: function (grid, record) {
        this.redirectTo(this.route + '?id=' + record.getId());
    },

    onTabChange: function( tabPanel, newCard, oldCard, eOpts ) {
        if (newCard.getXType() === 'host-card' && !Ext.isEmpty(newCard.itemId)) {
            this.redirectTo(this.route + '?id=' + newCard.itemId.replace(this.tabPrefix, '')); // remove 'host_'
        } else {
            this.redirectTo(this.route);
        }
    },

    onOpenBtn: function () {
        var record = this.getViewModel().get('hostGrid.selection');

        if (record) {
            this.redirectTo(this.route + '?id=' + record.getId());
        }
    },

    showCard: function (record) {
        var view = this.getView(),
            itemId = this.getTabItemId(record.getId()),
            tab = view.items.getByKey(itemId);

        if (tab) {
            view.setActiveTab(tab);
        } else {
            tab = view.add({
                xtype: 'host-card',
                itemId: itemId,
                viewModel: {
                    data: {
                        theHost: record
                    }
                }
            });

            view.setActiveTab(tab);
            tab.fireEvent('load');
        }
    },

    getTabItemId: function (id) {
        return (this.tabPrefix + id);
    },

    onImportBtn: function () {
        Ext.widget('host-import', {
            listeners: {
                importsuccess: 'onImportSuccess',
                scope: this
            }
        });
    },

    onImportSuccess: function () {
        this.getStore('Hosts').load();
    },

    onFilterApply: function () {
        var me = this,
            store = me.getStore('Hosts'),
            values = me.lookup('hostGridFilter').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function (key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {

                    // Основное
                    case 'hostname':
                        operator = 'like';
                        break;
                    case 'host_ip':
                        operator = 'contained_by';
                        value = value.split(',');
                        break;
                    case 'host_status':
                    case 'added':
                        operator = '=';
                        break;

                    // Периодичность
                    case 'create_timestamp':
                        operator = 'between';
                        value = me.getFilterTimeStamp('Created', value);
                        break;
                    case 'last_time_activity':
                        operator = 'between';
                        value = me.getFilterTimeStamp('Active', value);
                        break;
                    case 'alert_timestamp':
                        operator = 'between';
                        value = me.getFilterTimeStamp('Events', value);
                        break;

                    // Программное обеспечение
                    case 'port':
                    case 'protocol':
                        operator = 'like';
                        break;
                    case 'service':
                    case 'service_status':
                    case 'os':
                    case 'os_status':
                        operator = 'in';
                        break;

                    // События
                    case 'severity':
                        operator = 'between';
                        value = [value, 5];
                        break;
                    case 'is_new_po_alert':
                    case 'is_new_os_alert':
                    case 'is_new_service_alert':
                    case 'is_anomaly_alert':
                        operator = '=';
                        break;

                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            } else {
                store.removeFilter(key, true);
            }
        }, this);

        filtersConfig.push({
            property: 'system_component_id',
            operator: 'in',
            value: this.getTreeSelection()
        });

        store.addFilter(filtersConfig, true);
        store.load();
    },

    onFilterReset: function () {
        var store = this.getStore('Hosts');

        this.lookup('hostGridFilter').reset();
        store.clearFilter(true);
        store.addFilter({
            property: 'system_component_id',
            operator: 'in',
            value: this.getTreeSelection()
        }, true);
        store.load();
    },

    onFilterValidityChange: function (form, valid) {
        this.getViewModel().set('isFilterValid', valid);
    },

    getFilterTimeStamp: function (markReference, value) {
        var now = new Date(),
            year = now.getUTCFullYear(),
            month = now.getUTCMonth(),
            day = now.getUTCDate(),
            from, to;

        switch (value) {
            case 1:
                from = new Date(Date.UTC(year, month, day, 0, 0, 0, 0));
                to = new Date(Date.UTC(year, month, day, 23, 59, 59, 999));
                break;
            case 2: // from first day of week
                var monday = now.getDay(),
                    diff = now.getDate() - monday + (monday === 0 ? -6 : 1),
                    mondayDate = new Date(now.setDate(diff));

                from = new Date(Date.UTC(year, month, mondayDate.getUTCDate(), 0, 0, 0, 0));
                to = new Date(Date.UTC(year,month,day,23,59,59,999));
                break;
            case 3:
                from = new Date(Date.UTC(year, month, 1, 0, 0, 0, 0));
                to = new Date(Date.UTC(year, month, day , 23, 59, 59, 999));
                break;
            case 4:
                from = this.lookup('timestamp' + markReference + 'From').getValue();
                to = this.lookup('timestamp' + markReference + 'To').getValue();
                break;
        }

        return [
            Ext.Date.format(from, 'C'),
            Ext.Date.format(to, 'C')
        ];
    },

    getTreeSelection: function () {
        var selectedNodes = this.lookup('treePanelSystemComponent').getChecked(),
            result = [],
            sel = this.lookup('treePanelSystemComponent').getSelection();

        if (!Ext.isEmpty(sel)) {
            this.getViewModel().set('component_tree_selected_id', sel[0].getId());
        }

        if (!Ext.isEmpty(selectedNodes)) {
            selectedNodes.forEach(function (node) {
                result.push(node.getId());
            });
        }

        return result;
    }
});
