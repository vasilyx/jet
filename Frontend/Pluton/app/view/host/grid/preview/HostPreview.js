/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.HostPreview', {
    extend: 'Ext.container.Container',

    xtype: 'host-preview',

    requires: [
        'Ext.layout.container.HBox',
        'Pluton.view.host.grid.preview.HostPreviewModel',
        'Pluton.view.host.grid.preview.circles.HostPreviewCircleBase',
        'Pluton.view.host.grid.preview.circles.HostPreviewCircleColored',
        'Pluton.view.host.grid.preview.circles.HostPreviewCircleCreated',
        'Pluton.view.host.grid.preview.circles.HostPreviewCircleLastActivity',
        'Pluton.view.host.grid.preview.circles.HostPreviewCircleOS',
        'Pluton.view.host.grid.preview.circles.HostPreviewCircleSpeed'
    ],

    viewModel: {
        type: 'host-preview'
    },
    layout: 'hbox',
    defaults: {
        minWidth: 120,
        flex: 1
    },
    items: [
        {
            xtype: 'host-preview-circle-os',
            legend: 'Операционная<br>система',
            bind: {
                legend: 'Операционная<br>система ({record.host_os.length || 0})',
                value: '{record.host_os}'
            }
        },
        {
            xtype: 'host-preview-circle-created',
            legend: 'Создан',
            bind: {
                value: '{record.preview}'
            }
        },
        {
            xtype: 'host-preview-circle-last-activity',
            legend: 'Последняя<br>активность',
            bind: {
                value: '{record.preview.last_time_activity}'
            }
        },
        {
            xtype: 'host-preview-circle-base',
            legend: 'Сигнатурное<br>обнаружение',
            bind: {
                value: '{record.preview.signatures_count}'
            }
        },
        {
            xtype: 'host-preview-circle-base',
            legend: 'Чёрные списки',
            bind: {
                value: '{record.preview.blacklist_count}'
            }
        },
        {
            xtype: 'host-preview-circle-colored',
            legend: 'Программное<br>обеспечение',
            bind: {
                value: '{record.preview.application_count}'
            }
        },
        {
            xtype: 'host-preview-circle-colored',
            legend: 'Сетевые<br>сервисы',
            bind: {
                value: '{record.preview.service_count}'
            }
        },
        {
            xtype: 'host-preview-circle-base',
            legend: 'Уязвимости',
            bind: {
                value: '{record.preview.vulnerabilities_count}'
            }
        },
        {
            xtype: 'host-preview-circle-speed',
            legend: 'Интенсивность<br>обмена',
            bind: {
                value: '{record.preview.speed}'
            }
        }
    ]
});
