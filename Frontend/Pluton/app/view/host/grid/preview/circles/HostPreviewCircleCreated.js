/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.circles.HostPreviewCircleCreated', {
    extend: 'Pluton.view.host.grid.preview.circles.HostPreviewCircleBase',

    xtype: 'host-preview-circle-created',

    requires: [
        'Pluton.view.host.grid.preview.circles.HostPreviewIcon',
        'Pluton.view.host.util.Host'
    ],

    valueCls: null,

    initComponent: function () {
        var me = this,
            createWidget = Ext.widget,
            iconField,
            valueField,
            legendField;

        me.iconField = iconField = createWidget('host-preview-icon', {
            hidden: true
        });
        me.valueField = valueField = createWidget('component', {
            style: {
                textAlign: 'center'
            }
        });
        me.legendField = legendField = createWidget('label', {
            style: {
                textAlign: 'center'
            },
            html: me.getLegend()
        });

        me.items = [
            {
                xtype: 'container',
                cls: me.baseCircleCls,
                width: 100,
                height: 100,
                layout: {
                    type: 'vbox',
                    align: 'middle',
                    pack: 'center'
                },
                items: [
                    iconField,
                    valueField
                ]
            },
            legendField
        ];

        Ext.container.Container.prototype.initComponent.call(me, arguments);
    },

    onValueChange: function (preview) {
        var me = this,
            iconField = me.iconField,
            valueField = me.valueField,
            created = preview.get('created'),
            added = preview.get('added'),
            hint,
            cls;

        iconField.hide();
        valueField.hide();

        if (!Ext.isDate(created)) {
            valueField.setHtml(me.notFoundText);
            valueField.show();
        } else {
            hint = me.getHintByType(added);
            cls = hint.icon + ' small-icon';
            iconField.removeCls(iconField.cls).addCls(cls);
            iconField.cls = cls;
            iconField.setTooltip(hint.tooltip);
            valueField.setHtml(Ext.Date.format(created, 'd.m.Y') + '<br>' + Ext.Date.format(created, 'H:i:s'));
            iconField.show();
            valueField.show();
        }
    },

    getHintByType: function (type) {
        var me = this,
            icon,
            tooltip;

        switch (type) {
            case 'DETECTED':
                icon = 'globe';
                break;
            case 'MANUAL':
                icon = 'mouse-pointer';
                break;
            default:
                icon = me.notFoundIcon;
                break;
        }

        tooltip = HostUtil.getAddedTypeLabel(type);
        icon = 'x-fa fa-' + icon;

        return {
            icon: icon,
            tooltip: tooltip
        };
    }
});
