/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.circles.HostPreviewCircleSpeed', {
    extend: 'Pluton.view.host.grid.preview.circles.HostPreviewCircleBase',

    xtype: 'host-preview-circle-speed',

    requires: [
        'Ext.draw.Container'
    ],

    isCircle: false,
    valueCls: null,

    onValueChange: function (value) {
        var me = this,
            valueField = me.valueField,
            valueContainer = me.valueContainer,
            download, upload;

        if (!Ext.isObject(value)) {
            value = me.notFoundText;
            valueField.setHtml(value);
            valueContainer.addCls(me.baseCircleCls);
        } else {
            download = me.convertValue(value.download);
            upload = me.convertValue(value.upload);

            valueContainer.removeAll();
            valueContainer.add([
                {
                    xtype: 'draw',
                    height: 100,
                    width: 100,
                    bodyStyle: {
                        background:'rgba(0,0,0,0)'
                    },
                    sprites: [
                        {
                            type: 'circle',
                            cx: 50,
                            cy: 50,
                            r: 49,
                            fillStyle: 'rgba(0,0,0,0)',
                            strokeStyle: 'black'
                        },
                        {
                            type: 'circle',
                            cx: 50,
                            cy: 50,
                            r: 46,
                            fillStyle: 'rgba(0,0,0,0)',
                            strokeStyle: 'black'
                        },
                        {
                            type: 'line',
                            fromX: 50,
                            fromY: 3,
                            toX: 50,
                            toY: 95,
                            strokeStyle: 'black',
                            lineWidth: 1
                        },
                        {
                            type: 'text',
                            x: 28,
                            y: 55,
                            textAlign: 'center',
                            text: upload.value,
                            font: '500 13px Open Sans, sans-serif'
                        },
                        {
                            type: 'text',
                            x: 73,
                            y: 55,
                            textAlign: 'center',
                            text: download.value,
                            font: '500 13px Open Sans, sans-serif'
                        },
                        {
                            type: 'text',
                            x: 30,
                            y: 75,
                            textAlign: 'center',
                            text: upload.text,
                            font: '500 13px Open Sans, sans-serif'
                        },
                        {
                            type: 'text',
                            x: 70,
                            y: 75,
                            textAlign: 'center',
                            text: download.text,
                            font: '500 13px Open Sans, sans-serif'
                        }
                    ]
                }
            ]);
        }
    },

    convertValue: function (value) {
        var denominator,
            text;

        value = +value * 8 || 0; // converting bytes to bits

        if (value >= 1e9) {
            text = 'Гб/с';
            denominator = 1e9;
        } else if (value >= 1e6) {
            text = 'Мб/с';
            denominator = 1e6;
        } else if (value >= 1e3) {
            text = 'Кб/с';
            denominator = 1e3;
        } else {
            text = 'б/с';
        }

        if (denominator) {
            value = value / denominator;
        }
        if (value % 1 !== 0) {
            value = value.toFixed(1);
        }

        return {
            value: value,
            text: text
        };
    }
});
