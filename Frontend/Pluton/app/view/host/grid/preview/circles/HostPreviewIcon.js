/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.circles.HostPreviewIcon', {
    extend: 'Ext.Component',

    xtype: 'host-preview-icon',

    requires: [
        'Ext.tip.ToolTip'
    ],

    onRender: function () {
        var me = this;

        me.callParent(arguments);

        if (me.tooltip) {
            me.setTooltip(me.tooltip);
        }
    },
    
    setTooltip: function (tooltip) {
        var me = this,
            text;

        if (me.rendered) {
            if (!tooltip) {
                me.clearTip();
            }
            if (tooltip) {
                if (Ext.isArray(tooltip) || Ext.isObject(tooltip)) {
                    text = new Ext.XTemplate(
                        '<ul>',
                        '<tpl for=".">',
                            '<li class="{cls}">{label}</li>',
                        '</tpl>',
                        '</ul>'
                    ).apply(tooltip);
                } else {
                    text = tooltip;
                }

                me.tooltip = Ext.create('Ext.tip.ToolTip', {
                    target: me.getEl(),
                    trackMouse: true,
                    renderTo: Ext.getBody(),
                    html: text
                });
            }
        } else {
            me.tooltip = tooltip;
        }

        return me;
    },

    clearTip: function() {
        var me = this,
            el = me.getEl();

        if (me.tooltip) {
            Ext.destroy(me.tooltip);
        }
    },

    onDestroy: function () {
        var me = this;

        if (me.tooltip) {
            me.clearTip();
        }

        me.callParent();
    }
});
