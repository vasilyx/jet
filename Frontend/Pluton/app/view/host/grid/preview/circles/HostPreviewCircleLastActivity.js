/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.circles.HostPreviewCircleLastActivity', {
    extend: 'Pluton.view.host.grid.preview.circles.HostPreviewCircleBase',

    xtype: 'host-preview-circle-last-activity',

    valueCls: null,

    onValueChange: function (value) {
        var me = this,
            valueField = me.valueField,
            datePart,
            timePart;

        if (!Ext.isDate(value)) {
            valueField.setHtml(me.notFoundText);
        } else {
            datePart = Ext.Date.format(value, 'd.m.Y');
            timePart = Ext.Date.format(value, 'H:i:s');
            valueField.setHtml(datePart + '<br>' + timePart);
        }
    }
});
