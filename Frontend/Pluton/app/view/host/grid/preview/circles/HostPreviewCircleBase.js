/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.circles.HostPreviewCircleBase', {
    extend: 'Ext.container.Container',

    xtype: 'host-preview-circle-base',

    requires: [
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.layout.container.VBox'
    ],

    config: {
        legend: null,
        value: null
    },
    isCircle: true,
    baseCircleCls: 'base-circle',
    valueCls: 'base-value',
    notFoundText: 'Нет данных',
    notFoundIcon: 'question',

    layout: {
        type: 'vbox',
        align: 'middle',
        pack: 'center'
    },

    initComponent: function () {
        var me = this,
            createWidget = Ext.widget,
            valueContainerConfig = {
                xtype: 'container',
                width: 100,
                height: 100,
                layout: {
                    type: 'vbox',
                    align: 'middle',
                    pack: 'center'
                }
            },
            valueFieldConfig = {
                xtype: 'component',
                cls: me.valueCls,
                style: {
                    textAlign: 'center'
                }
            },
            legendFieldConfig = {
                xtype: 'label',
                style: {
                    textAlign: 'center'
                },
                html: me.getLegend()
            },
            valueContainer, valueField, legendField;

        if (me.isCircle) {
            valueContainerConfig.cls = me.baseCircleCls;
        }

        me.valueField = valueField = createWidget(valueFieldConfig);
        me.valueContainer = valueContainer = createWidget(Ext.apply(valueContainerConfig, {
            items: [valueField]
        }));
        me.legendField = legendField = createWidget(legendFieldConfig);

        me.items = [valueContainer, legendField];

        me.callParent(arguments);
    },

    setLegend: function (newLegend) {
        var me = this,
            oldLegend = me.getLegend();

        me.legend = newLegend;

        if (Ext.isFunction(me.onLegendChange)) {
            me.onLegendChange.call(me, newLegend, oldLegend);
        }
    },

    setValue: function (newValue) {
        var me = this,
            oldValue = me.getValue();

        me.value = newValue;

        if (Ext.isFunction(me.onValueChange)) {
            me.onValueChange.call(me, newValue, oldValue);
        }
    },

    onLegendChange: function (newLegend, oldLegend) {
        var me = this,
            legendField = me.legendField;

        if (legendField) {
            legendField.setHtml(newLegend);
        }
    },

    onValueChange: function (value) {
        var me = this,
            valueField = me.valueField,
            valueCls = me.valueCls;

        if (Ext.isEmpty(value)) {
            value = me.notFoundText;
            valueField.removeCls(valueCls);
        } else {
            if (!valueField.hasCls(valueCls)) {
                valueField.addCls(valueCls);
            }
        }

        valueField.setHtml('' + value);
    }
});
