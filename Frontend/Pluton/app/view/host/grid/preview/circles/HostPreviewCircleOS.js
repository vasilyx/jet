/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.circles.HostPreviewCircleOS', {
    extend: 'Pluton.view.host.grid.preview.circles.HostPreviewCircleBase',

    xtype: 'host-preview-circle-os',

    requires: [
        'Ext.layout.container.HBox',
        'Pluton.view.host.grid.preview.circles.HostPreviewIcon'
    ],

    osMap: {
        android: 'android',
        apple: 'apple',
        linux: '(linux|freebsd)',
        windows: 'windows'
    },

    initComponent: function () {
        var me = this,
            createWidget = Ext.widget,
            firstIcon,
            secondIcon,
            thirdIcon,
            notFoundField,
            legendField;

        me.firstIcon = firstIcon = createWidget('host-preview-icon', {
            hidden: true
        });
        me.secondIcon = secondIcon = createWidget('host-preview-icon', {
            hidden: true
        });
        me.thirdIcon = thirdIcon = createWidget('host-preview-icon', {
            hidden: true
        });
        me.notFoundField = notFoundField = createWidget('label', {
            text: me.notFoundText,
            hidden: true
        });
        me.legendField = legendField = createWidget('label', {
            style: {
                textAlign: 'center'
            },
            html: me.getLegend()
        });

        me.items = [
            {
                xtype: 'container',
                width: 100,
                height: 100,
                layout: {
                    type: 'vbox',
                    align: 'middle',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'middle',
                            pack: 'center'
                        },
                        items: [
                            notFoundField,
                            firstIcon,
                            secondIcon
                        ]
                    },
                    thirdIcon
                ]
            },
            legendField
        ];

        Ext.container.Container.prototype.initComponent.call(me, arguments);
    },

    onValueChange: function (value) {
        var me = this,
            ExtArray = Ext.Array,
            ExtArrayPush = ExtArray.push,
            firstIcon = me.firstIcon,
            secondIcon = me.secondIcon,
            thirdIcon = me.thirdIcon,
            notFoundField = me.notFoundField,
            getOsType = me.getOsType,
            getStatus = me.getStatus,
            cls,
            statusCls,
            iconField,
            group,
            groups = {},
            groupsKeys,
            groupsKeysLength,
            hiddenItems = [],
            hiddenItemsCls,
            item;

        firstIcon.hide();
        secondIcon.hide();
        thirdIcon.hide();
        notFoundField.hide();
        firstIcon.setTooltip(null);
        secondIcon.setTooltip(null);
        thirdIcon.setTooltip(null);

        value = ExtArray.from(value);
        if (!Ext.isEmpty(value)) {
            ExtArray.forEach(value, function (obj) {
                group = getOsType.call(me, obj.label);
                statusCls = getStatus(obj.status);
                if (groups.hasOwnProperty(group)) {
                    ExtArrayPush(groups[group].tooltip, {
                        cls: statusCls,
                        label: obj.label
                    });
                } else {
                    groups[group] = {
                        cls: Ext.baseCSSPrefix + 'fa fa-' + group,
                        statusCls: statusCls,
                        tooltip: [{
                            cls: statusCls,
                            label: obj.label
                        }]
                    };
                }
            });

            groupsKeys = Ext.Object.getKeys(groups);
            groupsKeysLength = groupsKeys.length;

            if (groupsKeysLength) {
                if (groupsKeysLength > 1) {
                    ExtArray.forEach(groupsKeys, function (groupKey, index) {
                        item = groups[groupKey];
                        if (index > 1 && index !== groupsKeysLength - 1) {
                            if (Ext.isEmpty(hiddenItemsCls)) {
                                hiddenItemsCls = item.statusCls;
                            }
                            hiddenItems = hiddenItems.concat(item.tooltip);
                        } else {
                            if (index === 0) {
                                iconField = firstIcon;
                            } else if (index === 1) {
                                iconField = secondIcon;
                            } else if (index === 2) {
                                iconField = thirdIcon;
                            }

                            cls = item.cls + ' medium-icon' + (item.statusCls ? ' ' + item.statusCls : '');
                            iconField.removeCls(iconField.cls).addCls(cls);
                            iconField.cls = cls;
                            iconField.setTooltip(item.tooltip);
                            iconField.show();
                        }
                    });

                    if (!Ext.isEmpty(hiddenItems)) {
                        cls = 'x-fa fa-ellipsis-h medium-icon' + (hiddenItemsCls ? ' ' + hiddenItemsCls : '');
                        thirdIcon.removeCls(thirdIcon.cls).addCls(cls);
                        thirdIcon.cls = cls;
                        thirdIcon.setTooltip(hiddenItems);
                        thirdIcon.show();
                    }
                } else {
                    item = groups[groupsKeys[0]];
                    cls = item.cls + ' large-icon' + (item.statusCls ? ' ' + item.statusCls : '');
                    firstIcon.removeCls(firstIcon.cls).addCls(cls);
                    firstIcon.cls = cls;
                    firstIcon.setTooltip(item.tooltip);
                    firstIcon.show();
                }
            } else {
                notFoundField.show();
            }
        } else {
            notFoundField.show();
        }
    },

    getOsType: function (os) {
        var me = this,
            map = me.osMap,
            regExp,
            icon;

        if (!Ext.isEmpty(os)) {
            Ext.Object.each(map, function (key, value) {
                regExp = new RegExp(value, 'gi');
                if (regExp.test(os)) {
                    icon = key;
                    return false;
                }
            });
        }

        return icon || me.notFoundIcon;
    },

    getStatus: function (status) {
        var cls = '';

        status = (status || '').toUpperCase();
        if (status !== 'ACCEPTED') {
            cls = 'negative-color';
        }
        return cls;
    }
});
