/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.circles.HostPreviewCircleColored', {
    extend: 'Pluton.view.host.grid.preview.circles.HostPreviewCircleBase',

    xtype: 'host-preview-circle-colored',

    onValueChange: function (value) {
        var me = this,
            valueField = me.valueField,
            valueContainer = me.valueContainer,
            valueCls = me.valueCls,
            negativeCircleCls = 'negative-circle',
            negativeValueCls = 'negative-color',
            allValue, disapprovedValue;

        if (!value || !Ext.isObject(value)) {
            allValue = me.notFoundText;
            disapprovedValue = 0;
            valueField.removeCls(valueCls);
        } else {
            allValue = value.all;
            disapprovedValue = value.disapproved;
            if (!valueField.hasCls(valueCls)) {
                valueField.addCls(valueCls);
            }
        }

        valueContainer.toggleCls(negativeCircleCls, disapprovedValue > 0);
        valueField.toggleCls(negativeValueCls, disapprovedValue > 0);

        valueField.setHtml('' + allValue);
    }
});
