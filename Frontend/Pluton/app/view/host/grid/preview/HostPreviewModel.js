/**
 *
 */
Ext.define('Pluton.view.host.grid.preview.HostPreviewModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.host-preview',

    stores: {

    },

    data: {
        record: null
    }
});
