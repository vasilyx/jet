/**
 *
 */
Ext.define('Pluton.view.host.grid.import.HostImportModel', {
    extend: 'Ext.app.ViewModel',
 
    alias: 'viewmodel.host-import',

    stores: {
         
    },
 
    data: {
        fileData: null
    }
});
