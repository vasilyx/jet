/**
 *
 */
Ext.define('Pluton.view.host.grid.import.HostImport', {
    extend: 'Ext.window.Window',

    permissions: 'host-import_page',

    xtype: 'host-import',

    requires: [
        'Ext.button.Button',
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.form.field.File',
        'Ext.form.field.TextArea',
        'Ext.layout.container.Fit',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Pluton.view.host.grid.import.HostImportController',
        'Pluton.view.host.grid.import.HostImportModel'
    ],

    controller: 'host-import',
    viewModel: {
        type: 'host-import'
    },

    title: 'Импорт активов из файла',
    autoShow: true,
    modal: true,
    constrain: true,
    width: 500,
    height: 650,
    bodyPadding: 10,
    layout: 'fit',

    buttons: [
        {
            text: 'Отменить',
            reference: 'cancelBtn',
            handler: 'onCancelBtn'
        },
        {
            reference: 'completeBtn',
            text: 'Завершить',
            disabled: true,
            handler: 'onCompleteBtn'
        }
    ],

    items: [

        {
            xtype: 'form',
            scrollable: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'filefield',
                    name: 'file',
                    fieldLabel: 'Файл активов',
                    msgTarget: 'side',
                    allowBlank: false,
                    buttonText: 'Выбрать файл',
                    listeners: {
                        change: 'onFileChange'
                    }
                },
                {
                    xtype: 'fieldset',
                    title: 'Информация об отчете',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Имя файла',
                            bind: {
                                value: '{fileData.name}'
                            }
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Тип',
                            bind: {
                                value: '{fileData.type}'
                            }
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Дата',
                            bind: {
                                value: '{fileData.date}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    items: {
                        reference: 'importButton',
                        xtype: 'button',
                        text: 'Импортировать',
                        disabled: true,
                        handler: 'importHostFile'
                    }
                },
                {
                    xtype: 'textarea',
                    height: 150,
                    readOnly: true,
                    fieldLabel: 'Ход импорта',
                    labelAlign: 'top'
                },
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Решение конфликтов',
                    defaultType: 'radiofield',
                    layout: 'hbox',
                    labelAlign: 'top',
                    items: [
                        {
                            boxLabel: 'Доверять результатам импорта',
                            name: 'conflict_resolution',
                            checked: true,
                            flex: 1,
                            inputValue: 'prefer_report'

                        }, {
                            boxLabel: 'Доверять результатам БД',
                            name: 'conflict_resolution',
                            flex: 1,
                            inputValue: 'prefer_db'
                        }
                    ]
                }
            ]
        }
    ]
});
