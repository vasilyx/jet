/**
 *
 */
Ext.define('Pluton.view.host.grid.import.HostImportController', {
    extend: 'Ext.app.ViewController', 
 
    alias: 'controller.host-import',

    onCancelBtn: function () {
        this.getView().close();
    },

    onCompleteBtn: function () {
        this.fireViewEvent('importsuccess');
        this.getView().close();
    },

    onFileChange: function () {
        var me = this,
            view = me.getView(),
            textarea = view.down('textarea'),
            file = view.down('filefield').fileInputEl.dom.files[0],
            reader = new FileReader();

        me.lookupReference('importButton').setDisabled(false);
        textarea.setValue('');

        reader.onload = (function (file) {
            return function (e) {
                var node,
                    parser = new DOMParser(),
                    doc = parser.parseFromString(e.target.result, 'application/xml'),
                    fileData = {
                        name: file.name,
                        type: (doc.doctype && doc.doctype.name) ? doc.doctype.name : '',
                        date: ''
                    };

                if (doc.doctype && doc.doctype.name && (node = doc.getElementsByTagName(doc.doctype.name)[0])) {
                    fileData.date = node.getAttribute('startstr');
                }

                me.getViewModel().set('fileData', fileData);
            };
        })(file);

        reader.readAsText(file);
    },

    importHostFile: function () {
        var me = this,
            form = me.getView().down('form'),
            cancelBtn = me.lookup('cancelBtn');

        cancelBtn.disable();
        me.logIntoTextarea('Импорт файла...', true);

        form.submit({
            url: '/host/list/upload',
            success: function (fp, o) {
                me.logIntoTextarea('Начало анализа...');
                me.jobRequest(o.result)
            },
            failure: function () {
                cancelBtn.enable();
                Ext.Msg.alert('Импорт активов', 'Ошибка загрузки файла');
            }
        });
    },

    jobRequest: function (id) {
        var me = this,
            completeBtn = me.lookup('completeBtn'),
            cancelBtn = me.lookup('cancelBtn');

        Ext.Ajax.request({
            url: '/jobs',
            method: 'POST',
            jsonData: {
                filters: [
                    {field: 'id', type: '=',  value: id}
                ]
            },
            success: function (response) {
                var resp = Ext.JSON.decode(response.responseText, true);

                me.logIntoTextarea(resp.rows[0].job_output, true);

                switch (resp.rows[0].status){
                    case 'SUCCESS':
                        completeBtn.enable();
                        cancelBtn.disable();
                        Ext.Msg.alert('Импорт активов', 'Импорт успешно завершен');
                        break;
                    case 'ERROR':
                        completeBtn.disable();
                        cancelBtn.enable();
                        Ext.Msg.alert('Импорт активов', 'Ошибка импорта');
                        break;
                    case 'RUNNING':
                        completeBtn.disable();
                        cancelBtn.disable();
                        setTimeout(me.jobRequest(id), 1000);
                        break;
                }
            },
            failure: function () {
                completeBtn.disable();
                cancelBtn.enable();
                Ext.Msg.alert('Импорт активов', 'Ошибка доступа к серверу job');
            }
        });
    },

    logIntoTextarea: function (text, clear){
        var textarea = this.getView().down('textarea');

        if (clear === true) {
            textarea.setValue(text);
        } else {
            textarea.setValue(textarea.getValue() + "\r\n" + text);
        }
    }
});
