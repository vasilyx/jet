/**
 *
 */
Ext.define('Pluton.view.host.grid.toolbar.FilterToolbar', {
    extend: 'Ext.form.FieldSet',

    xtype: 'host-filter-toolbar',

    requires: [
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Tag',
        'Ext.form.field.Text',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        'Ext.slider.Single',
        'Ext.tab.Panel',
        'Pluton.view.host.util.Host'
    ],

    title: 'Фильтры',
    ui: 'fieldset-without-padding',
    style: {
        background: 'transparent'
    },
    collapsible: true,
    collapsed: true,
    flex: 1,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            reference: 'hostGridFilter',
            xtype: 'form',
            layout: 'hbox',
            scrollable: true,
            fieldDefaults: {
                labelAlign: 'top'
            },
            listeners: {
                validitychange: 'onFilterValidityChange'
            },
            defaults: {
                flex: 1,
                minWidth: 300
            },
            bbar: {
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                items: [
                    {
                        text: 'Применить',
                        tooltip: 'Применить фильтры',
                        ui: 'md-blue',
                        handler: 'onFilterApply',
                        bind: {
                            disabled: '{!isFilterValid}'
                        }
                    },
                    {
                        text: 'Сбросить',
                        tooltip: 'Сбросить фильтры',
                        handler: 'onFilterReset'
                    }
                ]
            },
            items: [
                {
                    xtype: 'tabpanel',
                    cls: 'tabpanel-filter-host',
                    plain: true,
                    items: [
                        {
                            xtype: 'panel',
                            title: 'Основное',
                            bodyPadding: '0 0 10 0',
                            layout: {
                                type: 'vbox',
                                align: 'middle'
                            },
                            defaults: {
                                flex: 1
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    defaults: {
                                        flex: 1,
                                        minWidth: 300
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            name: 'hostname',
                                            fieldLabel: 'Имя',
                                            margin: '0 10 0 0'
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'host_ip',
                                            fieldLabel: 'IP-адрес'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    defaults: {
                                        flex: 1,
                                        minWidth: 300
                                    },
                                    items: [
                                        {
                                            xtype: 'combo',
                                            name: 'host_status',
                                            fieldLabel: 'Статус',
                                            valueField: 'id',
                                            displayField: 'label',
                                            clearTrigger: true,
                                            forceSelection: true,
                                            anyMatch: true,
                                            queryMode: 'local',
                                            margin: '0 10 0 0',
                                            bind: {
                                                store: '{HostStatusList}'
                                            }
                                        },
                                        {
                                            xtype: 'combo',
                                            name: 'added',
                                            fieldLabel: 'Актив создан',
                                            valueField: 'id',
                                            displayField: 'label',
                                            clearTrigger: true,
                                            forceSelection: true,
                                            anyMatch: true,
                                            queryMode: 'local',
                                            store: HostUtil.addedTypes
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            title: 'Периодичность',
                            bodyPadding: '10 0 0 0',
                            layout: {
                                type: 'vbox',
                                align: 'middle'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            reference: 'timestampFieldCreated',
                                            publishes: ['value'],
                                            name: 'create_timestamp',
                                            xtype: 'combo',
                                            fieldLabel: 'Создан за',
                                            emptyText: 'Всё время',
                                            labelAlign: 'left',
                                            labelWidth: 130,
                                            minWidth: 350,
                                            margin: '0 10 0 0',
                                            editable: false,
                                            clearTrigger: true,
                                            store: [
                                                [1, 'Последний день'],
                                                [2, 'Последнюю неделю'],
                                                [3, 'Последний месяц'],
                                                [4, 'Период']
                                            ]
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    reference: 'timestampCreatedFrom',
                                                    xtype: 'datefield',
                                                    format: 'd.m.Y H:i:s.u',
                                                    altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                                    emptyText: 'c',
                                                    minWidth: 210,
                                                    bind: {
                                                        disabled: '{timestampFieldCreated.value != 4}'
                                                    }
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 5 0 5',
                                                    value: ' - '
                                                },
                                                {
                                                    reference: 'timestampCreatedTo',
                                                    xtype: 'datefield',
                                                    format: 'd.m.Y H:i:s.u',
                                                    altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                                    emptyText: 'по',
                                                    minWidth: 210,
                                                    bind: {
                                                        disabled: '{timestampFieldCreated.value != 4}'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            reference: 'timestampFieldActive',
                                            publishes: ['value'],
                                            name: 'last_time_activity',
                                            xtype: 'combo',
                                            fieldLabel: 'Активные хосты за',
                                            emptyText: 'Всё время',
                                            labelAlign: 'left',
                                            labelWidth: 130,
                                            minWidth: 350,
                                            margin: '0 10 0 0',
                                            editable: false,
                                            clearTrigger: true,
                                            store: [
                                                [1, 'Последний день'],
                                                [2, 'Последнюю неделю'],
                                                [3, 'Последний месяц'],
                                                [4, 'Период']
                                            ]
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    reference: 'timestampActiveFrom',
                                                    xtype: 'datefield',
                                                    format: 'd.m.Y H:i:s.u',
                                                    altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                                    emptyText: 'c',
                                                    minWidth: 210,
                                                    bind: {
                                                        disabled: '{timestampFieldActive.value != 4}'
                                                    }
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 5 0 5',
                                                    value: ' - '
                                                },
                                                {
                                                    reference: 'timestampActiveTo',
                                                    xtype: 'datefield',
                                                    format: 'd.m.Y H:i:s.u',
                                                    altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                                    emptyText: 'по',
                                                    minWidth: 210,
                                                    bind: {
                                                        disabled: '{timestampFieldActive.value != 4}'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            reference: 'timestampFieldEvents',
                                            publishes: ['value'],
                                            name: 'alert_timestamp',
                                            xtype: 'combo',
                                            fieldLabel: 'Событий за',
                                            emptyText: 'Всё время',
                                            labelAlign: 'left',
                                            labelWidth: 130,
                                            minWidth: 350,
                                            margin: '0 10 0 0',
                                            editable: false,
                                            clearTrigger: true,
                                            store: [
                                                [1, 'Последний день'],
                                                [2, 'Последнюю неделю'],
                                                [3, 'Последний месяц'],
                                                [4, 'Период']
                                            ]
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    reference: 'timestampEventsFrom',
                                                    xtype: 'datefield',
                                                    format: 'd.m.Y H:i:s.u',
                                                    altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                                    emptyText: 'c',
                                                    minWidth: 210,
                                                    bind: {
                                                        disabled: '{timestampFieldEvents.value != 4}'
                                                    }
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 5 0 5',
                                                    value: ' - '
                                                },
                                                {
                                                    reference: 'timestampEventsTo',
                                                    xtype: 'datefield',
                                                    format: 'd.m.Y H:i:s.u',
                                                    altFormats: 'd|d.m|d.m.Y|d.m.Y H|d.m.Y H:i',
                                                    emptyText: 'по',
                                                    minWidth: 210,
                                                    bind: {
                                                        disabled: '{timestampFieldEvents.value != 4}'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            title: 'Программное обеспечение',
                            bodyPadding: '0 0 10 0',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            name: 'port',
                                            fieldLabel: 'Порт',
                                            margin: '0 10 0 0',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'tagfield',
                                            name: 'service',
                                            fieldLabel: 'Сервис',
                                            clearTrigger: true,
                                            grow: false,
                                            anyMatch: true,
                                            valueField: 'id',
                                            displayField: 'label',
                                            queryMode: 'local',
                                            margin: '0 10 0 0',
                                            flex: 3,
                                            bind: {
                                                store: '{ServiceCatalogResource}'
                                            }
                                        },
                                        {
                                            xtype: 'tagfield',
                                            name: 'service_status',
                                            fieldLabel: 'Статус сервиса',
                                            clearTrigger: true,
                                            grow: false,
                                            anyMatch: true,
                                            valueField: 'id',
                                            displayField: 'label',
                                            queryMode: 'local',
                                            flex: 3,
                                            bind: {
                                                store: '{RecordStatusList}'
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            name: 'protocol',
                                            fieldLabel: 'Протокол',
                                            margin: '0 10 0 0',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'tagfield',
                                            name: 'os',
                                            fieldLabel: 'Операционная система',
                                            clearTrigger: true,
                                            grow: false,
                                            anyMatch: true,
                                            valueField: 'id',
                                            displayField: 'label',
                                            queryMode: 'local',
                                            margin: '0 10 0 0',
                                            flex: 3,
                                            bind: {
                                                store: '{OperationSystemList}'
                                            }
                                        },
                                        {
                                            xtype: 'tagfield',
                                            name: 'os_status',
                                            fieldLabel: 'Статус операционной системы',
                                            clearTrigger: true,
                                            grow: false,
                                            anyMatch: true,
                                            valueField: 'id',
                                            displayField: 'label',
                                            queryMode: 'local',
                                            flex: 3,
                                            bind: {
                                                store: '{RecordStatusList}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            title: 'События',
                            bodyPadding: '0 0 5 0',
                            layout: {
                                type: 'vbox',
                                align: 'middle'
                            },
                            defaults: {
                                width: 400
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'slider',
                                            name: 'severity',
                                            fieldLabel: 'Критичность событий',
                                            value: 0,
                                            increment: 1,
                                            minValue: 0,
                                            maxValue: 5,
                                            flex: 1
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [

                                        {
                                            xtype: 'checkbox',
                                            name: 'is_new_po_alert',
                                            boxLabel: 'Обнаружено новое ПО',
                                            inputValue: true,
                                            uncheckedValue: '',
                                            margin: '0 10 0 0',
                                            width: 210
                                        },
                                        {
                                            xtype: 'checkbox',
                                            name: 'is_new_os_alert',
                                            boxLabel: 'Обнаружена новая ОС',
                                            inputValue: true,
                                            uncheckedValue: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'checkbox',
                                            name: 'is_new_service_alert',
                                            boxLabel: 'Обнаружено новое событие',
                                            inputValue: true,
                                            uncheckedValue: '',
                                            margin: '0 10 0 0',
                                            width: 210
                                        },
                                        {
                                            xtype: 'checkbox',
                                            name: 'is_anomaly_alert',
                                            boxLabel: 'Аномальное поведение',
                                            inputValue: true,
                                            uncheckedValue: ''
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});
