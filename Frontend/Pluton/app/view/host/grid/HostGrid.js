/**
 *
 */
Ext.define('Pluton.view.host.grid.HostGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'host-grid',

    requires: [
        'Ext.grid.column.Date',
        'Ext.grid.plugin.RowWidget',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Separator',
        'Pluton.utility.button.CSVExport',
        'Pluton.view.host.grid.preview.HostPreview',
        'Pluton.view.host.grid.toolbar.FilterToolbar',
        'Pluton.view.host.util.Host'
    ],

    bind: {
        store: '{Hosts}'
    },

    reference: 'hostGrid',
    bufferedRenderer: false,
    plugins: [
        {
            ptype: 'rowwidget',
            expandOnDblClick: false,
            widget: {
                xtype: 'host-preview'
            }
        }
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('unconfirmed_applications_count') > 0 ? 'negative-row': '';
        },
        listeners: {
            // {delay: 1} for correct LoadMask at first expand body
            expandbody: {
                fn: 'onPreviewExpand',
                delay: 1
            }
        }
    },
    listeners: {
        itemdblclick: 'onItemDblClick'
    },

    tbar: [
        {
            xtype: 'host-filter-toolbar'
        }
    ],
    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefreshBtn'
        },
        '-',
        {
            iconCls: 'x-fa fa-external-link',
            ui: 'md-blue',
            tooltip: 'Открыть',
            disabled: true,
            bind: {
                disabled: '{!hostGrid.selection}'
            },
            handler: 'onOpenBtn'
        },
        '-',
        {
            iconCls: 'x-fa fa-upload',
            ui: 'md-yellow',
            tooltip: 'Импорт активов из файла',
            permissions: 'host-import_page',
            handler: 'onImportBtn'
        },
        {
            xtype: 'utility-button-csv-export'
        }
    ],
    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{Hosts}'
                }
            }
        ]
    },

    columns: [
        {
            dataIndex: 'id',
            text: 'Идентификатор',
            hidden: true,
            flex: 1
        },
        {
            xtype: 'datecolumn',
            format: Ext.Date.defaultFormat,
            dataIndex: 'discovery_timestamp',
            text: 'Дата/время',
            flex: 2
        },
        {
            dataIndex: 'host_ip',
            sortable: false,
            text: 'IP-адрес',
            flex: 2,
            renderer: Ext.labelRenderer
        },
        {
            dataIndex: 'hostname',
            text: 'Имя',
            flex: 1,
            renderer: HostUtil.hostnameRenderer
        },
        {
            dataIndex: 'host_status',
            text: 'Статус',
            flex: 1,
            renderer: Ext.labelRenderer
        },
        {
            dataIndex: 'host_os',
            sortable: false,
            text: 'Операционная система',
            flex: 2,
            renderer: Ext.labelRenderer
        },
        {
            dataIndex: 'system_component',
            sorter: 'system_component_label_',
            text: 'Сенсор',
            flex: 1,
            renderer: Ext.labelRenderer
        }
    ]
});
