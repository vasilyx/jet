/**
 *
 */
Ext.define('Pluton.view.host.HostModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.host',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.host.Host'
    ],

    stores: {
        Hosts: {
            model: 'Pluton.model.host.Host',
            remoteSort: true,
            remoteFilter: true,
            sorters: {
                property: 'discovery_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/host/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        OperationSystemList: {
            autoLoad: true,
            pageSize: 0,
            proxy: {
                type: 'json-ajax',
                url: '/os/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        HostStatusList: {
            autoLoad: true,
            pageSize: 0,
            proxy: {
                type: 'json-ajax',
                url: '/host/statuses',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        ServiceCatalogResource: {
            autoLoad: true,
            pageSize: 0,
            proxy: {
                type: 'json-ajax',
                url: '/service/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        RecordStatusList: {
            autoLoad: true,
            pageSize: 0,
            proxy: {
                type: 'json-ajax',
                url: '/utils/record_statuses',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        }
    },

    data: {
        isFilterValid: true
    }
});
