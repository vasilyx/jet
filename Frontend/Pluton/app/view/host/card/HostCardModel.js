/**
 *
 */
Ext.define('Pluton.view.host.card.HostCardModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.host-card',

    stores: {
        HostStatus: {
            fields: ['id', 'label'],
            data: [
                {
                    id: 'UNKNOWN',
                    label: 'Обнаружено'
                },
                {
                    id: 'DELETED',
                    label: 'Удалено оператором'
                },
                {
                    id: 'ACCEPTED',
                    label: 'Одобрено'
                },
                {
                    id: 'REDETECTED',
                    label: 'Обнаружено заново'
                },
                {
                    id: 'DECLINED',
                    label: 'Отклонено оператором'
                }
            ]
        }
    },

    data: {
        theHost: null
    },
    
    formulas: {
        hostCardTitle: {
            bind: {
                bindTo: '{theHost}',
                deep: true
            },
            get: function (theHost) {
                var hostName,
                    hostIp,
                    title;

                if (theHost) {
                    if ((hostName = theHost.get('hostname'))) {
                        title = hostName;
                    } else {
                        if ((hostIp = theHost.get('host_ip'))) {
                            if (Ext.isArray(hostIp)) {
                                title = hostIp.join(', ');
                            } else {
                                title = hostIp;
                            }
                        }
                    }
                }

                return title || '';
            }
        },
        systemComponentHref: {
            bind: {
                bindTo: '{theHost}',
                deep: true
            },
            get: function (theHost) {
                var systemComponent,
                    systemComponentId,
                    data = {
                        ip: '',
                        label: ''
                    };

                if (theHost && Ext.isObject(systemComponent = theHost.get('system_component')) && (systemComponentId = systemComponent.id)) {
                    data = {
                        ip: systemComponent.ip ? Ext.String.format('<a target="_blank" href="#settings/system-component?id={0}">{1}</a>', systemComponentId, systemComponent.ip) : '',
                        label: systemComponent.label ? Ext.String.format('<a target="_blank" href="#settings/system-component?id={0}">{1}</a>', systemComponentId, systemComponent.label) : ''
                    };
                }

                return data;
            }
        },
        changeStatusPossible: {
            bind: {
                bindTo: '{theHost.host_status}',
                deep: true
            },
            get: function (status) {
                var statuses = {
                        ACCEPTED: false,
                        DECLINED: false,
                        DELETED: false
                    },
                    statusId = status && status.id;

                switch (statusId) {
                    case 'ACCEPTED':
                        statuses.DECLINED = true;
                        statuses.DELETED = true;
                        break;
                    case 'DECLINED':
                        statuses.ACCEPTED = true;
                        statuses.DELETED = true;
                        break;
                    case 'UNKNOWN':
                    case 'REDETECTED':
                        statuses.ACCEPTED = true;
                        statuses.DECLINED = true;
                        statuses.DELETED = true;
                        break;
                }

                return statuses;
            }
        }
    }
});
