/**
 *
 */
Ext.define('Pluton.view.host.card.HostCardController', {
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.host-card',

    onLoad: function () {
        var view = this.getView(),
            theHost = view.getViewModel().get('theHost');

        view.setLoading();
        theHost.load({
            callback: function () {
                view.setLoading(false);
            }
        });
    },

    onChangeStatusBtn: function (btn) {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            theHost = vModel.get('theHost'),
            currentSusId = vModel.get('current_sus_id'),
            ownerComponentId = theHost.get('owner_component_id'),
            changeStatusPossible = vModel.get('changeStatusPossible'),
            hostStatusStore = vModel.get('HostStatus'),
            statusId = btn.getValue();

        // Изменeние статуса возможно только если владелец - текущий СУС
        if (ownerComponentId && (ownerComponentId === currentSusId) && changeStatusPossible[statusId] === true) {
            view.setLoading();
            Ext.Ajax.request({
                url: '/host/profile/update',
                method: 'POST',
                jsonData: {
                    id: theHost.getId(),
                    status: statusId
                }
            }).then(function () {
                theHost.set('host_status', hostStatusStore.getById(statusId).getData(), {commit: true});
            }).always(function () {
                view.setLoading(false);
            });
        }
    }
});
