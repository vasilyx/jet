/**
 *
 */
Ext.define('Pluton.view.host.card.tab.user.HostUserModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.host-user',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.host.HostUser'
    ],

    stores: {
        HostUser: {
            model: 'Pluton.model.host.HostUser',
            remoteSort: true,
            remoteFilter: true,
            proxy: {
                type: 'json-ajax',
                url: '/host/profile/users',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        }
    },

    data: {

    }
});
