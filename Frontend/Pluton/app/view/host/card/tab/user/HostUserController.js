/**
 *
 */
Ext.define('Pluton.view.host.card.tab.user.HostUserController', {
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.host-user',

    control: {
        '#': {
            expand: 'onExpand'
        }
    },

    onBeforeStoreLoad: function (store) {
        var hostId = this.getViewModel().get('theHost.id');

        if (hostId) {
            store.getProxy().setExtraParam('host_id', hostId);
            return true;
        } else {
            return false;
        }
    },

    onExpand: function () {
        var store = this.getView().getStore();

        if (!store.isLoaded()) {
            store.load();
        }
    }
});
