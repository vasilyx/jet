/**
 *
 */
Ext.define('Pluton.view.host.card.tab.user.HostUser', {
    extend: 'Ext.grid.Panel',

    xtype: 'host-user',

    requires: [
        'Ext.grid.column.Date',
        'Ext.toolbar.Paging',
        'Pluton.view.host.card.tab.user.HostUserController',
        'Pluton.view.host.card.tab.user.HostUserModel'
    ],

    controller: 'host-user',
    viewModel: {
        type: 'host-user'
    },

    bind: {
        store: '{HostUser}'
    },

    title: 'Пользователи',
    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{HostUser}'
                }
            }
        ]
    },

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'login',
                text: 'Имя пользователя'
            },
            {
                dataIndex: 'protocol',
                text: 'Протокол'
            },
            {
                dataIndex: 'status',
                sorter: 'status_label_',
                text: 'Статус',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'email',
                text: 'E-mail'
            },
            {
                xtype: 'datecolumn',
                format: Ext.Date.defaultFormat,
                dataIndex: 'create_timestamp',
                text: 'Время создания записи'
            },
            {
                xtype: 'datecolumn',
                format: Ext.Date.defaultFormat,
                dataIndex: 'update_timestamp',
                text: 'Время обновления записи'
            }
        ]
    }
});
