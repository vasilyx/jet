/**
 *
 */
Ext.define('Pluton.view.host.card.tab.HostBasic', {
    extend: 'Ext.panel.Panel',

    xtype: 'host-basic',

    requires: [
        'Ext.container.Container',
        'Ext.form.field.Display',
        'Ext.layout.container.Center',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Pluton.view.host.util.Host'
    ],

    title: 'Основное',
    bodyPadding: '10 10 0 10',
    scrollable: true,
    layout: 'center',

    items: {
        xtype: 'container',
        width: '70%',
        height: '100%',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Имя',
                        bind: {
                            value: '{theHost.hostname}'
                        },
                        renderer: HostUtil.hostnameRenderer
                    },
                    {
                        fieldLabel: 'Контролируемая система',
                        bind: {
                            value: '{theHost.monitored_system}'
                        },
                        renderer: Ext.labelRenderer
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'IP-адрес',
                        bind: {
                            value: '{theHost.host_ip}'
                        },
                        renderer: Ext.labelRenderer
                    },
                    {
                        fieldLabel: 'Сенсор',
                        bind: {
                            value: '{systemComponentHref.label}'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    xtype: 'displayfield',
                    flex: 1
                },
                items: [
                    {
                        fieldLabel: 'Статус',
                        bind: {
                            value: '{theHost.host_status}'
                        },
                        renderer: Ext.labelRenderer
                    },
                    {
                        fieldLabel: 'IP сенсора',
                        bind: {
                            value: '{systemComponentHref.ip}'
                        }
                    }
                ]
            }
        ]
    }
});
