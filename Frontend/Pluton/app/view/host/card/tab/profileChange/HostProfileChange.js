/**
 *
 */
Ext.define('Pluton.view.host.card.tab.profileChange.HostProfileChange', {
    extend: 'Ext.grid.Panel',

    xtype: 'host-profile-change',

    requires: [
        'Ext.grid.column.Date',
        'Ext.toolbar.Paging',
        'Pluton.view.host.card.tab.profileChange.HostProfileChangeController',
        'Pluton.view.host.card.tab.profileChange.HostProfileChangeModel'
    ],

    controller: 'host-profile-change',
    viewModel: {
        type: 'host-profile-change'
    },

    bind: {
        store: '{HostProfileChange}'
    },

    listeners: {
        itemdblclick: 'onItemDblClick'
    },

    reference: 'hostProfileChangeGrid',
    title: 'Активность профиля',
    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{HostProfileChange}'
                }
            }
        ]
    },
    lbar: [
        {
            tooltip: 'Открыть карточку СИБ',
            iconCls: 'x-fa fa-external-link',
            ui: 'md-blue',
            hidden: true,
            bind: {
                hidden: '{!hostProfileChangeGrid.selection}'
            },
            handler: 'onOpen'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                xtype: 'datecolumn',
                format: Ext.Date.defaultFormat,
                dataIndex: 'alert_timestamp',
                text: 'Дата/время события'
            },
            {
                dataIndex: 'name',
                text: 'Имя'
            },
            {
                dataIndex: 'port',
                text: 'Порт'
            },
            {
                dataIndex: 'protocol_name',
                text: 'Протокол'
            },
            {
                dataIndex: 'object_name',
                text: 'Объект'
            },
            {
                dataIndex: 'severity',
                sorter: 'severity_label_',
                text: 'Критичность',
                hidden: true,
                renderer: Ext.labelRenderer
            }
        ]
    }
});
