/**
 *
 */
Ext.define('Pluton.view.host.card.tab.profileChange.HostProfileChangeModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.host-profile-change',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.host.HostProfileChange'
    ],

    stores: {
        HostProfileChange: {
            model: 'Pluton.model.host.HostProfileChange',
            remoteSort: true,
            sorters: {
                property: 'alert_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/host/profile/heuristic',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        }
    },

    data: {

    }
});
