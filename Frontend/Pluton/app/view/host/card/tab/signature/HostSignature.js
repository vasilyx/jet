/**
 *
 */
Ext.define('Pluton.view.host.card.tab.signature.HostSignature', {
    extend: 'Ext.grid.Panel',

    xtype: 'host-signature',

    requires: [
        'Ext.grid.column.Date',
        'Ext.toolbar.Paging',
        'Pluton.view.host.card.tab.signature.HostSignatureController',
        'Pluton.view.host.card.tab.signature.HostSignatureModel'
    ],

    controller: 'host-signature',
    viewModel: {
        type: 'host-signature'
    },

    bind: {
        store: '{HostSignature}'
    },

    listeners: {
        itemdblclick: 'onItemDblClick'
    },

    reference: 'hostSignatureGrid',
    title: 'Сигнатурное обнаружение',
    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{HostSignature}'
                }
            }
        ]
    },
    lbar: [
        {
            tooltip: 'Открыть карточку СИБ',
            iconCls: 'x-fa fa-external-link',
            ui: 'md-blue',
            hidden: true,
            bind: {
                hidden: '{!hostSignatureGrid.selection}'
            },
            handler: 'onOpen'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'id',
                text: 'Идентификатор',
                hidden: true
            },
            {
                xtype: 'datecolumn',
                format: 'd.m.Y H:i:s.u',
                dataIndex: 'alert_timestamp',
                text: 'Дата/время',
                flex: 2
            },
            {
                dataIndex: 'alert_rule_name',
                text: 'Событие'
            },
            {
                dataIndex: 'alert_type',
                sorter: 'alert_type_label_',
                text: 'Тип',
                flex: 2,
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'severity',
                sorter: 'severity_label_',
                text: 'Критичность',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'source_ip',
                text: 'IP-источник'
            },
            {
                dataIndex: 'target_ip',
                text: 'IP-получатель'
            },
            {
                dataIndex: 'sensor',
                sorter: 'sensor_label_',
                text: 'Сенсор',
                renderer: Ext.labelRenderer
            }
        ]
    }
});
