/**
 *
 */
Ext.define('Pluton.view.host.card.tab.signature.HostSignatureModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.host-signature',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.alert.Alert'
    ],

    stores: {
        HostSignature: {
            model: 'Pluton.model.alert.Alert',
            remoteSort: true,
            sorters: {
                property: 'alert_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/host/profile/signature',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        }
    },

    data: {

    }
});
