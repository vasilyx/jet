/**
 *
 */
Ext.define('Pluton.view.host.card.tab.signature.HostSignatureController', {
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.host-signature',

    control: {
        '#': {
            expand: 'onExpand'
        }
    },

    onBeforeStoreLoad: function (store) {
        var hostId = this.getViewModel().get('theHost.id');

        if (hostId) {
            store.getProxy().setExtraParam('host_id', hostId);
            return true;
        } else {
            return false;
        }
    },

    onExpand: function () {
        var store = this.getView().getStore();

        if (!store.isLoaded()) {
            store.load();
        }
    },

    onOpen: function () {
        var selection = this.getViewModel().get('hostSignatureGrid.selection');

        if (selection) {
            this.openAlert(selection.getId());
        }
    },

    onItemDblClick: function (grid, record) {
        this.openAlert(record.getId());
    },

    openAlert: function (id) {
        window.open('#events/alert?id=' + id, '_blank');
    }
});
