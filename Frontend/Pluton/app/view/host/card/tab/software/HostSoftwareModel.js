/**
 *
 */
Ext.define('Pluton.view.host.card.tab.software.HostSoftwareModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.host-software',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.host.HostSoftware'
    ],

    stores: {
        HostSoftware: {
            model: 'Pluton.model.host.HostSoftware',
            remoteSort: true,
            remoteFilter: true,
            sorters: {
                property: 'discovery_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/host/profile/applications',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        },
        SoftwareType: {
            fields: ['id', 'label'],
            data: [
                {
                    id: 'OS',
                    label: 'Операционная система'
                },
                {
                    id: 'SERVICE',
                    label: 'Сервис'
                },
                {
                    id: 'APPLICATION',
                    label: 'Программное обеспечение'
                },
                {
                    id: 'UNKNOWN',
                    label: 'Неизвестен'
                }
            ]
        },
        SoftwareStatus: {
            fields: ['id', 'label'],
            data: [
                {
                    id: 'UNKNOWN',
                    label: 'Обнаружено'
                },
                {
                    id: 'DELETED',
                    label: 'Удалено оператором'
                },
                {
                    id: 'ACCEPTED',
                    label: 'Одобрено'
                },
                {
                    id: 'REDETECTED',
                    label: 'Обнаружено заново'
                },
                {
                    id: 'DECLINED',
                    label: 'Отклонено оператором'
                }
            ]
        }
    },

    data: {

    },

    formulas: {
        changeStatusPossible: {
            bind: {
                bindTo: '{hostSoftwareGrid.selection.status}',
                deep: true
            },
            get: function (status) {
                var statuses = {
                        ACCEPTED: false,
                        DECLINED: false,
                        DELETED: false
                    },
                    statusId = status && status.id;

                switch (statusId) {
                    case 'ACCEPTED':
                        statuses.DECLINED = true;
                        statuses.DELETED = true;
                        break;
                    case 'DECLINED':
                        statuses.ACCEPTED = true;
                        statuses.DELETED = true;
                        break;
                    case 'UNKNOWN':
                    case 'REDETECTED':
                        statuses.ACCEPTED = true;
                        statuses.DECLINED = true;
                        statuses.DELETED = true;
                        break;
                }

                return statuses;
            }
        }
    }
});
