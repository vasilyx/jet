/**
 *
 */
Ext.define('Pluton.view.host.card.tab.software.HostSoftware', {
    extend: 'Ext.grid.Panel',

    xtype: 'host-software',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.grid.column.Date',
        'Ext.layout.container.VBox',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Separator',
        'Pluton.view.host.card.tab.software.HostSoftwareController',
        'Pluton.view.host.card.tab.software.HostSoftwareModel',
        'Pluton.view.host.util.Host'
    ],

    controller: 'host-software',
    viewModel: {
        type: 'host-software'
    },

    bind: {
        store: '{HostSoftware}'
    },

    listeners: {
        itemdblclick: 'onItemDblClick'
    },

    reference: 'hostSoftwareGrid',
    title: 'Программное обеспечение',
    tbar: [
        {
            reference: 'softwareFilterCombo',
            xtype: 'combo',
            queryMode: 'local',
            valueField: 'id',
            displayField: 'label',
            emptyText: 'Все типы',
            clearTrigger: true,
            editable: false,
            width: 300,
            bind: {
                store: '{SoftwareType}'
            }
        }
    ],
    lbar: [
        {
            tooltip: 'Открыть связанный СИБ',
            iconCls: 'x-fa fa-external-link',
            ui: 'md-blue',
            hidden: true,
            bind: {
                hidden: '{!hostSoftwareGrid.selection || !hostSoftwareGrid.selection.alert_id}'
            },
            handler: 'onOpen'
        },
        {
            xtype: 'tbseparator',
            hidden: true,
            bind: {
                hidden: '{!hostSoftwareGrid.selection || !hostSoftwareGrid.selection.alert_id}'
            }
        },
        {
            xtype: 'toolbar',
            layout: 'vbox',
            padding: 0,
            hidden: true,
            bind: {
                hidden: '{!theHost.owner_component_id || theHost.owner_component_id !== current_sus_id}'
            },
            items: [
                {
                    iconCls: 'x-fa fa-thumbs-up',
                    ui: 'md-green',
                    tooltip: 'Одобрить',
                    value: 'ACCEPTED',
                    hidden: true,
                    bind: {
                        hidden: '{!hostSoftwareGrid.selection || !changeStatusPossible.ACCEPTED}'
                    },
                    handler: 'onChangeStatusBtn'
                },
                {
                    iconCls: 'x-fa fa-thumbs-down',
                    ui: 'md-yellow',
                    tooltip: 'Отклонить',
                    value: 'DECLINED',
                    hidden: true,
                    bind: {
                        hidden: '{!hostSoftwareGrid.selection || !changeStatusPossible.DECLINED}'
                    },
                    handler: 'onChangeStatusBtn'
                },
                {
                    iconCls: 'x-fa fa-trash-o',
                    ui: 'md-red',
                    tooltip: 'Удалить',
                    value: 'DELETED',
                    hidden: true,
                    bind: {
                        hidden: '{!hostSoftwareGrid.selection || !changeStatusPossible.DELETED}'
                    },
                    handler: 'onChangeStatusBtn'
                }
            ]
        }
    ],
    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{HostSoftware}'
                }
            }
        ]
    },

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'id',
                text: 'Идентификатор',
                hidden: true
            },
            {
                dataIndex: 'software_type',
                sorter: 'software_type',
                text: 'Тип',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'name',
                text: 'ПО',
                flex: 2
            },
            {
                dataIndex: 'port',
                text: 'Порт'
            },
            {
                dataIndex: 'protocol',
                text: 'Протокол'
            },
            {
                xtype: 'datecolumn',
                format: Ext.Date.defaultFormat,
                dataIndex: 'discovery_timestamp',
                text: 'Обнаружено',
                flex: 2
            },
            {
                dataIndex: 'vulnerabilities',
                sortable: false,
                text: 'Уязвимости',
                flex: 3,
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'status',
                sorter: 'status_label_',
                text: 'Статус',
                flex: 2,
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'added',
                text: 'ПО создано',
                flex: 2,
                renderer: HostUtil.getAddedTypeLabel
            }
        ]
    }
});
