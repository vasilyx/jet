/**
 *
 */
Ext.define('Pluton.view.host.card.tab.software.HostSoftwareController', {
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.host-software',

    control: {
        '#': {
            expand: 'onExpand'
        }
    },

    bindings: {
        onSoftwareTypeFilterChange: '{softwareFilterCombo.selection}'
    },

    onBeforeStoreLoad: function (store) {
        var hostId = this.getViewModel().get('theHost.id');

        if (hostId) {
            store.getProxy().setExtraParam('host_id', hostId);
            return true;
        } else {
            return false;
        }
    },

    onExpand: function () {
        var store = this.getStore('HostSoftware');

        if (!store.isLoaded()) {
            store.load();
        }
    },

    onSoftwareTypeFilterChange: function (selection) {
        var store = this.getStore('HostSoftware');

        if (selection) {
            store.addFilter({
                property: 'software_type',
                operator: '=',
                value: selection.getId()
            });
        } else {
            store.clearFilter();
        }
    },

    onChangeStatusBtn: function (btn) {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            theHost = vModel.get('theHost'),
            currentSusId = vModel.get('current_sus_id'),
            ownerComponentId = theHost.get('owner_component_id'),
            changeStatusPossible = vModel.get('changeStatusPossible'),
            record = vModel.get('hostSoftwareGrid.selection'),
            softwareStatusStore = vModel.get('SoftwareStatus'),
            statusId = btn.getValue();

        // Изменeние статуса возможно только если владелец - текущий СУС
        if (record && ownerComponentId && (ownerComponentId === currentSusId) && changeStatusPossible[statusId] === true) {
            view.setLoading();
            Ext.Ajax.request({
                url: '/host/profile/applications/update',
                method: 'POST',
                jsonData: {
                    host_application_id: record.getId(),
                    status: statusId
                }
            }).then(function () {
                record.set('status', softwareStatusStore.getById(statusId).getData(), {commit: true});
            }).always(function () {
                view.setLoading(false);
            });
        }
    },

    onOpen: function () {
        var selection = this.getViewModel().get('hostSoftwareGrid.selection');

        if (selection) {
            this.openAlert(selection.get('alert_id'));
        }
    },

    onItemDblClick: function (grid, record) {
        this.openAlert(record.get('alert_id'));
    },

    openAlert: function (alertId) {
        if (alertId) {
            window.open('#events/alert?id=' + alertId, '_blank');
        } else {
            return false;
        }
    }
});
