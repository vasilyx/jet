/**
 *
 */
Ext.define('Pluton.view.host.card.tab.blackList.HostBlackList', {
    extend: 'Ext.grid.Panel',

    xtype: 'host-blacklist',

    requires: [
        'Ext.grid.column.Date',
        'Ext.toolbar.Paging',
        'Pluton.view.host.card.tab.blackList.HostBlackListController',
        'Pluton.view.host.card.tab.blackList.HostBlackListModel'
    ],

    controller: 'host-blacklist',
    viewModel: {
        type: 'host-blacklist'
    },

    bind: {
        store: '{HostBlackList}'
    },

    listeners: {
        itemdblclick: 'onItemDblClick'
    },

    reference: 'hostBlackListGrid',
    title: 'Чёрные списки',
    bbar: {
        layout: {
            pack: 'center'
        },
        items: [
            {
                xtype: 'pagingtoolbar',
                displayInfo: true,
                bind: {
                    store: '{HostBlackList}'
                }
            }
        ]
    },
    lbar: [
        {
            tooltip: 'Открыть карточку СИБ',
            iconCls: 'x-fa fa-external-link',
            ui: 'md-blue',
            hidden: true,
            bind: {
                hidden: '{!hostBlackListGrid.selection}'
            },
            handler: 'onOpen'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                xtype: 'datecolumn',
                format: 'd.m.Y H:i:s.u',
                dataIndex: 'alert_timestamp',
                text: 'Дата/время события'
            },
            {
                dataIndex: 'severity',
                sorter: 'severity_label_',
                text: 'Критичность',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'alert_rule_name',
                text: 'Имя сработавшего правила',
                flex: 2
            },
            {
                dataIndex: 'black_list_type',
                sorter: 'black_list_type_label_',
                text: 'Тип',
                renderer: Ext.labelRenderer
            },
            {
                dataIndex: 'black_list_value',
                text: 'Найденное значение'
            },
            {
                dataIndex: 'black_list_direction',
                sorter: 'black_list_direction_label_',
                text: 'Направление',
                renderer: Ext.labelRenderer
            }
        ]
    }
});
