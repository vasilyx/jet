/**
 *
 */
Ext.define('Pluton.view.host.card.tab.blackList.HostBlackListModel', {
    extend: 'Ext.app.ViewModel',
    
    alias: 'viewmodel.host-blacklist',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.host.HostBlackList'
    ],

    stores: {
        HostBlackList: {
            model: 'Pluton.model.host.HostBlackList',
            remoteSort: true,
            sorters: {
                property: 'alert_timestamp',
                direction: 'DESC'
            },
            proxy: {
                type: 'json-ajax',
                url: '/host/profile/black_list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            },
            listeners: {
                beforeload: 'onBeforeStoreLoad'
            }
        }
    },

    data: {

    }
});
