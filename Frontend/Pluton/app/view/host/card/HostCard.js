/**
 *
 */
Ext.define('Pluton.view.host.card.HostCard', {
    extend: 'Ext.form.Panel',

    xtype: 'host-card',

    requires: [
        'Ext.layout.container.Accordion',
        'Ext.panel.Panel',
        'Pluton.view.host.card.HostCardController',
        'Pluton.view.host.card.HostCardModel',
        'Pluton.view.host.card.tab.HostBasic',
        'Pluton.view.host.card.tab.blackList.HostBlackList',
        'Pluton.view.host.card.tab.profileChange.HostProfileChange',
        'Pluton.view.host.card.tab.signature.HostSignature',
        'Pluton.view.host.card.tab.software.HostSoftware',
        'Pluton.view.host.card.tab.user.HostUser',
        'Pluton.view.host.card.tab.vulnerability.HostVulnerability'
    ],

    controller: 'host-card',
    viewModel: {
        type: 'host-card'
    },

    bind: {
        title: '{hostCardTitle}'
    },
    bodyPadding: 10,
    scrollable: true,
    layout: {
        type: 'accordion',
        fill: false,
        multi: true,
        reserveScrollbar: true
    },
    defaults: {
        collapsed: true,
        scrollable: true,
        minHeight: 500,
        header: {
            titlePosition: 1
        }
    },
    fieldDefaults: {
        labelWidth: 180
    },
    listeners: {
        load: 'onLoad'
    },

    lbar: {
        padding: '10 0 0 10',
        hidden: true,
        bind: {
            hidden: '{!theHost.owner_component_id || theHost.owner_component_id !== current_sus_id}'
        },
        items: [
            {
                iconCls: 'x-fa fa-thumbs-up',
                ui: 'md-green',
                tooltip: 'Одобрить',
                value: 'ACCEPTED',
                hidden: true,
                bind: {
                    hidden: '{!changeStatusPossible.ACCEPTED}'
                },
                handler: 'onChangeStatusBtn'
            },
            {
                iconCls: 'x-fa fa-thumbs-down',
                ui: 'md-yellow',
                tooltip: 'Отклонить',
                value: 'DECLINED',
                hidden: true,
                bind: {
                    hidden: '{!changeStatusPossible.DECLINED}'
                },
                handler: 'onChangeStatusBtn'
            },
            {
                iconCls: 'x-fa fa-trash-o',
                ui: 'md-red',
                tooltip: 'Удалить',
                value: 'DELETED',
                hidden: true,
                bind: {
                    hidden: '{!changeStatusPossible.DELETED}'
                },
                handler: 'onChangeStatusBtn'
            }
        ]
    },

    items: [
        {
            xtype: 'panel',
            collapsed: false,
            minHeight: 0,
            maxHeight: 0,
            margin: 0,
            padding: 0
        },
        {
            xtype: 'host-basic',
            collapsed: false,
            minHeight: null
        },
        {
            xtype: 'host-signature'
        },
        {
            xtype: 'host-blacklist'
        },
        {
            xtype: 'host-profile-change'
        },
        {
            xtype: 'host-software'
        },
        {
            xtype: 'host-vulnerability'
        },
        {
            xtype: 'host-user'
        }
    ]
});
