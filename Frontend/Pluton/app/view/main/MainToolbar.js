/**
 *
 */
Ext.define('Pluton.view.main.MainToolbar', {
    extend: 'Ext.toolbar.Toolbar',

    xtype: 'main-toolbar',

    requires: [
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.layout.container.boxOverflow.Scroller',
        'Ext.toolbar.TextItem',
        'Ext.toolbar.Toolbar',
        'Pluton.ux.button.MainToolbarButton',
        'Pluton.view.main.MainToolbarController'
    ],

    controller: 'main-toolbar',

    cls: 'main-toolbar',
    padding: '0 10 0 10',
    enableOverflow: true,
    overflowHandler: 'scroller',

    items: [
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'middle'
            },
            items: [
                {
                    xtype: 'image',
                    width: 50,
                    height: 50,
                    margin: '5 10 5 0',
                    src: 'resources/images/jet-logo.svg'
                },
                {
                    xtype: 'container',
                    height: 50,
                    layout: {
                        type: 'hbox',
                        align: 'middle'
                    },
                    items: [
                        {
                            xtype: 'label',
                            cls: 'main-toolbar-logo-text',
                            text: 'Плутон',
                            margin: '0 3 0 0',
                            listeners: {
                                click: {
                                    element: 'el',
                                    fn: 'onPlutonLabelClick'
                                }
                            }
                        },
                        {
                            xtype: 'tbtext',
                            cls: 'main-toolbar-logo-version',
                            bind: {
                                html: '<sup>{version}</sup>'
                            }
                        }
                    ]
                }
            ]
        },
        '->',
        {
            reference: 'buttonContainer',
            xtype: 'container',
            height: '100%',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                xtype: 'main-toolbar-button',
                scale: 'medium',
                arrowVisible: false,
                minWidth: 140
            },
            items: [
                {
                    section: 'reports',
                    text: 'Статистика',
                    iconCls: 'x-fa fa-line-chart',
                    permissions: 'report-statistics-page',
                    href: '#reports/statistics',
                    hrefTarget: '_self'
                },
                {
                    section: 'events',
                    text: 'События',
                    iconCls: 'x-fa fa-eye',
                    permissions: [
                        'alert_page',
                        'audit_page',
                        'bro-logs_page'
                    ],
                    menu: {
                        defaults: {
                            hrefTarget: '_self'
                        },
                        items: [
                            {
                                text: 'Журнал событий (СИБ)',
                                permissions: 'alert_page',
                                href: '#events/alert'
                            },
                            {
                                text: 'Журнал аудита',
                                permissions: 'audit_page',
                                href: '#events/audit'
                            },
                            {
                                text: 'Журналы сетевой активности',
                                permissions: 'bro-logs_page',
                                href: '#events/bro-logs'

                            }
                        ]
                    }
                },
                {
                    section: 'env',
                    text: 'Окружение',
                    iconCls: 'x-fa fa-sitemap',
                    permissions: [
                        'host_page',
                        'black-list_page'
                    ],
                    menu: {
                        defaults: {
                            hrefTarget: '_self'
                        },
                        items: [
                            {
                                text: 'Активы',
                                permissions: 'host_page',
                                href: '#env/host'
                            },
                            {
                                text: 'Решающие правила',
                                permissions: 'black-list_page',
                                href: '#env/black-list'
                            }
                        ]
                    }
                },
                {
                    section: 'settings',
                    text: 'Администрирование',
                    iconCls: 'x-fa fa-wrench',
                    permissions: [
                        'component-registration_page',
                        'system-component_page',
                        'settings-user_page',
                        'settings-role_page',
                        'component-monitoring_page'
                    ],
                    menu: {
                        defaults: {
                            hrefTarget: '_self'
                        },
                        items: [
                            {
                                text: 'Управление регистрацией компонентов',
                                permissions: 'component-registration_page',
                                href: '#settings/log-component'
                            },
                            {
                                text: 'Компоненты',
                                permissions: 'system-component_page',
                                href: '#settings/system-component'
                            },
                            {
                                text: 'Пользователи',
                                permissions: 'settings-user_page',
                                href: '#settings/user'
                            },
                            {
                                text: 'Роли',
                                permissions: 'settings-role_page',
                                href: '#settings/role'
                            },
                            {
                                text: 'Работоспособность',
                                permissions: 'component-monitoring_page',
                                href: '#env/component-monitoring'
                            }
                        ]
                    }
                },
                {
                    section: 'map',
                    text: 'Карта',
                    iconCls: 'x-fa fa-map',
                    permissions: 'global-map_page',
                    href: '#map/global-map',
                    hrefTarget: '_self'
                }
            ]
        },
        '->',
        {
            section: 'private',
            tooltip: 'Мой компонент',
            iconCls: 'x-fa fa-cog',
            scale: 'medium',
            permissions: 'private-component_page',
            href: '#private/private-component',
            hrefTarget: '_self'
        },
        {
            xtype: 'tbtext',
            bind: {
                text: '{username}'
            }
        },
        {
            xtype: 'tbtext',
            reference: 'usernameExpirationBlock',
            bind: {
                text: '{userExpiration}'
            }
        },
        {
            xtype: 'tbtext',
            text: '|'
        },
        {
            xtype: 'tbtext',
            reference: 'passwordExpirationBlock',
            bind: {
                text: '{passwordExpiration}'
            }
        },
        {
            tooltip: 'Пользователь',
            iconCls: 'main-toolbar-user-icon',
            cls: 'user-btn',
            arrowVisible: false,
            menuAlign: 'tr-br',
            menu: [
                {
                    iconCls: 'x-fa fa-key',
                    text: 'Изменить пароль',
                    handler: 'onShowProfileBtn'
                },
                {
                    iconCls: 'x-fa fa-sign-out',
                    text: 'Выход',
                    handler: 'onLogoutBtn'
                }
            ]
        }
    ]
});
