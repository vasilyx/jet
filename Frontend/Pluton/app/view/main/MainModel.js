/**
 *
 */
Ext.define('Pluton.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        username: null,
        current_sus_id: null,
        current_sus_name: null,
        version: null,
        password_expiration_timestamp: null,
        user_expiration_timestamp: null,
        level_id: null,
        category_id: null,
        privileges: null,
        component_tree_selected_id: null
    },
    formulas: {
        userExpiration: {
            bind: {
                bindTo: '{user_expiration_timestamp}',
                deep: true
            },
            get: function(date) {
                if (date) {
                    var expDate = new Date(date);
                    return Ext.Date.format(expDate, Ext.Date.defaultFormat);
                } else {
                    return 'Бессрочно';
                }
            }
        },
        passwordExpiration: {
            bind: {
                bindTo: '{password_expiration_timestamp}',
                deep: true
            },
            get: function(date) {
                if (date == "never") {
                    return "Бессрочно";
                } else if (date == "already") {
                    return "Истёк";
                } else {
                    var expDate = new Date(date);
                    return Ext.Date.format(expDate, Ext.Date.defaultFormat);
                }
            }
        }
    },
    stores: {
        
    }
});
