/**
 *
 */
Ext.define('Pluton.view.main.window.ProfileSettingsWindowController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.profile-settings-window',

    requires: [
        'Pluton.util.LoginManager'
    ],

    onSave: function () {
        var view = this.getView();

        view.setLoading();
        Ext.Ajax.request({
            url: '/user/update',
            method: 'POST',
            jsonData: {
                password: this.lookupReference('passwordField').getValue()
            },
            success: function () {
                LoginManager.logout(true);
            },
            failure: function () {
                view.setLoading(false);
            },
            scope: this
        });
    },

    onCancel: function () {
        this.getView().close();
    },

    onPasswordValidityChange: function (field) {
        field.next().validate();
    },

    onPasswordDirtyChange: function (field, isDirty) {
        this.lookup('passwordFieldConfirm').allowBlank = !isDirty;
    },

    onPasswordChange: function (field) {
        field.next().validate();
    },

    onUserAccountFormValidityChange: function (form, valid) {
        this.lookupReference('saveButton').setDisabled(!valid);
    }
});
