/**
 *
 */
Ext.define('Pluton.view.main.window.ProfileSettingsWindow', {
    extend: 'Ext.window.Window',

    xtype: 'profile-settings-window',

    requires: [
        'Ext.form.Label',
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.layout.container.Form',
        'Ext.layout.container.VBox',
        'Pluton.view.main.window.ProfileSettingsWindowController'
    ],

    controller: 'profile-settings-window',

    title: 'Изменение пароля',
    modal: true,
    constrain: true,
    minHeight: 230,
    minWidth: 500,
    width: 500,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'label',
            text: 'Пароль должен быть не менее 8 символов, содержать минимум 2 цифры, 2 буквы, 1 букву в верхнем регистре и 2 не алфавитных символа.',
            margin: 5,
            style: {
                textAlign: 'center',
                color: 'red'
            }
        },
        {
            xtype: 'form',
            layout: 'form',
            flex: 1,
            buttons: [
                {
                    text: 'Применить',
                    formBind: true,
                    disabled: true,
                    handler: 'onSave'
                },
                {
                    text: 'Отменить',
                    handler: 'onCancel'
                }
            ],
            defaults: {
                xtype: 'textfield',
                inputType: 'password',
                allowBlank: false
            },
            items: [
                {
                    reference: 'passwordField',
                    fieldLabel: 'Пароль',
                    name: 'password',
                    listeners: {
                        validitychange: 'onPasswordValidityChange',
                        blur: 'onPasswordValidityChange',
                        dirtychange: 'onPasswordDirtyChange',
                        change: 'onPasswordChange'
                    }
                },
                {
                    reference: 'passwordFieldConfirm',
                    fieldLabel: 'Подтверждение пароля',
                    passFieldName: 'password',
                    submitValue: false,
                    vtype: 'password'
                }
            ]
        }
    ]
});
