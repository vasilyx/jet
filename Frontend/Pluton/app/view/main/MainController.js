/**
 *
 */
Ext.define('Pluton.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    requires: [
        'Ext.util.History'
    ],

    listen: {
        component: {
            'container': {
                beforeadd: function (container, component) {
                    return component.permissions ? this.permissionsCheck(component.permissions, container, component) : true;
                }
            }
        }
    },

    routes: {
        ':section:page:params': {
            action: 'onUrlChange',
            conditions: {
                ':section': '(.+?(?=\/)|)',
                ':page': '([^?]+)',
                ':params': '(.+)?'
            }
        }
    },

    bindings: {
        onPrivilegesObtained: '{privileges}'
    },

    onPrivilegesObtained: function () {
        var currentToken = Ext.History.getToken(),
            defaultToken;

        if (this.permissionsCheck('report-statistics-page')) {
            defaultToken = 'reports/statistics';
        } else if (this.permissionsCheck('alert_page')) {
            defaultToken = 'events/alert';
        } else {
            defaultToken = null;
        }

        Pluton.getApplication().setDefaultToken(defaultToken);

        if (currentToken) {
            this.redirectTo(currentToken, true);
        } else if (defaultToken) {
            this.redirectTo(defaultToken, true);
        }
    },

    /**
     * Проверяем, есть ли такие section и page в главном меню, чтобы не могли через url запускать любые компоненты.
     */
    isUrlInMainToolbar: function (section, page) {
        var view = this.getView(),
            mainToolbar = view.lookup('mainToolbar'),
            hash = '#' + section + page,
            isFound = false;
  
        mainToolbar.cascade(function () {
            if (this.isButton) {
                if (this.href && this.href === hash) {
                   isFound = true;
                }
                if (this.menu) {
                    this.menu.cascade(function () {
                        if (this.href && this.href === hash) {
                            isFound = true;
                        }
                    });  
                }
            }
        });
        
        return isFound;
    },

    onUrlChange: function (section, page, params) {
        var view = this.getView(),
            mainContainer = view.lookup('mainContainer'),
            mainToolbar = view.lookup('mainToolbar'),
            buttonContainer = mainToolbar.lookup('buttonContainer'),
            widgetName = (section ? section + '.' : '') + page.replace(/^\//, ''),
            widget,
            paramsObject = params? Ext.Object.fromQueryString(params): null,
            activeWindow = Ext.WindowManager.getActive();

        if (activeWindow && activeWindow.isWindow) { // закроем модальное окно при переходе в другой раздел
            activeWindow.close();
        }
       
        view.suspendLayouts();

        // Check url for the associated class
        if (!Ext.ClassManager.getByAlias('widget.' + widgetName) || !this.isUrlInMainToolbar(section, page)) {
            widgetName = 'page-404';
            section = null;
        }

        // Find widget on mainContainer or create it
        widget = Ext.Array.findBy(mainContainer.items.items, function(item) {
            return item.xtype === widgetName;
        }) || Ext.widget(widgetName);

        // Update main-toolbar buttons
        Ext.Array.each(buttonContainer.items.items, function(btn) {
            if (btn.section === section) {
                btn.addCls(btn._pressedCls);
            } else {
                btn.removeCls(btn._pressedCls);
            }
        });

        // Activate widget in mainContainer
        mainContainer.setActiveItem(widget);

        view.resumeLayouts(true);

        if (!Ext.Object.isEmpty(paramsObject)) {
            widget.fireEvent('routeWithParams', paramsObject);
        }
    },

    permissionsCheck: function (permissions, container, component) {
        var privileges = this.getViewModel().get('privileges');

        if (permissions instanceof Array) {

            return (
                    permissions.findIndex(function (permission) {
                        if (privileges.indexOf(permission) !== -1) {
                            return true;
                        }
                    }) !== -1
                );

        } else {
            return (privileges.indexOf(permissions) !== -1);
        }
    }
});
