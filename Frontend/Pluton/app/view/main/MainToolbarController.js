/**
 *
 */
Ext.define('Pluton.view.main.MainToolbarController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main-toolbar',

    requires: [
        'Ext.tip.ToolTip',
        'Pluton.util.LoginManager',
        'Pluton.view.main.window.ProfileSettingsWindow'
    ],

    control: {
        '[reference=buttonContainer] button': {
            mouseover: {
                fn: 'onMenuBtnMouseOver',
                delay: 1
            },
            mouseout: {
                fn: 'onMenuBtnMouseOut',
                delay: 1
            }
        },
        '[reference=buttonContainer] button > menu': {
            mouseleave: 'onMenuItemMouseLeave'
        }
    },

    onMenuBtnMouseOver: function (btn) {
        btn.showMenu();
    },

    onMenuBtnMouseOut: function (btn, e) {
        var relatedTarget = e.getRelatedTarget(null, null, true),
            menu = btn.menu,
            menuEl;

        if (menu) {
            menuEl = menu.getEl();
            if (!menuEl.contains(relatedTarget)) {
                menu.hide();
            }
        }
    },

    onMenuItemMouseLeave: function (menu) {
        menu.hide();
    },

    onPlutonLabelClick: function () {
        var defaultToken = Pluton.getApplication().getDefaultToken();

        if (defaultToken) {
            this.redirectTo(defaultToken);
        }
    },

    onLogoutBtn: function () {
        LoginManager.logout();
    },

    onShowProfileBtn: function () {
        Ext.widget('profile-settings-window').show();
    },

    afterRender: function () {
        var tips = [
            {
                target: this.lookup('usernameExpirationBlock').getEl(),
                html: 'Срок действия учетной записи'
            },
            {
                target: this.lookup('passwordExpirationBlock').getEl(),
                html: 'Срок действия пароля'
            }
        ];

        this.tips = Ext.Array.map(tips, function (cfg) {
            Ext.applyIf(cfg, {
                anchor: true,
                autoHide: true,
                showOnTap: true,
                defaultAlign: 't100-b'
            });
            return new Ext.tip.ToolTip(cfg);
        });
    }
});
