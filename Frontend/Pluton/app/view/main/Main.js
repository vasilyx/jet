/**
 *
 */
Ext.define('Pluton.view.main.Main', {
    extend: 'Ext.panel.Panel',

    xtype: 'main',

    requires: [
        'Ext.container.Container',
        'Ext.layout.container.Card',
        'Ext.layout.container.Fit',
        'Ext.plugin.Viewport',
        'Pluton.view.main.MainController',
        'Pluton.view.main.MainModel',
        'Pluton.view.main.MainToolbar'
    ],

    plugins: 'viewport',
    controller: 'main',
    viewModel: {
        type: 'main'
    },

    layout: 'fit',
    scrollable: true,
    bodyPadding: 20,
    bodyStyle: {
        background: '#f2f2f2'
    },

    tbar: {
        reference: 'mainToolbar',
        xtype: 'main-toolbar'
    },

    items: [
        {
            reference: 'mainContainer',
            xtype: 'container',
            cls: 'main-container',
            layout: 'card',
            minHeight: 700,
            minWidth: 1000
        }
    ]
});
