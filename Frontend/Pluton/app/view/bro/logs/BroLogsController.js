/**
 *
 */
Ext.define('Pluton.view.bro.logs.BroLogsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.bro-logs',

    requires: [
        'Ext.grid.column.Date'
    ],

    config: {
        configured: false
    },

    control: {
        'bro-logs-grid': {
            beforeexpand: 'onBroLogsGridBeforeExpand'
        }
    },

    updateConfigured: function (configured) {
        var view = this.getView();

        if (view) {
            view.setLoading(configured === 'inProgress');
        }
    },

    initViewModel: function (vModel) {
        vModel.getStore('SystemComponent').on({
            load: {
                fn: 'onLoadSystemComponent',
                delay: 200,
                scope: this
            }
        });
    },

    onLoadSystemComponent: function (store, records) {
        if (Ext.isEmpty(records)) return;

        var me = this,
            view = me.lookup('systemComponentTree'),
            currentSusId = me.getViewModel().get('current_sus_id'),
            previousSelectedId = me.getViewModel().get('component_tree_selected_id'),
            record = previousSelectedId ? store.getNodeById(previousSelectedId) : store.getNodeById(currentSusId);

        view.expandAll(function () {
            if (record) {
                record.set('selected', true);
                view.getSelectionModel().select(record);
            }
        });
    },

    onActivate: function () {
        var systemComponentStore = this.getStore('SystemComponent');

        if (!systemComponentStore.isLoaded()) {
            systemComponentStore.load();
            this.loadBroTables();
        } else {
            this.onLoadSystemComponent(systemComponentStore, systemComponentStore.getRange());
        }
    },

    loadBroTables: function () {
        if (this.getConfigured() !== true) {
            this.setConfigured('inProgress');
            Ext.Ajax.request({
                url: '/bro/tables',
                method: 'POST'
            }).then({
                success: this.onLoadBroTablesSuccess,
                failure: this.onLoadBroTablesFailure,
                scope: this
            });
        }
    },

    onLoadBroTablesSuccess: function (response) {
        var ExtArray = Ext.Array,
            broLogsPanel = this.lookup('broLogsPanel'),
            data = Ext.JSON.decode(response.responseText, true),
            items = [],
            tableName;

        if (data && Ext.isArray(data)) {
            ExtArray.forEach(data, function (item) {
                tableName = item.tablename;
                if (!tableName) {
                    return;
                } else {
                    ExtArray.push(items, {
                        title: item.label || tableName,
                        tableName: tableName
                    });
                }
            });

            broLogsPanel.add(items);
            this.setConfigured(true);
        } else {
            this.onLoadBroTablesFailure();
        }
    },

    onLoadBroTablesFailure: function () {
        this.setConfigured(false);
    },

    onComponentBeforeSelect: function (rowModel, record) {
            var componentType = record.get('component_type');

            return (Ext.isObject(componentType) && componentType.id === 'SENSOR');
    },

    onComponentSelectionChange: function (selModel, selected) {
        var currentExpandedGrid = this.lookup('broLogsPanel').getLayout().getExpanded()[0],
            systemComponent = selected[0],
            systemComponentId = systemComponent && systemComponent.getId();

            this.getViewModel().set('component_tree_selected_id', systemComponentId);

        if (currentExpandedGrid) {
            this.loadBroLogsGrid(currentExpandedGrid, systemComponentId);
        }
    },

    onBroLogsGridBeforeExpand: function (grid) {
        this.loadBroLogsGrid(grid, this.getViewModel().get('systemComponentTree.selection.id'));
    },

    onFilterApply: function () {
        var currentExpandedGrid = this.lookup('broLogsPanel').getLayout().getExpanded()[0];

        if (currentExpandedGrid) {
            this.loadBroLogsGrid(currentExpandedGrid, this.getViewModel().get('systemComponentTree.selection.id'));
        }
    },

    onFilterReset: function () {
        var broLogsFiltersForm = this.lookup('broLogsFilters').getForm(),
            currentExpandedGrid = this.lookup('broLogsPanel').getLayout().getExpanded()[0];

        broLogsFiltersForm.setValues({
            ts: [Ext.Date.clearTime(new Date()), Ext.Date.add(Ext.Date.clearTime(new Date), Ext.Date.DAY, 1)],
            orig_h: '',
            resp_h: ''
        });
        if (currentExpandedGrid) {
            this.loadBroLogsGrid(currentExpandedGrid, this.getViewModel().get('systemComponentTree.selection.id'));
        }
    },

    loadBroLogsGrid: function (grid, systemComponentId) {
        if (systemComponentId) {
            var store = grid.getStore(),
                configured = grid.getConfigured(),
                filterConfig = this.getFilterConfig(),
                options;

            if (configured !== true) {
                store.getProxy().setExtraParam('tablename', grid.getTableName());
                options = {
                    params: {
                        need_meta: true
                    },
                    callback: function (records, operation, success) {
                        var resultSet, metadata;

                        if (success && (resultSet = operation.getResultSet()) && (metadata = resultSet.getMetadata())) {
                            this.onBroLogsGridMetaChange(grid, metadata);
                        }
                    },
                    scope: this
                };
            }

            store.clearFilter(true);
            store.addFilter([{
                property: 'system_component_id',
                value: systemComponentId
            }].concat(filterConfig), true);
            store.load(options);
        } else {
            Ext.MessageBox.alert('Ошибка', 'Компонент системы не выбран.');
        }
    },

    onBroLogsGridMetaChange: function (grid, meta) {
        var getDefaultColumnConfig = function (name) {
                return {
                    dataIndex: name,
                    text: name,
                    tooltip: name
                };
            },
            getTypedColumnConfig = function (type) {
                switch (type) {
                    case 'Date':
                        return {
                            xtype: 'datecolumn',
                            format: 'd.m.Y'
                        };
                    default:
                        return {};
                }
            };

        grid.reconfigure(
            Ext.Array.map(meta, function(item) {
                return Ext.apply(getDefaultColumnConfig(item.name), getTypedColumnConfig(item.type));
            })
        );
        grid.setConfigured(true);
    },

    getFilterConfig: function () {
        var values = this.lookup('broLogsFilters').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function(key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'orig_h':
                    case 'resp_h':
                        operator = '=';
                        break;
                    case 'ts':
                        operator = 'between';
                        break;
                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            }
        });

        return filtersConfig;
    }
});
