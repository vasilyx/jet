/**
 *
 */
Ext.define('Pluton.view.bro.logs.grid.BroLogsGridModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.bro-logs-grid',

    requires: [
        'Ext.data.BufferedStore',
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax'
    ],

    stores: {
        BroLogsGrid: {
            type: 'buffered',
            trailingBufferZone: 1,
            leadingBufferZone: 1,
            purgePageCount: 1,
            remoteSort: true,
            remoteFilter: true,
            proxy: {
                type: 'json-ajax',
                url: '/bro/list',
                reader: {
                    type: 'json',
                    metaProperty: 'meta',
                    rootProperty: 'rows',
                    totalProperty: 'total'
                }
            }
        }
    },

    data: {
        
    }
});
