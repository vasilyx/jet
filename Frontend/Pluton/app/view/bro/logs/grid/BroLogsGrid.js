/**
 *
 */
Ext.define('Pluton.view.bro.logs.grid.BroLogsGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'bro-logs-grid',

    requires: [
        'Pluton.view.bro.logs.grid.BroLogsGridModel'
    ],

    viewModel: {
        type: 'bro-logs-grid'
    },

    bind: {
        store: '{BroLogsGrid}'
    },
    config: {
        tableName: null,
        configured: false
    },
    header: {
        titlePosition: 1,
        cls: 'x-panel-middle-header-grid'
    },
    columns: {
        defaults: {
            flex: 1,
            minWidth: 100,
            cls: 'x-panel-middle-calumn-grid'
        },
        items: []
    },
    viewConfig: {
        getRowClass: function (record, index) {
                return 'x-panel-middle-calumn-grid';
        }
    }
});


