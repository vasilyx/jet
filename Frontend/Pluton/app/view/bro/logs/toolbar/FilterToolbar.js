/**
 *
 */
Ext.define('Pluton.view.bro.logs.toolbar.FilterToolbar', {
    extend: 'Ext.toolbar.Toolbar',

    xtype: 'bro-logs-filter-toolbar',

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Pluton.ux.container.DateRangeContainer'
    ],

    items: [
        {
            title: 'Фильтры',
            xtype: 'fieldset',
            ui: 'fieldset-without-padding',
            style: {
                background: 'transparent'
            },
            collapsible: true,
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    reference: 'broLogsFilters',
                    xtype: 'form',
                    layout: 'hbox',
                    scrollable: true,
                    defaults: {
                        flex: 1,
                        minWidth: 300
                    },
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    bbar: {
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                text: 'Применить',
                                tooltip: 'Фильтровать по выбранным параметрам',
                                ui: 'md-blue',
                                formBind: true,
                                handler: 'onFilterApply'
                            },
                            {
                                text: 'Сбросить',
                                tooltip: 'Сброс фильтров',
                                handler: 'onFilterReset'
                            }
                        ]
                    },
                    items: [
                        {
                            xtype: 'date-range-container',
                            name: 'ts',
                            submitFormat: 'Y-m-d H:i:sP',
                            margin: '0 10 0 0',
                            fromFieldConfig: {
                                fieldLabel: 'Время регистрации события',
                                value: Ext.Date.clearTime(new Date())
                            },
                            toFieldConfig: {
                                value: Ext.Date.add(Ext.Date.clearTime(new Date), Ext.Date.DAY, 1)
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'orig_h',
                            fieldLabel: 'Адрес источника',
                            margin: '0 10 0 0',
                            vtype: 'ipAddress'
                        },
                        {
                            xtype: 'textfield',
                            name: 'resp_h',
                            fieldLabel: 'Адрес назначения',
                            vtype: 'ipAddress'
                        }
                    ]
                }
            ]
        }
    ]
});
