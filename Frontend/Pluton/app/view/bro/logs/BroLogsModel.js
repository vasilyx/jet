/**
 *
 */
Ext.define('Pluton.view.bro.logs.BroLogsModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.bro-logs',

    requires: [
        'Pluton.store.systemComponent.SystemComponentBrief'
    ],

    stores: {
        SystemComponent: {
            type: 'system-component-brief',
            autoLoad: false
        }
    },

    data: {
        
    }
});
