/**
 *
 */
Ext.define('Pluton.view.bro.logs.BroLogs', {
    extend: 'Ext.tab.Panel',

    permissions: 'bro-logs_page',

    xtype: 'events.bro-logs',

    requires: [
        'Ext.layout.container.Accordion',
        'Ext.layout.container.Border',
        'Ext.panel.Panel',
        'Ext.tree.Panel',
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.bro.logs.BroLogsController',
        'Pluton.view.bro.logs.BroLogsModel',
        'Pluton.view.bro.logs.grid.BroLogsGrid',
        'Pluton.view.bro.logs.toolbar.FilterToolbar'
    ],

    controller: 'bro-logs',
    viewModel: {
        type: 'bro-logs'
    },

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onActivate'
    },

    items: [
        {
            title: 'Журналы сетевой активности',
            xtype: 'panel',
            layout: 'border',
            items: [
                {
                    reference: 'systemComponentTree',
                    xtype: 'treepanel',
                    region: 'east',
                    width: 300,
                    rootVisible: false,
                    split: true,
                    collapsible: true,
                    header: false,
                    useArrows: true,
                    displayField: 'name',
                    bodyStyle: {
                        borderTop: '0px'
                    },
                    bind: {
                        store: '{SystemComponent}'
                    },
                    listeners: {
                        beforeselect: 'onComponentBeforeSelect',
                        beforecheckchange: 'onComponentBeforeCheckChange',
                        selectionchange: 'onComponentSelectionChange'
                    }
                },
                {
                    reference: 'broLogsPanel',
                    xtype: 'panel',
                    region: 'center',
                    scrollable: true,
                    tbar: {
                        xtype: 'bro-logs-filter-toolbar'
                    },
                    layout: {
                        type: 'accordion'
                    },
                    defaults: {
                        xtype: 'bro-logs-grid',
                        minHeight: 700
                    },
                    items: [
                        {
                            xtype: 'panel',
                            hidden: true
                        }
                    ]
                }
            ]
        }
    ]
});
