/**
 *
 */
Ext.define('Pluton.view.blackList.BlackListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.black-list',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.blackList.BlackList',
        'Pluton.model.blackList.BlackListType',
        'Pluton.model.blackList.Reaction',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.voc.Label'
    ],

    stores: {
        BlackList: {
            model: 'Pluton.model.blackList.BlackList',
            pageSize: 0,
            remoteSort: true,
            remoteFilter: true,
            sorters: 'name',
            proxy: {
                type: 'json-ajax',
                url: '/black_list/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        BlackListType: {
            model: 'Pluton.model.blackList.BlackListType',
            pageSize: 0,
            remoteSort: true,
            autoLoad: true,
            proxy: {
                type: 'json-ajax',
                url: '/black_list_type/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        Reaction: {
            model: 'Pluton.model.blackList.Reaction',
            pageSize: 0,
            remoteSort: true,
            sorters: 'label',
            autoLoad: true,
            proxy: {
                type: 'json-ajax',
                url: '/reaction/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        Direction: {
            model: 'Pluton.model.voc.Label',
            sorters: 'label',
            autoLoad: true,
            pageSize: 0,
            proxy: {
                type: 'json-ajax',
                url: '/black_list/direction'
            }
        }
    },

    data: {
        
    }
});
