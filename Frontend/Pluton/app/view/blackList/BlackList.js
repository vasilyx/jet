/**
 *
 */
Ext.define('Pluton.view.blackList.BlackList', {
    extend: 'Ext.tab.Panel',

    permissions: 'black-list_page',

    xtype: 'env.black-list',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Check',
        'Ext.grid.column.Date',
        'Ext.layout.container.Fit',
        'Ext.layout.container.HBox',
        'Ext.toolbar.Separator',
        'Pluton.utility.button.CSVExport',
        'Pluton.ux.form.FiltersFieldSet',
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.blackList.BlackListController',
        'Pluton.view.blackList.BlackListModel'
    ],

    controller: 'black-list',
    viewModel: {
        type: 'black-list'
    },

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onActivate'
    },
    defaults: {
        closable: true
    },

    items: [
        {
            reference: 'blackListGrid',
            xtype: 'grid',
            title: 'Чёрные списки',
            closable: false,
            viewConfig: {
                loadMask: false
            },
            bind: {
                store: '{BlackList}'
            },
            listeners: {
                itemdblclick: 'onItemDblClick'
            },
            lbar: [
                {
                    iconCls: 'x-fa fa-refresh',
                    tooltip: 'Обновить',
                    handler: 'loadStores'
                },
                '-',
                {
                    iconCls: 'x-fa fa-plus',
                    ui: 'md-green',
                    tooltip: 'Создать',
                    handler: 'onCreate'
                },
                {
                    iconCls: 'x-fa fa-pencil',
                    ui: 'md-yellow',
                    tooltip: 'Редактировать',
                    disabled: true,
                    bind: {
                        disabled: '{!blackListGrid.selection}'
                    },
                    handler: 'onEdit'
                },
                {
                    iconCls: 'x-fa fa-trash-o',
                    ui: 'md-red',
                    tooltip: 'Удалить',
                    disabled: true,
                    bind: {
                        disabled: '{!blackListGrid.selection || blackListGrid.selection.source.id != "USER"}'
                    },
                    handler: 'onRemove'
                },
                '-',
                {
                    xtype: 'utility-button-csv-export'
                }
            ],
            tbar: [
                {
                    xtype: 'filters-fieldset',
                    layout: 'fit',
                    flex: 1,
                    items: [
                        {
                            reference: 'blackListFilters',
                            xtype: 'form',
                            layout: 'hbox',
                            scrollable: true,
                            bodyPadding: '0 0 10 0',
                            defaults: {
                                flex: 1,
                                minWidth: 300
                            },
                            fieldDefaults: {
                                labelAlign: 'top'
                            },
                            bbar: {
                                layout: {
                                    pack: 'center'
                                },
                                items: [
                                    {
                                        text: 'Применить',
                                        tooltip: 'Применить фильтры',
                                        ui: 'md-blue',
                                        handler: 'onFilterApply',
                                        bind: {
                                            disabled: '{!isValid}'
                                        }
                                    },
                                    {
                                        text: 'Сбросить',
                                        tooltip: 'Сбросить фильтры',
                                        handler: 'onFilterReset'
                                    }
                                ]
                            },
                            items: [
                                {
                                    name: 'name',
                                    fieldLabel: 'Имя',
                                    xtype: 'textfield',
                                    margin: '0 10 0 0'
                                },
                                {
                                    name: 'black_list_type_id_',
                                    xtype: 'combo',
                                    fieldLabel: 'Тип',
                                    queryMode: 'local',
                                    anyMatch: true,
                                    forceSelection: true,
                                    clearTrigger: true,
                                    valueField: 'id',
                                    displayField: 'label',
                                    margin: '0 10 0 0',
                                    bind: {
                                        store: '{BlackListType}'
                                    }
                                },
                                {
                                    name: 'is_active',
                                    xtype: 'combo',
                                    fieldLabel: 'Активный',
                                    forceSelection: true,
                                    editable: false,
                                    clearTrigger: true,
                                    margin: 0,
                                    store: [
                                        [true, 'Да'],
                                        [false, 'Нет']
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            columns: {
                defaults: {
                    flex: 1
                },
                items: [
                    {
                        dataIndex: 'id',
                        text: 'Идентификатор',
                        hidden: true
                    },
                    {
                        dataIndex: 'name',
                        text: 'Имя',
                        flex: 2
                    },
                    {
                        dataIndex: 'description',
                        text: 'Описание',
                        flex: 4
                    },
                    {
                        dataIndex: 'black_list_type',
                        sorter: 'black_list_type_label_',
                        text: 'Тип',
                        renderer: Ext.labelRenderer
                    },
                    {
                        dataIndex: 'source',
                        text: 'Источник',
                        renderer: Ext.labelRenderer
                    },
                    {
                        xtype: 'checkcolumn',
                        dataIndex: 'is_active',
                        text: 'Активный',
                        renderer: function (value, metaData) {
                            metaData.tdCls += Ext.baseCSSPrefix + 'item-disabled';

                            return Ext.grid.column.Check.prototype.defaultRenderer.call(this, value, metaData);
                        },
                        listeners: {
                            beforecheckchange: function () {
                                return false;
                            }
                        }
                    },
                    {
                        dataIndex: 'version',
                        text: 'Версия'
                    },
                    {
                        dataIndex: 'create_user',
                        text: 'Создал',
                        sorter: 'create_user_label_',
                        renderer: Ext.labelRenderer
                    },
                    {
                        xtype: 'datecolumn',
                        dataIndex: 'create_timestamp',
                        text: 'Время создания',
                        format: 'd.m.Y H:i:s'
                    },
                    {
                        dataIndex: 'update_user',
                        text: 'Изменил',
                        sorter: 'update_user_label_',
                        renderer: Ext.labelRenderer
                    },
                    {
                        xtype: 'datecolumn',
                        dataIndex: 'update_timestamp',
                        text: 'Время изменения',
                        format: 'd.m.Y H:i:s'
                    }
                ]
            }
        }
    ]
});
