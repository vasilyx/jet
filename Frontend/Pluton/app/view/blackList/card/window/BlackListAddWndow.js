/**
 *
 */
Ext.define('Pluton.view.blackList.card.window.BlackListAddWndow', {
    extend: 'Ext.window.Window',

    xtype: 'black-list-add-window',

    requires: [
        'Ext.layout.container.Fit',
        'Pluton.view.blackList.card.form.BlackListAddForm',
        'Pluton.view.blackList.card.window.BlackListAddWindowController'
    ],

    controller: 'black-list-add-window',

    title: 'Создание черного списка',
    modal: true,
    constrain: true,
    minHeight: 350,
    minWidth: 350,
    width: 600,
    layout: 'fit',
    listeners: {
        beforeclose: 'onBeforeClose'
    },

    items: [
        {xtype: 'black-list-add-form'}
    ]
});
