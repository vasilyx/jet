/**
 *
 */
Ext.define('Pluton.view.blackList.card.window.BlackListUploadWindow', {
    extend: 'Pluton.ux.window.UploadWindow',

    xtype: 'black-list-upload-window',

    requires: [
        'Ext.data.reader.Array',
        'Ext.form.field.ComboBox',
        'Pluton.data.proxy.JsonAjax'
    ],

    viewModel: {
        stores: {
            Delimiters: {
                fields: [
                    {name: 'id', mapping: 0},
                    {name: 'label', mapping: 1}
                ],
                autoLoad: true,
                pageSize: 0,
                sorters: 'label',
                proxy: {
                    type: 'json-ajax',
                    url: '/utils/delimiters',
                    reader: 'array'
                }
            }
        },
        formulas: {
            params: {
                bind: '{theBlackList}',
                get: function(theBlackList) {
                    if (theBlackList) {
                        return {
                            black_list_id: theBlackList.getId()
                        }
                    }

                    return null;
                }
            }
        }
    },

    bind: {
        params: '{params}'
    },

    url: '/black_list/record/upload',
    accept: '.csv',

    additionalFields: [
        {
            name: 'delimiter',
            xtype: 'combo',
            fieldLabel: 'Разделитель',
            editable: false,
            allowBlank: false,
            valueField: 'id',
            displayField: 'label',
            queryMode: 'local',
            bind: {
                store: '{Delimiters}'
            }
        }
    ]
});
