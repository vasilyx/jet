/**
 *
 */
Ext.define('Pluton.view.blackList.card.window.BlackListAddWindowController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.black-list-add-window',

    control: {
        '#': {
            save: 'onSave',
            reset: 'onReset'
        }
    },

    init: function(view) {
        view.relayEvents(view.down('black-list-add-form'), ['save', 'reset']);
    },

    onSave: function() {
        this.getView().close();
    },

    onReset: function() {
        this.getView().close();
    },

    onBeforeClose: function() {
        var view = this.getView(),
            theBlackList = view.getViewModel().get('theBlackList');

        if (theBlackList && theBlackList.isDirty()) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Имеются несохраненные данные, продолжить без сохранения?',
                function(msg) {
                    if (msg == 'yes') {
                        theBlackList.reject();
                        view.close();
                    }
                }
            );

            return false;
        }

        return true;
    }
});
