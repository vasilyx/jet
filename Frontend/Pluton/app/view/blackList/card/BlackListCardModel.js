/**
 *
 */
Ext.define('Pluton.view.blackList.card.BlackListCardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.black-list-card',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.blackList.BlackListRecord',
        'Pluton.model.user.User'
    ],

    stores: {
        BlackListRecordList: {
            model: 'Pluton.model.blackList.BlackListRecord',
            pageSize: 0,
            remoteSort: true,
            remoteFilter: true,
            proxy: {
                type: 'json-ajax',
                url: '/black_list/record/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        SystemUsers: {
            model: 'Pluton.model.user.User',
            pageSize: 0,
            remoteSort: true,
            autoLoad: false,
            proxy:{
                type: 'json-ajax',
                url: '/user/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        }
    },

    data: {
        editing: false,
        theBlackList: null,
        blackListRecordValue: null
    },

    formulas: {
        blackListTitle: {
            bind: {
                bindTo: '{theBlackList}',
                deep: true
            },
            get: function(theBlackList) {
                if (theBlackList) {
                    return theBlackList.get('name');
                } else {
                    return '';
                }
            }
        }
    }
});
