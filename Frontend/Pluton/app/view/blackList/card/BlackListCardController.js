/**
 *
 */
Ext.define('Pluton.view.blackList.card.BlackListCardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.black-list-card',

    requires: [
        'Pluton.view.blackList.card.window.BlackListUploadWindow'
    ],

    control: {
        'black-list-form': {
            reset: 'onReset'
        }
    },

    onBeforeClose: function() {
        var view = this.getView(),
            theBlackList = view.getViewModel().get('theBlackList');

        if (!theBlackList.erased && theBlackList.isDirty()) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Имеются несохраненные данные, продолжить без сохранения?',
                function(msg) {
                    if (msg == 'yes') {
                        theBlackList.reject();
                        view.close();
                    }
                }
            );

            return false;
        }

        return true;
    },

    onLoad: function() {
        this.loadStores();
    },

    onReset: function() {
        this.getView().close();
    },

    loadStores: function() {
        var me = this,
            view = me.getView();

        view.setLoading();

        Ext.apply(me.getStore('BlackListRecordList').getProxy().extraParams, {
            'black_list_id': me.getViewModel().get('theBlackList').getId()
        });

        Ext.Deferred.all([
            me.loadBlackList(), me.loadBlackListRecordList()
        ]).always(function() {
            view.setLoading(false);
            var blackListRecordGrid = me.getBlackListRecordGrid(),
                rowplugin = blackListRecordGrid.findPlugin('rowediting'),
                theBlackList = me.getViewModel().get('theBlackList'),
                source = theBlackList.get('source'),
                black_list_type = theBlackList.get('black_list_type'),
                addForm = me.lookup('blackListAddForm'),
                dirField = addForm.lookup('blackListDirectionField');

            if (rowplugin && source && source.id != 'USER') {
                rowplugin.disable();
            }
            if (dirField && black_list_type && black_list_type.label == "DNS") {
                dirField.setReadOnly(true);
            }
        });
    },
    
    loadBlackList: function() {
        var deferred = new Ext.Deferred(),
            theBlackList = this.getViewModel().get('theBlackList');

        theBlackList.load({
            callback: function(record, operation, success) {
                if (success) {
                    deferred.resolve(record);
                } else {
                    deferred.reject();
                }
            }
        });

        return deferred.promise;    
    },
    
    loadBlackListRecordList: function() {
        var me = this,
            deferred = new Ext.Deferred(),
            vModel = me.getViewModel(),
            store = vModel.getStore('BlackListRecordList'),
            blackListRecordValue = vModel.get('blackListRecordValue'),
            blackListFiltersFieldset = me.lookup('blackListFiltersFieldset'),
            blackListRecordValueField = blackListFiltersFieldset.down('[name=value]');

        if (blackListRecordValue) {
            blackListFiltersFieldset.expand();
            blackListRecordValueField.setValue(blackListRecordValue);
            store.addFilter({
                property: 'value',
                operator: 'like',
                value: blackListRecordValue
            }, true);
        } else {
            blackListRecordValueField.setValue('');
            store.removeFilter('value', true);
        }
        vModel.set('blackListRecordValue', null);

        store.load({
            callback: function(records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject();
                }
            }
        });

        return deferred.promise;
    },

    onBlackListRecordCreateBtn: function() {
        var blackListRecordGrid = this.getBlackListRecordGrid(),
            blackListRecordStore = blackListRecordGrid.getStore(),
            theBlackList = this.getViewModel().get('theBlackList');

        blackListRecordGrid.findPlugin('rowediting').startEdit(
            blackListRecordStore.add({
                black_list_id: theBlackList.getId(),
                direction: theBlackList.get('direction')
            })[0]
        );
    },

    onBlackListRecordEditBtn: function() {
        var blackListRecord = this.getViewModel().get('blackListRecordGrid.selection');

        if (blackListRecord) {
            this.getBlackListRecordGrid().findPlugin('rowediting').startEdit(blackListRecord);
        }
    },

    onBlackListRecordRemoveBtn: function() {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            blackListRecordStore = me.getBlackListRecordGrid().getStore(),
            blackListRecord = vModel.get('blackListRecordGrid.selection');

        if (blackListRecord) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Вы уверены, что хотите удалить запись?',
                function(msg) {
                    if (msg == 'yes') {
                        view.setLoading();
                        blackListRecord.erase({
                            params: {
                                black_list_id: vModel.get('theBlackList').getId()
                            },
                            callback: function(record, operation, success) {
                                if (success) {
                                    me.loadStores();
                                } else {
                                    blackListRecordStore.reload();
                                    view.setLoading(false);
                                }
                            }
                        });
                    }
                }
            );
        }
    },

    onBlackListRecordUploadBtn: function() {
        Ext.widget('black-list-upload-window', {
            viewModel: {
                parent: this.getViewModel()
            },
            listeners: {
                success: 'onBlackListRecordUploadSuccess',
                scope: this
            }
        }).show();
    },

    onBlackListRecordUploadSuccess: function(window, result) {
        if (Object.keys(result).length === 1 && result.hasOwnProperty('message')) {
            Ext.Msg.alert('Ошибка загрузки', result.message);
        } else {
            this.loadStores();
        }

        window.close();
    },


    onBlackListRecordBeforeEdit: function(editor, context) {
        var vModel = this.getViewModel(),
            theBlackList = vModel.get('theBlackList'),
            black_list_type = theBlackList.get('black_list_type'),
            form = editor.getEditor().getForm();

        if (this.getEditing()) {
            return false;
        } else {
            // Значение редактируется только в новых записях
            form.findField('value').setReadOnly(!context.record.phantom);
            this.setEditing(true);
        }
        // Направление для DNS всегда ANY
        if (black_list_type && black_list_type.label == "DNS") {
            var dirField = form.findField('direction'),
                dirStore = vModel.get('Direction'),
                direction = context.record.get('direction');
            if(!direction || direction.id != 'ANY') {
                context.record.set('direction', dirStore.getById('ANY').data);
            }
            dirField.setReadOnly(true);
        }
    },

    onBlackListRecordCancelEdit: function(editor, context) {
        var blackListRecord = context.record;

        if (blackListRecord.phantom) {
            blackListRecord.drop();
        } else {
            blackListRecord.reject();
        }

        this.setEditing(false);
    },

    onBlackListRecordEdit: function(editor, context) {
        var me = this,
            view = me.getView(),
            blackListRecordGrid = me.getBlackListRecordGrid(),
            blackListRecord = context.record;

        me.setEditing(false);

        if (blackListRecord.isDirty()) {
            view.setLoading();
            blackListRecord.save({
                callback: function(record, operation, success) {
                    if (success) {
                        me.loadStores();
                    } else {
                        blackListRecordGrid.findPlugin('rowediting').startEdit(blackListRecord);
                        view.setLoading(false);
                    }
                }
            })
        }
    },

    setEditing: function(editing) {
        this.getViewModel().set('editing', !!editing);
    },

    getEditing: function() {
        return this.getViewModel().get('editing');
    },

    getBlackListRecordGrid: function() {
        return this.lookup('blackListRecordGrid');
    },

    onFilterReset: function() {
        this.lookup('blackListCardFilters').reset();
        this.getStore('BlackListRecordList').clearFilter();
    },
    onFilterApply: function() {
        var store = this.getStore('BlackListRecordList'),
            values = this.lookup('blackListCardFilters').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function(key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'value':
                    case 'description':
                        operator = 'like';
                        break;
                    case 'direction':
                    case 'create_user_id_':
                        operator = '=';
                        break;

                    default:
                        operator = null;
                        break;
                }
                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            } else {
                store.removeFilter(key, true);
            }
        }, this);

        store.addFilter(filtersConfig, true);
        this.getStore('BlackListRecordList').load();
    }
});
