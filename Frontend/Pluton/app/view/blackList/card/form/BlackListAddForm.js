/**
 *
 */
Ext.define('Pluton.view.blackList.card.form.BlackListAddForm', {
    extend: 'Ext.form.Panel',

    xtype: 'black-list-add-form',

    requires: [
        'Ext.form.field.Checkbox',
        'Ext.form.field.ComboBox',
        'Ext.form.field.TextArea',
        'Ext.layout.container.Form',
        'Pluton.view.blackList.card.form.BlackListFormController',
        'Pluton.view.blackList.card.form.BlackListFormModel'
    ],

    controller: 'black-list-form',
    viewModel: {
        type: 'black-list-form'
    },

    layout: 'form',
    scrollable: true,
    defaultType: 'textfield',
    modelValidation: true,

    reference: 'blackListAddForm',

    bbar: [
        {
            text: 'Применить',
            tooltip: 'Применить',
            ui: 'md-blue',
            handler: 'onSave',
            bind: {
                disabled: '{!theBlackList.dirty || !theBlackList.valid}'
            }
        },
        {
            text: 'Отменить',
            tooltip: 'Отменить',
            handler: 'onReset'
        }
    ],

    items: [
        {
            fieldLabel: 'Имя',
            required: true,
            bind: {
                value: '{theBlackList.name}',
                readOnly: '{theBlackList.source.id != "USER"}'
            }
        },
        {
            xtype: 'combo',
            fieldLabel: 'Тип',
            required: true,
            forceSelection: true,
            editable: false,
            queryMode: 'local',
            valueField: 'id',
            displayField: 'label',
            bind: {
                readOnly: '{!theBlackList.phantom}',
                store: '{BlackListType}',
                value: '{theBlackList.black_list_type}'
            },
            listeners: {
                change: 'onBlackListTypeChange'
            }
        },
        {
            fieldLabel: 'Источник',
            readOnly: true,
            hidden: true,
            bind: {
                value: '{theBlackList.source.label}',
                hidden: '{theBlackList.phantom}'
            }
        },
        {
            xtype: 'combo',
            fieldLabel: 'Направление по умолчанию',
            reference: 'blackListDirectionField',
            required: true,
            forceSelection: true,
            editable: false,
            queryMode: 'local',
            valueField: 'id',
            displayField: 'label',
            bind: {
                store: '{Direction}',
                value: '{theBlackList.direction}',
                readOnly: '{theBlackList.source.id != "USER"}'
            }
        },
        {
            xtype: 'textarea',
            fieldLabel: 'Описание',
            maxLength: 2000,
            bind: {
                value: '{theBlackList.description}',
                readOnly: '{theBlackList.source.id != "USER"}'
            }
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Активный',
            bind: {
                value: '{theBlackList.is_active}',
                readOnly: '{theBlackList.source.id != "USER"}'
            }
        }
    ]
});
