/**
 *
 */
Ext.define('Pluton.view.blackList.card.form.BlackListForm', {
    extend: 'Pluton.view.blackList.card.form.BlackListAddForm',

    xtype: 'black-list-form',

    requires: [
        'Ext.form.field.Display'
    ],

    initComponent: function() {
        this.items = this.items.concat([
            {
                xtype: 'displayfield',
                fieldLabel: 'Версия',
                bind: {
                    value: '{theBlackList.version}'
                }
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Создал',
                bind: {
                    value: '{theBlackList.create_user}'
                },
                renderer: Ext.labelRenderer
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Время создания',
                renderer: 'dateRenderer',
                bind: {
                    value: '{theBlackList.create_timestamp}'
                }
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Изменил',
                bind: {
                    value: '{theBlackList.update_user}'
                },
                renderer: Ext.labelRenderer
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Время изменения',
                renderer: 'dateRenderer',
                bind: {
                    value: '{theBlackList.update_timestamp}'
                }
            }
        ]);

        this.callParent(arguments);
    }
});
