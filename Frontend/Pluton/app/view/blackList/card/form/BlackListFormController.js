/**
 *
 */
Ext.define('Pluton.view.blackList.card.form.BlackListFormController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.black-list-form',

    onSave: function() {
        var vModel = this.getViewModel(),
            theBlackList = vModel.get('theBlackList');

        if (theBlackList.isDirty() && theBlackList.isValid()) {
            theBlackList.save({
                success: function(record) {
                     this.fireViewEvent('save', record);
                },
                scope: this
            });
        }
    },

    onReset: function() {
        this.fireViewEvent('reset');
    },

    dateRenderer: function(value) {
        if (value) {
            return Ext.Date.format(value, 'd.m.Y H:i:s');
        }

        return value;
    },

    onBlackListTypeChange: function(field) {
        var dirField = this.lookup('blackListDirectionField');
        if(field.rawValue == "DNS") {
            var vModel = this.getViewModel(),
                dirStore = vModel.get('Direction'),
                theBlackList = vModel.get('theBlackList'),
                direction = theBlackList.get('direction');
            if(!direction || direction.id != 'ANY') {
                theBlackList.set('direction', dirStore.getById('ANY').data);
            }
            dirField.setReadOnly(true);
        } else {
            dirField.setReadOnly(false);
        }
    },
});
