/**
 *
 */
Ext.define('Pluton.view.blackList.card.BlackListCard', {
    extend: 'Ext.panel.Panel',

    xtype: 'black-list-card',

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.layout.container.Fit',
        'Ext.layout.container.HBox',
        'Ext.resizer.Splitter',
        'Pluton.ux.form.FiltersFieldSet',
        'Pluton.view.blackList.card.BlackListCardController',
        'Pluton.view.blackList.card.BlackListCardModel',
        'Pluton.view.blackList.card.form.BlackListForm',
        'Pluton.view.blackList.card.grid.BlackListRecordGrid'
    ],

    controller: 'black-list-card',
    viewModel: {
        type: 'black-list-card'
    },

    bind: {
        title: '{blackListTitle}'
    },
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    bodyPadding: 10,

    listeners: {
        beforeclose: 'onBeforeClose',
        load: 'onLoad'
    },

    items: [
        {
            xtype: 'fieldset',
            title: 'Общие сведения',
            layout: 'fit',
            ui: 'fieldset-without-padding',
            style: {
                background: 'transparent'
            },
            flex: 1.5,
            minWidth: 350,
            items: [
                {
                    xtype: 'black-list-form'
                }
            ]
        },
        {
            xtype: 'splitter'
        },
        {
            xtype: 'fieldset',
            title: 'Записи черного списка',
            layout: 'fit',
            ui: 'fieldset-without-padding',
            style: {
                background: 'transparent'
            },
            flex: 2.5,
            minWidth: 350,
            items: [
                {
                    xtype: 'black-list-record-grid',
                    tbar: [
                        {
                            reference: 'blackListFiltersFieldset',
                            xtype: 'filters-fieldset',
                            flex: 1,
                            items: [
                                {
                                    reference: 'blackListCardFilters',
                                    xtype: 'form',
                                    layout: 'hbox',
                                    scrollable: true,
                                    bodyPadding: '0 0 10 0',
                                    defaults: {
                                        flex: 1,
                                        minWidth: 200
                                    },
                                    fieldDefaults: {
                                        labelAlign: 'top'
                                    },
                                    bbar: {
                                        layout: {
                                            pack: 'center'
                                        },
                                        items: [
                                            {
                                                text: 'Применить',
                                                tooltip: 'Применить фильтры',
                                                ui: 'md-blue',
                                                handler: 'onFilterApply',
                                                bind: {
                                                    disabled: '{!isValid}'
                                                }
                                            },
                                            {
                                                text: 'Сбросить',
                                                tooltip: 'Сбросить фильтры',
                                                handler: 'onFilterReset'
                                            }
                                        ]
                                    },
                                    items: [
                                        {
                                            name: 'value',
                                            fieldLabel: 'Идентификатор сущности',
                                            xtype: 'textfield',
                                            margin: '0 10 0 0'
                                        },
                                        {
                                            name: 'direction',
                                            xtype: 'combo',
                                            fieldLabel: 'Направление',
                                            queryMode: 'local',
                                            editable: false,
                                            clearTrigger: true,
                                            valueField: 'id',
                                            displayField: 'label',
                                            margin: '0 10 0 0',
                                            bind: {
                                                store: '{Direction}'
                                            }
                                        },
                                        {
                                            name: 'description',
                                            fieldLabel: 'Описание',
                                            xtype: 'textfield',
                                            margin: '0 10 0 0'
                                        },
                                        {
                                            name: 'create_user_id_',
                                            xtype: 'combo',
                                            fieldLabel: 'Создал',
                                            queryMode: 'remote',
                                            editable: false,
                                            clearTrigger: true,
                                            valueField: 'id',
                                            displayField: 'login',
                                            permissions: 'settings-user_page',
                                            bind: {
                                                store: '{SystemUsers}'
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});
