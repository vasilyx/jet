/**
 *
 */
Ext.define('Pluton.view.blackList.card.grid.BlackListRecordGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'black-list-record-grid',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.grid.column.Date',
        'Ext.grid.plugin.RowEditing',
        'Ext.toolbar.Separator'
    ],

    reference: 'blackListRecordGrid',
    bind: {
        store: '{BlackListRecordList}'
    },

    viewConfig: {
        loadMask: false
    },
    plugins: {
        ptype: 'rowediting',
        clicksToEdit: 2,
        autoCancel: false,
        errorSummary: false,
        removeUnmodified: true
    },
    listeners: {
        beforeedit: 'onBlackListRecordBeforeEdit',
        canceledit: 'onBlackListRecordCancelEdit',
        edit: 'onBlackListRecordEdit'
    },

    lbar: [
        {
            iconCls: 'x-fa fa-plus',
            ui: 'md-green',
            tooltip: 'Создать',
            bind: {
                disabled: '{editing || theBlackList.source.id != "USER"}'
            },
            handler: 'onBlackListRecordCreateBtn'
        },
        {
            iconCls: 'x-fa fa-pencil',
            ui: 'md-yellow',
            tooltip: 'Редактировать',
            disabled: true,
            bind: {
                disabled: '{!blackListRecordGrid.selection || editing || theBlackList.source.id != "USER"}'
            },
            handler: 'onBlackListRecordEditBtn'
        },
        {
            iconCls: 'x-fa fa-trash-o',
            ui: 'md-red',
            tooltip: 'Удалить',
            disabled: true,
            bind: {
                disabled: '{!blackListRecordGrid.selection || editing || theBlackList.source.id != "USER"}'
            },
            handler: 'onBlackListRecordRemoveBtn'
        },
        '-',
        {
            iconCls: 'x-fa fa-upload',
            tooltip: 'Загрузить из файла',
            bind: {
                disabled: '{editing || theBlackList.source.id != "USER"}'
            },
            handler: 'onBlackListRecordUploadBtn'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'value',
                text: 'Идентификатор сущности',
                flex: 2,
                editor: {
                    xtype: 'textfield',
                    allowOnlyWhitespace: false,
                    maxLength: 255
                }
            },
            {
                dataIndex: 'direction',
                text: 'Направление',
                flex: 2,
                renderer: Ext.labelRenderer,
                editor: {
                    xtype: 'combo',
                    required: true,
                    forceSelection: true,
                    editable: false,
                    allowOnlyWhitespace: false,
                    queryMode: 'local',
                    valueField: 'id',
                    displayField: 'label',
                    bind: {
                        store: '{Direction}'
                    }
                }
            },
            {
                dataIndex: 'description',
                text: 'Описание',
                flex: 3,
                editor: {
                    xtype: 'textfield',
                    maxLength: 255
                }
            },
            {
                dataIndex: 'create_user',
                text: 'Создал',
                hidden: false,
                renderer: Ext.labelRenderer
            },
            {
                xtype: 'datecolumn',
                dataIndex: 'create_timestamp',
                text: 'Время создания',
                format: 'd.m.Y H:i:s',
                hidden: true
            },
            {
                dataIndex: 'update_user',
                text: 'Изменил',
                hidden: true,
                renderer: Ext.labelRenderer
            },
            {
                xtype: 'datecolumn',
                dataIndex: 'update_timestamp',
                text: 'Время изменения',
                format: 'd.m.Y H:i:s',
                hidden: true
            }
        ]
    }
});
