/**
 *
 */
Ext.define('Pluton.view.blackList.BlackListController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.black-list',

    requires: [
        'Pluton.model.blackList.BlackList',
        'Pluton.view.blackList.card.BlackListCard',
        'Pluton.view.blackList.card.window.BlackListAddWndow'
    ],

    route: 'env/black-list',
    tabPrefix: 'blacklist_',

    listen: {
        component: {
            '#': {
                routeWithParams: 'onRouteParams',
                tabchange: 'onTabChange'
            }
        }
    },

    onRouteParams: function(params) {
        if (params && params.id) {
            var record = new Pluton.model.blackList.BlackList;
            record.setId(params.id);
            this.showCard(record, params.value);
        }
    },

    onTabChange: function( tabPanel, newCard, oldCard, eOpts ) {
        if (newCard.getXType() === 'black-list-card' && !Ext.isEmpty(newCard.itemId)) {
            this.redirectTo(this.route + '?id=' + newCard.itemId.replace(this.tabPrefix, ''));
        } else {
            this.redirectTo(this.route);
        }
    },

    onActivate: function() {
        this.loadStores();
    },

    loadStores: function() {
        var me = this,
            blackListGrid = me.lookup('blackListGrid');

        blackListGrid.setLoading();
        Ext.Deferred.all([
            me.loadReactionStore(), me.loadBlackListTypeStore()
        ]).always(function() {
            me.getStore('BlackList').load(function() {
                blackListGrid.setLoading(false);
            });
        });
    },

    loadReactionStore: function() {
        var deferred = new Ext.Deferred();

        this.getStore('Reaction').load({
            callback: function(records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject();
                }
            }
        });

        return deferred.promise;
    },

    loadBlackListTypeStore: function() {
        var deferred = new Ext.Deferred();

        this.getStore('BlackListType').load({
            callback: function(records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject();
                }
            }
        });

        return deferred.promise;
    },

    loadBlackListStore: function() {
        var view = this.getView();

        view.setLoading();
        this.getStore('BlackList').load(function() {
            view.setLoading(false);
        });
    },

    onFilterApply: function() {
        var store = this.getStore('BlackList'),
            values = this.lookup('blackListFilters').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function(key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'name':
                        operator = 'like';
                        break;
                    case 'black_list_type_id_':
                    case 'reaction_list':
                    case 'is_active':
                        operator = '=';
                        break;
                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            } else {
                store.removeFilter(key, true);
            }
        }, this);

        store.addFilter(filtersConfig, true);
        this.loadBlackListStore();
    },

    onFilterReset: function() {
        this.lookup('blackListFilters').reset();
        this.getStore('BlackList').clearFilter();
    },

    onItemDblClick: function(grid, record) {
        this.redirectTo(this.route + '?id=' + record.getId());
    },

    onCreate: function() {
        Ext.widget('black-list-add-window', {
            viewModel: {
                parent: this.getViewModel(),
                data: {
                    theBlackList: new Pluton.model.blackList.BlackList()
                }
            },
            listeners: {
                save: 'onCreated',
                scope: this
            }
        }).show();
    },

    onCreated: function(form, record) {
        this.getStore('BlackList').insert(0, record);
        this.showCard(record);
    },

    onEdit: function() {
        var selection = this.getViewModel().get('blackListGrid.selection');

        if (selection) {
            this.redirectTo(this.route + '?id=' + selection.getId());
        }
    },

    onRemove: function() {
        var me = this,
            selection = me.getViewModel().get('blackListGrid.selection');

        if (selection) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Вы уверены, что хотите удалить запись?',
                function(msg) {
                    if (msg == 'yes') {
                        selection.erase({
                            success: function() {
                                var tab = me.getView().items.getByKey(me.getTabItemId(selection.getId()));

                                if (tab) {
                                    tab.close();
                                }
                            },
                            failure: function() {
                                me.loadBlackListStore();
                            }
                        });
                    }
                }
            );
        }
    },

    showCard: function(record, blackListRecordValue) {
        var view = this.getView(),
            itemId = this.getTabItemId(record.getId()),
            tab = view.items.getByKey(itemId);

        if (tab) {
            view.setActiveTab(tab);
        } else {
            tab = view.add({
                xtype: 'black-list-card',
                itemId: itemId,
                viewModel: {
                    data: {
                        theBlackList: record,
                        blackListRecordValue: blackListRecordValue || null
                    }
                }
            });

            view.setActiveTab(tab);
            tab.fireEvent('load');
        }
    },

    getTabItemId: function(id) {
        return (this.tabPrefix + id);
    }
});
