/**
 *
 */
Ext.define('Pluton.view.pages.Page404', {
    extend: 'Pluton.view.pages.AbstractPage',

    xtype: 'page-404',

    items: [
        {
            xtype: 'box',
            cls: 'abstract-page-container',
            html: '<div class=\'fa-outer-class\'><span class=\'x-fa fa-exclamation-circle\'></span></div><h1>404</h1><span>Раздел не найден!</span>'
        }
    ]
});
