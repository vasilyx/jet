/**
 *
 */
Ext.define('Pluton.view.pages.BlankPage', {
    extend: 'Pluton.view.pages.AbstractPage',

    xtype: 'blank-page',

    items: [
        {
            xtype: 'box',
            cls: 'abstract-page-container',
            html: '<div class=\'fa-outer-class\'><span class=\'x-fa fa-clock-o\'></span></div><h1>Раздел в разработке</h1><span>Скоро всё будет готово!</span>'
        }
    ]
});
