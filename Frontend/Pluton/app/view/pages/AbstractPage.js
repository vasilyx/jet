/**
 *
 */
Ext.define('Pluton.view.pages.AbstractPage', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.layout.container.VBox'
    ],

    cls: 'abstract-page',
    layout:{
        type:'vbox',
        pack:'center',
        align:'center'
    }
});
