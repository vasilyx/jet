/**
 *
 */
Ext.define('Pluton.view.user.card.UserAccountForm', {
    extend: 'Ext.form.Panel',

    xtype: 'user-account-form',

    requires: [
        'Ext.container.Container',
        'Ext.form.FieldContainer',
        'Ext.form.FieldSet',
        'Ext.form.field.Checkbox',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Radio',
        'Ext.form.field.Tag',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.util.Format',
        'Pluton.ux.form.BindableRadioGroup',
        'Pluton.view.user.util.User'
    ],

    title: 'Учетная запись',
    fieldDefaults: {
        labelWidth: 200
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    listeners: {
        validitychange: 'onUserAccountFormValidityChange'
    },
    defaults: {
        xtype: 'fieldset',
        style: {
            background: 'transparent'
        },
        layout: {
            type: 'vbox',
            align: 'stretch'
        }
    },

    items: [
        {
            title: 'Основные',
            items: [
                {
                    xtype: 'textfield',
                    fieldLabel: 'Имя учетной записи',
                    required: true,
                    allowBlank: false,
                    maxLength: 32,
                    bind: {
                        readOnly: '{!theUser.phantom}',
                        value: '{theUser.login}'
                    },
                    vtype: 'login'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaults: {
                        xtype: 'textfield',
                        flex: 1,
                        inputType: 'password'
                    },
                    items:[
                        {
                            reference: 'passwordField',
                            fieldLabel: 'Пароль',
                            name: 'password',
                            listeners: {
                                validitychange: 'onPasswordValidityChange',
                                blur: 'onPasswordValidityChange',
                                dirtychange: 'onPasswordDirtyChange',
                                change: 'onPasswordChange'
                            },
                            bind: {
                                value: '{theUser.password}'
                            }
                        },
                        {
                            reference: 'passwordFieldConfirm',
                            fieldLabel: 'Подтверждение пароля',
                            margin: '0 0 0 40',
                            passFieldName: 'password',
                            submitValue: false,
                            vtype: 'password'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'E-mail',
                    vtype: 'email',
                    maxLength: 255,
                    bind: {
                        value: '{theUser.email}'
                    }
                },
                {
                    xtype: 'bindable-radiogroup',
                    reference: 'expirationTimestampRadio',
                    publishes: ['value'],
                    vertical: false,
                    fieldLabel: 'Срок действия пользователя',
                    columns: 1,
                    simpleValue: true,
                    bind: '{expirationTimestampValue}',
                    items: [
                        {
                            boxLabel: 'Бессрочно',
                            inputValue: 1,
                            checked: true
                        },
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'radio',
                                    boxLabel: 'До',
                                    inputValue: 2,
                                    listeners: {
                                        focus: 'onExpTSRadioFocus'
                                    }
                                },
                                {
                                    reference: 'expirationTimestampField',
                                    xtype: 'datefield',
                                    format: Ext.Date.defaultFormat,
                                    margin: '0 0 0 10',
                                    minWidth: 180,
                                    disabled: true,
                                    listeners: {
                                        focusleave: 'onExpTSFieldFocusLeave'
                                    },
                                    bind: {
                                        disabled: '{expirationTimestampRadio.value != 2}',
                                        value: '{theUser.expiration_timestamp}'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Срок действия пароля',
                    renderer: UserUtil.expirationRenderer,
                    bind: {
                        value: '{theUser.password_expires_at}'
                    }
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Дата/время регистрации',
                    renderer: Ext.util.Format.dateRenderer(Ext.Date.defaultFormat),
                    bind: {
                        value: '{theUser.register_timestamp}'
                    }
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Впервые увиден',
                    renderer: Ext.util.Format.dateRenderer(Ext.Date.defaultFormat),
                    bind: {
                        value: '{theUser.first_login_timestamp}'
                    }
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Неудачных попыток входа',
                    bind: {
                        value: '{theUser.failures}'
                    }
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: 'Заблокирован',
                    bind: {
                        value: '{theUser.is_blocked}'
                    }
                }
            ]
        },
        {
            title: 'Принадлежность',
            defaults: {
                xtype: 'displayfield'
            },
            items: [
                {
                    fieldLabel: 'Тип регистрации',
                    renderer: Ext.labelRenderer,
                    bind: {
                        value: '{theUser.creation_type}'
                    }
                },
                {
                    fieldLabel: 'СУС-владелец',
                    bind: {
                        value: '{theUser.create_sus_name}'
                    }
                }
            ]
        },
        {
            title: 'Роли<span style="color:#D32F2F;">*</span>',
            required: true,
            items: [
                {
                    xtype: 'tagfield',
                    allowBlank: false,
                    valueField: 'id',
                    displayField: 'label',
                    queryMode: 'local',
                    anyMatch: true,
                    bind: {
                        store: '{Roles}',
                        value: '{theUser.roles}'
                    },
                    listeners: {
                        expand: 'onRolesFieldExpand'
                    }
                }
            ]
        },
        {
            title: 'Комментарий',
            items: [
                {
                    xtype: 'textarea',
                    bind: {
                        value: '{theUser.description}'
                    }
                }
            ]
        }
    ]
});
