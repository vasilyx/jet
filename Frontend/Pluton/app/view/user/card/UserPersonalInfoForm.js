/**
 *
 */
Ext.define('Pluton.view.user.card.UserPersonalInfoForm', {
    extend: 'Ext.form.Panel',

    xtype: 'user-personal-info-form',

    requires: [
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.VBox'
    ],

    title: 'Дополнительные данные',
    bodyPadding: 10,
    fieldDefaults: {
        labelWidth: 200
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    listeners: {
        validitychange: 'onUserPersonalInfoFormValidityChange'
    },
    defaults: {
        xtype: 'textfield'
    },

    items: [
        {
            fieldLabel: 'Фамилия',
            maxLength: 50,
            bind: {
                value: '{theUser.personal_info.family_name}'
            }
        },
        {
            fieldLabel: 'Имя',
            maxLength: 50,
            bind: {
                value: '{theUser.personal_info.first_name}'
            }
        },
        {
            fieldLabel: 'Отчество',
            maxLength: 50,
            bind: {
                value: '{theUser.personal_info.patronymic_name}'
            }
        },
        {
            fieldLabel: 'Должность',
            maxLength: 100,
            bind: {
                value: '{theUser.personal_info.position}'
            }
        },
        {
            fieldLabel: 'Звание',
            maxLength: 100,
            bind: {
                value: '{theUser.personal_info.rank}'
            }
        },
        {
            fieldLabel: 'Подразделение',
            maxLength: 100,
            bind: {
                value: '{theUser.personal_info.department}'
            }
        },
        {
            xtype: 'textarea',
            fieldLabel: 'Комментарий',
            bind: {
                value: '{theUser.personal_info.description}'
            }
        }
    ]
});
