/**
 *
 */
Ext.define('Pluton.view.user.card.UserCard', {
    extend: 'Ext.tab.Panel',

    xtype: 'user-card',

    requires: [
        'Pluton.view.user.card.UserAccountForm',
        'Pluton.view.user.card.UserCardController',
        'Pluton.view.user.card.UserCardModel',
        'Pluton.view.user.card.UserPersonalInfoForm'
    ],

    controller: 'user-card',
    viewModel: {
        type: 'user-card'
    },

    bind: {
        title: '{userCardTitle}'
    },
    listeners: {
        beforeclose: 'onBeforeClose',
        load: 'onLoad'
    },
    style: {
        background: '#f2f2f2 !important',
        padding: '0 5px 5px 5px !important'
    },
    plain: true,
    bodyPadding: 10,
    defaults: {
        scrollable: true
    },

    bbar: [
        {
            ui: 'md-blue',
            disabled: true,
            bind: {
                text: '{theUser.phantom ? "Создать" : "Применить"}',
                tooltip: '{theUser.phantom ? "Создать" : "Применить"}',
                disabled: '{!isDirty || !isUserAccountValid || !isUserPersonalInfoValid}'
            },
            handler: 'onSave'
        },
        {
            text: 'Отменить',
            tooltip: 'Отменить',
            handler: 'onReset'
        }
    ],

    items: [
        {
            xtype: 'user-account-form'
        },
        {
            xtype: 'user-personal-info-form'
        }
    ]
});
