/**
 *
 */
Ext.define('Pluton.view.user.card.UserCardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.user-card',

    stores: {

    },

    data: {
        isUserAccountValid: true,
        isUserPersonalInfoValid: true
    },

    formulas: {
        userCardTitle: {
            bind: {
                bindTo: '{theUser}',
                deep: true
            },
            get: function(theUser) {
                if (theUser) {
                    if (theUser.phantom) {
                        return 'Создание пользователя';
                    } else {
                        return theUser.get('login') || '';
                    }
                } else {
                    return '';
                }
            }
        },
        isDirty: {
            bind: {
                bindTo: '{theUser}',
                deep: true
            },
            get: function(theUser) {
                if (theUser) {
                    return theUser.isDirty() || theUser.getPersonal_info().isDirty();
                }

                return false;
            }
        },
        expirationTimestampValue: {
            bind: '{theUser.expiration_timestamp}',
            get: function(timestamp) {
                if (!Ext.isEmpty(timestamp)) {
                    return 2;
                } else {
                    return 1;
                }
            },
            set: function(value) {
                if (value === 1) {
                    this.get('theUser').set('expiration_timestamp', null);
                }
            }
        }
    }
});
