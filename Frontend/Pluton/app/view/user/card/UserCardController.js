/**
 *
 */
Ext.define('Pluton.view.user.card.UserCardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.user-card',

    onBeforeClose: function () {
        var view = this.getView(),
            vModel = view.getViewModel(),
            theUser = vModel.get('theUser');

        if (!theUser.erased && vModel.get('isDirty')) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Имеются несохраненные данные, продолжить без сохранения?',
                function(msg) {
                    if (msg == 'yes') {
                        theUser.reject();
                        theUser.getPersonal_info().reject();
                        vModel.notify();
                        view.close();
                    }
                }
            );

            return false;
        }

        return true;
    },

    onLoad: function () {
        var me = this,
            view = me.getView(),
            theUser = me.getViewModel().get('theUser');

        if (theUser.phantom) {
            me.updatePasswordField(theUser.get('creation_type'), true);
        } else {
            view.setLoading();
            theUser.load({
                success: 'onUserLoadSuccess',
                callback: function () {
                    view.setLoading(false);
                },
                scope: me
            });
        }
    },

    onUserLoadSuccess: function (record) {
        this.updatePasswordField(record.get('creation_type'), false);
    },

    updatePasswordField: function (creationType, required) {
        var creationTypeId = creationType && creationType.id,
            passwordField,
            passwordFieldConfirm,
            passwordFieldLabel,
            passwordFieldConfirmLabel;

        if (creationTypeId === 'LOCAL') {
            passwordField = this.lookup('passwordField');
            passwordFieldConfirm = this.lookup('passwordFieldConfirm');
            passwordFieldLabel = 'Пароль';
            passwordFieldConfirmLabel = 'Подтверждение пароля:';
            required = !!required;

            if (required === true) {
                passwordFieldLabel += '<span style="color:#D32F2F;">*</span>';
                passwordFieldConfirmLabel += '<span style="color:#D32F2F;">*</span>';
            }

            passwordField.allowBlank = !required;
            passwordField.setFieldLabel(passwordFieldLabel);
            passwordFieldConfirm.setFieldLabel(passwordFieldConfirmLabel);
        //    passwordField.validate();
        }
    },

    onSave: function () {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            theUser = vModel.get('theUser');

        if (vModel.get('isDirty') && vModel.get('isUserAccountValid') && vModel.get('isUserPersonalInfoValid')) {
            view.setLoading();
            theUser.save({
                success: function(record) {
                    me.fireViewEvent('save', record);
                    me.updatePasswordField(record.get('creation_type'), false);
                },
                callback: function () {
                    view.setLoading(false);
                }
            });
        }
    },

    onReset: function () {
        this.getView().close();
    },

    onUserAccountFormValidityChange: function (form, valid) {
        this.getViewModel().set('isUserAccountValid', valid);
    },

    onUserPersonalInfoFormValidityChange: function (form, valid) {
        this.getViewModel().set('isUserPersonalInfoValid', valid);
    },

    onExpTSRadioFocus: function () {
        var field = this.lookup('expirationTimestampField');

        setTimeout(function () {
            field.focus()
        }, 200);
    },

    onExpTSFieldFocusLeave: function () {
        var expiration_timestamp = this.getViewModel().get('theUser.expiration_timestamp');

        if (Ext.isEmpty(expiration_timestamp)) {
            this.lookup('expirationTimestampRadio').setValue(1);
        }
    },

    onPasswordValidityChange: function (field) {
        field.next().validate();
    },

    onPasswordDirtyChange: function (field, isDirty) {
        this.lookup('passwordFieldConfirm').allowBlank = !isDirty;
    },

    onPasswordChange: function (field) {
        field.next().validate();
    },

    onRolesFieldExpand: function (field) {
        field.getStore().reload();
    }
});
