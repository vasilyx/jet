/**
 *
 */
Ext.define('Pluton.view.user.util.User', {
    alternateClassName: 'UserUtil',

    singleton: true,

    expirationRenderer: function (value, metaData) {
        var expDate,
            now,
            color;

        if (!value || value == "never") {
            return "Бессрочно";
        } else if (value == "already") {
            return "Истёк";
        } else {
            expDate = new Date(value);
            now = new Date();

            if (now < expDate) {
                color = 'green';
            } else {
                color = 'red';
                if (arguments.length > 2) {
                    metaData.tdAttr = 'data-qtip="Срок действия истек"';
                }
            }

            return Ext.String.format('<span style="color:{0};">до {1}</span>', color, Ext.Date.format(expDate, Ext.Date.defaultFormat));
        }
    }
});
