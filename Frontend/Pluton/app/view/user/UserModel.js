/**
 *
 */
Ext.define('Pluton.view.user.UserModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.user',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.user.User',
        'Pluton.store.directory.UserRole',
        'Pluton.store.directory.UserCreateType'
    ],

    stores: {
        Users: {
            model: 'Pluton.model.user.User',
            pageSize: 0,
            remoteSort: true,
            remoteFilter: true,
            sorters: 'login',
            proxy: {
                type: 'json-ajax',
                url: '/user/list',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        },
        CreateTypes: {
            type: 'userCreateType'
        },
        Roles: {
            type: 'userRole'
        }
    },

    data: {
        isFiltersValid: true
    }
});
