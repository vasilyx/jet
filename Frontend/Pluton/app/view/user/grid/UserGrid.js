/**
 *
 */
Ext.define('Pluton.view.user.grid.UserGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'user-grid',

    requires: [
        'Ext.container.Container',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Tag',
        'Ext.form.field.Text',
        'Ext.grid.column.Check',
        'Ext.grid.column.Date',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.toolbar.Separator',
        'Pluton.utility.button.CSVExport',
        'Pluton.ux.container.DateRangeContainer',
        'Pluton.ux.form.FiltersFieldSet',
        'Pluton.view.user.util.User'
    ],

    reference: 'userGrid',
    bind: {
        store: '{Users}'
    },
    title: 'Пользователи',
    closable: false,
    listeners: {
        itemdblclick: 'onItemDblClick'
    },

    viewConfig: {
        getRowClass: function (record) {
            if (!Ext.isEmpty(record.get('creation_type').id) && record.get('creation_type').id !== 'LOCAL') {
                return 'disabled-row';
            } else {
                return '';
            }
        }
    },

    tbar: {
        xtype: 'filters-fieldset',
        layout: 'fit',
        margin: '10 10 0 0',
        items: [
            {
                reference: 'userGridFilters',
                xtype: 'form',
                layout: 'hbox',
                scrollable: true,
                listeners: {
                    validitychange: 'onFiltersFormValidityChange'
                },
                fieldDefaults: {
                    labelAlign: 'top'
                },
                defaults: {
                    flex: 1,
                    margin: '0 10 0 0',
                    minWidth: 300
                },
                bbar: {
                    layout: {
                        pack: 'center'
                    },
                    items: [
                        {
                            text: 'Применить',
                            tooltip: 'Применить фильтры',
                            ui: 'md-blue',
                            handler: 'onFilterApply',
                            bind: {
                                disabled: '{!isFiltersValid}'
                            }
                        },
                        {
                            text: 'Сбросить',
                            tooltip: 'Сбросить фильтры',
                            handler: 'onFilterReset'
                        }
                    ]
                },
                items: [
                    {
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'textfield',
                                name: 'login',
                                fieldLabel: 'Имя учетной записи'
                            },
                            {
                                xtype: 'tagfield',
                                name: 'roles_id',
                                fieldLabel: 'Роли',
                                valueField: 'id',
                                displayField: 'label',
                                queryMode: 'local',
                                forceSelection: true,
                                anyMatch: true,
                                bind: {
                                    store: '{Roles}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'textfield',
                                name: 'create_sus_name',
                                fieldLabel: 'СУС-владелец'
                            },
                            {
                                xtype: 'combo',
                                name: 'creation_type',
                                fieldLabel: 'Тип регистрации',
                                emptyText: 'Любой',
                                valueField: 'id',
                                displayField: 'label',
                                clearTrigger: true,
                                queryMode: 'local',
                                anyMatch: true,
                                forceSelection: true,
                                bind: {
                                    store: '{CreateTypes}'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'date-range-container',
                                name: 'register_timestamp',
                                fromFieldConfig: {
                                    fieldLabel: 'Зарегистрирован'
                                }
                            },
                            {
                                xtype: 'date-range-container',
                                name: 'expiration_timestamp',
                                margin: '2 0',
                                fromFieldConfig: {
                                    fieldLabel: 'Истекает'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'combo',
                                name: 'is_blocked',
                                fieldLabel: 'Заблокирован',
                                emptyText: 'Любой',
                                clearTrigger: true,
                                editable: false,
                                store: [
                                    [true, 'Да'],
                                    [false, 'Нет']
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    },

    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefresh'
        },
        '-',
        {
            iconCls: 'x-fa fa-plus',
            ui: 'md-green',
            tooltip: 'Создать',
            handler: 'onCreate'
        },
        {
            iconCls: 'x-fa fa-pencil',
            ui: 'md-yellow',
            tooltip: 'Редактировать',
            disabled: true,
            bind: {
                disabled: '{!userGrid.selection}'
            },
            handler: 'onEdit'
        },
        {
            iconCls: 'x-fa fa-trash-o',
            ui: 'md-red',
            tooltip: 'Удалить',
            disabled: true,
            bind: {
                disabled: '{!userGrid.selection || userGrid.selection.creation_type.id !== "LOCAL"}'
            },
            handler: 'onRemove'
        },
        '-',
        {
            xtype: 'utility-button-csv-export'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'id',
                text: 'Идентификатор',
                hidden: true
            },
            {
                dataIndex: 'login',
                text: 'Имя учетной записи'
            },
            {
                dataIndex: 'create_sus_name',
                text: 'СУС-владелец'
            },
            {
                dataIndex: 'create_sus_id',
                text: 'GUID СУС-владелец',
                hidden: true
            },
            {
                dataIndex: 'source_sus_name',
                text: 'СУС-источник',
                hidden: true
            },
            {
                dataIndex: 'source_sus_id',
                text: 'GUID СУС-источник',
                hidden: true
            },
            {
                dataIndex: 'creation_type',
                text: 'Тип регистрации',
                renderer: Ext.labelRenderer
            },
            {
                xtype: 'checkcolumn',
                dataIndex: 'is_blocked',
                text: 'Заблокирован',
                renderer: function (value, metaData) {
                    metaData.tdCls += Ext.baseCSSPrefix + 'item-disabled';

                    return Ext.grid.column.Check.prototype.defaultRenderer.call(this, value, metaData);
                },
                listeners: {
                    beforecheckchange: function () {
                        return false;
                    }
                }
            },
            {
                dataIndex: 'roles',
                text: 'Роли',
                sortable: false,
                flex: 2,
                renderer: Ext.labelRenderer
            },
            {
                xtype: 'datecolumn',
                dataIndex: 'register_timestamp',
                text: 'Зарегистрирован',
                format: Ext.Date.defaultFormat
            },
            {
                text: 'Срок действия',
                defaults: {
                    align: 'center',
                    width: 180
                },
                columns: [
                    {
                        dataIndex: 'expiration_timestamp',
                        text: 'Пользователя',
                        useCustomTip: true,
                        renderer: UserUtil.expirationRenderer
                    },
                    {
                        dataIndex: 'password_expires_at',
                        text: 'Пароля',
                        useCustomTip: true,
                        renderer: UserUtil.expirationRenderer
                    }
                ]
            },
            {
                dataIndex: 'failures',
                text: 'Неудачных попыток<br> входа',
                align: 'center'
            }
        ]
    }
});
