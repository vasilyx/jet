/**
 *
 */
Ext.define('Pluton.view.user.UserController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.user',

    requires: [
        'Pluton.model.user.User',
        'Pluton.model.user.UserPersonalInfo',
        'Pluton.view.user.card.UserCard',
        'Pluton.view.user.view.UserView',
    ],

    control: {
        'user-card': {
            save: 'onSave'
        }
    },
    onLoad: function () {
        if (document.location.href.split('?').length < 2) return;
        var params = Ext.Object.fromQueryString(document.location.href.split('?')[1]);
        if (params && params.id && this.getStore('Users').getData().count()) {
            var record = this.getStore('Users').getById(params.id);
            if (record) {
                this.showCard(record);
            } else {
                Ext.Msg.alert('Ошибка', 'Невозможно открыть компонент с таким id - ' + params.id)
            }
        }
    },
    onActivate: function () {
        var me = this;
        this.getStore('Users').load({
            callback: function (records) {
                me.onLoad();
            }
        });
    },

    onRefresh: function () {
        this.getStore('Users').load();
    },

    onFilterApply: function () {
        var store = this.getStore('Users'),
            values = this.lookup('userGridFilters').getValues(),
            filtersConfig = [],
            operator;

        Ext.Object.each(values, function(key, value) {
            if (!Ext.isEmpty(value)) {
                switch (key) {
                    case 'login':
                    case 'create_sus_name':
                        operator = 'like';
                        break;
                    case 'creation_type':
                    case 'is_blocked':
                        operator = '=';
                        break;
                    case 'roles_id':
                        operator = '&&';
                        break;
                    case 'register_timestamp':
                    case 'expiration_timestamp':
                        operator = 'between';
                        break;
                    default:
                        operator = null;
                        break;
                }

                if (operator) {
                    filtersConfig.push({
                        property: key,
                        operator: operator,
                        value: value
                    });
                }
            } else {
                store.removeFilter(key, true);
            }
        }, this);

        store.addFilter(filtersConfig, true);
        store.load();
    },

    onFilterReset: function () {
        this.lookup('userGridFilters').reset();
        this.getStore('Users').clearFilter();
    },

    onFiltersFormValidityChange: function (form, valid) {
        this.getViewModel().set('isFiltersValid', valid);
    },

    onItemDblClick: function(grid, record) {
            this.showCard(record);
    },

    onCreate: function () {
        var vModel = this.getViewModel(),
            newUser = Ext.create('Pluton.model.user.User', {
                creation_type: {
                    id: 'LOCAL',
                    label: 'Локальный'
                },
                create_sus_id: vModel.get('current_sus_id'),
                create_sus_name: vModel.get('current_sus_name')
            });

        newUser.setPersonal_info(Ext.create('Pluton.model.user.UserPersonalInfo'));

        this.showCard(newUser);
    },

    onSave: function (tab, record) {
        var view = this.getView(),
            store = this.getStore('Users'),
            itemId = this.getTabItemId(record.getId());

        view.items.updateKey(tab.itemId, itemId);
        tab.itemId = itemId;

        if (store.findBy(function(r, id) {return id === record.getId();}) === -1) {
            store.insert(0, record);
        }
    },

    onEdit: function () {
        var selection = this.getViewModel().get('userGrid.selection');

        if (selection) {
            this.showCard(selection);
        }
    },

    onRemove: function () {
        var me = this,
            store = me.getStore('Users'),
            selection = me.getViewModel().get('userGrid.selection');

        if (selection) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Вы уверены, что хотите удалить запись?',
                function(msg) {
                    if (msg == 'yes') {
                        selection.erase({
                            success: function() {
                                var tab = me.getView().items.getByKey(me.getTabItemId(selection.getId()));

                                if (tab) {
                                    tab.close();
                                }
                            },
                            failure: function() {
                                store.load();
                            }
                        });
                    }
                }
            );
        }
    },

    showCard: function (record) {
        var view = this.getView(),
            itemId = this.getTabItemId(record.getId()),
            tab = view.items.getByKey(itemId),
            xtype = 'user-view',
            creationType;

        if (tab) {
            view.setActiveTab(tab);
        } else {
            creationType = record.get('creation_type');
            if (creationType && creationType.id === 'LOCAL') {
                xtype = 'user-card';
            }

            tab = view.add({
                xtype: xtype,
                itemId: itemId,
                viewModel: {
                    parent: this.getViewModel(),
                    data: {
                        theUser: record
                    }
                }
            });

            view.setActiveTab(tab);
            tab.fireEvent('load');
        }
    },

    getTabItemId: function (id) {
        return 'user_' + id;
    }
});
