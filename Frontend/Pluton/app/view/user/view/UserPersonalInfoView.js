/**
 *
 */
Ext.define('Pluton.view.user.view.UserPersonalInfoView', {
    extend: 'Ext.form.Panel',

    xtype: 'user-personal-info-view',

    requires: [
        'Ext.form.field.Display',
        'Ext.layout.container.VBox'
    ],

    title: 'Дополнительные данные',
    bodyPadding: 10,
    fieldDefaults: {
        labelWidth: 200
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        xtype: 'displayfield'
    },

    items: [
        {
            fieldLabel: 'Фамилия',
            bind: {
                value: '{theUser.personal_info.family_name}'
            }
        },
        {
            fieldLabel: 'Имя',
            bind: {
                value: '{theUser.personal_info.first_name}'
            }
        },
        {
            fieldLabel: 'Отчество',
            bind: {
                value: '{theUser.personal_info.patronymic_name}'
            }
        },
        {
            fieldLabel: 'Должность',
            bind: {
                value: '{theUser.personal_info.position}'
            }
        },
        {
            fieldLabel: 'Звание',
            bind: {
                value: '{theUser.personal_info.rank}'
            }
        },
        {
            fieldLabel: 'Подразделение',
            bind: {
                value: '{theUser.personal_info.department}'
            }
        },
        {
            fieldLabel: 'Комментарий',
            bind: {
                value: '{theUser.personal_info.description}'
            }
        }
    ]
});
