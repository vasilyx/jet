/**
 *
 */
Ext.define('Pluton.view.user.view.UserAccountView', {
    extend: 'Ext.form.Panel',

    xtype: 'user-account-view',

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Display',
        'Ext.form.field.Tag',
        'Ext.layout.container.VBox',
        'Ext.util.Format',
        'Pluton.view.user.util.User'
    ],

    title: 'Учетная запись',
    fieldDefaults: {
        labelWidth: 200
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        xtype: 'fieldset',
        style: {
            background: 'transparent'
        },
        layout: {
            type: 'vbox',
            align: 'stretch'
        }
    },

    items: [
        {
            title: 'Основные',
            defaults: {
                xtype: 'displayfield'
            },
            items: [
                {
                    fieldLabel: 'Имя учетной записи',
                    bind: {
                        value: '{theUser.login}'
                    }
                },
                {
                    fieldLabel: 'E-mail',
                    bind: {
                        value: '{theUser.email}'
                    }
                },
                {
                    fieldLabel: 'Срок действия пользователя',
                    renderer: UserUtil.expirationRenderer,
                    bind: {
                        value: '{theUser.expiration_timestamp}'
                    }
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Дата/время регистрации',
                    renderer: Ext.util.Format.dateRenderer(Ext.Date.defaultFormat),
                    bind: {
                        value: '{theUser.register_timestamp}'
                    }
                },
                {
                    fieldLabel: 'Впервые увиден',
                    renderer: Ext.util.Format.dateRenderer(Ext.Date.defaultFormat),
                    bind: {
                        value: '{theUser.first_login_timestamp}'
                    }
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Неудачных попыток входа',
                    bind: {
                        value: '{theUser.failures}'
                    }
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: 'Заблокирован',
                    readOnly: true,
                    bind: {
                        value: '{theUser.is_blocked}'
                    }
                }
            ]
        },
        {
            title: 'Принадлежность',
            defaults: {
                xtype: 'displayfield'
            },
            items: [
                {
                    fieldLabel: 'Тип регистрации',
                    renderer: Ext.labelRenderer,
                    bind: {
                        value: '{theUser.creation_type}'
                    }
                },
                {
                    fieldLabel: 'СУС-владелец',
                    bind: {
                        value: '{theUser.create_sus_name}'
                    }
                }
            ]
        },
        {
            title: 'Роли',
            required: true,
            items: [
                {
                    xtype: 'tagfield',
                    allowBlank: false,
                    valueField: 'id',
                    displayField: 'label',
                    queryMode: 'local',
                    anyMatch: true,
                    readOnly: true,
                    bind: {
                        store: '{Roles}',
                        value: '{theUser.roles}'
                    }
                }
            ]
        },
        {
            title: 'Комментарий',
            items: [
                {
                    xtype: 'displayfield',
                    bind: {
                        value: '{theUser.description}'
                    }
                }
            ]
        }
    ]
});
