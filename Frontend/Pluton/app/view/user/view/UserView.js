/**
 *
 */
Ext.define('Pluton.view.user.view.UserView', {
    extend: 'Ext.tab.Panel',

    xtype: 'user-view',

    requires: [
        'Pluton.view.user.card.UserCardController',
        'Pluton.view.user.card.UserCardModel',
        'Pluton.view.user.view.UserAccountView',
        'Pluton.view.user.view.UserPersonalInfoView'
    ],

    controller: 'user-card',
    viewModel: {
        type: 'user-card'
    },

    bind: {
        title: '{userCardTitle}'
    },
    listeners: {
        beforeclose: 'onBeforeClose',
        load: 'onLoad'
    },
    style: {
        background: '#f2f2f2 !important',
        padding: '0 5px 5px 5px !important'
    },

    plain: true,
    bodyPadding: 10,
    defaults: {
        scrollable: true
    },

    bbar: [
        {
            text: 'Отменить',
            tooltip: 'Отменить',
            handler: 'onReset'
        }
    ],

    items: [
        {
            xtype: 'user-account-view'
        },
        {
            xtype: 'user-personal-info-view'
        }
    ]
});
