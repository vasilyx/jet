/**
 *
 */
Ext.define('Pluton.view.user.User', {
    extend: 'Ext.tab.Panel',

    permissions: 'settings-user_page',

    xtype: 'settings.user',

    requires: [
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.user.UserController',
        'Pluton.view.user.UserModel',
        'Pluton.view.user.grid.UserGrid'
    ],

    controller: 'user',
    viewModel: {
        type: 'user'
    },

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onActivate'
    },
    defaults: {
        closable: true
    },

    items: [
        {
            xtype: 'user-grid',
            closable: false
        }
    ]
});
