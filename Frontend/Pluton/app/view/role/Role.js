/**
 *
 */
Ext.define('Pluton.view.role.Role', {
    extend: 'Ext.tab.Panel',

    permissions: 'settings-role_page',

    xtype: 'settings.role',

    requires: [
        'Pluton.ux.plugin.HelpTool',
        'Pluton.view.role.RoleController',
        'Pluton.view.role.RoleModel',
        'Pluton.view.role.grid.RoleGrid'
    ],

    controller: 'role',
    viewModel: {
        type: 'role'
    },

    plugins: {
        ptype: 'help-tool'
    },
    listeners: {
        activate: 'onActivate'
    },
    defaults: {
        closable: true
    },

    items: [
        {
            title: 'Роли',
            xtype: 'role-grid',
            closable: false
        }
    ]
});
