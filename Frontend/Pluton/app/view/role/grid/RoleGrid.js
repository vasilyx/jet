/**
 *
 */
Ext.define('Pluton.view.role.grid.RoleGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'role-grid',

    requires: [
        'Ext.grid.Panel',
        'Ext.grid.column.Date',
        'Ext.toolbar.Separator',
        'Pluton.utility.button.CSVExport'
    ],

    reference: 'roleGrid',
    bind: {
        store: '{RoleList}'
    },
    listeners: {
        itemdblclick: 'onItemDblClick',
        select: 'onSelect'
    },
    viewConfig: {
        getRowClass: function (record) {
            if (!Ext.isEmpty(record.get('creation_type').id) && record.get('creation_type').id !== 'LOCAL') {
                return 'disabled-row';
            } else {
                return '';
            }
        }
    },
    lbar: [
        {
            iconCls: 'x-fa fa-refresh',
            tooltip: 'Обновить',
            handler: 'onRefresh'
        },
        '-',
        {
            iconCls: 'x-fa fa-plus',
            ui: 'md-green',
            tooltip: 'Создать',
            handler: 'onCreate'
        },
        {
            iconCls: 'x-fa fa-pencil',
            ui: 'md-yellow',
            tooltip: 'Редактировать',
            disabled: true,
            reference: 'editButton',
            handler: 'onEdit'
        },
        {
            iconCls: 'x-fa fa-trash-o',
            ui: 'md-red',
            tooltip: 'Удалить',
            disabled: true,
            reference: 'removeButton',
            handler: 'onRemove'
        },
        '-',
        {
            xtype: 'utility-button-csv-export'
        }
    ],

    columns: {
        defaults: {
            flex: 1
        },
        items: [
            {
                dataIndex: 'full_name',
                text: 'Роль',
                renderer: function (value, metaData, record) {
                    var abbr = record.get('short_name');
                    if (abbr) {
                        return value + ' (' + abbr + ')';
                    } else {
                        return value;
                    }
                }
            },
            {
                dataIndex: 'id',
                text: 'GUID роли',
                hidden: true
            },
            {
                dataIndex: 'create_sus_name',
                text: 'СУС-владелец'
            },
            {
                dataIndex: 'create_sus_id',
                text: 'GUID СУС-владелец',
                hidden: true
            },
            {
                dataIndex: 'source_sus_name',
                text: 'СУС-источник',
                hidden: true
            },
            {
                dataIndex: 'source_sus_id',
                text: 'GUID СУС-источник',
                hidden: true
            },
            {
                dataIndex: 'creation_type',
                text: 'Тип регистрации',
                renderer: Ext.labelRenderer
            },
            {
                xtype: 'datecolumn',
                dataIndex: 'create_timestamp',
                text: 'Создана',
                format: Ext.Date.defaultFormat
            },
            {
                xtype: 'datecolumn',
                dataIndex: 'update_timestamp',
                text: 'Изменена',
                format: Ext.Date.defaultFormat
            },
            {
                dataIndex: 'description',
                text: 'Комментарий'
            }
        ]
    }
});
