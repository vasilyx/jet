/**
 *
 */
Ext.define('Pluton.view.role.card.RoleCardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.role-card',

    formulas: {
        roleCardTitle: {
            bind: {
                bindTo: '{theRole}',
                deep: true
            },
            get: function (theRole) {
                if (theRole) {
                    if (theRole.phantom) {
                        return 'Создание роли';
                    } else {
                        return theRole.get('full_name') || '';
                    }
                } else {
                    return '';
                }
            }
        }
    }
});
