/**
 *
 */
Ext.define('Pluton.view.role.card.RoleCardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.role-card',

    onBeforeClose: function() {
        var view = this.getView(),
            theRole = view.getViewModel().get('theRole');

        if (!theRole.erased && theRole.isDirty()) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Имеются несохраненные данные, продолжить без сохранения?',
                function(msg) {
                    if (msg == 'yes') {
                        theRole.reject();
                        view.close();
                    }
                }
            );

            return false;
        }

        return true;
    },

    onLoad: function() {
        var view = this.getView(),
            theRole = this.getViewModel().get('theRole');

        if (theRole.phantom !== true) {
            view.setLoading();
            theRole.load({
                callback: function() {
                    view.setLoading(false);
                }
            })
        }
    },

    onChangePrivilege: function( da, newValue, oldValue, eOpts )  {
        var theRole = this.getViewModel().get('theRole');

        if (theRole && theRole.getId() == -1) {
            theRole.set('privileges', newValue); // hack for itemselector, onRemove - itemselector doesn't update binded data
        }
    },

    onSave: function() {
        var me = this,
            view = me.getView(),
            vModel = view.getViewModel(),
            theRole = vModel.get('theRole');

        if (theRole.isDirty() && theRole.isValid()) {
            view.setLoading();
            theRole.save({
                success: function(record) {
                    me.fireViewEvent('save', record);
                    record.load({
                        callback: function() {
                            view.setLoading(false);
                        }
                    });
                },
                failure: function () {
                    view.setLoading(false);
                }
            });
        }
    },

    onReset: function() {
        this.getView().close();
    }
});
