/**
 *
 */
Ext.define('Pluton.view.role.card.RoleCard', {
    extend: 'Ext.panel.Panel',

    xtype: 'role-card',

    requires: [
        'Ext.container.Container',
        'Ext.form.FieldSet',
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ext.layout.container.Form',
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Ext.ux.form.ItemSelector',
        'Pluton.view.role.card.RoleCardController',
        'Pluton.view.role.card.RoleCardModel'
    ],

    bind: {
        title: '{roleCardTitle}'
    },

    listeners: {
        beforeclose: 'onBeforeClose',
        load: 'onLoad'
    },

    controller: 'role-card',
    viewModel: {
        type: 'role-card'
    },

    bodyPadding: 10,
    scrollable: true,
    modelValidation: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        style: {
            background: 'transparent'
        }
    },

    bbar: [
        {
            text: 'Применить',
            tooltip: 'Применить',
            ui: 'md-blue',
            disabled: true,
            bind: {
                disabled: '{!theRole.dirty || !theRole.valid}'
            },
            handler: 'onSave'
        },
        {
            text: 'Отменить',
            tooltip: 'Отменить',
            handler: 'onReset'
        }
    ],


    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            defaults: {
                style: {
                    background: 'transparent'
                }
            },
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Основные',
                    flex: 1,
                    layout: 'form',
                    margin: '0 15 0 0',
                    defaults: {
                        xtype: 'textfield'
                    },
                    items: [
                        {
                            fieldLabel: 'Полное название роли',
                            required: true,
                            bind: {
                                value: '{theRole.full_name}'
                            }
                        },
                        {
                            fieldLabel: 'Аббревиатура роли',
                            required: true,
                            maxLength: 10,
                            enforceMaxLength: true,
                            bind: {
                                value: '{theRole.short_name}'
                            }
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Дата/время создания',
                            format: 'd.m.Y H:i:s',
                            createPicker: Ext.emptyFn,
                            onTriggerClick: Ext.emptyFn,
                            editable: false,
                            bind: {
                                value: '{theRole.create_timestamp}'
                            }
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Дата/время обновления',
                            format: 'd.m.Y H:i:s',
                            createPicker: Ext.emptyFn,
                            onTriggerClick: Ext.emptyFn,
                            editable: false,
                            bind: {
                                value: '{theRole.update_timestamp}'
                            }
                        }

                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Принадлежность',
                    flex: 1,
                    layout: 'form',
                    defaults: {
                        xtype: 'displayfield',
                        editable: false
                    },
                    items: [
                        {
                            fieldLabel: 'Тип регистрации',
                            bind: {
                                value: '{theRole.creation_type}'
                            },
                            renderer: Ext.labelRenderer
                        },
                        {
                            fieldLabel: 'СУС-владелец',
                            bind: {
                                value: '{theRole.create_sus_name}'
                            }
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Полномочия',
            layout: 'hbox',
            items: [
                {
                    xtype: 'itemselector',
                    store: {},
                    bind: {
                        store: '{PrivilegeCatalog}',
                        value: '{theRole.privileges}'
                    },
                    listeners: {
                        change: 'onChangePrivilege'
                    },
                    displayField: 'label',
                    valueField: 'id',
                    height: 250,
                    flex: 1,
                    allowBlank: false,
                    scrollable: true,
                    msgTarget: 'side',
                    fromTitle: 'Доступные',
                    toTitle: 'Выбранные <span style="color:#D32F2F;">*</span>',
                    blankText: 'Это поле обязательно для заполнения',
                    buttons: ['addall', 'add', 'remove', 'removeall'],
                    buttonsText: {
                        addall: 'Выбрать и добавить все полномочия',
                        add: 'Добавить выбранное полномочие',
                        remove: 'Удалить выбранное полномочие',
                        removeall: 'Выбрать и удалить все полномочия'
                    }
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Комментарий',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textarea',
                    bind: {
                        value: '{theRole.description}'
                    }
                }
            ]
        }
    ]
});
