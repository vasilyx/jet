/**
 *
 */
Ext.define('Pluton.view.role.RoleModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.role',

    requires: [
        'Ext.data.reader.Json',
        'Pluton.data.proxy.JsonAjax',
        'Pluton.model.privilege.PrivilegeCatalog',
        'Pluton.model.role.Role'
    ],

    stores: {
        RoleList: {
            model: 'Pluton.model.role.Role',
            pageSize: 0,
            remoteSort: true,
            remoteFilter: true,
            sorters: 'full_name',
            proxy: {
                type: 'json-ajax',
                url: '/role/list',
                reader: {
                    rootProperty: 'rows'
                }
            }
        },
        PrivilegeCatalog: {
            model: 'Pluton.model.privilege.PrivilegeCatalog',
            sorters: 'label',
            pageSize: 0,
            autoLoad: true,
            proxy: {
                type: 'json-ajax',
                url: '/privilege/catalog',
                reader: {
                    type: 'json',
                    rootProperty: 'rows'
                }
            }
        }
    },

    data: {
        
    }
});
