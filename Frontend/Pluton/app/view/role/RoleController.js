/**
 *
 */
Ext.define('Pluton.view.role.RoleController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.role',

    requires: [
        'Pluton.model.role.Role',
        'Pluton.view.role.card.RoleCard'
    ],

    control: {
        'role-card': {
            save: 'onSave'
        }
    },

    onActivate: function() {
        this.getStore('RoleList').load();
    },

    onRefresh: function() {
        this.getStore('RoleList').load();
    },

    onItemDblClick: function(grid, record) {
        if (record.get('creation_type').id &&  record.get('creation_type').id === 'LOCAL') {
            this.showCard(record);
        } else {
            return false;
        }
    },

    onCreate: function() {
        var vModel = this.getViewModel(),
            newRole = Ext.create('Pluton.model.role.Role', {
                creation_type: {
                    id: 'LOCAL',
                    label: 'Локальный'
                },
                create_sus_id: vModel.get('current_sus_id'),
                create_sus_name: vModel.get('current_sus_name')
            });

        this.showCard(newRole);
    },

    onRemove: function() {
        var me = this,
            store = me.getStore('RoleList'),
            selection = me.getViewModel().get('roleGrid.selection');

        if (selection) {
            Ext.MessageBox.confirm(
                'Подтвердите действие',
                'Вы уверены, что хотите удалить запись?',
                function(msg) {
                    if (msg == 'yes') {
                        selection.erase({
                            success: function() {
                                var tab = me.getView().items.getByKey(me.getTabItemId(selection.getId()));

                                if (tab) {
                                    tab.close();
                                }
                            },
                            failure: function() {
                                store.load();
                            }
                        });
                    }
                }
            );
        }
    },

    onSave: function(tab, record) {
        var view = this.getView(),
            store = this.getStore('RoleList'),
            itemId = this.getTabItemId(record.getId());

        view.items.updateKey(tab.itemId, itemId);
        tab.itemId = itemId;

        if (store.findBy(function(r, id) {return id === record.getId();}) === -1) {
            store.insert(0, record);
        }
    },

    onEdit: function() {
        var selection = this.getViewModel().get('roleGrid.selection');

        if (selection) {
            this.showCard(selection);
        }
    },

    showCard: function(record) {
        var view = this.getView(),
            itemId = this.getTabItemId(record.getId()),
            tab = view.items.getByKey(itemId);

        if (tab) {
            view.setActiveTab(tab);
        } else {
            tab = view.add({
                xtype: 'role-card',
                itemId: itemId,
                viewModel: {
                    parent: this.getViewModel(),
                    data: {
                        theRole: record
                    }
                }
            });

            view.setActiveTab(tab);
            tab.fireEvent('load');
        }
    },

    getTabItemId: function(id) {
        return 'role_' + id;
    },

    onSelect: function(grid, record) {
        if (record.get('creation_type').id &&  record.get('creation_type').id === 'LOCAL') {
            this.lookupReference('removeButton').setDisabled(false);
            this.lookupReference('editButton').setDisabled(false);
        } else {
            this.lookupReference('removeButton').setDisabled(true);
            this.lookupReference('editButton').setDisabled(true);
        }
    }
});
