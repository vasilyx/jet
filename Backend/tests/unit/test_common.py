import unittest

from backend.utils import update_value_in_list_of_dicts, \
    change_field_name_in_sort, change_field_name_in_filter, make_list_sys, \
    build_order_by, build_where
from peewee import Model, CharField


class TestModel(Model):
    id = CharField(verbose_name='идентификатор', primary_key=True)
    description = CharField(verbose_name='описание СУС', null=True)
    host = CharField(verbose_name='хост СУС', null=True)

    class Meta:
        db_table = 'test'


class TestCommonUtils(unittest.TestCase):
    def test_update_value_by_key_in_list_of_dict(self):
        list_of_dicts_source = [{'foo': 'bar',
                                 'acme': 'woodpecker',
                                 'звезда': 'Солнце'},
                                {'Foxtrot': 'Uniform',
                                 'Charlie': 'Kilo',
                                 'Планета': 'Земля'}]
        list_of_dicts_result = [{'foo': 'bar',
                                 'acme': 'woodpecker',
                                 'звезда': 'Альфа-Центавра'},
                                {'Foxtrot': 'Uniform',
                                 'Charlie': 'Kilo',
                                 'Планета': 'Земля'}]

        test_result = update_value_in_list_of_dicts(list_of_dicts_source,
                                                    'звезда',
                                                    'Солнце',
                                                    'Альфа-Центавра')
        self.assertDictEqual(test_result[0],
                             list_of_dicts_result[0])

    def test_change_field_name_in_sort(self):
        input_sort = [{"property": "label", "direction": "ASC"},
                      {"property": "id", "direction": "DESC"}]
        output_sort = [{"property": "description", "direction": "ASC"},
                       {"property": "id", "direction": "DESC"}]

        test_result = change_field_name_in_sort(input_sort, 'label',
                                                'description')
        self.assertDictEqual(test_result[0], output_sort[0])

    def test_change_field_name_in_filter(self):
        input_filter = [{"field": "id", "type": "<", "value": "100"},
                        {"field": "label", "type": "==", "value": "xxx"}]
        output_filter = [{"field": "id", "type": "<", "value": "100"},
                         {"field": "description", "type": "==",
                          "value": "xxx"}]

        test_result = change_field_name_in_filter(input_filter, 'label',
                                                  'description')
        self.assertDictEqual(test_result[0], output_filter[0])

    def test_make_list_sys(self):
        output_sys = {'error': True,
                      'fatal_error': True,
                      'msg': 'Описание ошибки',
                      'terminate': True}
        test_result = make_list_sys(True, True, 'Описание ошибки', True)
        self.assertDictEqual(test_result, output_sys)

    def test_wrong_filter_kind(self):
        filters = [{"field": "id", "type": "XXX", "value": "100"},
                   {"field": "description", "type": "==", "value": "xxx"}]
        with self.assertRaises(ValueError):
            build_where(TestModel.select().get_query_meta()[0], filters)

    def test_wrong_filter_column_name(self):
        filters = [{"field": "id", "type": "<", "value": "100"},
                   {"field": "label", "type": "==", "value": "xxx"}]
        with self.assertRaises(ValueError):
            build_where(TestModel.select().get_query_meta()[0], filters)

    def test_wrong_sort_kind(self):
        sorts = [{"property": "description", "direction": "XXX"},
                 {"property": "id", "direction": "DESC"}]
        with self.assertRaises(ValueError):
            build_order_by(TestModel.select().get_query_meta()[0], sorts)

    def test_wrong_sort_column_name(self):
        sorts = [{"property": "label", "direction": "ASC"},
                 {"property": "id", "direction": "DESC"}]
        with self.assertRaises(ValueError):
            build_order_by(TestModel.select().get_query_meta()[0], sorts)

    def test_good_filter(self):
        filters = [{"field": "id", "type": "<", "value": "100"}]
        good_expression = True
        good_expression = good_expression & (TestModel.id < 100)
        test_result = build_where(
            TestModel.select().get_query_meta()[0], filters)
        self.assertEqual(TestModel.select().where(good_expression).sql(),
                         TestModel.select().where(test_result).sql())

    def test_good_order(self):
        sorts = [{"property": "id", "direction": "ASC"},
                 {"property": "description", "direction": "DESC"}]
        good_sort = [getattr(TestModel, 'id'),
                     getattr(TestModel, 'description').desc()]
        test_result = build_order_by(
            TestModel.select().get_query_meta()[0], sorts)
        self.assertEqual(TestModel.select().order_by(good_sort).sql(),
                         TestModel.select().order_by(test_result).sql())

    def test_good_build_where_list(self):
        query_fields = [TestModel.description.alias('alert_type_descr'),
                        TestModel.id]
        filters = [{"field": "id", "type": "<", "value": "100"}]
        good_sql = ('SELECT "t1"."id", "t1"."description", "t1"."host"'
                    ' FROM "test" AS t1 WHERE (? AND'
                    ' ("t1"."id" < ?))', [True, '100'])
        test_result = build_where(query_fields, filters)
        test_sql = TestModel.select().where(test_result).sql()
        print('test_sq', test_sql)
        print('good_sq', good_sql)
        self.assertTupleEqual(test_sql, good_sql)

    def test_wrong_field_build_where_list(self):
        query_fields = [TestModel.description.alias('alert_type_descr'),
                        TestModel.id]

        filters = [{"field": "XXX", "type": "<", "value": "100"}]
        with self.assertRaises(ValueError):
            build_where(query_fields, filters)
