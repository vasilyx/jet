import json

from backend.orm.models import Host, Os, Sensor, HostOs, Ip
from backend.rest.application import api as application
from falcon import testing
from peewee import SqliteDatabase
from playhouse.test_utils import test_database

test_db = SqliteDatabase(':memory:')


class MyTestCase(testing.TestCase):
    def setUp(self):
        super(MyTestCase, self).setUp()
        self.app = application
        self.maxDiff = None


class SusResourceTest(MyTestCase):
    def create_test_data(self):
        Host.create(
            id=1, hostname='ya.ru', status='ok', type='sometype', sensor='3a')
        Host.create(
            id=2, hostname='ya.ru', status='err', type='sometype', sensor='3b')
        Host.create(
            id=3, hostname='ya.ru', status='ok', type='sometype', sensor='3c')
        Os.create(id=1, name='Windows95')
        HostOs.create(host=1, os=1)
        HostOs.create(host=2, os=1)
        HostOs.create(host=3, os=1)
        Sensor.create(id='3a', sensor_name='Sensor1', status='ACTIVATED')
        Sensor.create(id='3b', sensor_name='Sensor2', status='ERROR')
        Sensor.create(id='3c', sensor_name='Sensor3', status='ACTIVATED')
        Ip.create(id=1, ip='192.1.1.1', host=1)
        Ip.create(id=2, ip='192.1.1.2', host=2)
        Ip.create(id=3, ip='192.1.1.3', host=3)

    def test_get_host_list_without_filters(self):
        with test_database(test_db, (Host, Os, HostOs, Sensor, Ip)):
            self.create_test_data()

            request_json = {
                "page": 1,
                "page_size": 25,
                "filters": [],
                "sort": []
            }
            response_json = {
               'total': 3,
               'rows': [
                  {
                     'ip': '192.1.1.1',
                     'id': 1,
                     'type': 'sometype',
                     'hostname': 'ya.ru',
                     'host_status': 'ok',
                     'sensor_status': 'ACTIVATED',
                     'os': 'Windows95'
                  },
                  {
                     'ip': '192.1.1.2',
                     'id': 2,
                     'type': 'sometype',
                     'hostname': 'ya.ru',
                     'host_status': 'err',
                     'sensor_status': 'ERROR',
                     'os': 'Windows95'
                  },
                  {
                     'ip': '192.1.1.3',
                     'id': 3,
                     'type': 'sometype',
                     'hostname': 'ya.ru',
                     'host_status': 'ok',
                     'sensor_status': 'ACTIVATED',
                     'os': 'Windows95'
                  }
               ]
            }

            result = self.simulate_post('/host/list',
                                        body=json.dumps(request_json))
            self.assertEqual(result.json, response_json)

    def test_get_host_list_with_filters_and_order(self):
        with test_database(test_db, (Host, Os, HostOs, Sensor, Ip)):
            self.create_test_data()

            request_json = {
                "page": 1,
                "page_size": 25,
                "filters": [
                    {
                        "field": "id",
                        "type": ">",
                        "value": 0,
                    }, {
                        "field": "sensor_status",
                        "type": "=",
                        "value": "ACTIVATED"
                    }
                ],
                "sort": [{
                    "property": "id",
                    "direction": "asc"
                }]
            }
            response_json = {
               'rows': [
                  {
                     'hostname': 'ya.ru',
                     'host_status': 'ok',
                     'os': 'Windows95',
                     'ip': '192.1.1.1',
                     'sensor_status': 'ACTIVATED',
                     'type': 'sometype',
                     'id': 1
                  },
                  {
                     'hostname': 'ya.ru',
                     'host_status': 'ok',
                     'os': 'Windows95',
                     'ip': '192.1.1.3',
                     'sensor_status': 'ACTIVATED',
                     'type': 'sometype',
                     'id': 3
                  }
               ],
               'total': 2
            }

            result = self.simulate_post('/host/list',
                                        body=json.dumps(request_json))
            print(result.json)

            self.assertEqual(result.json, response_json)
