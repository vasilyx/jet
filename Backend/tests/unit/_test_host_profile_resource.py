import json

from backend.rest.application import api as application
from backend.orm.models.models import (
    Host, Os, Sensor, HostOs, Ip, Location, Country, Profile
)
from falcon import testing
from peewee import SqliteDatabase
from playhouse.test_utils import test_database

test_db = SqliteDatabase(':memory:')


class MyTestCase(testing.TestCase):
    def setUp(self):
        super(MyTestCase, self).setUp()
        self.app = application
        self.maxDiff = None


class SusResourceTest(MyTestCase):
    def create_test_data(self):
        Host.create(id=1,
                    hostname='ya.ru',
                    status='ok',
                    type='sometype',
                    sensor='3a')
        Host.create(id=2,
                    hostname='ya.ru',
                    status='err',
                    type='sometype',
                    sensor='3b',
                    location=10,
                    profile=11)
        Host.create(id=3,
                    hostname='ya.ru',
                    status='ok',
                    type='sometype',
                    sensor='3c')
        Os.create(id=1, name='Windows95')
        HostOs.create(host=1, os=1)
        HostOs.create(host=2, os=1)
        HostOs.create(host=3, os=1)
        Sensor.create(id='3a', sensor_name='Sensor1', status='ACTIVATED')
        Sensor.create(id='3b', sensor_name='Sensor2', status='ERROR')
        Sensor.create(id='3c', sensor_name='Sensor3', status='ACTIVATED')
        Ip.create(id=1, ip='192.1.1.1', host=1)
        Ip.create(id=2, ip='192.1.1.2', host=2)
        Ip.create(id=3, ip='192.1.1.3', host=3)
        Country.create(id=3,
                       latin_name='Russian Federation',
                       russian_name='Российская Федерация',
                       iso_code='RU')
        Location.create(id=10,
                        longitude=4.32323,
                        latitude=3.12345,
                        country=3,
                        region='Москва',
                        subject='ВЧ2323')
        Profile.create(id=11,
                       name='profile_name',
                       status='Обнаружение отклонений',
                       version='1.2.4')

    def test_get_host_list_without_filters(self):
        with test_database(test_db, (Host,
                                     Os,
                                     HostOs,
                                     Sensor,
                                     Ip,
                                     Location,
                                     Country,
                                     Profile)):
            self.create_test_data()

            request_json = {
                "id": 2
            }
            response_json = {
                'alerts': 0,
                'connections': 0,
                'country_en_name': 'Russian Federation',
                'country_iso': 'RU',
                'country_ru_name': 'Российская Федерация',
                'description': None,
                'events': 0,
                'host_ip': '192.1.1.2',
                'host_mac': 'TODO MAC-адрес',
                'host_network': 'TODO Cеть',
                'host_os': 'Windows95',
                'host_source': 'TODO Источник',
                'host_type': 'sometype',
                'hostname': 'ya.ru',
                'id': 2,
                'intensity': 0,
                'is_host_local': 'TODO Внутренняя сеть',
                'last_activity': 0,
                'location_latitude': 3.12345,
                'location_longitude': 4.32323,
                'location_region': 'Москва',
                'location_subject': 'ВЧ2323',
                'profile_name': 'profile_name',
                'profile_status': 'Обнаружение отклонений',
                'profile_version': '1.2.4',
                'sensor_name': 'Sensor2',
                'services': 0,
                'vulnerabilities': 0
            }

            result = self.simulate_post('/host/profile',
                                        body=json.dumps(request_json))

            self.assertEqual(result.json, response_json)
