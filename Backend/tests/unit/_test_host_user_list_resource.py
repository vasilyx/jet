import json

from backend.orm.models import HostUsers, Users
from backend.rest.application import api as application
from falcon import testing
from peewee import SqliteDatabase
from playhouse.test_utils import test_database

test_db = SqliteDatabase(':memory:')


class MyTestCase(testing.TestCase):
    def setUp(self):
        super(MyTestCase, self).setUp()
        self.app = application
        self.maxDiff = None


class SusResourceTest(MyTestCase):
    def create_test_data(self):
        HostUsers.create(host=1, user=1)
        HostUsers.create(host=1, user=2)
        Users.create(id=1,
                     login='testuser',
                     first_name='Ivan',
                     last_name='Ivanov',
                     middle_name='Ivanovich',
                     position="Officer",
                     email="some@email.com",
                     phone="88005553535")
        Users.create(id=2,
                     login='testuser2',
                     first_name='Григорий',
                     last_name='Лепс',
                     middle_name='Викторович',
                     position="Major",
                     email="some2@email.com",
                     phone="+75678551234")

    def test_get_host_users(self):
        with test_database(test_db, (HostUsers, Users)):
            self.create_test_data()

            request_json = {
                "id": 1
            }
            response_json = {
                'rows': [
                    {
                        'email': 'some@email.com',
                        'first_name': 'Ivan',
                        'id': 1,
                        'last_name': 'Ivanov',
                        'middle_name': 'Ivanovich',
                        'login': 'testuser',
                        'phone': '88005553535',
                        'position': 'Officer'
                    },
                    {
                        'email': 'some2@email.com',
                        'first_name': 'Григорий',
                        'id': 2,
                        'last_name': 'Лепс',
                        'middle_name': 'Викторович',
                        'login': 'testuser2',
                        'phone': '+75678551234',
                        'position': 'Major'
                    }
                ],
                'total': 2
            }

            result = self.simulate_post('/host/profile/users',
                                        body=json.dumps(request_json))

            self.assertEqual(result.json, response_json)
