import os
import unittest

from backend.utils import Configuration

TEST_INI = 'tests/tmp/test_scs_web_ui.ini'


class TestConfiguration(unittest.TestCase):
    def setUp(self):
        config = Configuration(TEST_INI)
        config.set_scs_database_hostname('localhost')
        config.set_scs_database_name('foo')
        config.set_scs_database_port(5432)
        config.write_configuration()
        try:
            config.write_configuration()
        except Exception:
            self.fail('Setup test failed!')

    def test_good_write_configfile(self):
        config = Configuration(TEST_INI)
        config.set_scs_database_hostname('localhost')
        config.set_scs_database_name('foo')
        config.set_scs_database_port(5432)
        config.write_configuration()
        try:
            config.write_configuration()
        except Exception:
            self.fail('Write configuration test failed!')

    def test_good_set_scs_database_hostname(self):
        config = Configuration(TEST_INI)
        result, message = config.set_scs_database_hostname('trak.spb.ru')
        self.assertTrue(result)

    # def test_good_set_scs_database_hostnameXX(self):
    #     config = Configuration(TEST_INI)
    #     result, message = config.set_scs_database_hostname(None)
    #     self.assertTrue(result)

    def test_bad_set_scs_database_hostname_type(self):
        config = Configuration(TEST_INI)
        result, message = config.set_scs_database_hostname(1231)
        self.assertFalse(result)

    def test_bad_set_scs_database_hostname(self):
        config = Configuration(TEST_INI)
        result, message = config.set_scs_database_hostname('idiotic string')
        self.assertFalse(result)

    def test_bad_set_scs_database_long_hostname(self):
        config = Configuration(TEST_INI)
        result, message = config.set_scs_database_hostname('x'.ljust(512, 'x'))
        self.assertFalse(result)

    def test_good_set_scs_database_port(self):
        config = Configuration(TEST_INI)
        result, message = config.set_scs_database_port('1234')
        self.assertTrue(result)

    def test_bad_set_scs_database_port(self):
        config = Configuration(TEST_INI)
        result, message = config.set_scs_database_port('1234 sdsd')
        self.assertFalse(result)

    def test_good_set_scs_database_name(self):
        config = Configuration(TEST_INI)
        result, message = config.set_scs_database_name('trak.spb.ru')
        self.assertTrue(result)

    def test_bad_set_scs_database_name(self):
        config = Configuration(TEST_INI)
        result, message = config.set_scs_database_name(1231)
        self.assertFalse(result)

    def tearDown(self):
        try:
            os.remove(TEST_INI)
        except Exception:
            pass
