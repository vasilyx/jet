import json

from backend.orm.models import Sus
from backend.rest.application import api as application
from falcon import testing
from peewee import SqliteDatabase
from playhouse.test_utils import test_database

test_db = SqliteDatabase(':memory:')


class MyTestCase(testing.TestCase):
    def setUp(self):
        super(MyTestCase, self).setUp()
        self.app = application
        self.maxDiff = None


class SusResourceTest(MyTestCase):
    def create_test_data(self):
        Sus.create(id=1, sus_name='Sus1', host='evm1337.lpr.jet.msk.su',
                   port=9000)
        Sus.create(id=2, sus_name='Sus2', host='evm1337.lpr.jet.msk.su',
                   port=9100, parent=1)
        Sus.create(id=3, sus_name='Sus3', host='evm1337.lpr.jet.msk.su',
                   port=9200, parent=2)
        Sus.create(id=4, sus_name='Sus4', host='evm1337.lpr.jet.msk.su',
                   port=9300, parent=1)
        Sus.create(id=5, sus_name='Sus5', host='evm1337.lpr.jet.msk.su',
                   port=9400, parent=3)

    def test_get_sus_without_filters(self):
        with test_database(test_db, (Sus,)):
            self.create_test_data()

            request_json = {
                "page": 1,
                "page_size": 25,
                "filters": [],
                "sort": []
            }
            response_json = {'rows': [
                {'sus_name': 'Sus1', 'port': 9000, 'parent': None,
                 'host': 'evm1337.lpr.jet.msk.su',
                 'reg_timestamp': None, 'description': None, 'id': '1'},
                {'sus_name': 'Sus2', 'port': 9100, 'parent': '1',
                 'host': 'evm1337.lpr.jet.msk.su',
                 'reg_timestamp': None, 'description': None, 'id': '2'},
                {'sus_name': 'Sus3', 'port': 9200, 'parent': '2',
                 'host': 'evm1337.lpr.jet.msk.su',
                 'reg_timestamp': None, 'description': None, 'id': '3'},
                {'sus_name': 'Sus4', 'port': 9300, 'parent': '1',
                 'host': 'evm1337.lpr.jet.msk.su',
                 'reg_timestamp': None, 'description': None, 'id': '4'},
                {'sus_name': 'Sus5', 'port': 9400, 'parent': '3',
                 'host': 'evm1337.lpr.jet.msk.su',
                 'reg_timestamp': None, 'description': None, 'id': '5'}
            ], 'total': 5}

            result = self.simulate_post('/scs_hierarchy',
                                        body=json.dumps(request_json))
            self.assertEqual(result.json, response_json)

    def test_get_sus_with_filters_and_order(self):
        with test_database(test_db, (Sus,)):
            self.create_test_data()

            request_json = {
                "page": 1,
                "page_size": 25,
                "filters": [
                    {
                        "field": "parent",
                        "type": "is not null"
                    }, {
                        "field": "port",
                        "type": "between",
                        "value": [8000, 9200]
                    }
                ],
                "sort": [{
                    "property": "id",
                    "direction": "desc"
                }]
            }
            response_json = {'rows': [
                {'sus_name': 'Sus3', 'port': 9200, 'parent': '2',
                 'host': 'evm1337.lpr.jet.msk.su',
                 'reg_timestamp': None, 'description': None, 'id': '3'},
                {'sus_name': 'Sus2', 'port': 9100, 'parent': '1',
                 'host': 'evm1337.lpr.jet.msk.su',
                 'reg_timestamp': None, 'description': None, 'id': '2'}
            ], 'total': 2}

            result = self.simulate_post('/scs_hierarchy',
                                        body=json.dumps(request_json))
            print(result.json)

            self.assertEqual(result.json, response_json)
