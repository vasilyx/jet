import os
import re

from pluton_commons.common.configuration import Configuration as BaseConfiguration
from pluton_commons.common.constants import DATABASE_HOSTNAME, DATABASE_PORT, DATABASE_NAME

DATABASE_SECTION = 'DATABASE'
COMPONENT_SECTION = 'COMPONENT'
UPDATE_SERVER = 'UPDATE_SERVER'
VERSION_FILE = '/etc/pluton/pluton-version'


class Configuration(BaseConfiguration):
    def get_scs_database_hostname(self):
        return self[DATABASE_SECTION, DATABASE_HOSTNAME]

    def set_scs_database_hostname(self, value=None):
        """Установка имени хоста базы данных, если забыли передать,
         то спросить с консоли"""
        if value is None:
            self.add_entry(DATABASE_HOSTNAME,
                           input('Укажите адрес хоста базы данных: '),
                           DATABASE_SECTION)
        else:
            self.add_entry(DATABASE_HOSTNAME, value, DATABASE_SECTION)
        return self.check_scs_database_hostname()

    def check_scs_database_hostname(self):
        """Проверка имени хоста"""
        result = True
        message = ''
        hostname = self[DATABASE_SECTION, DATABASE_HOSTNAME]
        if not isinstance(hostname, str):
            result = False
            message = 'Имя хоста базы данных задано не строкой!'

        if result and len(hostname) > 255:
            result = False
            message = 'Имя хоста базы данных слишком длинное (больше 255 символов)!'

        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
        if result and not all(allowed.match(x) for x in hostname.split(".")):
            result = False
            message = 'Имя хоста ({}) базы данных содержит недопустимые символы!'.format(hostname)
        return result, message

    def get_apikey(self):
        return self[UPDATE_SERVER, 'apikey']

    def set_apikey(self, value=None):
        self.add_entry('apikey', value, UPDATE_SERVER)

    def get_scs_database_name(self):
        return self[DATABASE_SECTION, DATABASE_NAME]

    def set_scs_database_name(self, value=None):
        if value is None:
            self.add_entry(DATABASE_NAME,
                           input('Укажите имя базы данных: '),
                           DATABASE_SECTION)
        else:
            self.add_entry(DATABASE_NAME, value, DATABASE_SECTION)
        return self.check_scs_database_name()

    def check_scs_database_name(self):
        """Проверка имени базы данных"""
        result = True
        message = ''
        name = self[DATABASE_SECTION, DATABASE_NAME]
        if not isinstance(name, str):
            result = False
            message = 'Имя базы данных задано не строкой!'
        if result and len(name) > 255:
            result = False
            message = 'Имя базы данных слишком длинное (больше 255 символов)!'
        return result, message

    def get_scs_database_port(self):
        return self[DATABASE_SECTION, DATABASE_PORT]

    def get_scs_database_user(self):
        return self[DATABASE_SECTION, 'user']

    def get_scs_database_password(self):
        return self[DATABASE_SECTION, 'password']

    def set_scs_database_port(self, value=None):
        """Установка порта базы данных, если забыли передать,
         то спросить с консоли"""
        if value is None:
            self.add_entry(DATABASE_PORT,
                           input('Укажите порт базы данных: '),
                           DATABASE_SECTION)
        else:
            self.add_entry(DATABASE_PORT, str(value), DATABASE_SECTION)
        return self.check_scs_database_port()

    def check_scs_database_port(self):
        """Проверка порта"""
        result = True
        message = ''
        port = self[DATABASE_SECTION, DATABASE_PORT]
        try:
            port = int(port)
        except (ValueError, TypeError):
            result = False
            message = 'Номер порта ({}) базы данных СУС задан не целым числом!'.format(port)
        return result, message

    def check_validity(self):
        """Проверка вменяемости конфигурации"""
        result, message = self.check_scs_database_hostname()
        if result:
            result, message = self.check_scs_database_name()
        if result:
            result, message = self.check_scs_database_port()
        return result, message


class VersionInformator():
    def __init__(self):
        if os.path.isfile(VERSION_FILE):
            with open(VERSION_FILE) as f:
                try:
                    self.version = f.read().strip().split('=')[1]
                except Exception:
                    self.version = 'unknown'
        else:
            self.version = 'unknown'


configuration = Configuration.load_configuration()
version = VersionInformator().version
