import subprocess
from cgi import FieldStorage
from pluton_commons.common.utils import get_current_component_id
from pluton_commons.db.peewee.models import SystemComponent


class Parser(FieldStorage):
    pass


def get_current_component_info():
    sc = SystemComponent.select(
        SystemComponent.name,
        SystemComponent.id).where(
            SystemComponent.id == get_current_component_id()).get()
    return {
        'id': str(sc.id),
        'name': sc.name
    }


def get_label_id(name):
    try:
        label_id = subprocess.check_output(['/usr/bin/sudo',
                                            '/usr/bin/pluton',
                                            'user',
                                            'maclabel',
                                            name]).decode().strip()
    except subprocess.CalledProcessError as error:
        print(error.stdout)
        print(error.returncode)
        label_id = -1
    return label_id


def get_self_label_id():
    try:
        label_id = subprocess.check_output(['/usr/bin/sudo',
                                            '/usr/bin/pluton',
                                            'user',
                                            'maclabel']).decode().strip()
    except subprocess.CalledProcessError as error:
        print(error.stdout)
        print(error.returncode)
        label_id = -1
    return label_id
