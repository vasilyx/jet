import getpass
import json
import codecs
import csv
from io import BytesIO, StringIO
from urllib.parse import parse_qs

import falcon
from peewee import datetime

from backend.utils.common import Parser
from backend.utils.configuration import configuration
from backend.utils.logger import logger
from pluton_audit import EventLogger
from pluton_audit.events import UserAuditEvent
from pluton_commons.db.peewee.models import (
    scs_db, SystemUser, SystemPrivilege, SystemRolePrivilege,
    SystemRole, SystemUserRole
)

event_logger = EventLogger(configuration, logger)


class UserVerificationMiddleware(object):
    def process_request(self, req, resp, **kwargs):
        username = getpass.getuser()
        try:
            userinfo = SystemUser.select().where(
                    SystemUser.login == username,
                    SystemUser.is_deleted != True,  # noqa: E712
                    SystemUser.is_blocked != True,
                    (SystemUser.expiration_timestamp.is_null(True) |
                     (SystemUser.expiration_timestamp >
                      datetime.datetime.now()))).get()

            req.userinfo = {
                "name": username,
                "id": str(userinfo.id),
                "email": userinfo.email,
                "expiration_time": userinfo.expiration_timestamp,
                "creation_type": userinfo.creation_type
            }

            request_path = req.path + '!'
            has_privileges = None
            error_msg = ''
            privileges = SystemUser.select(SystemPrivilege.api_rule) \
                .distinct() \
                .join(SystemUserRole,
                      on=SystemUserRole.system_user_id) \
                .join(SystemRole,
                      on=((SystemUserRole.system_role_id == SystemRole.id) &
                          (SystemRole.is_deleted != True))) \
                .join(SystemRolePrivilege,  # noqa: E712
                      on=SystemRolePrivilege.system_role_id) \
                .join(SystemPrivilege) \
                .where(SystemUser.id == str(userinfo.id),
                       SystemPrivilege.type == 'REST',
                       SystemPrivilege.api_rule.contains(request_path))

            pos_special_rules = set()
            neg_special_rules = set()
            for record in privileges.naive():
                has_privileges = False
                for api_rule in record.api_rule.split('|'):
                    rule_parts = api_rule.split('!')
                    if rule_parts[0] == request_path:
                        if rule_parts[1]:
                            for special_rule in rule_parts[1].split(','):
                                if special_rule.startswith('-'):
                                    neg_special_rules.add(special_rule[1:])
                                else:
                                    pos_special_rules.add(special_rule)
                        else:
                            # Отсутствие второстепенных правил означает,
                            # что полномочия даны полностью
                            has_privileges = True
                            break

            # Если мы не нашли соответствующих привилегий,
            # то проверим существуют ли они вообще
            if has_privileges is None:
                error_msg = req.path
                has_privileges = False
            # Если привилегии найдены, то проверяем,
            # остались ли непогашенные второстепенные правила
            else:
                if len(pos_special_rules ^ neg_special_rules) == 0:
                    has_privileges = True
                else:
                    body = req.data
                    only_neg = neg_special_rules - pos_special_rules
                    if len(only_neg) > 0:
                        if len(only_neg & body.keys()) == 0:
                            has_privileges = True
                        else:
                            has_privileges = False
                            error_msg = (req.path +
                                         ' с полями ' +
                                         ', '.join(only_neg & body.keys()))
                    else:
                        only_pos = pos_special_rules - neg_special_rules
                        if len(body.keys() - only_pos) == 0:
                            has_privileges = True
                        else:
                            has_privileges = False
                            error_msg = (req.path +
                                         ' с полями ' +
                                         ', '.join(only_neg & body.keys()))

            if not has_privileges:
                resp.status = falcon.HTTP_403
                raise ValueError('У пользователя отсутствуют права '
                                 'на выполнение запроса {}'.format(error_msg))

        except SystemUser.DoesNotExist:
            resp.status = falcon.HTTP_401
            event_logger.log(UserAuditEvent.TYPE_USER_LOGIN_FAILED,
                             user_id='',
                             login=getpass.getuser())
            raise ValueError('Пользователь заблокирован или не существует')


class MultipartMiddleware(object):

    def __init__(self, parser=None):
        self.parser = parser or Parser

    def parse(self, stream, environ):
        return self.parser(fp=stream, environ=environ)

    def parse_field(self, field):
        if isinstance(field, list):
            return [self.parse_field(subfield) for subfield in field]

        # When file name isn't ascii FieldStorage will not consider it.
        encoded = field.disposition_options.get('filename*')
        if encoded:
            # http://stackoverflow.com/a/93688
            encoding, filename = encoded.split("''")
            field.filename = filename
            # FieldStorage will decode the file content by itself when
            # file name is encoded, but we need to keep a consistent
            # API, so let's go back to bytes.
            # WARNING we assume file encoding will be same as filename
            # but there is no guaranty.
            field.file = BytesIO(field.file.read().encode(encoding))
        if getattr(field, 'filename', False):
            return field
        # This is not a file, thus get flat value (not
        # FieldStorage instance).
        return field.value

    def process_request(self, req, resp, **kwargs):

        if 'multipart/form-data' not in (req.content_type or ''):
            return

        # This must be done to avoid a bug in cgi.FieldStorage.
        req.env.setdefault('QUERY_STRING', '')

        # To avoid all stream consumption problem which occurs in falcon 1.0.0
        # or above.
        stream = (req.stream.stream if hasattr(req.stream, 'stream') else
        req.stream)
        try:
            form = self.parse(stream=stream, environ=req.env)
        except ValueError as e:  # Invalid boundary?
            raise falcon.HTTPBadRequest('Error parsing file', str(e))

        for key in form:
            # TODO: put files in req.files instead when #418 get merged.
            req._params[key] = self.parse_field(form[key])


class SCSWebUIConnectionMiddleware(object):
    def process_request(self, request, response):
        scs_db.connect()

    def process_response(self, request, response, resource):
        if not scs_db.is_closed():
            scs_db.close()


class RequestDataConvertMiddleware(object):
    def process_request(self, request, response):
        if request.content_type == 'application/json':
            body = request.stream.read()
            if body:
                request.data = json.loads(body.decode('UTF-8'))
            else:
                request.data = {}
        if request.content_type == 'application/x-www-form-urlencoded':
            body = request.stream.read()
            request.data = {}
            if body:
                parsed = parse_qs(body.decode('UTF-8'))
                for param_name, value in parsed.items():
                    request.data[param_name] = json.loads(value[0])

    def process_response(self, request, response, resource):
        report_type = getattr(request, 'data', {}).get('report_type')
        if report_type == 'csv':
            columns = request.data.get('columns')
            content = response.body
            rows = content.get('rows')
            if rows is not None:
                content = rows
            else:
                raise ValueError('Отчет не может быть сгенерирован для запрашиваемого сервиса')
            output = StringIO()
            csv_writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
            csv_writer.writerow(columns)
            for record in content:
                csv_writer.writerow([record[column]['label']
                                    if isinstance(record[column], dict)
                                    else record[column]
                                    for column in columns])
            response.body = codecs.encode(output.getvalue(), encoding='utf-8-sig')
            response.content_type = 'application/octet-stream'
            response.append_header('Content-Disposition',
                                   'attachment; filename=report.csv')
        else:
            if response.content_type != 'application/octet-stream':
                response.body = json.dumps(response.body)


class RequestAuditLoggerMiddleware:
    def process_request(self, request, response):
        if request.path == '/audit/list':
            event_logger = EventLogger(configuration, logger)
            url = request.url
            body = getattr(request, 'data', {})
            if body.get('report_type') == 'csv':
                type = UserAuditEvent.TYPE_USER_REPORT_AUDIT_REQUEST
            else:
                type = UserAuditEvent.TYPE_USER_REST_AUDIT_REQUEST

            event_logger.log(type,
                             user_id=request.userinfo['id'],
                             login=request.userinfo['name'],
                             url=url,
                             body=body)
