import logging
from logging import getLogger
from logging.handlers import SysLogHandler

DEFAULT_LOGGER_FORMATTER = logging.Formatter('%(asctime)s %(levelname)s %(module)s.%(funcName)s:%(lineno)d %(message)s')

logger = getLogger()
syslog_handler = SysLogHandler(address='/dev/log', facility=SysLogHandler.LOG_DAEMON)
syslog_handler.setFormatter(DEFAULT_LOGGER_FORMATTER)
logger.addHandler(syslog_handler)
logger.setLevel(logging.INFO)
        
