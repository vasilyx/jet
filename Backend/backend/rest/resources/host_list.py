import ipaddress
import time
from functools import reduce
from subprocess import Popen, DEVNULL
from tempfile import NamedTemporaryFile
from uuid import uuid4

from infi.clickhouse_orm.query import Q
from peewee import JOIN, fn
from pluton_commons.db.common import (
    enum_to_dict
)
from pluton_commons.common.constants import HOST_PROFILE_SECTION
from pluton_commons.common.configuration import Configuration, CONFIG_DEFAULT_PATH
from pluton_commons.db.peewee.models import (
    Host, Ip, Application, SystemComponent,
    HostApplication, TYPE_HOST_STATUS, Protocol, HostApplicationVulnerability, Vulnerability)
from pluton_commons.db.infi.models import Alert, Stat
from pluton_commons.db.infi.connection import clickhouse_db
from pluton_commons.db.peewee.utils import send_list_data, get_record_values, aggregate_to_array, \
    get_filter_value, cast, distinct, get_filter
from pluton_query_service.client import PlutonQueryClient
from pluton_query_service.common import OK_MSG, ERROR_MSG

SIGNATURE_BRO = '68c94d92-9f2c-4928-8862-6fb847153b80'
SIGNATURE_SURICATA = 'c28643eb-8541-41c1-a45a-2c31574db95b'

standard_alert_types = [
    '62b38077-0517-4eea-b18f-ed52e7697a69',
    '909280bb-388b-4347-bf3e-28ed679bb6eb',
    '09caa12d-acde-4f84-adc6-d524373d6af9',
    '3e542d92-8cab-11e7-bb31-be2e44b06b34'
]

signature_alert_types = [
    '68c94d92-9f2c-4928-8862-6fb847153b80',
    'c28643eb-8541-41c1-a45a-2c31574db95b'
]

blacklist_alert_types = [
    'efd52890-aa9e-457b-867d-9cd861cc5202'
]

heuristic_alert_types = [
    'f81e3702-7507-4b6c-8d44-25afa8b42fb8',
    'efd52890-aa9e-457b-867d-9cd861cc5202'
]


class HostListResource(object):
    def on_post(self, request, response):
        def records_processing(record):
            enum_to_dict(record, 'host_status', TYPE_HOST_STATUS)
            record['host_ip'] = [str(ipaddress.IPv4Address(int(x))) for x in record['host_ip']]
            record['host_os'] = []
            if record['host_os_id_'] and record['host_os_label_']:
                for idx, label, status in zip(record['host_os_id_'], record['host_os_label_'], record['host_os_status_']):
                    record['host_os'].append({'id': idx, 'label': label, 'status': status})
            record.pop('host_os_id_', None)
            record.pop('host_os_label_', None)
            record.pop('host_os_status_', None)
            return record

        unconfirmed_apps = HostApplication.alias()

        query = Host.select(
            Host.id,
            Host.hostname,
            Host.status.alias('host_status'),
            aggregate_to_array(distinct(cast(Ip.ip, 'character varying'))).alias('host_ip'),
            aggregate_to_array(distinct(cast(Application.unparsed_version, 'character varying'))).alias('host_os_label_'),
            aggregate_to_array(distinct(cast(Application.id, 'character varying'))).alias('host_os_id_'),
            aggregate_to_array(distinct(cast(HostApplication.status, 'character varying'))).alias('host_os_status_'),
            fn.Count(distinct(unconfirmed_apps.id)).alias('unconfirmed_applications_count'),
            SystemComponent.name.alias('system_component_label_'),
            SystemComponent.id.alias('system_component_id_'),
            Host.discovery_timestamp,
            Host.create_timestamp,
            Host.last_time_activity,
            Host.added
        ).distinct() \
            .join(Ip,
                  on=(Host.id == Ip.host_id),
                  join_type=JOIN.LEFT_OUTER) \
            .join(unconfirmed_apps,
                  on=((Host.id == unconfirmed_apps.host_id) & (unconfirmed_apps.status == 'UNKNOWN')),
                  join_type=JOIN.LEFT_OUTER) \
            .join(HostApplication,
                  on=((Host.id == HostApplication.host_id) & (HostApplication.status != 'DELETED')),
                  join_type=JOIN.LEFT_OUTER) \
            .join(Application,
                  on=((Application.id == HostApplication.application_id) & (Application.software_type == 'OS')),
                  join_type=JOIN.LEFT_OUTER) \
            .join(SystemComponent,
                  on=(Host.discovery_component_id == SystemComponent.id),
                  join_type=JOIN.LEFT_OUTER)

        host_ip_list = get_filter_value('host_ip', request.data.get('filters'))
        if host_ip_list:
            for i, item in enumerate(host_ip_list):
                host_ip_list[i] = int(ipaddress.IPv4Address(item.strip()))

        clickhouse_filter = []

        # C дерева компонент приходит без подчеркивания (фиксим тут)
        sc_filter = get_filter('system_component_id', request.data.get('filters'))
        if sc_filter:
            sc_filter['field'] = 'system_component_id_'

        # Ползунки на интерфейсе (условие - хотя бы один сиб из хоста соответствует уровню угрозы)
        severity_values = get_filter_value('severity', request.data.get('filters'))

        severity_filter_list = []
        if severity_values and severity_values[0] > 0:
            clickhouse_filter.append(Q(severity_num__between=severity_values))

        # Время событий
        alert_timestamp_values = get_filter_value('alert_timestamp', request.data.get('filters'))
        if alert_timestamp_values:
            clickhouse_filter.append(Q(alert_timestamp__between=alert_timestamp_values))

        # Допустимый тип событий
        is_anomaly_value = get_filter_value('is_anomaly_alert', request.data.get('filters'))
        is_new_po_value = get_filter_value('is_new_po_alert', request.data.get('filters'))
        is_new_os_value = get_filter_value('is_new_os_alert', request.data.get('filters'))
        is_new_service_value = get_filter_value('is_new_service_alert', request.data.get('filters'))
        alert_types = []
        if is_anomaly_value:
            alert_types.append('f81e3702-7507-4b6c-8d44-25afa8b42fb8')
        if is_new_po_value:
            alert_types.append('909280bb-388b-4347-bf3e-28ed679bb6eb')
        if is_new_os_value:
            alert_types.append('62b38077-0517-4eea-b18f-ed52e7697a69')
        if is_new_service_value:
            alert_types.append('3e542d92-8cab-11e7-bb31-be2e44b06b34')
        if alert_types:
            clickhouse_filter.append(Q(alert_type_id__in=alert_types))

        # Фильтр по портам, протоколам, сервисам, ОС хостов
        complicated_filter = None

        port_value = get_filter_value('port', request.data.get('filters'))
        protocol_value = get_filter_value('protocol', request.data.get('filters'))

        service_value = get_filter_value('service', request.data.get('filters'))
        service_status_value = get_filter_value('service_status', request.data.get('filters'))

        os_value = get_filter_value('os', request.data.get('filters'))
        os_status_value = get_filter_value('os_status', request.data.get('filters'))

        if port_value:
            condition = (HostApplication.port == port_value)
            if complicated_filter is not None:
                complicated_filter = complicated_filter & condition
            else:
                complicated_filter = condition
        if protocol_value:
            query = query.join(Protocol, on=(Protocol.id == HostApplication.protocol_id))
            condition = (Protocol.name.contains(protocol_value))
            if complicated_filter is not None:
                complicated_filter = complicated_filter & condition
            else:
                complicated_filter = condition
        if os_value:
            condition = ((HostApplication.application_id << os_value) & (Application.software_type == 'OS'))
            if os_status_value:
                condition = condition & (HostApplication.status << os_status_value)
            else:   # По умолчанию не показываем DELETED
                condition = condition & (HostApplication.status != 'DELETED')
            if complicated_filter is not None:
                complicated_filter = complicated_filter & condition
            else:
                complicated_filter = condition
        if service_value:
            condition = ((HostApplication.application_id << service_value) & (Application.software_type == 'SERVICE'))
            if service_status_value:
                condition = condition & (HostApplication.status << service_status_value)
            else:  # По умолчанию не показываем DELETED
                condition = condition & (HostApplication.status != 'DELETED')
            if complicated_filter is not None:
                complicated_filter = complicated_filter & condition
            else:
                complicated_filter = condition

        # Применяем все фильтры кликхауса
        if clickhouse_filter:
            q_object = reduce((lambda x, y: x & y), clickhouse_filter)
            host_ids = set()
            ch_query_1 = Alert.objects_in(clickhouse_db).only(
                Alert.target_host_id.alias
            ).filter(q_object).distinct()
            ch_query_2 = Alert.objects_in(clickhouse_db).only(
                Alert.source_host_id.alias
            ).filter(q_object).distinct()
            for record in ch_query_1:
                host_ids.add(record.target_host_id)
            for record in ch_query_2:
                host_ids.add(record.source_host_id)

            pg_condition = (Host.id << list(host_ids))
            if complicated_filter is not None:
                complicated_filter = complicated_filter & pg_condition
            else:
                complicated_filter = pg_condition

        send_list_data(
            query,
            request,
            response,
            result_processing=records_processing,
            additional_filters=complicated_filter,
            group_by=(Host.id, Host.hostname,
                      Host.discovery_timestamp,
                      SystemComponent.id,
                      SystemComponent.name,
                      Host.status)
        )


class HostListPreviewResource(object):

    def get_traffic_intensity(self, host_id, sc_id, stat_interval):
        host_ips_query = Ip.select(
            Ip.ip
        ).where(Ip.host_id == host_id)
        host_ips = []
        for rec in host_ips_query.naive():
            host_ips.append(rec.ip)

        result = PlutonQueryClient().send_command([sc_id], Stat.table_name(),
                                                  filters={
                                                      'operation': 'OR',
                                                      'operands': [
                                                          {
                                                              'field': 'src_ip',
                                                              'value': host_ips,
                                                              'type': 'in'
                                                          },
                                                          {
                                                              'field': 'dst_ip',
                                                              'value': host_ips,
                                                              'type': 'in'
                                                          }
                                                      ]
                                                  },
                                                  sort=[{
                                                      'property': 'finish_ts',
                                                      'direction': 'DESC'
                                                  }],
                                                  page_number=1,
                                                  page_size=1)
        if result['message'] == OK_MSG:
            speed_qs = result.get('data').get('rows')
            if speed_qs and (speed_qs[0]['finish_ts'] + stat_interval * 1e6) > (time.time() * 1e6):
                interval_in_sec = (speed_qs[0]['finish_ts'] - speed_qs[0]['start_ts']) / 1e6
                return {
                    'upload': int(speed_qs[0]['volume_out'] / interval_in_sec),
                    'download': int(speed_qs[0]['volume_in'] / interval_in_sec)
                }
            else:
                return {
                    'upload': 0,
                    'download': 0
                }
        elif result['message'] == ERROR_MSG:
            raise ValueError('Невозможно получить информацию об интенсивности обмена хоста'
                             ' с компоненты {}'.format(sc_id))


    def on_post(self, request, response):
        host_id = request.data.get('id')
        signatures_count = Alert.objects_in(clickhouse_db).only(Alert.id.alias).filter(
                Q(alert_type_id__in=signature_alert_types) & (Q(source_host_id__eq=host_id) | Q(target_host_id__eq=host_id))
        ).count()
        blacklist_count = Alert.objects_in(clickhouse_db).only(Alert.id.alias).filter(
                Q(alert_type_id__in=blacklist_alert_types) & (Q(source_host_id__eq=host_id) | Q(target_host_id__eq=host_id))
        ).count()
        anomalies_count = Alert.objects_in(clickhouse_db).only(Alert.id.alias).filter(
                Q(alert_type_id__in=heuristic_alert_types) & (Q(source_host_id__eq=host_id) | Q(target_host_id__eq=host_id))
        ).count()
        application_count = HostApplication.select(HostApplication.host_id) \
            .join(Application, on=(HostApplication.application_id == Application.id)) \
            .where((HostApplication.status != 'DELETED') &
                   (Application.software_type == 'APPLICATION') & (HostApplication.port == 0) &
                   (HostApplication.host_id == host_id)).count()
        service_count = HostApplication.select(HostApplication.host_id) \
            .join(Application, on=(HostApplication.application_id == Application.id)) \
            .where((HostApplication.status != 'DELETED') &
                   (((Application.software_type == 'APPLICATION') & (HostApplication.port != 0)) | (
                       Application.software_type == 'SERVICE')) &
                   (HostApplication.host_id == host_id)).count()
        disapproved_software = HostApplication.select(HostApplication.host_id) \
            .join(Application, on=(HostApplication.application_id == Application.id)) \
            .where((HostApplication.status != 'ACCEPTED') & (HostApplication.status != 'DELETED') &
                   (Application.software_type == 'APPLICATION') & (HostApplication.port == 0) &
                   (HostApplication.host_id == host_id)).count()
        disapproved_service = HostApplication.select(HostApplication.host_id) \
            .join(Application, on=(HostApplication.application_id == Application.id)) \
            .where((HostApplication.status != 'ACCEPTED') & (HostApplication.status != 'DELETED') &
                   (((Application.software_type == 'APPLICATION') & (HostApplication.port != 0)) | (
                        Application.software_type == 'SERVICE')) &
                   (HostApplication.host_id == host_id)).count()
        vulnerabilities_count = HostApplicationVulnerability.select(HostApplicationVulnerability.host_id) \
            .join(Vulnerability, on=(Vulnerability.id == HostApplicationVulnerability.vulnerability_id)) \
            .where((HostApplicationVulnerability.host_id == host_id)).count()
        #     AlertBroHost.select(AlertBroHost.id).join(
        #     Alert, on=(AlertBroHost.alert_id == Alert.id)
        # ).where(
        #     (Alert.source_host_id == host_id) |
        #     (Alert.target_host_id == host_id)
        # ).count()

        query = Host.select(
            Host.create_timestamp,
            Host.last_time_activity,
            Host.added,
            Host.discovery_component_id
        ).where(Host.id == host_id)
        result_host = get_record_values(query.naive().get())

        data = {
            'added': result_host.get('added'),
            'signatures_count': signatures_count,
            'blacklist_count': blacklist_count,
            'anomalies_count': anomalies_count,
            'vulnerabilities_count': vulnerabilities_count,
            'created': result_host.get('create_timestamp'),
            'last_time_activity': result_host.get('last_time_activity'),
            'speed': self.get_traffic_intensity(
                host_id,
                result_host.get('discovery_component_id'),
                Configuration(CONFIG_DEFAULT_PATH).get_int('stat_interval_sec', HOST_PROFILE_SECTION, default=300)
            ),
            'application_count': {
                'all': application_count,
                'disapproved': disapproved_software
            },
            'service_count': {
                'all': service_count,
                'disapproved': disapproved_service
            }
        }
        response.body = data


class HostListUploadResource(object):
    def on_post(self, request, response):
        file_name = None
        with NamedTemporaryFile(delete=False) as tf:
            file_name = tf.name
            tf.write(request.get_param('file').value)
        args = ['/usr/bin/pluton', 'profiles', 'import']
        if request.get_param('conflict_resolution') == "prefer_db":
            args += ['--prefer-db']
        job_id = str(uuid4())
        args += ['-j', job_id, file_name]
        Popen(args, stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL)
        response.body = job_id
