import dateutil.parser

from backend.utils.configuration import configuration
from backend.utils.logger import logger
from pluton_audit.integrity import PlutonIntegrityClient, Response, Status, Command
from pluton_commons.db.common import idlabels_to_dict
from pluton_commons.db.infi.models import AuditLog
from pluton_commons.db.peewee.models import EntityType, AuditType
from pluton_commons.db.peewee.utils import send_list_data
from pluton_query_service.client import PlutonQueryClient
from pluton_query_service.common import OK_MSG, ERROR_MSG

AUDIT_DAEMON_STATUS = {
    Response.IDLE: 'Готов к работе',
    Response.DELAY_SET: 'Следующая проверка отложена',
    Response.IN_PROGRESS: 'Проводится проверка...',
    Response.UPDATING: 'Проводится обновление БКЦ...'
}

AUDIT_DAEMON_LAST_STATE = {
    Status.OK: 'Нарушений не обнаружено',
    Status.ERROR: 'Произошла ошибка',
    Status.NA: 'Проверка не проводилась',
    Status.INTEGRITY_FAILURE: 'Обнаружены нарушения: {}'
}

AUDIT_DAEMON_ERROR = {
    Response.TIMEOUT: 'Превышено время ожидания ответа',
    Response.ERROR: 'Произошла ошибка'
}


class AuditListResource:
    def on_post(self, request, response):
        filters = request.data.get('filters')
        sort = request.data.get('sort')
        page_number = request.data.get('page')
        page_size = request.data.get('page_size')
        sc_id = None
        if filters:
            sc_id = [sc['value'][0] for sc in filters
                     if sc['field'] == 'system_component_id' and len(sc['value']) > 0]
            filters = [f for f in filters if f['field'] != 'system_component_id']

        result = PlutonQueryClient().send_command(sc_id, AuditLog.table_name(),
                                                  filters=filters,
                                                  sort=sort,
                                                  page_number=page_number,
                                                  page_size=page_size,
                                                  timeout=15)

        if result['message'] == OK_MSG:
            data = result.get('data').get('rows')
            total_count = result.get('data').get('rows_total')
            rows = []
            for record in data:
                record['registered_timestamp'] = dateutil.parser.parse(
                        record['registered_timestamp']).replace(
                        microsecond=int(record['registered_timestamp_millis']) // 1000
                ).isoformat()
                del record['registered_timestamp_millis']
                rows.append(idlabels_to_dict(record))

            response.body = {'total': total_count, 'rows': rows}
        elif result['message'] == ERROR_MSG:
            raise ValueError(result['error'])


class EntityTypeListResource:
    def on_post(self, request, response):
        send_list_data(EntityType.select(
                EntityType.id,
                EntityType.name.alias('label')
        ), request, response)


class AuditTypeListResource:
    def on_post(self, request, response):
        send_list_data(
            AuditType.select(
                AuditType.id,
                AuditType.name.alias('label')
            )
        , request, response)


def processIntegrityRequest(request, response, command, **arguments):
    destination_id = request.data['system_component_id']
    client = PlutonIntegrityClient(configuration, logger)
    reply = client.send_command(
            command, destination_id, timeout=5, **arguments)
    status = reply['response']
    if status in AUDIT_DAEMON_ERROR:
        msg_text = AUDIT_DAEMON_ERROR[status]
        if status == Response.TIMEOUT:
            raise ValueError(msg_text)
        else:
            error = reply.get('error')
            raise ValueError('{}:\n{}\n{}'.format(
                    msg_text, error.get('message'), error.get('stack')))
    elif status in AUDIT_DAEMON_STATUS:
        integrity = reply['integrity']
        reply['status'] = AUDIT_DAEMON_STATUS[status]
        if integrity == Status.INTEGRITY_FAILURE:
            reply['last_state'] = AUDIT_DAEMON_LAST_STATE[integrity] \
                .format(reply['total_changes'])
        else:
            reply['last_state'] = AUDIT_DAEMON_LAST_STATE[integrity]
        response.body = reply


class AuditStartIntegrityResource:
    def on_post(self, request, response):
        processIntegrityRequest(request, response, Command.START)


class AuditStatusIntegrityResource:
    def on_post(self, request, response):
        processIntegrityRequest(request, response, Command.STATUS)


class AuditDelayIntegrityResource:
    def on_post(self, request, response):
        processIntegrityRequest(request, response,
                                Command.DELAY, delay=request.data['delay'])


class AuditUpdateDBIntegrityResource:
    def on_post(self, request, response):
        processIntegrityRequest(request, response, Command.UPDATE_DB)
