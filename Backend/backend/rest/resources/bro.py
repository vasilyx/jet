import ipaddress
import dateutil.parser
from collections import OrderedDict

from pluton_query_service.client import PlutonQueryClient
from pluton_commons.common.utils import convert_microsecond_to_datetime
from pluton_commons.db.connection import DatabaseConnection
from pluton_commons.db.peewee.utils import get_filter
from pluton_query_service.common import OK_MSG, ERROR_MSG

BRO_TABLES = OrderedDict()
BRO_TABLES['conn'] = {'label': 'Сетевые сессии'}
BRO_TABLES['known_host'] = {'label': 'Хосты', 'filter_map': {'orig_h': 'host', 'resp_h': 'host'}}
BRO_TABLES['software'] = {'label': 'Программное обеспечение', 'filter_map': {'orig_h': 'host', 'resp_h': 'host'}}
BRO_TABLES['known_service'] = {'label': 'Сервисы', 'filter_map': {'orig_h': 'host', 'resp_h': 'host'}}
BRO_TABLES['os_found'] = {'label': 'Операционные системы'}
BRO_TABLES['weird'] = {'label': 'Нарушения'}
BRO_TABLES['notice'] = {'label': 'Подозрительные события'}
BRO_TABLES['signature'] = {'label': 'Сигнатурные события', 'filter_map': {'orig_h': 'src_addr', 'resp_h': 'dst_addr'}}
BRO_TABLES['file'] = {'label': 'Файлы', 'filter_map': {'orig_h': 'tx_hosts', 'resp_h': 'rx_hosts'}}
BRO_TABLES['pe'] = {'label': 'Исполняемые модули', 'filter_map': {'orig_h': None, 'resp_h': None}}
BRO_TABLES['smb_files'] = {'label': 'SMB-файлы'}
BRO_TABLES['smb_cmd'] = {'label': 'SMB-команды'}
BRO_TABLES['known_cert'] = {'label': 'Сертификаты', 'filter_map': {'orig_h': 'host', 'resp_h': 'host'}}
BRO_TABLES['traceroute'] = {'label': 'Traceroute', 'filter_map': {'orig_h': 'src', 'resp_h': 'dst'}}
BRO_TABLES['dhcp'] = {'label': 'DHCP'}
BRO_TABLES['dns'] = {'label': 'DNS'}
BRO_TABLES['dpd'] = {'label': 'DPD'}
BRO_TABLES['ftp'] = {'label': 'FTP'}
BRO_TABLES['http'] = {'label': 'HTTP'}
BRO_TABLES['irc'] = {'label': 'IRC'}
BRO_TABLES['kerberos'] = {'label': 'Kerberos'}
BRO_TABLES['ntlm'] = {'label': 'NTLM'}
BRO_TABLES['rdp'] = {'label': 'RDP'}
BRO_TABLES['rfb'] = {'label': 'RFB'}
BRO_TABLES['socks'] = {'label': 'Socks'}
BRO_TABLES['smb_mapping'] = {'label': 'Samba Mapping'}
BRO_TABLES['smtp'] = {'label': 'SMTP'}
BRO_TABLES['snmp'] = {'label': 'SNMP'}
BRO_TABLES['ssh'] = {'label': 'SSH'}
BRO_TABLES['ssl'] = {'label': 'SSL'}
BRO_TABLES['x509'] = {'label': 'x509', 'filter_map': {'orig_h': 'host', 'resp_h': 'host'}}
BRO_TABLES['tunnel'] = {'label': 'Tunnel'}


class BroTablelistResource:
    def on_post(self, request, response):
        data = []
        for k, v in BRO_TABLES.items():
            data.append({'tablename': k, 'label': v['label']})

        response.body = data


class BroListResource:
    def on_post(self, request, response):
        filters = request.data.get('filters')
        sort = request.data.get('sort')
        page_number = request.data.get('page')
        page_size = request.data.get('page_size')
        tablename = request.data.get('tablename')
        if tablename not in BRO_TABLES:
            raise ValueError('Таблица не относится к BRO: {}'.format(tablename))
        need_meta = request.data.get('need_meta', False)
        sc_id = None
        if filters:
            filter_map = BRO_TABLES[tablename].get('filter_map', {})
            if filter_map:
                for orig_filter_name, mapped_filter_name in filter_map.items():
                    filter_to_map = get_filter(orig_filter_name, filters)
                    if filter_to_map:
                        filter_to_map['field'] = mapped_filter_name

            sc_id_value = [sc['value'] for sc in filters if sc['field'] == 'system_component_id']
            if len(sc_id_value) != 0:
                sc_id = sc_id_value[0]
            filters = [f for f in filters if f['field'] != 'system_component_id']
            orig_h_filter = get_filter(filter_map.get('orig_h') or 'orig_h', filters)
            if orig_h_filter and not isinstance(orig_h_filter['value'], int):
                orig_h_filter['value'] = int(ipaddress.IPv4Address(orig_h_filter['value'].strip()))
            resp_h_filter = get_filter(filter_map.get('resp_h') or 'resp_h', filters)
            if resp_h_filter and not isinstance(resp_h_filter['value'], int):
                resp_h_filter['value'] = int(ipaddress.IPv4Address(resp_h_filter['value'].strip()))
            time_filter = get_filter('ts', request.data.get('filters'))
            if time_filter:
                time_filter['value'][0] = int(dateutil.parser.parse(
                    time_filter['value'][0]
                ).timestamp() * 1e6) if time_filter['value'][0] else None
                time_filter['value'][1] = int(dateutil.parser.parse(
                    time_filter['value'][1]
                ).timestamp() * 1e6) if time_filter['value'][1] else None

        result = PlutonQueryClient().send_command(sc_id, tablename,
                                                  filters=filters,
                                                  sort=sort,
                                                  page_number=page_number,
                                                  page_size=page_size,
                                                  need_meta=need_meta)
        if result['message'] == OK_MSG:
            data = []
            for record in result.get('data').get('rows'):
                if 'orig_h' in record:
                    record['orig_h'] = str(ipaddress.IPv4Address(int(record['orig_h'])))
                if 'resp_h' in record:
                    record['resp_h'] = str(ipaddress.IPv4Address(int(record['resp_h'])))
                if 'host' in record:
                    record['host'] = str(ipaddress.IPv4Address(int(record['host'])))
                if 'ip' in record:
                    record['ip'] = str(ipaddress.IPv4Address(int(record['ip'])))
                if 'ts' in record:
                    record['ts'] = convert_microsecond_to_datetime(record['ts']).isoformat()
                data.append(record)

            total_count = result.get('data').get('rows_total')
            meta = [column for column in result.get('data').get('meta') if 'reserved' not in column['name']]
            response.body = {'total': total_count, 'rows': data, 'meta': meta}
        if result['message'] == ERROR_MSG:
            raise ValueError(result['error'])
