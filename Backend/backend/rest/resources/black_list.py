from pluton_commons.common.utils import get_current_component_id, get_current_user_info
from backend.utils.logger import logger
from backend.utils.configuration import configuration
from pluton_audit import EventLogger
from pluton_audit.events import RuleControlEvent

from pluton_commons.db.peewee.models import (
    BlackList, BlackListType, SystemUser, TYPE_DIRECTION_BL,
    Reaction, TYPE_SOURCE_BL)
from pluton_commons.db.peewee.utils import (
    get_record_values, send_list_data
)
from pluton_commons.db.common import(
    enum_to_dict, idlabels_to_dict
)

from peewee import JOIN

event_logger = EventLogger(configuration, logger)
username, user_id = get_current_user_info(configuration)


def enum_direction_processing(record):
    enum_to_dict(record, 'direction', TYPE_DIRECTION_BL)
    return record


def enum_processing(record):
    enum_to_dict(record, 'direction', TYPE_DIRECTION_BL)
    enum_to_dict(record, 'source', TYPE_SOURCE_BL)
    return record


def check_blacklist(record):
    if record.is_deleted:
        raise ValueError('Черный список удален')
    if record.source in ('REPLICATED', 'UPDATE'):
        raise ValueError('Редактирование черного списка из данного источника запрещено')


def get_blacklist_by_id(blacklist_id):
    try:
        update_user = SystemUser.alias()
        create_user = SystemUser.alias()

        data = BlackList.select(
            BlackList.id,
            BlackList.name,
            BlackList.description,
            BlackListType.name.alias('black_list_type_label_'),
            BlackListType.id.alias('black_list_type_id_'),
            Reaction.name.alias('reaction_list_label_'),
            Reaction.id.alias('reaction_list_id_'),
            BlackList.is_deleted,
            BlackList.is_active,
            BlackList.create_timestamp,
            BlackList.update_timestamp,
            BlackList.version,
            BlackList.direction,
            BlackList.source,
            update_user.login.alias('update_user_label_'),
            create_user.login.alias('create_user_label_'),
            update_user.id.alias('update_user_id_'),
            create_user.id.alias('create_user_id_')
        ).join(BlackListType, on=(BlackListType.id == BlackList.black_list_type_id))\
         .join(Reaction, on=(Reaction.id == BlackList.reaction_list_id))\
            .join(update_user,
               on=(BlackList.update_user_id == update_user.id),
               join_type=JOIN.LEFT_OUTER)\
         .switch(BlackList) \
         .join(create_user,
               on=(BlackList.create_user_id == create_user.id),
               join_type=JOIN.LEFT_OUTER)\
         .where(BlackList.is_deleted != True,  # noqa: E712
                BlackList.id == blacklist_id).naive().get()

        return enum_processing(
            idlabels_to_dict(get_record_values(data)))
    except BlackList.DoesNotExist:
        raise ValueError("Record from black list by id='{}' not found"
                         .format(blacklist_id))


class BlackListTypeResource(object):
    def on_post(self, request, response):
        send_list_data(BlackListType.select(
            BlackListType.id, BlackListType.name.alias('label')), request, response)


class BlackListResource(object):
    def on_post(self, request, response):
        update_user = SystemUser.alias()
        create_user = SystemUser.alias()
        send_list_data(BlackList.select(
            BlackList.id,
            BlackList.name,
            BlackList.description,
            BlackListType.name.alias('black_list_type_label_'),
            BlackListType.id.alias('black_list_type_id_'),
            Reaction.name.alias('reaction_list_label_'),
            Reaction.id.alias('reaction_list_id_'),
            BlackList.is_deleted,
            BlackList.is_active,
            BlackList.create_timestamp,
            BlackList.update_timestamp,
            BlackList.version,
            BlackList.direction,
            BlackList.source,
            update_user.login.alias('update_user_label_'),
            create_user.login.alias('create_user_label_'),
            update_user.id.alias('update_user_id_'),
            create_user.id.alias('create_user_id_')
        ).join(BlackListType, on=(BlackListType.id == BlackList.black_list_type_id))
                       .join(Reaction, on=(Reaction.id == BlackList.reaction_list_id))
                       .join(update_user,
               on=(BlackList.update_user_id == update_user.id),
               join_type=JOIN.LEFT_OUTER)
                       .switch(BlackList)
                       .join(create_user,
               on=(BlackList.create_user_id == create_user.id),
               join_type=JOIN.LEFT_OUTER)
                       .where(BlackList.is_deleted != True),  # noqa: E712
                       request, response,
                       result_processing=enum_processing)


class BlackListByIdResource(object):
    def on_post(self, request, response):
        id = request.data['id']
        response.body = get_blacklist_by_id(id)


class CreateBlackListResource(object):
    def on_post(self, request, response):
        new_data = request.data
        record = BlackList()
        record.name = new_data['name']
        if new_data.get('description'):
            record.description = new_data['description']
        record.black_list_type_id = new_data['black_list_type']
        record.reaction_list_id = new_data.get('reaction_list') or '7f11f8bf-2a42-47f2-9531-811ad7bc7bf0'
        record.is_active = new_data['is_active']
        record.is_deleted = False
        record.direction = new_data['direction'] or 'ANY'
        record.system_component_id = get_current_component_id()
        record.create_user_id = request.userinfo['id']
        record.save(force_insert=True)
        response.body = get_record_values(record)
        event_logger.log(RuleControlEvent.TYPE_RULES_STATE_CHANGED,
                         acting_component_id=configuration.current_component_id,
                         Type='Blacklist',
                         Id=record.id,
                         AttributeChanged='created',
                         User=username,
                         UserId=user_id)


class UpdateBlackListResource(object):
    def on_post(self, request, response):
        new_data = request.data
        try:
            record = BlackList.get(BlackList.id == new_data['id'])
        except BlackList.DoesNotExist:
            raise ValueError('Record from black list by id=\'{}\' not found'
                             .format(new_data['id']))
        check_blacklist(record)
        attr = new_data.get('name')
        if attr is not None:
            record.name = attr
        attr = new_data.get('description')
        if attr is not None:
            record.description = attr
        attr = new_data.get('is_active')
        if attr is not None:
            record.is_active = attr
        attr = new_data.get('reaction_list')
        if attr is not None:
            record.reaction_list_id = attr
        attr = new_data.get('direction')
        if attr is not None:
            record.direction = attr
        record.update_user_id = request.userinfo['id']
        record.save()
        response.body = get_blacklist_by_id(new_data['id'])
        event_logger.log(RuleControlEvent.TYPE_RULES_STATE_CHANGED,
                         acting_component_id=configuration.current_component_id,
                         Type='Blacklist',
                         Id=record.id,
                         AttributeChanged='"{}"'.format(', '.join(attr for attr in sorted(new_data)
                                                                  if not attr.endswith('id'))),
                         User=username,
                         UserId=user_id)


class DeleteBlackListResource(object):
    def on_post(self, request, response):
        id_to_delete = request.data['id']

        try:
            record = BlackList.get(BlackList.id == id_to_delete)
        except BlackList.DoesNotExist:
            raise ValueError('Record from black list by id=\'{}\' not found'
                             .format(id_to_delete))
        check_blacklist(record)
        record.update_user_id = request.userinfo['id']
        record.is_deleted = True
        record.save()
        event_logger.log(RuleControlEvent.TYPE_RULES_STATE_CHANGED,
                         acting_component_id=configuration.current_component_id,
                         Type='Blacklist',
                         Id=record.id,
                         AttributeChanged='is_deleted',
                         User=username,
                         UserId=user_id)


class BlackListDirectionCatalog:
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_DIRECTION_BL.items():
            data.append({'id': k, 'label': v})

        response.body = data
