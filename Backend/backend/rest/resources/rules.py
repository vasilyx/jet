from backend.utils.configuration import configuration
from peewee import JOIN, R

from pluton_commons.common.utils import get_current_component_id, get_current_user_info
from pluton_commons.db.peewee.models import (
    Signature, SignatureClass, Reaction, SignatureBro, UpdateRegistry, SystemComponent
)
from pluton_commons.db.peewee.utils import get_record_values, build_order_by, build_where
from pluton_suricata_rule.client import SuricataRuleUpdaterClient

username, user_id = get_current_user_info(configuration)


class GetSuricataRulesListResource(object):
    def on_post(self, request, response):
        page_number = request.data.get('page')
        page_size = request.data.get('page_size')
        filters = request.data.get('filters', [])
        sort = request.data.get('sort', [])
        system_component_id = request.data.get('system_component_id',
                                               get_current_component_id(configuration))
        the_component = (SystemComponent
                         .get(SystemComponent.id == system_component_id))
        result = []
        total = 0
        ids = []
        rows = {}
        if the_component.component_type == 'SENSOR':
            answer = SuricataRuleUpdaterClient() \
                .send_command('get', system_component_id,
                              user=username, user_id=user_id,
                              page_number=page_number, page_size=page_size,
                              filters=filters, sort=sort)
            error = answer.get('error')
            if error:
                raise ValueError(error.get('message'))
            rows = answer['rule_state']['rows']
            total = answer['rule_state']['total']
            ids = list(rows)
        bro_query = (SignatureBro
                     .select(SignatureBro.id,
                             SignatureBro.sid,
                             SignatureBro.name.alias('signature_name'),
                             R("'Bro'").alias('source'),
                             R("NULL").alias('class_name'),
                             R("NULL").alias('priority'),
                             UpdateRegistry.version.alias('update_version'),
                             Reaction.name.alias('reaction_name'))
                     .join(UpdateRegistry,
                           join_type=JOIN.LEFT_OUTER,
                           on=(UpdateRegistry.id == SignatureBro.update_id,
                               UpdateRegistry.update_status == 'WORKING'))
                     .join(Reaction, on=(Reaction.id == SignatureBro.reaction_list_id)))
        if ids:
            bro_query = bro_query.where(SignatureBro.id << ids)
        else:
            bro_query = bro_query.where(
                build_where(bro_query.get_query_meta()[0], filters))
        sur_query = (Signature
                     .select(Signature.id,
                             Signature.sid,
                             Signature.name.alias('signature_name'),
                             R("'Suricata'").alias('source'),
                             SignatureClass.name.alias('class_name'),
                             Signature.priority,
                             UpdateRegistry.version.alias('update_version'),
                             Reaction.name.alias('reaction_name'))
                     .join(UpdateRegistry,
                           join_type=JOIN.LEFT_OUTER,
                           on=(UpdateRegistry.id == SignatureBro.update_id,
                               UpdateRegistry.update_status == 'WORKING'))
                     .join(SignatureClass,
                           on=(SignatureClass.id == Signature.signature_class_id),
                           join_type=JOIN.LEFT_OUTER)
                     .join(Reaction, on=(Reaction.id == Signature.reaction_list_id)))
        if ids:
            sur_query = sur_query.where(Signature.id << ids)
        else:
            sur_query = sur_query.where(
                build_where(sur_query.get_query_meta()[0], filters))
        query = bro_query | sur_query
        if not ids and page_number and page_size:
            total = query.count()
            query = query.paginate(page_number, page_size)
        if sort:
            query = query.order_by(*build_order_by(query.get_query_meta()[0], sort))

        for record in query.naive():
            row = get_record_values(record)
            if rows:
                row['is_active'] = rows[str(record.id)]['is_active']
            result.append(row)
        response.body = {'rows': result,
                         'total': total}


class UpdateSuricataRuleResource:
    def on_post(self, request, response):
        system_component_id = request.data['system_component_id']
        activity_to_update = {'rows': request.data['rule_state']}
        answer = SuricataRuleUpdaterClient() \
            .send_command('update',
                          system_component_id,
                          user=username, user_id=user_id,
                          rule_state=activity_to_update)
        response.body = answer
        error = answer.get('error')
        if error:
            raise ValueError(error.get('message'))


class UpdateAllSuricataRuleResource:
    def on_post(self, request, response):
        system_component_id = request.data['system_component_id']
        filters = request.data.get('filters', [])
        is_active_to_all = request.data.get('is_active_to_all')
        answer = SuricataRuleUpdaterClient() \
            .send_command('update_all',
                          system_component_id,
                          user=username, user_id=user_id,
                          is_active_to_all=is_active_to_all, filters=filters)
        response.body = answer
        error = answer.get('error')
        if error:
            raise ValueError(error.get('message'))
