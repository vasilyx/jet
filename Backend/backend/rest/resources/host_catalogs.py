from pluton_commons.db.peewee.models import (
    HostClass, HostType, Application,
    Protocol, TYPE_DIRECTION_TRAFFIC,
    TYPE_HOST_STATUS)
from pluton_commons.db.peewee.utils import send_list_data


class OsCatalogResource(object):
    def on_post(self, request, response):
        send_list_data(
            Application
            .select(
                Application.id,
                Application.application_name.alias('label'))
            .where(Application.software_type == 'OS'), request, response)


class ApplicationCatalogResource:
    def on_post(self, request, response):
        send_list_data(
            Application
            .select(
                Application.id,
                Application.application_name.alias('label')).
            where(Application.software_type == 'APPLICATION'), request, response)


class ServiceCatalogResource:
    def on_post(self, request, response):
        send_list_data(
            Application
            .select(
                Application.application_name.alias('label'),
                Application.id).
            where(Application.software_type == 'SERVICE'), request, response)


class HostClassCatalogResource(object):
    def on_post(self, request, response):
        send_list_data(HostClass.select(
            HostClass.name.alias('label'),
            HostClass.id
        ), request, response)


class HostTypeCatalogResource(object):
    def on_post(self, request, response):
        send_list_data(HostType.select(
            HostType.name.alias('label'),
            HostType.id,
            HostType.class_id
        ), request, response)


class ProtocolCatalogResource:
    def on_post(self, request, response):
        send_list_data(Protocol.select(
            Protocol.id,
            Protocol.name.alias('label')
        ), request, response)


class PortCatalogResource:
    def on_post(self, request, response):
        response.body = '[]'


class TrafficDirectionResource:
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_DIRECTION_TRAFFIC.items():
            data.append({'id': k, 'label': v})

        response.body = data


class HostStatusCatalogResource:
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_HOST_STATUS.items():
            data.append({'id': k, 'label': v})

        response.body = data
