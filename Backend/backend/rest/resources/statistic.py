from pluton_commons.db.peewee.utils import get_filter_value
from pluton_query_service.client import PlutonQueryClient
from pluton_query_service.common import OK_MSG, ERROR_MSG


class StatisticsTrafficResource:
    def on_post(self, request, response):
        group_by = request.data.get('group_by')
        filters = request.data.get('filters')
        sc_id = get_filter_value('system_component_id', filters)
        filters = [f for f in filters if f.get('field') != 'system_component_id']
        result = PlutonQueryClient().send_command(sc_id, 'statistics',
                                                  filters=filters,
                                                  group_by=group_by)
        if result['message'] == OK_MSG:
            total_count = result.get('data').get('rows_total')
            rows = result.get('data').get('rows')
            response.body = {'total': total_count, 'rows': rows}
        if result['message'] == ERROR_MSG:
            raise ValueError(result['error'])