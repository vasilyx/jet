from pluton_query_service.client import PlutonQueryClient
from pluton_query_service.common import OK_MSG, ERROR_MSG

from backend.utils.configuration import configuration
from pluton_commons.db.common import enum_to_dict, idlabels_to_dict

from pluton_commons.db.peewee.models import (
    TYPE_UPDATE_TYPES, TYPE_UPDATE_STATUS, TYPE_AGENT_STATE,
    UpdateRegistry, UpdateServerAgent
)
from pluton_commons.db.peewee.utils import send_list_data, get_record_values
from pluton_updater.update_server import UpdateServer


def _enum_processing(record):
    enum_to_dict(record, 'state', TYPE_AGENT_STATE)
    return record


def _update_agent_query():
    return UpdateServerAgent \
        .select(UpdateServerAgent.system_component_id,
                UpdateServerAgent.uname,
                UpdateServerAgent.dname,
                UpdateServerAgent.short_dname,
                UpdateServerAgent.parent_dname,
                UpdateServerAgent.address,
                UpdateServerAgent.resp,
                UpdateServerAgent.tel,
                UpdateServerAgent.email,
                UpdateServerAgent.apikey,
                UpdateServerAgent.state)


def _send_update_agent(cid, request, response):
    try:
        data = _update_agent_query() \
            .where(UpdateServerAgent.system_component_id == cid) \
            .naive() \
            .get()
        response.body = _enum_processing(idlabels_to_dict(get_record_values(data)))
    except UpdateServerAgent.DoesNotExist:
        raise ValueError('Нет такой записи в таблице агентов обновления')


class UpdateAgentResource(object):
    def on_post(self, request, response):
        send_list_data(_update_agent_query(),
                       request, response, result_processing=_enum_processing)


class GetCreateUpdateAgentResource(object):
    def on_post(self, request, response):
        new_data = request.data
        cid = new_data['system_component_id']
        try:
            UpdateServerAgent.get(UpdateServerAgent.system_component_id == cid)
        except UpdateServerAgent.DoesNotExist:
            UpdateServerAgent.create(**new_data)
        _send_update_agent(cid, request, response)


class EditUpdateAgentResource(object):
    def on_post(self, request, response):
        new_data = request.data
        cid = new_data.pop('system_component_id', None)
        try:
            UpdateServerAgent.update(**new_data) \
                .where(UpdateServerAgent.system_component_id == cid) \
                .execute()
        except UpdateServerAgent.DoesNotExist:
            raise ValueError('Нет такой записи в таблице агентов обновления')
        _send_update_agent(cid, request, response)


class RegisterUpdateAgentResource(object):
    def on_post(self, request, response):
        cid = request.data['system_component_id']
        valid, message = UpdateServer(configuration, cid) \
            .send_registration_request()
        if valid:
            message = 'Запрос на регистрацию отправлен на сервер обновлений'
        response.body = {'result': valid, 'message': message}


class CheckApiKeyResource(object):
    def on_post(self, request, response):
        cid = request.data['system_component_id']
        valid, message = UpdateServer(configuration, cid, request.data.get('api_key')) \
            .check_registration(True)
        if valid:
            message = 'Компонент зарегистрирован на сервере обновлений'
        response.body = {'result': valid, 'message': message}


class UpdateTypesResource(object):
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_UPDATE_TYPES.items():
            data.append({'id': k, 'label': v})
        response.body = data


class UpdateStatusResource(object):
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_UPDATE_STATUS.items():
            data.append({'id': k, 'label': v})
        response.body = data


class UpdatesListResource(object):
    def on_post(self, request, response):
        def enum_processing(record):
            enum_to_dict(record, 'update_type', TYPE_UPDATE_TYPES)
            enum_to_dict(record, 'update_status', TYPE_UPDATE_STATUS)
            return record

        sc_id = request.data.get('system_component_id')
        result = PlutonQueryClient().send_command([sc_id],
                                                  UpdateRegistry._meta.db_table,
                                                  filters=request.data.get('filters'),
                                                  sort=request.data.get('sort'),
                                                  page_number=request.data.get('page'),
                                                  page_size=request.data.get('page_size'))
        if result['message'] == OK_MSG:
            rows = [enum_processing(record) for record in result['data']['rows']]
            response.body = {'total': result['data']['rows_total'], 'rows': rows}
        elif result['message'] == ERROR_MSG:
            raise ValueError(result['error'])


class UpdateInstallResource(object):
    def on_post(self, request, response):
        # action = request.data['action']
        # update_id = request.data.get('update_id')
        # something
        pass
