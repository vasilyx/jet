import json
import dateutil.parser
from peewee import JOIN, SQL
from pluton_commons.db.common import idlabels_to_dict
from pluton_commons.db.infi.connection import clickhouse_db
from pluton_commons.db.infi.models import Alert
from pluton_commons.db.infi.utils import get_data, record_to_dict
from pluton_commons.db.peewee.models import (
    AlertType, AlertStatus, Signature, SignatureClass,
    AlertCategory, BlackListRecord, Host, HostType, Vulnerability)
from pluton_commons.db.peewee.utils import send_list_data, get_record_values, get_filter

SURICATA_ALERT_ID = 'c28643eb-8541-41c1-a45a-2c31574db95b'
BLACK_LIST_ID = 'efd52890-aa9e-457b-867d-9cd861cc5202'


class AlertListResource:
    def on_post(self, request, response):
        filters = request.data['filters']
        alert_type_filter = get_filter('alert_type_id_', filters)
        if alert_type_filter:
            alert_type_filter['field'] = 'alert_type_id'
        severity_filter = get_filter('severity_id_', filters)
        if severity_filter:
            severity_filter['field'] = 'severity_num'

        fields = {Alert.id, Alert.alert_timestamp,
                  Alert.alert_microseconds,
                  Alert.source_ip, Alert.target_ip,
                  Alert.severity_num.make_synonym('severity_id_'),
                  Alert.severity_name.make_synonym('severity_label_'),
                  Alert.system_component_id.make_synonym('sensor_id_'),
                  Alert.system_component_name.make_synonym('sensor_label_'),
                  Alert.monitored_system_id.make_synonym('controlled_system_id_'),
                  Alert.monitored_system_name.make_synonym('controlled_system_label_'),
                  Alert.alert_type_id.make_synonym('alert_type_id_'),
                  Alert.alert_type_name.make_synonym('alert_type_label_'),
                  Alert.rule_name.make_synonym('alert_rule_name')}

        data, total_count = get_data(Alert.objects_in(clickhouse_db).only(*[field.alias for field in fields]),
                                     request, fields)

        rows = []
        for record in data:
            record['alert_timestamp'] = dateutil.parser.parse(record['alert_timestamp']).replace(
                microsecond=int(record['alert_microseconds'])
            ).isoformat()
            del record['alert_microseconds']
            rows.append(idlabels_to_dict(record))

        response.body = {'total': total_count, 'rows': rows}


def _get_category_name(alert_type_id):
    query = AlertCategory.select(AlertCategory.name.alias('category_name')).join(AlertType, on=(
        AlertType.category_id == AlertCategory.id)).where(AlertType.id == alert_type_id)
    try:
        result = get_record_values(query.naive().get())
    except AlertCategory.DoesNotExist:
        return None
    else:
        return result.get('category_name')


def _get_signature_data(rule_id, system_component_id):
    if not system_component_id:
        system_component_id = ''
    query = Signature.select(
        SignatureClass.name.alias('signature_class'),
        Signature.action.alias('signature_action'),
        Signature.rule_tag.alias('signature_tag'),
        Signature.name.alias('signature_name'),
        SQL("'{}'".format(system_component_id)).alias('id_of_component')) \
        .join(SignatureClass, on=(Signature.signature_class_id == SignatureClass.id), join_type=JOIN.LEFT_OUTER) \
        .where(Signature.id == rule_id)

    try:
        result = get_record_values(query.naive().get())
        print(result)
    except Signature.DoesNotExist:
        return None
    else:
        return result


def _get_vulnerability_values(record):
    return dict(
        id=record.id,
        description=record.description,
        problem_type=record.problem_type,
        references=json.loads(record.refs) if record.refs else None,
        base_vector_v2=record.base_vector_v2,
        base_score_v2=str(record.base_score_v2) if record.base_score_v2 else None,
        base_vector_v3=record.base_vector_v3,
        base_score_v3=str(record.base_score_v3) if record.base_score_v3 else None
    )


class AlertTypeResource(object):
    def on_post(self, request, response):
        def unnecessary_field_remover(record):
            record.pop('category_id')
            return record

        send_list_data(AlertType.select(
            AlertType.id,
            AlertType.alert_type,
            AlertType.category_id,
            AlertType.description.alias('label')
        ), request, response, result_processing=unnecessary_field_remover)


class AlertStatusResource(object):
    def on_post(self, request, response):
        send_list_data(AlertStatus.select(
            AlertStatus.id,
            AlertStatus.name,
            AlertStatus.description.alias('label')
        ), request, response)


def get_vuln_scores(vuln_id):
    if vuln_id:
        result = (Vulnerability.select(Vulnerability.base_score_v2,
                                       Vulnerability.base_score_v3)
                               .where(Vulnerability.id == vuln_id))
        for vuln in result:
            return (str(vuln.base_score_v2),
                    str(vuln.base_score_v3))
    return None, None


class AlertGetResource(object):
    def on_post(self, request, response):
        new_data = request.data

        query = Alert.objects_in(clickhouse_db).only(Alert.id.alias, Alert.alert_timestamp.alias,
                                                     Alert.alert_microseconds.alias,
                                                     Alert.route_distance.alias, Alert.connection_timestamp.alias,
                                                     Alert.duration.alias, Alert.source_host_id.alias,
                                                     Alert.source_name.alias, Alert.source_port.alias,
                                                     Alert.source_internal.alias, Alert.source_ip.alias,
                                                     Alert.target_ip.alias, Alert.target_host_id.alias,
                                                     Alert.target_name.alias, Alert.target_port.alias,
                                                     Alert.target_internal.alias,
                                                     Alert.alert_type_id.alias,
                                                     Alert.alert_type_name.alias,
                                                     Alert.protocol_name.alias,
                                                     Alert.system_component_id.alias,
                                                     Alert.system_component_name.alias,
                                                     Alert.system_component_ip.alias,
                                                     Alert.monitored_system_id.alias,
                                                     Alert.monitored_system_name.alias,
                                                     Alert.severity_num.alias,
                                                     Alert.severity_name.alias,
                                                     Alert.suricata_rule_id.alias,
                                                     Alert.rule_name.alias,
                                                     Alert.rule_description.alias,
                                                     Alert.application_name.alias,
                                                     Alert.application_type.alias,
                                                     Alert.application_version.alias,
                                                     Alert.application_protocol_name.alias,
                                                     Alert.url.alias,
                                                     Alert.host_id.alias,
                                                     Alert.host_ip.alias,
                                                     Alert.os_type.alias,
                                                     Alert.new_os_name.alias,
                                                     Alert.os_version_inference.alias,
                                                     Alert.route_distance.alias,
                                                     Alert.service_name.alias,
                                                     Alert.black_list_id.alias,
                                                     Alert.black_list_record_id.alias,
                                                     Alert.file_name.alias,
                                                     Alert.alert_category_name.alias,
                                                     Alert.vulnerability_id.alias,
                                                     Alert.alert_trust.alias,
                                                     Alert.alert_count.alias,
                                                     Alert.alert_timestamp_start.alias,
                                                     Alert.alert_microseconds_start.alias,
                                                     Alert.alert_timestamp_end.alias,
                                                     Alert.alert_microseconds_end.alias,
                                                     ).filter(
            id__eq=new_data['id'])
        for record in query:
            record = record_to_dict(record)
            rule_id = record.get(Alert.suricata_rule_id.alias)
            alert_type_id = record.get(Alert.alert_type_id.alias)
            system_component_id = record.get(Alert.system_component_id.alias)

            record['alert_timestamp'] = dateutil.parser.parse(record['alert_timestamp']).replace(
                microsecond=int(record['alert_microseconds'])
            ).isoformat()
            record['alert_timestamp_start'] = dateutil.parser.parse(record['alert_timestamp_start']).replace(
                microsecond=int(record['alert_microseconds_start'])
            ).isoformat()
            del record['alert_microseconds_start']
            record['alert_timestamp_end'] = dateutil.parser.parse(record['alert_timestamp_end']).replace(
                microsecond=int(record['alert_microseconds_end'])
            ).isoformat()
            del record['alert_microseconds_end']
            record.update({'severity_id_': record.get(Alert.severity_num.alias),
                           'severity_label_': record.get(Alert.severity_name.alias)})
            del record[Alert.severity_name.alias]
            del record[Alert.severity_num.alias]

            record.update({'alert_type': record.get(Alert.alert_type_name.alias)})
            del record[Alert.alert_type_name.alias]

            record.update({'category_name': record.get(Alert.alert_category_name.alias)})
            del record[Alert.alert_category_name.alias]

            record.update({'sensor_name': record.get(Alert.system_component_name.alias)})
            record.update({'sensor_ip': record.get(Alert.system_component_ip.alias)})
            del record[Alert.system_component_name.alias]
            del record[Alert.system_component_ip.alias]

            record.update({'controlled_system': record.get(Alert.monitored_system_name.alias)})
            del record[Alert.monitored_system_name.alias]

            record.update({'host_id_': record.get(Alert.host_id.alias),
                           'host_label_': record.get(Alert.host_ip.alias)})
            del record[Alert.host_id.alias]
            del record[Alert.host_ip.alias]

            if record.get(Alert.source_host_id.alias):
                query_source_type = Host.select(HostType.name)\
                              .join(HostType, on=(Host.host_type_id == HostType.id))\
                              .where(Host.id == record.get(Alert.source_host_id.alias))
                try:
                    source_type = query_source_type.naive().get().name
                    record.update({'source_type': source_type})
                except Host.DoesNotExist:
                    pass

            if record.get(Alert.target_host_id.alias):
                query_target_type = Host.select(HostType.name)\
                              .join(HostType, on=(Host.host_type_id == HostType.id))\
                              .where(Host.id == record.get(Alert.target_host_id.alias))
                try:
                    target_type = query_target_type.naive().get().name
                    record.update({'target_type': target_type})
                except Host.DoesNotExist:
                    pass

            #ДЛЯ СУРИКАТНЫХ СИГНАТУР
            if rule_id and alert_type_id == SURICATA_ALERT_ID:
                data = _get_signature_data(rule_id, system_component_id)
                if data:
                    form_data = idlabels_to_dict(data)
                    record.update(form_data)

            record.update({'transport_protocol': record.get(Alert.protocol_name.alias)})
            del record[Alert.protocol_name.alias]

            record.update({'application_url': record.get(Alert.url.alias)})
            del record[Alert.url.alias]

            vuln_id = record.get('vulnerability_id')
            (record['base_score_v2'],
             record['base_score_v3']) = get_vuln_scores(vuln_id)

            bl_rec_id = record.get('black_list_record_id')
            if bl_rec_id:
                rec = BlackListRecord.get(BlackListRecord.id == bl_rec_id)
                record['black_list_value'] = rec.value

            response.body = idlabels_to_dict(record)
            return

        response.body = []
