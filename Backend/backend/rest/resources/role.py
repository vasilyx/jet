from backend.utils.configuration import configuration
from backend.utils.logger import logger
from peewee import JOIN
from pluton_audit import EventLogger
from pluton_audit.events import RoleAuditEvent
from pluton_commons.common.utils import get_current_component_id
from pluton_commons.db.common import (
    enum_to_dict
)
from pluton_commons.db.peewee.models import (
    SystemRole, SystemPrivilege, SystemRolePrivilege, TYPE_ROLE_CREATION,
    SystemComponent, SystemUser
)
from pluton_commons.db.peewee.utils import (
    send_list_data, get_record_values, cast, aggregate_to_array
)

event_logger = EventLogger(configuration, logger)


def group_privileges_in_record(record):
    record['privileges'] = []
    if record['privileges_id'] and record['privileges_label']:
        for idx, label in zip(record['privileges_id'],
                              record['privileges_label']):
            record['privileges'].append({'id': idx, 'label': label})
    record.pop('privileges_id', None)
    record.pop('privileges_label', None)
    enum_to_dict(record, 'creation_type', TYPE_ROLE_CREATION)
    return record


def get_role_by_id(role_id):
    create_sus = SystemComponent.alias()
    source_sus = SystemComponent.alias()

    query = SystemRole.select(
            SystemRole.id,
            SystemRole.full_name,
            SystemRole.short_name,
            create_sus.id.alias('create_sus_id'),
            source_sus.id.alias('source_sus_id'),
            create_sus.name.alias('create_sus_name'),
            source_sus.name.alias('source_sus_name'),
            SystemRole.creation_type,
            SystemRole.create_timestamp,
            SystemRole.update_timestamp,
            SystemRole.description,
            aggregate_to_array(SystemPrivilege.name).alias('privileges_label'),
            aggregate_to_array(cast(SystemPrivilege.id, 'character varying'))
                .alias('privileges_id')) \
        .join(create_sus,
              join_type=JOIN.LEFT_OUTER,
              on=SystemUser.create_system_component_id) \
        .switch(SystemRole) \
        .join(source_sus,
              join_type=JOIN.LEFT_OUTER,
              on=SystemUser.source_system_component_id) \
        .switch(SystemRole) \
        .join(SystemRolePrivilege,
              join_type=JOIN.LEFT_OUTER,
              on=SystemRolePrivilege.system_role_id) \
        .join(SystemPrivilege,
              join_type=JOIN.LEFT_OUTER,
              on=SystemRolePrivilege.system_privilege_id) \
        .where(SystemRole.is_deleted != True, SystemRole.id == role_id) \
        .group_by(SystemRole, create_sus, source_sus)  # noqa: E712

    try:
        result = query.naive().get()
    except SystemRole.DoesNotExist:
        raise ValueError('Роль с id=\'{}\' не найдена'.format(role_id))

    record = get_record_values(result)
    group_privileges_in_record(record)
    return record


class RoleListResource(object):
    def on_post(self, request, response):
        create_sus = SystemComponent.alias()
        source_sus = SystemComponent.alias()

        send_list_data(SystemRole.select(
                SystemRole.id,
                SystemRole.full_name,
                SystemRole.short_name,
                create_sus.id.alias('create_sus_id'),
                source_sus.id.alias('source_sus_id'),
                create_sus.name.alias('create_sus_name'),
                source_sus.name.alias('source_sus_name'),
                SystemRole.creation_type,
                SystemRole.create_timestamp,
                SystemRole.update_timestamp,
                SystemRole.description,
                aggregate_to_array(SystemPrivilege.name).alias(
                        'privileges_label'),
                aggregate_to_array(cast(SystemPrivilege.id,
                                        'character varying'))
                    .alias('privileges_id'))
                       .join(create_sus,
                             join_type=JOIN.LEFT_OUTER,
                             on=SystemUser.create_system_component_id)
                       .switch(SystemRole)
                       .join(source_sus,
                             join_type=JOIN.LEFT_OUTER,
                             on=SystemUser.source_system_component_id)
                       .switch(SystemRole)
                       .join(SystemRolePrivilege,
                             join_type=JOIN.LEFT_OUTER,
                             on=SystemRolePrivilege.system_role_id)
                       .join(SystemPrivilege,
                             join_type=JOIN.LEFT_OUTER,
                             on=SystemRolePrivilege.system_privilege_id)
                       .where(SystemRole.is_deleted != True),  # noqa: E712
                       request,
                       response,
                       group_by=(SystemRole, create_sus, source_sus),
                       result_processing=group_privileges_in_record)


class RoleGetResource(object):
    def on_post(self, request, response):
        role_id = request.data.get('id')
        record = get_role_by_id(role_id)
        response.body = record


class RoleCreateResource(object):
    def on_post(self, request, response):
        new_data = request.data

        record = SystemRole.create(
                full_name=new_data['full_name'],
                short_name=new_data['short_name'],
                creation_type='LOCAL',
                source_system_component_id=get_current_component_id(),
                create_system_component_id=get_current_component_id()
        )

        if new_data.get('description'):
            record.description = new_data['description']

        record.save()

        for privilege_id in new_data['privileges']:
            SystemRolePrivilege.insert(
                    system_role_id=record.id,
                    system_privilege_id=privilege_id).execute()
        event_logger.log(RoleAuditEvent.TYPE_ROLE_NEW,
                         role_id=record.id,
                         short_name=new_data['short_name'])
        response.body = get_role_by_id(record.id)


class RoleUpdateResource(object):
    def on_post(self, request, response):
        new_data = request.data

        try:
            record = SystemRole.get(
                    SystemRole.id == new_data['id'],
                    SystemRole.is_deleted != True)  # noqa: E712
        except SystemRole.DoesNotExist:
            raise ValueError('Роль с id=\'{}\' не найдена'
                             .format(new_data['id']))

        if new_data.get('description') is not None:
            record.description = new_data['description']

        if new_data.get('full_name') is not None:
            record.full_name = new_data['full_name']

        if new_data.get('short_name') is not None:
            record.short_name = new_data['short_name']

        if new_data.get('privileges'):
            SystemRolePrivilege.delete().where(
                    SystemRolePrivilege.system_role_id == new_data['id']).execute()
            for privilege_id in new_data['privileges']:
                SystemRolePrivilege.insert(
                        system_role_id=record.id,
                        system_privilege_id=privilege_id).execute()
        event_logger.log(RoleAuditEvent.TYPE_ROLE_CHANGED_PERMISSIONS,
                         role_id=record.id,
                         short_name=record.short_name)
        record.save()
        response.body = get_role_by_id(record.id)


class RoleDeleteResource(object):
    def on_post(self, request, response):
        new_data = request.data

        try:
            record = SystemRole.get(
                    SystemRole.id == new_data['id'],
                    SystemRole.is_deleted != True)  # noqa: E712
        except SystemRole.DoesNotExist:
            raise ValueError('Роль с id=\'{}\' не найдена'
                             .format(new_data['id']))
        event_logger.log(RoleAuditEvent.TYPE_ROLE_DEL,
                         role_id=record.id,
                         short_name=record.short_name)
        record.is_deleted = True
        record.save()


class PrivilegeCatalogResource(object):
    def on_post(self, request, response):
        send_list_data(SystemPrivilege.select(
                SystemPrivilege.id,
                SystemPrivilege.name.alias('label'),
                SystemPrivilege.description
        ), request, response)
