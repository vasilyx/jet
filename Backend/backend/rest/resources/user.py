import os
import subprocess
from datetime import datetime as dt, timedelta
from subprocess import Popen, PIPE

from backend.utils.common import get_label_id
from backend.utils.configuration import configuration
from backend.utils.logger import logger
from peewee import JOIN, fn, datetime
from pluton_audit import EventLogger
from pluton_audit.events import UserAuditEvent
from pluton_commons.common.utils import get_current_component_id
from pluton_commons.db.common import (
    enum_to_dict
)
from pluton_commons.db.peewee.models import (
    SystemUser, SystemUserRole, SystemRole, SystemComponent,
    SystemUserPersonalInfo, TYPE_USER_CREATION, TYPE_USER_IDENTITY_DOCUMENT
)
from pluton_commons.db.peewee.utils import (
    send_list_data, cast, aggregate_to_array,
    get_record_values, get_list_data
)

ID_PRIV_CREATE_USER = '7ef5bcf0-13f9-4bbd-9dfa-5d2a16be7648'

event_logger = EventLogger(configuration, logger)


def get_password_expiration(login):
    proc = subprocess.Popen(
        ['/usr/bin/sudo', '/usr/bin/pluton', 'user', 'info', login],
        env={'LANG': ''}, stdout=subprocess.PIPE)
    out, _ = proc.communicate()
    for line in out.decode().splitlines():
        if line.strip().startswith('Password expires'):
            _, exp = line.split(':')
            if exp.strip() == 'never':
                return 'never'
            elif exp.strip() == 'password must be changed':
                return 'already'
            else:
                return dt.strptime(exp.strip(), '%b %d, %Y').date().isoformat()


def group_roles_in_record(record):
    record['roles'] = []
    if record['roles_id'] and record['roles_label']:
        for idx, label in zip(record['roles_id'], record['roles_label']):
            record['roles'].append({'id': idx, 'label': label})
    record.pop('roles_id', None)
    record.pop('roles_label', None)

    enum_to_dict(record, 'creation_type', TYPE_USER_CREATION)
    return record


def get_user_by_id(user_id):
    create_sus = SystemComponent.alias()
    source_sus = SystemComponent.alias()

    query = SystemUser.select(
        SystemUser.id,
        SystemUser.login,
        SystemUser.creation_type,
        SystemUser.is_blocked,
        SystemUser.register_timestamp,
        SystemUser.expiration_timestamp,
        SystemUser.first_login_timestamp,
        SystemUser.last_login_timestamp,
        SystemUser.create_timestamp,
        SystemUser.update_timestamp,
        SystemUser.email,
        SystemUser.description,
        SystemUser.version,
        SystemUser.is_deleted,
        create_sus.id.alias('create_sus_id'),
        source_sus.id.alias('source_sus_id'),
        create_sus.name.alias('create_sus_name'),
        source_sus.name.alias('source_sus_name'),
        aggregate_to_array(SystemRole.full_name).alias('roles_label'),
        aggregate_to_array(cast(
            SystemRole.id, 'character varying')).alias('roles_id'),
        SystemUserPersonalInfo.family_name.alias(
            'personal_info_family_name'),
        SystemUserPersonalInfo.first_name.alias(
            'personal_info_first_name'),
        SystemUserPersonalInfo.patronymic_name.alias(
            'personal_info_patronymic_name'),
        SystemUserPersonalInfo.position.alias(
            'personal_info_position'),
        SystemUserPersonalInfo.rank.alias(
            'personal_info_rank'),
        SystemUserPersonalInfo.department.alias(
            'personal_info_department'),
        SystemUserPersonalInfo.identity_document.alias(
            'personal_info_identity_document'),
        SystemUserPersonalInfo.identity_document_type.alias(
            'personal_info_identity_document_type'),
        SystemUserPersonalInfo.description.alias(
            'personal_info_description'),
        SystemUserPersonalInfo.update_timestamp.alias(
            'personal_info_update_timestamp')) \
        .join(SystemUserPersonalInfo,
              join_type=JOIN.LEFT_OUTER,
              on=(SystemUser.id == SystemUserPersonalInfo.id)) \
        .join(create_sus,
              join_type=JOIN.LEFT_OUTER,
              on=(create_sus.id == SystemUser.create_system_component_id)) \
        .switch(SystemUser) \
        .join(source_sus,
              join_type=JOIN.LEFT_OUTER,
              on=(source_sus.id == SystemUser.source_system_component_id)) \
        .switch(SystemUser) \
        .join(SystemUserRole,
              join_type=JOIN.LEFT_OUTER,
              on=SystemUserRole.system_user_id) \
        .join(SystemRole,
              join_type=JOIN.LEFT_OUTER,
              on=((SystemUserRole.system_role_id == SystemRole.id) &
                  (SystemRole.is_deleted != True))) \
        .where(SystemUser.id == user_id, SystemUser.is_deleted != True) \
        .group_by(SystemUser,
                  SystemUserPersonalInfo,
                  create_sus,
                  source_sus)  # noqa: E712

    try:
        result = query.naive().get()
    except SystemUser.DoesNotExist:
        raise ValueError('Пользователь с id=\'{}\' не найден'.format(user_id))

    record = get_record_values(result)
    group_roles_in_record(record)

    record['maclabel'] = get_label_id(record['login'])
    user_fail, unlock_time = get_user_failures(record['login'])
    record['failures'] = user_fail
    record['is_blocked'] = record['is_blocked'] | (
            (record['failures'] >= get_max_failures()) & (dt.now() < unlock_time))
    record['password_expires_at'] = get_password_expiration(record['login'])

    enum_to_dict(record,
                 'personal_info_identity_document_type',
                 TYPE_USER_IDENTITY_DOCUMENT)

    personal_info = {}
    for key in list(record.keys()):
        if 'personal_info' in key:
            personal_info[key.replace('personal_info_', '')] = record[key]
            del record[key]
    record['personal_info'] = personal_info
    return record


def get_user_failures(login):
    process = subprocess.Popen(['/usr/bin/sudo', '/usr/bin/pluton', 'user', 'failures', login], stdout=subprocess.PIPE)
    process.stdout.readline()
    result = process.stdout.readline()
    if len(result) > 0:
        count = int(result.split()[1].decode('utf8'))
    else:
        count = 0
    if len(result.split()) >= 4:
        time_unlock = dt.strptime("{} {}".format(result.split()[2].decode('utf8'), result.split()[3].decode('utf8')),
                                  "%m/%d/%y %H:%M:%S") + timedelta(seconds=get_user_unlock_period())
    else:
        time_unlock = dt(1900, 1, 1)
    return count, time_unlock


def get_max_failures():
    result = 99999
    config = open('/etc/pam.d/common-auth', 'r')
    for line_param in [line.strip() for line in config]:
        if 'pam_tally2.so' in line_param:
            for param in line_param.split():
                if 'deny=' in param:
                    result = param[5:]
    return int(result)


def get_user_unlock_period():
    result = 0
    config = open('/etc/pam.d/common-auth', 'r')
    for line_param in [line.strip() for line in config]:
        if 'pam_tally2.so' in line_param:
            for param in line_param.split():
                if 'unlock_time=' in param:
                    result = param[12:]
    return int(result)


def user_unlock(user_name):
    process = Popen(['/usr/bin/sudo', '/usr/bin/pluton',
                     'user', 'unlock', user_name], stdin=PIPE, stdout=PIPE, stderr=PIPE,
                    env=dict(os.environ, LANG='ru_RU.UTF8'))
    process_stdout, process_stderr = process.communicate(timeout=20)
    return_code = process.returncode
    if return_code != 0:
        raise ValueError(
            'Произошла ошибка при разблокировании пользователя '
            '{}, ошибка: {}'.format(user_name, process_stderr.decode()))


class UserListResource(object):
    def on_post(self, request, response):
        create_sus = SystemComponent.alias()
        source_sus = SystemComponent.alias()

        result = get_list_data(SystemUser.select(
            SystemUser.id,
            SystemUser.login,
            SystemUser.creation_type,
            SystemUser.is_blocked,
            SystemUser.register_timestamp,
            SystemUser.expiration_timestamp,
            fn.Count(SystemUser.id).alias('roles_count'),
            create_sus.id.alias('create_sus_id'),
            source_sus.id.alias('source_sus_id'),
            create_sus.name.alias('create_sus_name'),
            source_sus.name.alias('source_sus_name'),
            aggregate_to_array(SystemRole.full_name).alias('roles_label'),
            aggregate_to_array(cast(
                SystemRole.id, 'character varying')).alias('roles_id')
        ).join(create_sus,
               join_type=JOIN.LEFT_OUTER,
               on=SystemUser.create_system_component_id)
                               .switch(SystemUser)
                               .join(source_sus,
                                     join_type=JOIN.LEFT_OUTER,
                                     on=SystemUser.source_system_component_id)
                               .switch(SystemUser)
                               .join(SystemUserRole,
                                     join_type=JOIN.LEFT_OUTER,
                                     on=SystemUserRole.system_user_id)
                               .join(SystemRole, join_type=JOIN.LEFT_OUTER,
                                     on=((SystemUserRole.system_role_id == SystemRole.id) &
                                         (SystemRole.is_deleted != True)))  # noqa: E712
                               .where(SystemUser.is_deleted != True),
                               request, response,
                               group_by=(SystemUser, create_sus, source_sus),
                               result_processing=group_roles_in_record
                               )

        for record in result['rows']:
            user_fail, unlock_time = get_user_failures(record['login'])
            record['failures'] = user_fail
            record['is_blocked'] = record['is_blocked'] | (
                    (record['failures'] >= get_max_failures()) & (dt.now() < unlock_time))
            record['password_expires_at'] = get_password_expiration(record['login'])

        response.body = result


class UserGetResource(object):
    def on_post(self, request, response):
        user_id = request.data.get('id', request.userinfo['id'])
        record = get_user_by_id(user_id)
        response.body = record


class UserCreateResource(object):
    def create_os_db_user(self, user_name, password, mark):

        process = Popen(['/usr/bin/sudo', '/usr/bin/pluton',
                         'user', 'create', user_name,
                         '-p', password, '-m', mark], stdin=PIPE, stdout=PIPE, stderr=PIPE,
                        env=dict(os.environ, LANG='ru_RU.UTF8'))
        process_stdout, process_stderr = process.communicate(timeout=20)
        return_code = process.returncode
        if return_code != 0:
            raise ValueError(
                'Произошла ошибка при создании пользователя '
                '{} в ОС или БД, ошибка: {}'.format(user_name, process_stderr.decode()))

    def on_post(self, request, response):
        new_data = request.data

        record = SystemUser.create(
            login=new_data['login'],
            email=new_data['email'],
            expiration_timestamp=new_data['expiration_timestamp'],
            source_system_component_id=get_current_component_id(),
            create_system_component_id=get_current_component_id(),
            creation_type='LOCAL',
            register_timestamp=datetime.datetime.now(),
            description=new_data.get('description'),
            is_blocked=new_data.get('is_blocked', False))

        password = new_data['password']
        mark = new_data['maclabel']
        try:
            self.create_os_db_user(new_data['login'], password, mark)
        except Exception as error:
            record.delete_instance()
            record.save()
            raise error

        p_info = new_data.get('personal_info')
        if p_info and len(p_info) > 1:
            record_personal_info = SystemUserPersonalInfo()
            record_personal_info.id = record.id
            if p_info.get('family_name'):
                record_personal_info.family_name = p_info.get('family_name')
            if p_info.get('first_name'):
                record_personal_info.first_name = p_info.get('first_name')
            if p_info.get('patronymic_name'):
                record_personal_info.patronymic_name = \
                    p_info.get('patronymic_name')
            if p_info.get('position'):
                record_personal_info.position = p_info.get('position')
            if p_info.get('rank'):
                record_personal_info.rank = p_info.get('rank')
            if p_info.get('department'):
                record_personal_info.department = p_info.get('department')
            if p_info.get('identity_document_type'):
                record_personal_info.identity_document_type = \
                    p_info.get('identity_document_type')
            if p_info.get('identity_document'):
                record_personal_info.identity_document = \
                    p_info.get('identity_document')
            if p_info.get('description'):
                record_personal_info.description = p_info.get('description')
            record_personal_info.save(force_insert=True)
        for role_id in new_data['roles']:
            SystemUserRole.insert(system_user_id=record.id,
                                  system_role_id=role_id).execute()
        event_logger.log(UserAuditEvent.TYPE_USER_NEW,
                         user_id=record.id,
                         login=new_data['login'])
        response.body = get_user_by_id(record.id)


class UserUpdateResource(object):
    def on_post(self, request, response):
        new_data = request.data
        user_id = new_data.get('id', request.userinfo['id'])
        try:
            record = SystemUser.get(
                SystemUser.id == user_id,
                SystemUser.is_deleted != True)  # noqa: E712
        except SystemUser.DoesNotExist:
            raise ValueError('Пользователь с id=\'{}\' не найден'
                             .format(user_id))
        old_attributes_changed = False
        if 'expiration_timestamp' in new_data:
            attr = new_data.get('expiration_timestamp')
            old_attributes_changed |= record.expiration_timestamp != attr
            record.expiration_timestamp = attr
        attr = new_data.get('email')
        if attr is not None:
            old_attributes_changed |= record.email != attr
            record.email = attr
        attr = new_data.get('description')
        if attr is not None:
            old_attributes_changed |= record.description != attr
            record.description = attr
        attr = new_data.get('is_blocked')
        user_fail, unlock_time = get_user_failures(record.login)
        record.is_blocked = record.is_blocked | ((user_fail >= get_max_failures()) & (dt.now() < unlock_time))
        if attr is not None:
            old_blocked_state = record.is_blocked
            record.is_blocked = attr
            if old_blocked_state != record.is_blocked:
                if record.is_blocked:
                    event_logger.log(UserAuditEvent.TYPE_USER_BLOCKED,
                                     user_id=record.id,
                                     login=record.login)
                else:
                    user_unlock(record.login)
                    event_logger.log(UserAuditEvent.TYPE_USER_UNBLOCKED,
                                     user_id=record.id,
                                     login=record.login)
        p_info = new_data.get('personal_info')
        if p_info and len(p_info) > 1:
            record_personal_info, create = \
                SystemUserPersonalInfo.get_or_create(id=record.id)
            attr = p_info.get('family_name')
            if attr is not None:
                old_attributes_changed |= \
                    record_personal_info.family_name != attr
                record_personal_info.family_name = attr
            attr = p_info.get('first_name')
            if attr is not None:
                old_attributes_changed |= \
                    record_personal_info.first_name != attr
                record_personal_info.first_name = attr
            attr = p_info.get('patronymic_name')
            if attr is not None:
                old_attributes_changed |= \
                    record_personal_info.patronymic_name != attr
                record_personal_info.patronymic_name = attr
            attr = p_info.get('position')
            if attr is not None:
                old_attributes_changed |= record_personal_info.position != attr
                record_personal_info.position = attr
            attr = p_info.get('rank')
            if attr is not None:
                old_attributes_changed |= record_personal_info.rank != attr
                record_personal_info.rank = attr
            attr = p_info.get('department')
            if attr is not None:
                old_attributes_changed |= \
                    record_personal_info.department != attr
                record_personal_info.department = attr
            attr = p_info.get('identity_document_type')
            if attr is not None:
                old_attributes_changed |= \
                    record_personal_info.identity_document_type != attr
                record_personal_info.identity_document_type = attr
            attr = p_info.get('identity_document')
            if attr is not None:
                old_attributes_changed |= \
                    record_personal_info.identity_document != attr
                record_personal_info.identity_document = attr
            attr = p_info.get('description')
            if attr is not None:
                old_attributes_changed |= \
                    record_personal_info.description != attr
                record_personal_info.description = attr
            record_personal_info.save()
        if old_attributes_changed:
            event_logger.log(UserAuditEvent.TYPE_USER_CHANGED_ATTRS,
                             user_id=record.id,
                             login=record.login)

        if new_data.get('roles'):
            SystemUserRole.delete().where(
                SystemUserRole.system_user_id == new_data['id']).execute()
            for role_id in new_data['roles']:
                SystemUserRole.insert(system_user_id=record.id,
                                      system_role_id=role_id).execute()
            event_logger.log(UserAuditEvent.TYPE_USER_CHANGED_ROLES,
                             user_id=record.id,
                             login=record.login)

        record.save()

        if new_data.get('password') is not None:
            process = Popen(['/usr/bin/sudo', '/usr/bin/pluton',
                             'user', 'modify', record.login,
                             '-p', new_data.get('password')], stdin=PIPE, stdout=PIPE, stderr=PIPE,
                            env=dict(os.environ, LANG='ru_RU.UTF8'))
            process_stdout, process_stderr = process.communicate(timeout=10)
            return_code = process.returncode
            if return_code != 0:
                raise ValueError(process_stderr.decode())

        if new_data.get('maclabel') is not None:
            result = subprocess.call(['/usr/bin/sudo', '/usr/bin/pluton',
                                      'user', 'modify', record.login,
                                      '-m', new_data.get('maclabel')])
            if result > 0:
                raise ValueError('Не удалось изменить метку')

        response.body = get_user_by_id(record.id)


class UserDeleteResource(object):
    def delete_os_db_user(self, user_name):

        process = Popen(['/usr/bin/sudo', '/usr/bin/pluton',
                         'user', 'delete', user_name], stdin=PIPE, stdout=PIPE, stderr=PIPE,
                        env=dict(os.environ, LANG='ru_RU.UTF8'))
        process_stdout, process_stderr = process.communicate(timeout=20)
        return_code = process.returncode
        if return_code != 0:
            raise ValueError(
                'Произошла ошибка при удалении пользователя '
                '{} в ОС или БД, ошибка: {}'.format(user_name, process_stderr.decode()))

    def on_post(self, request, response):
        new_data = request.data

        try:
            record = SystemUser.get(
                SystemUser.id == new_data['id'],
                SystemUser.is_deleted != True)  # noqa: E712
            self.delete_os_db_user(record.login)
        except SystemUser.DoesNotExist:
            raise ValueError('Пользователь с id=\'{}\' не найден'
                             .format(new_data['id']))
        event_logger.log(UserAuditEvent.TYPE_USER_DEL,
                         user_id=record.id,
                         login=record.login)
        record.is_deleted = True
        record.save()


class UserCreateTypeListResource(object):
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_USER_CREATION.items():
            data.append({'id': k, 'label': v})

        response.body = data


class UserRoleListResource(object):
    def on_post(self, request, response):
        send_list_data(SystemRole.select(
            SystemRole.id,
            SystemRole.full_name.alias('label')
        ).where(SystemRole.is_deleted != True),
                       request, response)  # noqa: E712


class UserIdentityDocumentTypeListResource(object):
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_USER_IDENTITY_DOCUMENT.items():
            data.append({'id': k, 'label': v})

        response.body = data
