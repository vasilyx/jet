import ipaddress
import json
from infi.clickhouse_orm.query import Q
from pluton_updater.control_client import PlutonUpdaterClient
from peewee import SQL, JOIN

from pluton_commons.db.common import (
    enum_to_dict, idlabels_to_dict
)
from pluton_commons.db.peewee.connection import scs_db
from pluton_commons.db.peewee.models import (
    Host, Ip, Location, HostUsers, Users,
    Application, HostApplication, SystemComponent, MonitoredSystem,
    Protocol, HostApplicationVulnerability,
    TYPE_HOST_STATUS, TYPE_REC_STATUS,
    Vulnerability, TYPE_SOFTWARE_TYPES, SystemComponentConnection, BlackList, BlackListType,
    BlackListRecord, TYPE_DIRECTION_BL)
from pluton_commons.db.infi.models import Alert
from pluton_commons.db.infi.connection import clickhouse_db
from pluton_commons.db.infi.utils import get_data

from pluton_commons.db.peewee.utils import (
    get_record_values, send_list_data,
    aggregate_to_array, cast, distinct, get_filter)
from pluton_query_service.client import PlutonQueryClient
from pluton_query_service.common import ERROR_MSG, OK_MSG

HEURISTIC_ALERTS_UUID = [
    '62b38077-0517-4eea-b18f-ed52e7697a69',
    '909280bb-388b-4347-bf3e-28ed679bb6eb',
    '3e542d92-8cab-11e7-bb31-be2e44b06b34',
    '09caa12d-acde-4f84-adc6-d524373d6af9'
]
SIGNATURE_ALERTS_UUID = [
    '68c94d92-9f2c-4928-8862-6fb847153b80',
    'c28643eb-8541-41c1-a45a-2c31574db95b'
]



host_aggregated_query_fields = [
    Host.id,
    SQL("0").alias('vulnerabilities'),
    SQL("0").alias('alerts'),
    SQL("0").alias('events'),
    SQL("0").alias('last_activity'),
    SQL("0").alias('services'),
    SQL("0").alias('intense'),
    SQL("0").alias('connections'),

    Location.latitude,
    Location.longitude,
    SQL("0").alias('statistic_model')
]


class HostProfileResource(object):
    def on_post(self, request, response):

        host_id = request.data['id']

        def records_processing(record):
            enum_to_dict(record, 'host_status', TYPE_HOST_STATUS)
            record['host_ip'] = [str(ipaddress.IPv4Address(int(x))) for x in record['host_ip']]
            record['host_os'] = []
            if record['host_os_id_'] and record['host_os_label_']:
                for idx, label in zip(record['host_os_id_'], record['host_os_label_']):
                    record['host_os'].append({'id': idx, 'label': label})
            record.pop('host_os_id_', None)
            record.pop('host_os_label_', None)

            record['system_component'] = {
                'ip': record.pop('system_component_ip', None),
                'label': record.pop('system_component_name', None),
                'id': record.pop('system_component_id', None)
            }
            return record

        apps_query = (Application
                      .select(
                          HostApplication.host_id,
                          aggregate_to_array(cast(Application.id, 'character varying')).alias('id'),
                          aggregate_to_array(
                              cast(Application.application_name, 'character varying')
                          ).alias('application_name'))
                      .join(HostApplication,
                            on=(HostApplication.application_id == Application.id))
                      .where(Application.software_type == 'OS', HostApplication.status != 'DELETED')
                      .group_by(HostApplication.host_id)).alias('aq')

        ips_query = (Ip
                     .select(
                         Ip.host_id,
                         aggregate_to_array(cast(Ip.ip, 'character varying')).alias('host_ip'),
                         aggregate_to_array(cast(Ip.mac, 'character varying')).alias('host_mac'))
                     .group_by(Ip.host_id)).alias('hips')

        query = (Host.select(
            Host.id,
            Host.hostname,
            Host.status.alias('host_status'),
            Host.owner_component_id,
            MonitoredSystem.id.alias('monitored_system_id_'),
            MonitoredSystem.name.alias('monitored_system_label_'),
            SystemComponent.name.alias('system_component_name'),
            SystemComponent.id.alias('system_component_id'),
            SystemComponentConnection.ip.alias('system_component_ip'),
            apps_query.c.id.alias('host_os_id_'),
            apps_query.c.application_name.alias('host_os_label_'),
            ips_query.c.host_ip.alias('host_ip'),
            ips_query.c.host_mac.alias('host_mac')
        ).join(ips_query,
               on=(Host.id == ips_query.c.host_id),
               join_type=JOIN.LEFT_OUTER)
         .join(apps_query,
               on=(Host.id == apps_query.c.host_id),
               join_type=JOIN.LEFT_OUTER)
         .join(SystemComponent,
               on=(Host.discovery_component_id == SystemComponent.id))
         .join(SystemComponentConnection,
               on=((SystemComponent.id == SystemComponentConnection.system_component_id) &
                   (SystemComponentConnection.connection_param_type == 'EXTERNAL_IP')),
               join_type=JOIN.LEFT_OUTER)
         .join(MonitoredSystem,
               on=(SystemComponent.monitored_system_id ==
                   MonitoredSystem.id),
               join_type=JOIN.LEFT_OUTER)
         .where(Host.id == host_id))

        try:
            result = query.naive().get()
        except Host.DoesNotExist:
            raise ValueError('Host profile by id=\'{}\' not found'
                             .format(host_id))

        response.body = idlabels_to_dict(records_processing(get_record_values(result)))


class HostProfileUpdateResource():
    @scs_db.atomic()
    def on_post(self, request, response):
        host_id = request.data.get('id')
        record = Host.select().where(Host.id == host_id).get()
        if request.data.get('status'):
            record.status = request.data['status']
        record.save()

        update_type = 'hosts/upstream'
        answer = PlutonUpdaterClient().start_update('children', 'hosts/upstream')
        if answer['message'] != 'OK':
            raise ValueError('Failed to start {} update: {}'
                           .format(update_type, answer.get('error')))


class HostProfileCreateResource:
    @scs_db.atomic()
    def on_post(self, request, response):

        record = Host(
            hostname=request.data['hostname'],
            owner_component_id=request.data['sensor']
        )

        if request.data.get('host_description'):
            record.description = request.data['host_description']

        if request.data.get('host_type'):
            record.host_type_id = request.data['host_type']
        record.status = 'ACCEPTED'
        record.save(force_insert=True)

        if request.data.get('host_os'):
            HostApplication(host_id=record.id,
                   application_id=request.data['host_os'],
                   ).save(force_insert=True)
        if request.data.get('host_ip'):
            Ip(ip=request.data['host_ip'],
               mac=request.data.get('host_mac'),
               host_id=record.id).save(force_insert=True)
        if request.data.get('application'):
            for app_id in request.data['application']:
                HostApplication.insert(host_id=record.id,
                                       application_id=app_id).execute()

        response.body = {'id': str(record.id)}


class HostAggregatedDataResource(object):
    def on_post(self, request, response):
        host_id = request.data['host_id']

        query = Host.select(*host_aggregated_query_fields) \
            .join(Location, on=(Host.location_id == Location.id)) \
            .where(Host.id == host_id)

        try:
            result = query.naive().get()
        except Host.DoesNotExist:
            raise ValueError('Host profile by id=\'{}\' not found'
                             .format(host_id))

        response.body = get_record_values(result)


class HostNetworkActivityInfoResource:
    def get_traffic_info(self, host_id, sc_id, period, protocol_label):
        host_ips_query = Ip.select(
            Ip.ip
        ).where(Ip.host_id == host_id)
        host_ips = []
        for rec in host_ips_query.naive():
            host_ips.append(rec.ip)

        traffic_filters = {
            'operation': 'AND',
            'operands': [
                {
                    'operation': 'OR',
                    'operands': [
                        {
                            'field': 'src_ip',
                            'value': host_ips,
                            'type': 'in'
                        },
                        {
                            'field': 'dst_ip',
                            'value': host_ips,
                            'type': 'in'
                        }
                    ]
                }
            ]
        }

        if period:
            traffic_filters['operands'].append({
                'field': 'finish_ts',
                'value': period,
                'type': 'between'
            })
        if protocol_label:
            traffic_filters['operands'].append({
                'field': 'proto',
                'value': protocol_label.upper(),
                'type': '='
            })

        result = PlutonQueryClient().send_command([sc_id], 'aggregated_host_volumes',
                                                  filters=traffic_filters,
                                                  sort=[{
                                                      'property': 'finish_ts',
                                                      'direction': 'ASC'
                                                  }])
        if result['message'] == OK_MSG:
            stat_data = result.get('data').get('rows')
            total = result.get('data').get('rows_total')
            return stat_data, total

        elif result['message'] == ERROR_MSG:
            raise ValueError('Невозможно получить информацию об интенсивности обмена хоста'
                             ' с компоненты {}'.format(sc_id))


    def on_post(self, request, response):
        host_id = request.data['host_id']
        protocol_label = request.data.get('protocol_label')
        period = request.data.get('period')
        stat_data, total = self.get_traffic_info(host_id, str(Host.get(Host.id == host_id).discovery_component_id.id),
                                                              period, protocol_label)
        response.body = {
            'data': stat_data,
            'total': total
        }


class HostUserListResource(object):
    def on_post(self, request, response):
        host_id = request.data['host_id']

        def records_processing(record):
            enum_to_dict(record, 'status', TYPE_REC_STATUS)
            return record

        try:
            send_list_data(
                HostUsers.select(
                    HostUsers.username.alias('login'),
                    HostUsers.email,
                    Protocol.name.alias('protocol'),
                    SystemComponent.name.alias('discovery_component'),
                    HostUsers.status,
                    HostUsers.discovery_timestamp.alias('create_timestamp')
                )
                .join(Protocol, on=(HostUsers.protocol_id == Protocol.id))
                .join(SystemComponent, on=(HostUsers.discovery_component_id == SystemComponent.id))
                .where(HostUsers.host_id == host_id),
                request,
                response,
                result_processing=records_processing
            )
        except Host.DoesNotExist:
            raise ValueError('Host profile by id=\'{}\' not found'
                             .format(host_id))


class HostVulnerabilitiesResource:
    def on_post(self, request, response):
        host_id = request.data['host_id']

        def record_processing(record):
            record['cwe_list'] = json.loads(record['cwe_list'])
            print(record['cwe_list'])
            return record

        try:
            send_list_data(
                Vulnerability.select(
                    Vulnerability.id,
                    Vulnerability.cwe_list,
                    aggregate_to_array(cast(Application.application_name, 'character varying')).alias('applications'),
                    Vulnerability.description,
                    Vulnerability.base_score_v2.alias('base_v2'),
                    Vulnerability.base_score_v3.alias('base_v3')
                ).join(HostApplicationVulnerability, on=(
                    (HostApplicationVulnerability.host_id == host_id) &
                    (HostApplicationVulnerability.vulnerability_id == Vulnerability.id))
                ).join(Application, on=(HostApplicationVulnerability.application_id == Application.id)),
                request,
                response,
                group_by=(Vulnerability.id, Vulnerability.cwe_list, Vulnerability.description,
                          Vulnerability.base_vector_v2, Vulnerability.base_vector_v3),
                result_processing=record_processing

            )
        except Host.DoesNotExist:
            raise ValueError('Host profile by id=\'{}\' not found'
                             .format(host_id))


class HostAppsListResource(object):
    def on_post(self, request, response):
        host_id = request.data['host_id']
        filters = request.data.get('filters')

        def records_processing(record):
            if record['port'] == 0:
                record['port'] = None
            if record['software_type'] != 'OS' and record['port']:
                record['software_type'] = 'SERVICE'
            if not record['name']:
                record['name'] = record['application_name']
            del record['application_name']
            enum_to_dict(record, 'software_type', TYPE_SOFTWARE_TYPES)
            enum_to_dict(record, 'status', TYPE_REC_STATUS)
            return record

        type_clause_filters = None
        type_filter = get_filter('software_type', filters)
        if type_filter is not None:
            if type_filter['value'] == 'APPLICATION':
                type_clause_filters = (HostApplication.port == 0) & (Application.software_type == 'APPLICATION')
                filters.remove(type_filter)
            if type_filter['value'] == 'SERVICE':
                type_clause_filters = (
                    (HostApplication.port != 0) & (Application.software_type == 'APPLICATION')
                ) | (Application.software_type == 'SERVICE')
                filters.remove(type_filter)

        try:
            send_list_data(
                Application.select(
                    HostApplication.id,
                    Application.id.alias('application_id'),
                    Application.software_type,
                    Application.unparsed_version.alias('name'),
                    Application.application_name,
                    HostApplication.port,
                    Protocol.name.alias('protocol'),
                    HostApplication.discovery_timestamp,
                    HostApplication.alert_id,
                    HostApplication.status,
                    HostApplication.added,
                    aggregate_to_array(distinct(cast(Vulnerability.id, 'character varying'))).alias('vulnerabilities'))
                .join(HostApplication,
                      on=(Application.id == HostApplication.application_id))
                .join(Protocol,
                      on=(HostApplication.protocol_id == Protocol.id),
                      join_type=JOIN.LEFT_OUTER)
                .join(HostApplicationVulnerability,
                      on=((Application.id == HostApplicationVulnerability.application_id) &
                          (HostApplication.host_id == HostApplicationVulnerability.host_id)),
                      join_type=JOIN.LEFT_OUTER)
                .join(Vulnerability,
                      on=(HostApplicationVulnerability.vulnerability_id == Vulnerability.id),
                      join_type=JOIN.LEFT_OUTER)
                .where(HostApplication.host_id == host_id,
                       HostApplication.status != 'DELETED'),
                request,
                response,
                group_by=(Application.software_type, Application.id, Application.application_name,
                          Protocol.name, HostApplication.port, HostApplication.discovery_timestamp,
                          HostApplication.status, HostApplication.added, HostApplication.alert_id,
                          HostApplication.id),
                result_processing=records_processing,
                additional_filters=type_clause_filters
            )
        except Host.DoesNotExist:
            raise ValueError('Host profile by id=\'{}\' not found'
                             .format(host_id))


class HostAppsChangeStatusResource(object):
    def on_post(self, request, response):
        host_application_id = request.data.get('host_application_id')
        new_status = request.data.get('status')
        if new_status not in ['DELETED', 'ACCEPTED', 'DECLINED']:
            raise ValueError('Неизвестный статус: {}'.format(new_status))

        try:
            record = HostApplication.select().where(
                HostApplication.id == host_application_id).get()
            record.status = new_status
            record.save()

            update_type = 'hosts/upstream'
            answer = PlutonUpdaterClient().start_update('children', 'hosts/upstream')
            if answer['message'] != 'OK':
                raise ValueError('Failed to start {} update: {}'
                                 .format(update_type, answer.get('error')))

        except HostApplication.DoesNotExist:
            raise ValueError('Запись с id=\'{}\' не найдена'
                             .format(host_application_id))


class HostAppsCreateResource:
    def on_post(self, request, response):
        host_id = request.data.get('host_id')
        application_id = request.data.get('application_id')
        record = HostApplication(host_id=host_id,
                                 application_id=application_id,
                                 added='MANUAL')
        record.save(force_insert=True)
        created = Application.select(
            Application.id.alias('application_id'),
            Application.create_timestamp,
            Application.application_name.alias('name'),
            HostApplication.added.alias('source')
        ).join(HostApplication,
               on=(Application.id == HostApplication.application_id))\
         .where(HostApplication.host_id == host_id,
                HostApplication.application_id == application_id,
                HostApplication.status != 'DELETED').naive().get()
        response.body = get_record_values(created)


class HostHeuristicAlertList:
    def on_post(self, request, response):
        host_id = request.data['host_id']

        host_ips = Ip.select(aggregate_to_array(cast(Ip.ip, 'character varying')).alias('host_ip'))\
                       .where(Ip.host_id == host_id).get().host_ip

        host_ips = [str(ipaddress.IPv4Address(int(ip))) for ip in host_ips]

        field_list = {
            Alert.id,
            Alert.alert_timestamp,
            Alert.alert_microseconds,
            Alert.alert_type_id,
            Alert.host_id,
            Alert.source_host_id,
            Alert.target_host_id,
            Alert.protocol_name,
            Alert.host_ip,
            Alert.port,
            Alert.source_ip, Alert.target_ip,
            Alert.severity_num.make_synonym('severity_id_'),
            Alert.severity_name.make_synonym('severity_label_'),
            Alert.alert_type_name.make_synonym('name'),
            Alert.application_version,
            Alert.service_name,
            Alert.new_os_name,

        }

        data, total_count = get_data(Alert.objects_in(clickhouse_db).only(*[field.alias for field in field_list])
                                     .filter(Q(alert_type_id__in=HEURISTIC_ALERTS_UUID) &
                                             (Q(host_id=host_id) | Q(host_ip__in=host_ips))), request, field_list)

        processed_data = []
        for record in data:
            record['object_name'] = record['application_version'] or record['service_name'] or record['new_os_name']
            if record['object_name'] and record['object_name'].startswith('unknown_'):
                try:
                    if not record['protocol_name']:
                        record['protocol_name'] = record['object_name'].split('_')[2].upper()
                    if not record['port']:
                        record['port'] = record['object_name'].split('_')[3]
                except Exception:
                    pass
            record.pop('application_version', None)
            record.pop('service_name', None)
            record.pop('new_os_name', None)
            if record.get('port') == 0:
                record['port'] = None
            processed_data.append(record)

        response.body = {'rows': processed_data, 'total': total_count}


class HostSignatureAlertList:
    def on_post(self, request, response):
        host_id = request.data['host_id']

        host_ips = Ip.select(aggregate_to_array(cast(Ip.ip, 'character varying')).alias('host_ip'))\
                       .where(Ip.host_id == host_id).get().host_ip

        fields = {Alert.id, Alert.alert_timestamp,
                  Alert.alert_microseconds,
                  Alert.source_host_id,
                  Alert.target_host_id,
                  Alert.source_ip, Alert.target_ip,
                  Alert.severity_num.make_synonym('severity_id_'),
                  Alert.severity_name.make_synonym('severity_label_'),
                  Alert.system_component_id.make_synonym('sensor_id_'),
                  Alert.system_component_name.make_synonym('sensor_label_'),
                  Alert.monitored_system_id.make_synonym('controlled_system_id_'),
                  Alert.monitored_system_name.make_synonym('controlled_system_label_'),
                  Alert.alert_type_id.make_synonym('alert_type_id_'),
                  Alert.alert_type_name.make_synonym('alert_type_label_'),
                  Alert.rule_name.make_synonym('alert_rule_name')}

        data, total_count = get_data(Alert.objects_in(clickhouse_db).only(*[field.alias for field in fields])
                                     .filter(Q(alert_type_id__in=SIGNATURE_ALERTS_UUID) &
                                             (Q(source_host_id=host_id) | Q(target_host_id=host_id) |
                                              Q(source_ip__in=host_ips) | Q(target_ip__in=host_ips))), request, fields)

        response.body = {'rows': data, 'total': total_count}


class HostBlackListResource:
    def on_post(self, request, response):
        host_id = request.data['host_id']

        host_ips = Ip.select(aggregate_to_array(cast(Ip.ip, 'character varying')).alias('host_ip'))\
                       .where(Ip.host_id == host_id).get().host_ip

        fields = {Alert.id, Alert.alert_timestamp,
                  Alert.alert_microseconds,
                  Alert.source_host_id,
                  Alert.target_host_id,
                  Alert.alert_type_id,
                  Alert.source_ip, Alert.target_ip,
                  Alert.severity_num.make_synonym('severity_id_'),
                  Alert.severity_name.make_synonym('severity_label_'),
                  Alert.alert_type_id,
                  Alert.reaction_list_id.make_synonym('reaction_id_'),
                  Alert.reaction_list_name.make_synonym('reaction_label_'),
                  Alert.rule_name.make_synonym('alert_rule_name'),
                  Alert.black_list_value,
                  Alert.black_list_id
                  }

        data, total_count = get_data(Alert.objects_in(clickhouse_db).only(*[field.alias for field in fields])
                                     .filter(Q(alert_type_id='efd52890-aa9e-457b-867d-9cd861cc5202') &
                                             (Q(source_host_id=host_id) | Q(target_host_id=host_id)|
                                              Q(source_ip__in=host_ips) | Q(target_ip__in=host_ips))), request, fields)

        black_lists = {}
        for record in data:
            black_lists[record['black_list_id']] = {}
        for bl_info in BlackList.select(BlackList.id,
                                        BlackList.direction,
                                        BlackListRecord.value.alias('black_list_value'),
                                        BlackListType.id.alias('black_list_type_id'),
                                        BlackListType.name.alias('black_list_type_label'))\
                                .join(BlackListType, on=(BlackListType.id == BlackList.black_list_type_id))\
                                .join(BlackListRecord, on=(BlackListRecord.black_list_id == BlackList.id))\
                                .where(BlackList.id << list(black_lists.keys())).naive():
            black_lists[str(bl_info.id)] = {
                'direction': bl_info.direction,
                'black_list_type_id': bl_info.black_list_type_id,
                'black_list_type_label': bl_info.black_list_type_label,
                'black_list_value': bl_info.black_list_value
            }
        for record in data:
            bl = black_lists[record['black_list_id']]
            record['black_list_type'] = {
                'id': str(bl['black_list_type_id']),
                'label': bl['black_list_type_label']
            },
            record['black_list_direction'] = bl['direction']
            record['black_list_value'] = bl['black_list_value']
            enum_to_dict(record, 'black_list_direction', TYPE_DIRECTION_BL)
        response.body = {'rows': data, 'total': total_count}


class HostMetricDataResource:
    def on_post(self, request, response):
        # start_time = request.data.get('start_time')
        # end_time = request.data.get('end_time')
        # host_id = request.data.get('host_id')
        # protocol_id = request.data.get('protocol_id')
        # direction = request.data.get('direction')
        # TODO when analytic and db will be done
        response.body = []
