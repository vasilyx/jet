import time
from datetime import datetime, timedelta

from pluton_commons.common.utils import get_current_component_id
from pluton_commons.dateutil.relativedelta import relativedelta, SECONDS_IN_DAY
from pluton_commons.db.infi.connection import clickhouse_db
from pluton_commons.db.infi.models import Alert as AlertCH
from pluton_commons.db.peewee.connection import scs_db
from pluton_commons.db.peewee.models import SystemComponent, Location

ANOMALIES_UUID = 'f81e3702-7507-4b6c-8d44-25afa8b42fb8'
BLACK_RECORD_UUID = 'efd52890-aa9e-457b-867d-9cd861cc5202'


class MapAlertsResource(object):
    @scs_db.atomic()
    def on_post(self, request, response):
        trace = request.data.get('trace', 15)

        data = {}
        current_time_stamp = datetime.now()
        trace_interval_begin = int(time.mktime((current_time_stamp - timedelta(seconds=trace)).timetuple()))

        alert_query = AlertCH.objects_in(clickhouse_db).only(AlertCH.id.alias,
                                                             AlertCH.system_component_id.alias,
                                                             AlertCH.source_latitude.alias,
                                                             AlertCH.source_longitude.alias,
                                                             AlertCH.target_latitude.alias,
                                                             AlertCH.target_longitude.alias,
                                                             AlertCH.alert_type_id.alias).filter(
            alert_timestamp__gte=trace_interval_begin)

        component_query = SystemComponent.select(
            SystemComponent.id,
            SystemComponent.name,
            Location.latitude,
            Location.longitude,
            SystemComponent.component_type.alias('type')
        ).where(SystemComponent.component_type != 'SCS') \
            .join(Location, on=(SystemComponent.location_id == Location.id))

        for record in alert_query:
            data[str(record.id)] = {
                'target_latitude': record.target_latitude,
                'target_longitude': record.target_longitude,
                'source_latitude': record.source_latitude,
                'source_longitude': record.source_longitude,
                'target': str(record.system_component_id),
                'type': record.alert_type_id,
                'name': None,
                'info': None
            }

        for record in component_query.naive():
            data[str(record.id)] = {
                'target_latitude': None,
                'target_longitude': None,
                'source_latitude': record.latitude,
                'source_longitude': record.longitude,
                'target': None,
                'type': record.type,
                'name': record.name,
                'info': None
            }

        begin_of_current_period = int(time.mktime(
            datetime(current_time_stamp.year, current_time_stamp.month, current_time_stamp.day).timetuple()))

        alerts_count = AlertCH.objects_in(clickhouse_db).filter(alert_timestamp__gte=begin_of_current_period).aggregate(
            'system_component_id', alerts_count='count()')

        anomalies_count = AlertCH.objects_in(clickhouse_db).filter(alert_timestamp__gte=begin_of_current_period,
                                                                   alert_type_id__eq=ANOMALIES_UUID).aggregate(
            'system_component_id', anomalies_count='count()')

        bl_count = AlertCH.objects_in(clickhouse_db).filter(alert_timestamp__gte=begin_of_current_period,
                                                            alert_type_id__eq=BLACK_RECORD_UUID).aggregate(
            'system_component_id', bl_count='count()')

        for record in alerts_count:
            if data.get(str(record.system_component_id)):
                data[str(record.system_component_id)]['info'] = {
                    'alerts_count': record.alerts_count,
                    'anomalies_count': 0,
                    'bl_count': 0
                }

        for record in anomalies_count:
            if data.get(str(record.system_component_id)):
                data[str(record.system_component_id)]['info']['anomalies_count'] = \
                    record.anomalies_count

        for record in bl_count:
            if data.get(str(record.system_component_id)):
                data[str(record.system_component_id)]['info']['bl_count'] = record.bl_count

        response.body = data


class MapAggregatedInfoService(object):
    @scs_db.atomic()
    def on_post(self, request, response):
        period = request.data.get('period', 'month')
        current_time_stamp = datetime.utcnow()
        if period == 'month':
            begin_of_month = current_time_stamp.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
            begin_of_current_period = int(time.mktime(begin_of_month.timetuple()))
            begin_of_previous_period = int(time.mktime((begin_of_month + relativedelta(months=-1)).timetuple()))
        elif period == 'week':
            begin_of_week = current_time_stamp.replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(
                days=current_time_stamp.weekday())
            begin_of_current_period = int(time.mktime(begin_of_week.timetuple()))
            begin_of_previous_period = int(time.mktime((begin_of_week - timedelta(days=7)).timetuple()))
        elif period == 'day':
            today_midnight = current_time_stamp.replace(hour=0, minute=0, second=0, microsecond=0)
            begin_of_current_period = int(time.mktime(today_midnight.timetuple()))
            begin_of_previous_period = begin_of_current_period - SECONDS_IN_DAY
        else:
            raise RuntimeError('На текущий момент поддерживается только период в один месяц, неделю и один день!')

        alerts_count = AlertCH.objects_in(clickhouse_db).filter(alert_timestamp__gte=begin_of_current_period).count()

        alerts_count_trend = alerts_count - AlertCH.objects_in(clickhouse_db).filter(
            alert_timestamp__gte=begin_of_previous_period, alert_timestamp__lt=begin_of_current_period).count()

        anomalies_count = AlertCH.objects_in(clickhouse_db).filter(alert_timestamp__gte=begin_of_current_period,
                                                                   alert_type_id__eq=ANOMALIES_UUID).count()

        anomalies_count_trend = anomalies_count - AlertCH.objects_in(clickhouse_db).filter(
            alert_timestamp__gte=begin_of_previous_period, alert_timestamp__lt=begin_of_current_period,
            alert_type_id__eq=ANOMALIES_UUID).count()

        bl_count = AlertCH.objects_in(clickhouse_db).filter(alert_timestamp__gte=begin_of_current_period,
                                                            alert_type_id__eq=BLACK_RECORD_UUID).count()

        bl_count_trend = bl_count - AlertCH.objects_in(clickhouse_db).filter(
            alert_timestamp__gte=begin_of_previous_period, alert_timestamp__lt=begin_of_current_period,
            alert_type_id__eq=BLACK_RECORD_UUID).count()

        child_scs_count = SystemComponent.select().where(
            SystemComponent.component_type != 'SENSOR',
            SystemComponent.parent_id == get_current_component_id()
        ).count()

        local_sensor_count = SystemComponent.select().where(
            SystemComponent.component_type == 'SENSOR',
            SystemComponent.parent_id == get_current_component_id()
        ).count()

        all_scs_count = SystemComponent.select().where(
            SystemComponent.component_type != 'SENSOR',
        ).count()

        all_sensor_count = SystemComponent.select().where(
            SystemComponent.component_type == 'SENSOR',
        ).count()

        response.body = {
            'alerts_count': alerts_count,
            'alerts_count_trend': alerts_count_trend,
            'anomalies_count': anomalies_count,
            'anomalies_count_trend': anomalies_count_trend,
            'bl_count': bl_count,
            'bl_count_trend': bl_count_trend,
            'child_scs_count': child_scs_count,
            'local_sensor_count': local_sensor_count,
            'all_scs_count': all_scs_count,
            'all_sensor_count': all_sensor_count
        }
