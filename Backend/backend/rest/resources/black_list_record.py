import csv
import time
from backend.rest.resources.black_list import get_blacklist_by_id, enum_direction_processing, \
    check_blacklist
from peewee import JOIN

from backend.utils.logger import logger
from backend.utils.configuration import configuration
from pluton_audit import EventLogger
from pluton_audit.events import RuleControlEvent
from pluton_commons.common.utils import get_current_user_info
from pluton_commons.db.common import idlabels_to_dict
from pluton_commons.db.peewee.models import BlackListRecord, SystemUser, BlackList
from pluton_commons.db.peewee.connection import scs_db
from pluton_commons.db.peewee.utils import get_record_values, send_list_data

event_logger = EventLogger(configuration, logger)
username, user_id = get_current_user_info(configuration)


def _get_blacklist(black_list_id):
    record = BlackList.get(BlackList.id == black_list_id)
    check_blacklist(record)
    return record


class BlackListRecordResource(object):
    def on_post(self, request, response):
        parent_id = request.data.get('black_list_id')
        update_user = SystemUser.alias()
        create_user = SystemUser.alias()

        send_list_data(BlackListRecord.select(
            BlackListRecord.black_list_id,
            BlackListRecord.create_timestamp,
            update_user.login.alias('update_user_label_'),
            create_user.login.alias('create_user_label_'),
            update_user.id.alias('update_user_id_'),
            create_user.id.alias('create_user_id_'),
            BlackListRecord.description,
            BlackListRecord.id,
            BlackListRecord.is_deleted,
            BlackListRecord.update_timestamp,
            BlackListRecord.direction,
            BlackListRecord.value
        ).join(update_user,
               on=(BlackListRecord.update_user_id == update_user.id),
               join_type=JOIN.LEFT_OUTER)
                       .switch(BlackListRecord)
                       .join(create_user,
               on=(BlackListRecord.create_user_id == create_user.id),
               join_type=JOIN.LEFT_OUTER)
                       .where(BlackListRecord.black_list_id == parent_id,
                BlackListRecord.is_deleted != True),  # noqa: E712
                       request, response, result_processing=enum_direction_processing)


class CreateBlackListRecordResource(object):
    def on_post(self, request, response):
        new_data = request.data
        # get corresponding blacklist
        record_bl = _get_blacklist(new_data['black_list_id'])
        # check if there's already such record
        if BlackListRecord.select() \
                .where(BlackListRecord.black_list_id == new_data['black_list_id'],
                       BlackListRecord.is_deleted != True,
                       BlackListRecord.value == new_data['value']).count() > 0:
            raise ValueError('Запись черного списка со значением \'{}\' уже существует'.format(new_data['value']))
        # create new blacklist record and edit the blacklist
        record = BlackListRecord()
        record.value = new_data['value']
        if new_data.get('description'):
            record.description = new_data['description']
        record.black_list_id = new_data['black_list_id']
        record.create_user_id = request.userinfo['id']
        record.is_deleted = False
        record.direction = new_data['direction'] or record_bl.direction or 'ANY'
        record_bl.update_user_id = request.userinfo['id']
        record.save(force_insert=True)
        record_bl.save()
        # return the new record
        result = BlackListRecord.select(
            BlackListRecord.black_list_id,
            BlackListRecord.id,
            SystemUser.id.alias('create_user_id_'),
            SystemUser.login.alias('create_user_label_'),
            BlackListRecord.description,
            BlackListRecord.direction,
            BlackListRecord.create_timestamp
        ).join(SystemUser,
               on=(SystemUser.id == BlackListRecord.create_user_id),
               join_type=JOIN.LEFT_OUTER)\
         .where(BlackListRecord.id == record.id).naive().get()
        response.body = enum_direction_processing(idlabels_to_dict(get_record_values(result)))
        event_logger.log(RuleControlEvent.TYPE_RULES_STATE_CHANGED,
                         acting_component_id=configuration.current_component_id,
                         Type='BlacklistRecord',
                         Id=record.id,
                         BlacklistId=record_bl.id,
                         BlacklistName=record_bl.name,
                         AttributeChanged='created',
                         User=username,
                         UserId=user_id)


class UpdateBlackListRecordResource(object):
    def on_post(self, request, response):
        new_data = request.data
        # get corresponding blacklist
        record_bl = _get_blacklist(new_data['black_list_id'])
        # edit the blacklist record and the blacklist
        try:
            record = BlackListRecord.get(
                BlackListRecord.id == new_data['id'],
                BlackListRecord.is_deleted != True)  # noqa: E712
        except BlackListRecord.DoesNotExist:
            raise ValueError('Record from black list by id=\'{}\' not found'
                             .format(new_data['id']))
        description = new_data.get('description')
        if description:
            record.description = description
        record.direction = new_data['direction'] or record_bl.direction or 'ANY'
        record.update_user_id = request.userinfo['id']
        record_bl.update_user_id = request.userinfo['id']
        record.save()
        record_bl.save()
        response.body = get_blacklist_by_id(new_data['black_list_id'])
        event_logger.log(RuleControlEvent.TYPE_RULES_STATE_CHANGED,
                         acting_component_id=configuration.current_component_id,
                         Type='BlacklistRecord',
                         Id=record.id,
                         BlacklistId=record_bl.id,
                         BlacklistName=record_bl.name,
                         AttributeChanged='"{}"'.format(', '.join(attr for attr in sorted(new_data)
                                                                  if not attr.endswith('id'))),
                         User=username,
                         UserId=user_id)


class DeleteBlackListRecordResource(object):
    def on_post(self, request, response):
        new_data = request.data
        # get corresponding blacklist
        record_bl = _get_blacklist(new_data['black_list_id'])
        # edit the blacklist record and the blacklist
        try:
            record = BlackListRecord.get(
                    BlackListRecord.id == new_data['id'],
                    BlackListRecord.is_deleted != True)  # noqa: E712
        except BlackListRecord.DoesNotExist:
            raise ValueError('Record from black list by id=\'{}\' not found'
                             .format(new_data['id']))
        record.is_deleted = True
        record.update_user_id = request.userinfo['id']
        record_bl.update_user_id = request.userinfo['id']
        record.save()
        record_bl.save()
        response.body = get_blacklist_by_id(new_data['black_list_id'])
        event_logger.log(RuleControlEvent.TYPE_RULES_STATE_CHANGED,
                         acting_component_id=configuration.current_component_id,
                         Type='BlacklistRecord',
                         Id=record.id,
                         BlacklistId=record_bl.id,
                         BlacklistName=record_bl.name,
                         AttributeChanged='deleted',
                         User=username,
                         UserId=user_id)


class UploadBlackListResource(object):
    @scs_db.atomic()
    def on_post(self, request, response):
        parent_id = request.get_param('black_list_id')
        # get corresponding blacklist and check it
        record_bl = _get_blacklist(parent_id)
        # get all the data needed for the export
        delimiter = request.get_param('delimiter')
        received_file = request.get_param('file')
        content = received_file.value.decode('UTF-8')
        csv_reader = csv.reader(content.split('\n'),
                                delimiter=delimiter,
                                skipinitialspace=True)

        start_time = int(round(time.time() * 1000))
        try:
            black_list_records = []
            for raw_number, record in enumerate(csv_reader):
                if len(record) == 0:
                    continue
                if len(record) == 0 or record[0].strip().startswith('#'):
                    continue
                if not record[0].strip():
                    raise ValueError(
                        'Ошибка в файле на {} строке. Первая колонка не может быть пустой.'.format(raw_number + 1))
                if len(record[0]) > 2000:
                    raise ValueError(
                        'На {} строке превышен максимальный размер значения ({} > 2000).'.format(raw_number + 1, len(record[0]))
                    )
                if len(record) > 2:
                    raise ValueError(
                        'Ошибка в файле на {} строке. Неправильное количество '
                        'колонок: {} (ожидается 1 или 2)'.format(
                            raw_number + 1, len(record)))
                db_record = {'value': record[0], 'black_list_id': parent_id, 'create_user_id': request.userinfo['id']}
                if len(record) == 2:
                    db_record['description'] = record[1].replace('\t', ' ')
                black_list_records.append(db_record)
            with scs_db.atomic():
                for idx in range(0, len(black_list_records), 1000):
                    try:
                        rows = black_list_records[idx:idx + 1000]
                        BlackListRecord.insert_many(rows).execute()
                    except Exception as e:
                        raise ValueError('Ошибка при вставке в базу строчек {}-{}.\n{}'.format(idx, idx+1000, str(e)))
            event_logger.log(RuleControlEvent.TYPE_RULES_STATE_CHANGED,
                             acting_component_id=configuration.current_component_id,
                             Type='Blacklist',
                             Id=parent_id,
                             NewRules=len(black_list_records),
                             AttributeChanged='rules_uploaded',
                             User=username,
                             UserId=user_id)
        finally:
            end_time = int(round(time.time() * 1000))
            print(end_time - start_time,' ms for bl uploading')
            record_bl.update_user_id = request.userinfo['id']
            record_bl.save()
        response.body = get_blacklist_by_id(request.get_param('black_list_id'))
