from pluton_commons.db.peewee.models import SystemJobRun
from pluton_commons.db.peewee.utils import send_list_data

class JobsResource(object):
    def on_post(self, request, response):
        send_list_data(SystemJobRun.select(
            SystemJobRun.id,
            SystemJobRun.job_type,
            SystemJobRun.job_parameters,
            SystemJobRun.description,
            SystemJobRun.status,
            SystemJobRun.job_progress,
            SystemJobRun.job_start_datetime,
            SystemJobRun.job_end_datetime,
            SystemJobRun.job_output,
            SystemJobRun.pid
        ), request, response)
