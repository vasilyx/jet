from pluton_commons.db.peewee.models import Protocol
from pluton_commons.db.peewee.utils import send_list_data


class ProtocolResource(object):
    def on_post(self, request, response):
        send_list_data(Protocol.select(
            Protocol.id,
            Protocol.name.alias('label')
        ), request, response)
