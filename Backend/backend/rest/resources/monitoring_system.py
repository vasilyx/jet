from pluton_commons.common.utils import get_current_component_id

from pluton_commons.db.peewee.models import MonitoredSystem, SystemComponent
from pluton_commons.db.peewee.utils import get_record_values, send_list_data


def _ms_query():
    return MonitoredSystem.select(MonitoredSystem.id,
                                  SystemComponent.id.alias('system_component_id'),
                                  SystemComponent.name.alias('system_component_name'),
                                  SystemComponent.parent_id.alias('parent_component_id'),
                                  MonitoredSystem.name,
                                  MonitoredSystem.description,
                                  MonitoredSystem.subnet_ip) \
        .join(SystemComponent,
              on=(SystemComponent.monitored_system_id == MonitoredSystem.id))


class MonitoredSystemListResource(object):
    def on_post(self, request, response):
        send_list_data(_ms_query(), request, response)


class MonitoredSystemGetResource(object):
    def on_post(self, request, response):
        sc_id = request.data.get('system_component_id', get_current_component_id())
        query = _ms_query().where(SystemComponent.id == sc_id)
        try:
            result = query.naive().get()
        except MonitoredSystem.DoesNotExist:
            raise ValueError('Контролируемая система для компоненты id=\'{}\' не найдена'
                             .format(sc_id))
        record = get_record_values(result)
        response.body = record
