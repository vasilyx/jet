from pluton_commons.db.peewee.models import Reaction
from pluton_commons.db.peewee.utils import send_list_data


class ReactionListResource(object):
    def on_post(self, request, response):
        send_list_data(Reaction.select(Reaction.id,
                                       Reaction.name.alias('label')), request, response)
