import re
import subprocess

from pluton_pcap_client.pcap_client import PCAPClient

TCPDUMP_CMD = '/usr/sbin/tcpdump -ttttnnvXXr {}'


class GetPcapResource:
    def on_post(self, request, response):
        alert_id = request.data.get('alert_id')
        system_component_id = request.data.get('system_component_id')
        alert_timestamp = request.data.get('alert_timestamp')
        pcap_client = PCAPClient()
        pcap_filename = pcap_client.pcap_file(alert_id)
        if pcap_filename is None:
            result = pcap_client.get_pcap(system_component_id, alert_id, alert_timestamp)
            if result['message'] == 'OK':
                pcap_filename = result['filename']
            else:
                raise ValueError(result['error'])
        out = subprocess.getoutput(TCPDUMP_CMD.format(pcap_filename))
        splitted_by_lines = out.split('\n')[1:]
        pcap_structure = []
        package_object = {
            'head': '',
            'body': []
        }
        is_head = True
        for line in splitted_by_lines:
            line = line.strip()
            if re.match(r'^0x[0-9a-fA-F]+:', line):
                if is_head:
                    is_head = False
                package_object['body'].append(line)
            else:
                if not is_head:
                    pcap_structure.append(package_object)
                    package_object = {
                        'head': '',
                        'body': []
                    }
                    is_head = True
                package_object['head'] += line + '\n'
        pcap_structure.append(package_object)

        if pcap_structure:
            pcap_len = len(pcap_structure)
            pcap_structure[int(pcap_len/2)]['target'] = True

        response.body = pcap_structure
