from peewee import JOIN, RawQuery, fn

from backend.utils.configuration import configuration
from backend.utils.logger import logger
from pluton_commons.db.common import (
    enum_to_dict, idlabels_to_dict
)
from pluton_commons.db.peewee.models import (
    TYPE_COMPONENT_STATUS, SystemComponent, SensorType, Location,
    MonitoredSystem, SystemComponentCluster, SystemDepartment,
    TYPE_COMPONENT, TYPE_SENSOR_MODE, SystemSensorGroup,
    SystemComponentConnection, SystemComponentActiveUpdates,
    UpdateRegistry,
    Host, PendingSystemComponent)
from pluton_commons.db.peewee.utils import (
    send_list_data, get_record_values
)
from pluton_health_monitor import PlutonHealthMonitorClient


def get_component_updates(system_component_id=None):
    result = {}
    scu = SystemComponentActiveUpdates
    ur = UpdateRegistry
    rows = (scu.select(scu.system_component_id,
                       scu.update_type,
                       scu.version,
                       fn.MAX(ur.version))
            .join(ur, on=(ur.update_type == scu.update_type))
            .group_by(scu.system_component_id, scu.update_type, scu.version))
    if system_component_id:
        rows = rows.where(scu.system_component_id == system_component_id)
    for comp_id, up_type, cur_version, max_version in rows.tuples():
        result.setdefault(
                str(comp_id), {})[up_type] = {'current_version': cur_version,
                                              'available_version': max_version}
    return result


def get_health_status(system_component_id, with_children):
    client = PlutonHealthMonitorClient(configuration, logger)
    health = client.get_health_status(system_component_id, with_children=with_children)
    health_error = health.get('error')
    if health_error:
        logger.error('Unable to get health status of system components: %s, %s',
                     health_error['code'], health_error['message'])
        return None
    return client.to_dict(health, calculate_status=True)


class SystemComponentBriefResource:
    def on_post(self, request, response):
        def post_process(record):
            enum_to_dict(record, 'component_type', TYPE_COMPONENT)
            if str(record['id']) == str(configuration.current_component_id):
                record['current'] = True
            return record

        send_list_data(SystemComponent.select(
            SystemComponent.id,
            SystemComponent.name.alias('name'),
            SystemComponent.parent_id,
            SystemComponent.component_type),
            request,
            response,
            result_processing=post_process)


class SystemComponentListResource(object):
    def on_post(self, request, response):
        updates = get_component_updates()
        health = get_health_status('self', True)

        def post_process(record):
            enum_to_dict(record, 'status', TYPE_COMPONENT_STATUS)
            enum_to_dict(record, 'component_type', TYPE_COMPONENT)
            enum_to_dict(record, 'sensor_mode', TYPE_SENSOR_MODE)
            cid = record.get('id')
            record['updates'] = updates.get(cid) if updates else None
            record['health'] = health.get(cid) if health else None
            if record['status']['id'] == 'COMPROMISED':
                if record['health']:
                    record['health']['state'] = 2
            if str(record['id']) == str(configuration.current_component_id):
                record['current'] = True
            return record

        hosts_query = (
            Host.select(
                Host.discovery_component_id,
                fn.Count(Host.id).alias('host_count')
            ).group_by(Host.discovery_component_id)).alias('hosts')

        send_list_data(SystemComponent.select(
            SystemComponent.id,
            SystemComponent.name.alias('name'),
            MonitoredSystem.name.alias('monitored_system_label_'),
            MonitoredSystem.id.alias('monitored_system_id_'),
            SystemComponentCluster.name.alias('cluster_label_'),
            SystemComponentCluster.id.alias('cluster_id_'),
            SystemComponentConnection.id.alias('host_id_'),
            SystemComponentConnection.ip.alias('host_label_'),
            Location.region.alias('region_label_'),
            Location.id.alias('region_id_'),
            SystemDepartment.name.alias('department_label_'),
            SystemDepartment.id.alias('department_id_'),
            SensorType.name.alias('sensor_type_label_'),
            SensorType.id.alias('sensor_type_id_'),
            SystemComponent.sensor_mode,
            SystemComponent.status,
            SystemComponent.parent_id,
            SystemComponent.component_type,
            hosts_query.c.host_count
        ).join(SensorType,
               join_type=JOIN.LEFT_OUTER,
               on=(SystemComponent.sensor_type_id == SensorType.id))
         .join(Location,
               join_type=JOIN.LEFT_OUTER,
               on=(SystemComponent.location_id == Location.id))
         .join(MonitoredSystem,
               join_type=JOIN.LEFT_OUTER,
               on=(SystemComponent.monitored_system_id == MonitoredSystem.id))
         .join(SystemDepartment,
               join_type=JOIN.LEFT_OUTER,
               on=(SystemComponent.department_id == SystemDepartment.id))
         .join(SystemComponentCluster,
               join_type=JOIN.LEFT_OUTER,
               on=(SystemComponent.cluster_id == SystemComponentCluster.id))
         .join(SystemComponentConnection,
               join_type=JOIN.LEFT_OUTER,
               on=(SystemComponent.id == SystemComponentConnection.system_component_id))
         .join(PendingSystemComponent,
               join_type=JOIN.LEFT_OUTER,
               on=(SystemComponent.id == PendingSystemComponent.id))
         .join(hosts_query,
               join_type=JOIN.LEFT_OUTER,
               on=(SystemComponent.id == hosts_query.c.discovery_component_id))
         .where(SystemComponentConnection.connection_param_type == 'EXTERNAL_IP',
                (PendingSystemComponent.id == None) | (PendingSystemComponent.state == 'ACCEPTED')),
            request, response, result_processing=post_process)


class SystemComponentGetResource(object):
    def on_post(self, request, response):
        component_id = request.data.get('id')
        if component_id is None:
            component_id = configuration.current_component_id
        parentComponent = SystemComponent.alias()

        result = SystemComponent.select(
                SystemComponent.id,
                SystemComponent.name,
                SystemComponent.description,
                SystemComponent.component_type,
                SystemComponent.domain_host,
                SystemComponent.status,
                SystemComponent.sensor_mode,
                SystemComponent.reg_timestamp,
                SystemDepartment.name.alias('department_label_'),
                SystemDepartment.id.alias('department_id_'),
                MonitoredSystem.id.alias('monitored_system_id_'),
                MonitoredSystem.name.alias('monitored_system_label_'),
                parentComponent.name.alias('parent_label_'),
                parentComponent.id.alias('parent_id_'),
                SystemComponentCluster.name.alias('cluster_label_'),
                SystemComponentCluster.id.alias('cluster_id_'),
                SensorType.name.alias('sensor_type_label_'),
                SensorType.id.alias('sensor_type_id_'),
                SystemSensorGroup.name.alias('sensor_group_label_'),
                SystemSensorGroup.id.alias('sensor_group_id_'),
        ).where(SystemComponent.id == component_id) \
            .join(SensorType,
                  join_type=JOIN.LEFT_OUTER,
                  on=(SystemComponent.sensor_type_id == SensorType.id)) \
            .join(MonitoredSystem,
                  join_type=JOIN.LEFT_OUTER,
                  on=(SystemComponent.monitored_system_id == MonitoredSystem.id)) \
            .join(SystemDepartment,
                  join_type=JOIN.LEFT_OUTER,
                  on=(SystemComponent.department_id == SystemDepartment.id)) \
            .join(SystemComponentCluster,
                  join_type=JOIN.LEFT_OUTER,
                  on=(SystemComponent.cluster_id == SystemComponentCluster.id)) \
            .join(SystemSensorGroup,
                  join_type=JOIN.LEFT_OUTER,
                  on=(SystemComponent.sensor_group_id == SystemSensorGroup.id)) \
            .join(parentComponent,
                  join_type=JOIN.LEFT_OUTER,
                  on=(SystemComponent.parent_id == parentComponent.id)) \
            .naive().get()

        record = idlabels_to_dict(get_record_values(result))

        enum_to_dict(record, 'status', TYPE_COMPONENT_STATUS)
        enum_to_dict(record, 'component_type', TYPE_COMPONENT)
        enum_to_dict(record, 'sensor_mode', TYPE_SENSOR_MODE)
        # check_integrity_status(record)

        cid = record.get('id')
        updates = get_component_updates(cid)
        health = get_health_status(cid, False)
        record['updates'] = updates.get(cid) if updates else None
        record['health'] = health.get(cid) if health else None
        if record['status']['id'] == 'COMPROMISED':
            if not record.get('health'):
                record['health'] = {}
            record['health']['state'] = 2

        response.body = record


class SystemComponentUpdateResource():
    def on_post(self, request, response):
        new_data = request.data

        try:
            record = SystemComponent.get(SystemComponent.id == new_data['id'])
        except SystemComponent.DoesNotExist:
            raise ValueError('Системная компонента с id=\'{}\' не найдена'
                             .format(new_data['id']))

        if new_data.get('name'):
            record.name = new_data['name']
        if new_data.get('description'):
            record.description = new_data['description']
        if new_data.get('department_id'):
            record.department_id = new_data['department_id']
        if new_data.get('domain_host'):
            record.domain_host = new_data['domain_host']
        if new_data.get('cluster_id'):
            record.cluster_id = new_data['cluster_id']
        if new_data.get('sensor_type_id'):
            record.sensor_type_id = new_data['sensor_type_id']

        record.save()


class SystemComponentStatusesResource(object):
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_COMPONENT_STATUS.items():
            data.append({'id': k, 'label': v})

        response.body = data


class SensorListResource(object):
    def on_post(self, request, response):
        send_list_data(SystemComponent.select(
                SystemComponent.id,
                SystemComponent.name.alias('label'),
                SystemComponent.monitored_system_id
        ).where(SystemComponent.component_type == 'SENSOR'), request, response)


class SystemComponentTypesResource(object):
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_COMPONENT.items():
            data.append({'id': k, 'label': v})

        response.body = data


class SystemDepartmentListResource():
    def on_post(self, request, response):
        send_list_data(SystemDepartment.select(
                SystemDepartment.id,
                SystemDepartment.name.alias('label')
        ), request, response)


class SensorTypeCatalogResource:
    def on_post(self, request, response):
        send_list_data(SensorType.select(
                SensorType.id,
                SensorType.name.alias('label')
        ), request, response)


class ChildSystemComponentCatalogResource:
    def on_post(self, request, response):
        system_component_id = configuration.current_component_id
        query = RawQuery(SystemComponent, '''
        WITH RECURSIVE r AS (

          SELECT s.* from system_component s
            where id = '{}'

          UNION

          SELECT s.*
          FROM system_component s JOIN r ON s.parent_id = r.id
        )

        select * from r;
        '''.format(system_component_id))
        data = []
        for rec in query.execute():
            data.append({'id': str(rec.id), 'label': rec.name})

        response.body = data


class SystemComponentChangeStatus(object):
    @staticmethod
    def set_component_status(component_id, status):
        if not status:
            try:
                record = SystemComponent.get(SystemComponent.id == component_id)
                record.status = status
                record.save()
            except SystemComponent.DoesNotExist:
                raise ValueError('Системная компонента с id={} не найдена'.format(component_id))

    def on_post(self, request, response):
        from pluton_component_status_client.client import PlutonComponentStatusClient
        status = request.data['status']
        comp_id = request.data['system_component_id']
        answer = PlutonComponentStatusClient(logger=logger).update_status(comp_id, status)
        error = answer.get('error')
        if error:
            raise ValueError(error.get('message'))
        if comp_id != configuration.current_component_id:
            self.set_component_status(comp_id, status)
            from pluton_updater.control_client import PlutonUpdaterClient
            update_type = 'hierarchy'
            answer = PlutonUpdaterClient(configuration, logger) \
                .start_update('self', update_type)
            if answer['message'] != 'OK':
                logger.warning('Failed to start {} update: {}'
                               .format(update_type, answer.get('error')))
            else:
                logger.debug('Started %s update', update_type)
