import getpass

from backend.rest.resources.user import get_password_expiration
from backend.utils.common import get_current_component_info, get_self_label_id
from backend.utils.configuration import configuration, version
from backend.utils.logger import logger
from peewee import datetime
from pluton_audit import EventLogger
from pluton_audit.events import UserAuditEvent
from pluton_commons.db.infi.connection import clickhouse_db
from pluton_commons.db.infi.models import AuditLog
from pluton_commons.db.peewee.models import SystemUser, SystemPrivilege, SystemRolePrivilege, SystemRole, SystemUserRole

event_logger = EventLogger(configuration, logger)


def _last_user_audit_event(user_id):
    return AuditLog.objects_in(clickhouse_db) \
        .filter(subject_id=user_id) \
        .only('audit_type_id') \
        .order_by('-registered_timestamp')


class UserInfoResource(object):
    def on_post(self, request, response):
        user = SystemUser.select(SystemUser.first_login_timestamp,
                                 SystemUser.last_login_timestamp) \
            .where(SystemUser.id == request.userinfo['id']).get()

        if user.first_login_timestamp is None:
            query = SystemUser.update(
                    first_login_timestamp=datetime.datetime.now()) \
                .where(SystemUser.id == request.userinfo['id'])
            query.execute()

        query = SystemUser.update(
                last_login_timestamp=datetime.datetime.now()) \
            .where(SystemUser.id == request.userinfo['id'])
        query.execute()

        sc_info = get_current_component_info()
        query_log = _last_user_audit_event(request.userinfo['id'])
        should_log = True
        for record in query_log:
            if record.audit_type_id == UserAuditEvent.TYPE_USER_LOGIN:
                should_log = False
                break
            if record.audit_type_id == UserAuditEvent.TYPE_USER_LOGOUT:
                break

        if should_log:
            event_logger.log(UserAuditEvent.TYPE_USER_LOGIN,
                             user_id=request.userinfo['id'],
                             login=request.userinfo['name'])

        label_id = get_self_label_id()

        #List of UI privileges
        ui_privileges_query = SystemUser.select(SystemPrivilege.api_rule)\
                                        .distinct() \
                                        .join(SystemUserRole,
                                                  on=SystemUserRole.system_user_id) \
                                        .join(SystemRole,
                                                  on=((SystemUserRole.system_role_id == SystemRole.id) &
                                                      (SystemRole.is_deleted != True))) \
                                        .join(SystemRolePrivilege,
                                                  on=SystemRolePrivilege.system_role_id) \
                                        .join(SystemPrivilege) \
                                        .where((SystemPrivilege.type == 'UI') &
                                               (SystemUser.id == request.userinfo['id'])).naive()
        ui_privileges_list = []
        for rec in ui_privileges_query:
            ui_privileges_list.append(rec.api_rule)

        account_expiration_time = request.userinfo['expiration_time']

        response.body = {
            'username': getpass.getuser(),
            'current_sus_id': sc_info['id'],
            'current_sus_name': sc_info['name'],
            'user_expiration_timestamp': account_expiration_time.isoformat() if account_expiration_time else None,
            'password_expiration_timestamp': get_password_expiration(request.userinfo['name']),
            'level_id': label_id,
            'version': version,
            'privileges': ui_privileges_list
        }


class LogoutResource(object):
    def on_post(self, request, response):
        query_log = _last_user_audit_event(request.userinfo['id'])
        should_log = False
        for record in query_log:
            if record.audit_type_id == UserAuditEvent.TYPE_USER_LOGOUT:
                break
            if record.audit_type_id == UserAuditEvent.TYPE_USER_LOGIN:
                should_log = True
                break

        if should_log:
            event_logger.log(UserAuditEvent.TYPE_USER_LOGOUT,
                             user_id=request.userinfo['id'],
                             login=request.userinfo['name'])
