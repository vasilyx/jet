from pluton_commons.db.peewee.models import TYPE_REC_STATUS


class DelimitersResource(object):
    def on_post(self, request, response):
        data = [
            (';', '; (Точка с запятой)'),
            (',', ', (Запятая)'),
            ('\t', 'Табуляция'),
        ]

        response.body = data


class RecordStatusCatalogResource:
    def on_post(self, request, response):
        data = []
        for k, v in TYPE_REC_STATUS.items():
            data.append({'id': k, 'label': v})

        response.body = data
