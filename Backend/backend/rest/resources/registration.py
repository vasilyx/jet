from pluton_commons.db.common import enum_to_dict
from pluton_commons.db.peewee.models import PendingSystemComponent, TYPE_COMPONENT, TYPE_REGISTRATION_STATE
from pluton_query_service.client import PlutonQueryClient
from pluton_query_service.common import OK_MSG, ERROR_MSG
from pluton_registration import PlutonRegistrationControlClient


class RegistrationJournalResource:
    def on_post(self, request, response):
        def enum_processing(record):
            enum_to_dict(record, 'component_type', TYPE_COMPONENT)
            enum_to_dict(record, 'state', TYPE_REGISTRATION_STATE)
            return record

        sc_id = request.data.get('system_component_id')
        result = PlutonQueryClient().send_command([sc_id],
                                                  PendingSystemComponent._meta.db_table,
                                                  request.data.get('filters'),
                                                  request.data.get('sort'),
                                                  request.data.get('page'),
                                                  request.data.get('page_size'))

        if result['message'] == OK_MSG:
            rows = [enum_processing(record) for record in result['data']['rows']]
            response.body = {'total': result['data']['rows_total'], 'rows': rows}
        elif result['message'] == ERROR_MSG:
            raise ValueError(result['error'])


class AcceptRegistration:
    def on_post(self, request, response):
        cid = request.data['id']
        reason = request.data['reason']
        result = PlutonRegistrationControlClient().send_command(cid, 'ACCEPT', reason)
        error = result.get('error')
        if error:
            raise ValueError(error['message'])
        response.body = {}


class RejectRegistration:
    def on_post(self, request, response):
        cid = request.data['id']
        reason = request.data['reason']
        result = PlutonRegistrationControlClient().send_command(cid, 'REJECT', reason)
        error = result.get('error')
        if error:
            raise ValueError(error['message'])
        response.body = {}


class DeleteRejectedRegistration:
    def on_post(self, request, response):
        cid = request.data['id']
        result = PlutonRegistrationControlClient().send_command(cid, 'DELETE')
        error = result.get('error')
        if error:
            raise ValueError(error['message'])
        response.body = {}
