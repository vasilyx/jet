from pluton_commons.db.peewee.models import Severity
from pluton_commons.db.peewee.utils import send_list_data


class SeverityResource(object):
    def on_post(self, request, response):
        send_list_data(Severity.select(
            Severity.severity.alias('id'),
            Severity.description.alias('label')
        ), request, response)
