import logging
import traceback

import falcon

from backend.rest.resources.alert import (
    AlertListResource, AlertTypeResource, AlertStatusResource, AlertGetResource
)
from backend.rest.resources.audit import (
    AuditListResource, EntityTypeListResource, AuditStartIntegrityResource,
    AuditStatusIntegrityResource, AuditDelayIntegrityResource,
    AuditUpdateDBIntegrityResource,
    AuditTypeListResource)
from backend.rest.resources.black_list import (
    BlackListResource, BlackListByIdResource, BlackListTypeResource,
    CreateBlackListResource, UpdateBlackListResource, DeleteBlackListResource,
    BlackListDirectionCatalog
)
from backend.rest.resources.black_list_record import (
    BlackListRecordResource, CreateBlackListRecordResource,
    UpdateBlackListRecordResource, DeleteBlackListRecordResource,
    UploadBlackListResource
)
from backend.rest.resources.bro import BroTablelistResource, BroListResource
from backend.rest.resources.host_catalogs import (
    OsCatalogResource, HostTypeCatalogResource, HostClassCatalogResource,
    ApplicationCatalogResource, ServiceCatalogResource, PortCatalogResource,
    ProtocolCatalogResource, TrafficDirectionResource,
    HostStatusCatalogResource)
from backend.rest.resources.host_list import (
    HostListResource, HostListUploadResource, HostListPreviewResource
)
from backend.rest.resources.host_profile import (
    HostProfileResource, HostUserListResource, HostAggregatedDataResource,
    HostAppsListResource,
    HostProfileUpdateResource, HostHeuristicAlertList,
    HostSignatureAlertList, HostMetricDataResource,
    HostNetworkActivityInfoResource, HostAppsChangeStatusResource,
    HostVulnerabilitiesResource)
from backend.rest.resources.map import (
    MapAlertsResource, MapAggregatedInfoService
)
from backend.rest.resources.monitoring_system import (
    MonitoredSystemListResource, MonitoredSystemGetResource
)
from backend.rest.resources.pcap_reader import GetPcapResource
from backend.rest.resources.protocol import ProtocolResource
from backend.rest.resources.reaction import ReactionListResource
from backend.rest.resources.role import (
    RoleListResource, RoleGetResource, RoleCreateResource,
    RoleUpdateResource, RoleDeleteResource, PrivilegeCatalogResource
)
from backend.rest.resources.rules import GetSuricataRulesListResource, UpdateSuricataRuleResource, \
    UpdateAllSuricataRuleResource
from backend.rest.resources.severity import SeverityResource
from backend.rest.resources.statistic import StatisticsTrafficResource
from backend.rest.resources.system_component import (
    SystemComponentListResource, SystemComponentStatusesResource,
    SensorListResource, SystemComponentTypesResource,
    SystemComponentGetResource, SystemDepartmentListResource,
    SystemComponentUpdateResource, SystemComponentBriefResource,
    ChildSystemComponentCatalogResource, SystemComponentChangeStatus,
    SensorTypeCatalogResource)
from backend.rest.resources.update_server import (
    UpdateAgentResource, RegisterUpdateAgentResource, CheckApiKeyResource,
    UpdatesListResource, UpdateStatusResource, UpdateTypesResource,
    EditUpdateAgentResource, GetCreateUpdateAgentResource)
from backend.rest.resources.user import (
    UserListResource, UserGetResource, UserCreateResource, UserUpdateResource,
    UserDeleteResource, UserCreateTypeListResource, UserRoleListResource,
    UserIdentityDocumentTypeListResource
)
from backend.rest.resources.user_info import UserInfoResource, LogoutResource
from backend.rest.resources.utils import DelimitersResource, RecordStatusCatalogResource
from backend.utils.middlewares import (
    SCSWebUIConnectionMiddleware, RequestDataConvertMiddleware,
    UserVerificationMiddleware, MultipartMiddleware, RequestAuditLoggerMiddleware
)

from backend.rest.resources.jobs import JobsResource

from backend.rest.resources.registration import RegistrationJournalResource, AcceptRegistration, \
    RejectRegistration, DeleteRejectedRegistration

from backend.rest.resources.host_profile import HostBlackListResource


def exception_handler(ex, req, resp, params):
    local_logger = logging.getLogger('scsWebUi-exception-handler')
    ex_trace = traceback.format_exc()
    print(ex_trace)
    resp.content_type = 'application/json'
    split_error = ex_trace.split('\n')
    error_message = ''
    for i in range(1, len(split_error)):
        if 'Error' in split_error[-i].strip():
            error_message = split_error[-i]
            break
    if not error_message:
        error_message = str(ex)
    local_logger.error(error_message)
    resp.body = {'message': error_message}
    if resp.status != falcon.HTTP_403 and resp.status != falcon.HTTP_401:
        resp.status = falcon.HTTP_500


api = falcon.API(middleware=[
    SCSWebUIConnectionMiddleware(),
    RequestDataConvertMiddleware(),
    UserVerificationMiddleware(),
    RequestAuditLoggerMiddleware(),
    MultipartMiddleware()
])

api.add_error_handler(BaseException, exception_handler)

api.add_route('/utils/delimiters', DelimitersResource())
api.add_route('/utils/record_statuses', RecordStatusCatalogResource())

api.add_route('/sensor/types', SensorTypeCatalogResource())
api.add_route('/system_department/list', SystemDepartmentListResource())
api.add_route('/system_component/list', SystemComponentListResource())
api.add_route('/system_component/brief', SystemComponentBriefResource())
api.add_route('/system_component/get', SystemComponentGetResource())
api.add_route('/system_component/update', SystemComponentUpdateResource())
api.add_route('/system_component/status_codes',
              SystemComponentStatusesResource())
api.add_route('/system_component/types', SystemComponentTypesResource())
api.add_route('/system_component/change_status', SystemComponentChangeStatus())
api.add_route('/sensor/list', SensorListResource())
api.add_route('/system_component/rules/get', GetSuricataRulesListResource())
api.add_route('/system_component/rules/update', UpdateSuricataRuleResource())
api.add_route('/system_component/rules/update_all', UpdateAllSuricataRuleResource())
api.add_route('/system_component/child_nodes',
              ChildSystemComponentCatalogResource())

api.add_route('/alert/list', AlertListResource())
api.add_route('/alert/get', AlertGetResource())
api.add_route('/alert/types', AlertTypeResource())
api.add_route('/alert/statuses', AlertStatusResource())
api.add_route('/alert/pcap', GetPcapResource())

api.add_route('/protocols', ProtocolResource())

api.add_route('/severity', SeverityResource())

api.add_route('/get_userinfo', UserInfoResource())
api.add_route('/logout', LogoutResource())

api.add_route('/host/list', HostListResource())
api.add_route('/host/list/upload', HostListUploadResource())
api.add_route('/host/preview', HostListPreviewResource())
api.add_route('/host/profile', HostProfileResource())
api.add_route('/host/profile/update', HostProfileUpdateResource())
# api.add_route('/host/profile/create', HostProfileCreateResource())
api.add_route('/host/profile/aggregated', HostAggregatedDataResource())
api.add_route('/host/profile/users', HostUserListResource())
api.add_route('/host/profile/metric', HostMetricDataResource())
api.add_route('/host/profile/heuristic', HostHeuristicAlertList())
api.add_route('/host/profile/signature', HostSignatureAlertList())
api.add_route('/host/profile/black_list', HostBlackListResource())
api.add_route('/host/profile/applications', HostAppsListResource())
api.add_route('/host/profile/applications/update', HostAppsChangeStatusResource())
# api.add_route('/host/profile/applications/create', HostAppsCreateResource())
api.add_route('/host/profile/vulnerability', HostVulnerabilitiesResource())
api.add_route('/os/list', OsCatalogResource())
api.add_route('/application/list', ApplicationCatalogResource())
api.add_route('/service/list', ServiceCatalogResource())
api.add_route('/port/list', PortCatalogResource())
api.add_route('/protocol/list', ProtocolCatalogResource())
api.add_route('/host/type/list', HostTypeCatalogResource())
api.add_route('/host/class/list', HostClassCatalogResource())
api.add_route('/host/traffic/direction', TrafficDirectionResource())
api.add_route('/host/statuses', HostStatusCatalogResource())
api.add_route('/host/profile/network', HostNetworkActivityInfoResource())

api.add_route('/reaction/list', ReactionListResource())
api.add_route('/black_list/direction', BlackListDirectionCatalog())
api.add_route('/black_list_type/list', BlackListTypeResource())
api.add_route('/black_list/list', BlackListResource())
api.add_route('/black_list/get', BlackListByIdResource())
api.add_route('/black_list/create', CreateBlackListResource())
api.add_route('/black_list/update', UpdateBlackListResource())
api.add_route('/black_list/delete', DeleteBlackListResource())
api.add_route('/black_list/record/upload', UploadBlackListResource())
api.add_route('/black_list/record/list', BlackListRecordResource())
api.add_route('/black_list/record/create', CreateBlackListRecordResource())
api.add_route('/black_list/record/update', UpdateBlackListRecordResource())
api.add_route('/black_list/record/delete', DeleteBlackListRecordResource())

api.add_route('/role/list', RoleListResource())
api.add_route('/role/get', RoleGetResource())
api.add_route('/role/create', RoleCreateResource())
api.add_route('/role/update', RoleUpdateResource())
api.add_route('/role/delete', RoleDeleteResource())

api.add_route('/user_create_type/list', UserCreateTypeListResource())
api.add_route('/user_id_doc_type/list', UserIdentityDocumentTypeListResource())
api.add_route('/user_role/list', UserRoleListResource())
api.add_route('/user/list', UserListResource())
api.add_route('/user/get', UserGetResource())
api.add_route('/user/create', UserCreateResource())
api.add_route('/user/update', UserUpdateResource())
api.add_route('/user/delete', UserDeleteResource())

api.add_route('/privilege/catalog', PrivilegeCatalogResource())

api.add_route('/map/alerts', MapAlertsResource())
api.add_route('/map/aggregated', MapAggregatedInfoService())

api.add_route('/audit/list', AuditListResource())
api.add_route('/audit/type/list', AuditTypeListResource())
api.add_route('/entity_type/list', EntityTypeListResource())
api.add_route('/audit/integrity/start', AuditStartIntegrityResource())
api.add_route('/audit/integrity/status', AuditStatusIntegrityResource())
api.add_route('/audit/integrity/delay', AuditDelayIntegrityResource())
api.add_route('/audit/integrity/updatedb', AuditUpdateDBIntegrityResource())

api.add_route('/monitored_system/list', MonitoredSystemListResource())
api.add_route('/monitored_system/get', MonitoredSystemGetResource())

api.add_route('/update_server/check', CheckApiKeyResource())
api.add_route('/update_server/agent/list', UpdateAgentResource())
api.add_route('/update_server/agent/get', GetCreateUpdateAgentResource())
api.add_route('/update_server/agent/update', EditUpdateAgentResource())
api.add_route('/update_server/register', RegisterUpdateAgentResource())
api.add_route('/update/list', UpdatesListResource())
api.add_route('/update/types/list', UpdateTypesResource())
api.add_route('/update/status/list', UpdateStatusResource())

api.add_route('/statistics/traffic', StatisticsTrafficResource())

api.add_route('/bro/tables', BroTablelistResource())
api.add_route('/bro/list', BroListResource())

api.add_route('/jobs', JobsResource())

api.add_route('/registration/journal', RegistrationJournalResource())
api.add_route('/registration/accept', AcceptRegistration())
api.add_route('/registration/reject', RejectRegistration())
api.add_route('/registration/delete', DeleteRejectedRegistration())
